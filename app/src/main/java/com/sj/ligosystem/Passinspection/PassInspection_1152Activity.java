package com.sj.ligosystem.Passinspection;
/**
 * 2020/03/09 未完成
 */
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.Package.VerifyPassInspectionAddActivity;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;

public class PassInspection_1152Activity extends AppCompatActivity implements View.OnClickListener{

    Toolbar mToolBar;
    TextView mOrderNoBarCode, mManufactureOrderID, mProductsDescription, mProcess, mExamineDate,
            mThicknessLowest, mThicknessHighest;
    EditText mExamineTime, mHeaterNumber, mThickness, mBadReasons, mPassword;
    ImageView mImgTimeView;
    RadioGroup mGroup_self;
    RadioButton mRadioBtnV_self, mRadioBtnX_self;
    Button mCancelBtn, mDeleteBtn, mSaveBtn;
    Spinner spinner_Solder;

    ArrayAdapter<String> SolderIndex;

    List<String> SolderList = new ArrayList<>();

    int year, month, day, hour, min;
    //選擇類型
    String Type;
    //時間
    String InputStartTime, InputEndDate;
    //資料
    String OrderID, SerialNumber;
    String InputOrderNoBarCode, InputManufactureOrderID, InputProductsDescription, InputMachineID,
            InputProcessID, InputProcessDesc, InputWorkShiftype, InputStartDate,
            InputEndTime, InputStandardQty, InputProductsTypeID, InputStandardRequestID,
            InputSeamTypeID, InputRawJobTypeID, InputSaleTypeID, InputInspectionFormID, InputSizeID, InputSizeID2, InputThicknessID, InputThicknessID2,
            InputCompletedFormID, InputMaterialFormID, InputDiagramFormID,PassWord;
    //人員與機台
    String EmployeeName, EmployeeID, MachineID, MachineName;
    String InspectionDate, InspectionTime, now;
    String SelfDetermination = "V";
    Float ThicknessSU, ThicknessSL, Thickness;
    String Solder, HeaterNumber, BadReasons;

    Timer timer;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_inspection_1152);

        initView();
        initData();
    }

    private void initView(){
        //region Toolbar
        mToolBar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //spinner_Solder
        spinner_Solder = findViewById(R.id.spinner_Solder);
        spinner_Solder.setSelection(0, true);

        SolderList.add("308L");
        SolderList.add("316L");

        runOnUiThread(()->{
            SolderIndex = new ArrayAdapter<>(this, R.layout.item_spinner, SolderList);
            spinner_Solder.setAdapter(SolderIndex);
        });

        spinner_Solder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Solder = SolderList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //region TextView
        mOrderNoBarCode = findViewById(R.id.VPA_OrderNoBarCodeTextView);
        mManufactureOrderID = findViewById(R.id.VPA_ManufactureOrderIDTextView);
        mProductsDescription = findViewById(R.id.VPA_ProductsDescriptionTextView);
        mProcess = findViewById(R.id.VPA_ProcessTextView);
        mExamineDate = findViewById(R.id.text_examine_date);
        mThicknessLowest = findViewById(R.id.text_Thickness_lowest);
        mThicknessHighest = findViewById(R.id.text_Thickness_highest);
        //endregion

        //region EditText
        mExamineTime = findViewById(R.id.edit_examine_time);
        mHeaterNumber = findViewById(R.id.edit_HeaterNumber);
        mThickness = findViewById(R.id.edit_Thickness);
        mBadReasons = findViewById(R.id.edit_BadReasons);
        mPassword = findViewById(R.id.edit_password);
        //endregion

        //region
        mImgTimeView = findViewById(R.id.img_time);
        mImgTimeView.setOnClickListener(this);
        //endregion

        //region RadioGroup
        mGroup_self = findViewById(R.id.radioGroup_self);
        mGroup_self.setOnCheckedChangeListener((group, checkedId) ->{
            switch (checkedId){
                case R.id.radio_self_V:
                    SelfDetermination = "V";
                    break;
                case R.id.radio_self_X:
                    SelfDetermination = "X";
                    break;
            }
        });
        //endregion

        //region RadioButton
        mRadioBtnV_self = findViewById(R.id.radio_self_V);
        mRadioBtnX_self = findViewById(R.id.radio_self_X);
        //endregion

        //region Button
        mCancelBtn = findViewById(R.id.VPA_btnQuit);
        mDeleteBtn = findViewById(R.id.VPA_btnDelete);
        mSaveBtn = findViewById(R.id.VPA_btnSave);

        mCancelBtn.setOnClickListener(this);
        mDeleteBtn.setOnClickListener(this);
        mSaveBtn.setOnClickListener(this);
        //endregion
    }

    private void initData(){
        Intent intent = getIntent();
        Type = intent.getStringExtra("Type");
        if(intent.getStringExtra("InspectionDate") != null){
            InspectionDate = GetStartDate(intent.getStringExtra("InspectionDate"));
            InspectionTime = intent.getStringExtra("InspectionTime");
            Log.e("TAG", InspectionDate + " " + InspectionTime);
        }

        OrderID = intent.getStringExtra("OrderID");
        SerialNumber = intent.getStringExtra("SerialNumber");
        InputOrderNoBarCode = intent.getStringExtra("OrderNoBarCode");

        InputManufactureOrderID = intent.getStringExtra("ManufactureOrderID");
        InputProductsDescription = intent.getStringExtra("ProductsDescription");
        InputMachineID = intent.getStringExtra("MachineID");
        InputProcessID = intent.getStringExtra("ProcessID");

        InputWorkShiftype = intent.getStringExtra("WorkShiftype");
        InputStartDate = intent.getStringExtra("StartDate");
        InputStartTime = intent.getStringExtra("StartTime");
        InputEndDate = intent.getStringExtra("EndDate");
        InputEndTime = intent.getStringExtra("EndTime");
        InputProductsTypeID = intent.getStringExtra("ProductsTypeID");

        InputStandardRequestID = intent.getStringExtra("StandardRequestID");
        InputSeamTypeID = intent.getStringExtra("SeamTypeID");
        InputRawJobTypeID = intent.getStringExtra("RawJobTypeID");
        InputSaleTypeID = intent.getStringExtra("SaleTypeID");

        InputSizeID = intent.getStringExtra("SizeID");
        InputSizeID2 = intent.getStringExtra("SizeID2");
        InputThicknessID = intent.getStringExtra("ThicknessID");
        InputThicknessID2 = intent.getStringExtra("ThicknessID2");

        InputInspectionFormID = intent.getStringExtra("InspectionFormID");
        InputCompletedFormID = intent.getStringExtra("CompletedFormID");
        InputMaterialFormID = intent.getStringExtra("MaterialFormID");
        InputDiagramFormID = intent.getStringExtra("DiagramFormID");
        PassWord = intent.getStringExtra("PassWord");
        InputStandardQty = intent.getStringExtra("StandardQty");

        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeeID = intent.getStringExtra("EmployeeID");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");

        GetNotblOManufactureDailyDetailByID();

        runOnUiThread(()->{
            mOrderNoBarCode.setText(InputOrderNoBarCode.substring(0,8) +"-"+ InputOrderNoBarCode.substring(8,11) + "-" + InputOrderNoBarCode.substring(11));
            mManufactureOrderID.setText(InputManufactureOrderID);
            mProductsDescription.setText(InputProductsDescription);
            mExamineDate.setText(InputEndDate);
            mProcess.setText(InputProcessID + " " + InputProcessDesc);
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        //endregion

        //region 取得系統時間
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);
        TimeFormat(hour,min);
        mExamineTime.setText(String.format("%tR", calendar1.getTime()));
        //endregion
        TimeJudgment();

        if(Type.equals("編輯")){
            Log.e("TYPE", Type);
            mImgTimeView.setEnabled(false);
            mExamineTime.setEnabled(false);
            SearchOManufactureDailyDetailInspection();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.VPA_btnSave:
                clearFocus();
                if(mPassword.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "密碼請勿空白", Toast.LENGTH_SHORT).show();
                    mPassword.requestFocus();
                    return;
                }
                if(mPassword.getText().toString().equals(PassWord)){
                    if(Type.equals("新增")){
                        InspectionDate = mExamineDate.getText().toString();
                        InspectionTime = mExamineTime.getText().toString();
                        AddDate();
                    }else{
                        UpDate();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確!", Toast.LENGTH_LONG).show();
                    mPassword.requestFocus();
                }
                break;
            case R.id.VPA_btnDelete:
                if(mPassword.getText().toString().equals(PassWord)){
                    DeleteDate();
                }else{
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確!", Toast.LENGTH_LONG).show();
                    mPassword.requestFocus();
                }
                break;
            case R.id.VPA_btnQuit:
                GoBack();
                break;
            case R.id.img_time:
                OpenPopTime(v);
                break;
        }
    }

    void clearFocus(){mExamineTime.clearFocus();}

    void GoBack(){
        Intent intent = new Intent();
        intent.setClass(this, VerifyPassInspectionAddActivity.class);
        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",InputOrderNoBarCode);

        intent.putExtra("ManufactureOrderID",InputManufactureOrderID);
        intent.putExtra("ProductsDescription",InputProductsDescription);
        intent.putExtra("MachineID",InputMachineID);
        intent.putExtra("ProcessID",InputProcessID);

        intent.putExtra("ProcessDesc",InputProcessDesc);
        intent.putExtra("WorkShiftype",InputWorkShiftype);
        intent.putExtra("StartDate",InputStartDate);
        intent.putExtra("StartTime",InputStartTime);
        intent.putExtra("EndDate",InputEndDate);
        intent.putExtra("EndTime",InputEndTime);
        intent.putExtra("ProductsTypeID",InputProductsTypeID);

        intent.putExtra("StandardRequestID",InputStandardRequestID);
        intent.putExtra("SeamTypeID",InputSeamTypeID);
        intent.putExtra("RawJobTypeID",InputRawJobTypeID);
        intent.putExtra("SaleTypeID",InputSaleTypeID);

        intent.putExtra("SizeID", InputSizeID);
        intent.putExtra("SizeID2", InputSizeID2);
        intent.putExtra("ThicknessID", InputThicknessID);
        intent.putExtra("ThicknessID2", InputThicknessID2);

        intent.putExtra("InspectionFormID",InputInspectionFormID);
        intent.putExtra("CompletedFormID",InputCompletedFormID);
        intent.putExtra("MaterialFormID",InputMaterialFormID);
        intent.putExtra("DiagramFormID",InputDiagramFormID);
        intent.putExtra("PassWord",PassWord);
        intent.putExtra("StandardQty",InputStandardQty);

        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        startActivity(intent);
        finish();
    }

    void GetNotblOManufactureDailyDetailByID(){
        Log.e("ManufactureDailyDetail", "取得開始");
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = "SELECT tblOManufactureDailyDetail.*, tblBProcess.ProcessDesc FROM tblOManufactureDailyDetail INNER JOIN tblBProcess ON tblOManufactureDailyDetail.ProcessID = tblBProcess.ProcessID WHERE tblOManufactureDailyDetail.OrderID=" + OrderID + " AND tblOManufactureDailyDetail.SerialNumber=" + SerialNumber ;
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            GetInspectionTolerance();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void GetInspectionTolerance(){
        Log.e("InspectionTolerance", "取得開始" + " " + InputProductsTypeID+ " " + InputSizeID);
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = " SELECT tblBProductInspectionStandards.*" +
                        " FROM tblBProductInspectionStandards" +
                        " WHERE tblBProductInspectionStandards.StandardRequestID='" + InputStandardRequestID +
                        "' AND tblBProductInspectionStandards.ProductsTypeID='" + InputProductsTypeID +
                        "' AND tblBProductInspectionStandards.SizeID='" + InputSizeID +
                        "' AND tblBProductInspectionStandards.SizeID2='" + InputSizeID2 +
                        "' AND tblBProductInspectionStandards.ThicknessID='" + InputThicknessID +
                        "' AND tblBProductInspectionStandards.ThicknessID2='" + InputThicknessID2 + "'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            if (SingleResultSet.getString("T1") != null) {
                                ThicknessSU = SingleResultSet.getFloat("T1");
                                ThicknessSL = SingleResultSet.getFloat("T1Lowerlimit");
                            }
                            runOnUiThread(()->{
                                mThicknessHighest.setText(String.valueOf(ThicknessSU));
                                mThicknessLowest.setText(String.valueOf(ThicknessSL));
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void SearchOManufactureDailyDetailInspection(){
        Log.e("編輯", "有跑嗎?");
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = "SELECT tblOManufactureDailyDetailInspection15.*" +
                        " FROM tblOManufactureDailyDetailInspection15" +
                        " WHERE tblOManufactureDailyDetailInspection15.OrderID=" + OrderID +
                        " AND tblOManufactureDailyDetailInspection15.SerialNumber=" + SerialNumber +
                        " AND tblOManufactureDailyDetailInspection15.InspectionDate='" + InspectionDate +
                        "' AND tblOManufactureDailyDetailInspection15.InspectionTime='" + InspectionTime + "'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            if(SingleResultSet.getString("Solder") != null){
                                Solder = SingleResultSet.getString("Solder");
                            }
                            if(SingleResultSet.getString("HeaterNumber") != null){
                                HeaterNumber = SingleResultSet.getString("HeaterNumber");
                            }
                            if(SingleResultSet.getString("ThicknessSU") != null){
                                ThicknessSU = SingleResultSet.getFloat("ThicknessSU");
                            }
                            if(SingleResultSet.getString("ThicknessSL") != null){
                                ThicknessSL = SingleResultSet.getFloat("ThicknessSL");
                            }
                            if(SingleResultSet.getString("Thickness") != null){
                                Thickness = SingleResultSet.getFloat("Thickness");
                            }
                            if(SingleResultSet.getString("BadReasons") != null){
                                BadReasons = SingleResultSet.getString("BadReasons");
                            }
                            runOnUiThread(()->{
                                for(int i = 0; i < SolderList.size(); i++){
                                    if(SolderList.get(i).equals(Solder)){
                                        spinner_Solder.setSelection(i);
                                    }
                                }
                                mHeaterNumber.setText(HeaterNumber);
                                mThicknessHighest.setText(String.valueOf(ThicknessSU));
                                mThicknessLowest.setText(String.valueOf(ThicknessSL));
                                mThickness.setText(String.valueOf(Thickness));
                                mBadReasons.setText(BadReasons);
                            });
                            if(SingleResultSet.getString("SelfDetermination") != null){
                                if(SingleResultSet.getString("SelfDetermination").equals("1")){
                                    mGroup_self.check(R.id.radio_self_V);
                                    SelfDetermination = "V";
                                }else{
                                    mGroup_self.check(R.id.radio_self_X);
                                    SelfDetermination = "X";
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void DeleteDate(){
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single = "DELETE tblOManufactureDailyDetailInspection15 FROM tblOManufactureDailyDetailInspection15 WHERE OrderID = " + OrderID +
                        " AND SerialNumber = " + SerialNumber +
                        " AND InspectionDate = '" + InspectionDate +
                        "' AND InspectionTime = '" + InspectionTime +"'";
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                ShowToast("刪除成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask, 2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
            Log.e("SQL", "資料內容有誤");
        }
    }

    void AddDate(){
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e("SQL", "現在時間: " + now);

                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection01_Modify =
                        "INSERT into tblOManufactureDailyDetailInspection15(" +
                                "OrderID, SerialNumber, InspectionDate, InspectionTime," +
                                " ThicknessSU, ThicknessSL, Thickness," +
                                " Solder, HeaterNumber," +
                                " SelfDetermination, BadReasons," +
                                " CreateDate, CreateUserNo, CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate)" +
                                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection01_Modify, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1, OrderID);
                comm.setString(2, SerialNumber);
                comm.setString(3, InspectionDate);
                comm.setString(4, InspectionTime);

                comm.setString(5, String.valueOf(ThicknessSU));
                comm.setString(6, String.valueOf(ThicknessSL));
                comm.setString(7, mThickness.getText().toString());

                comm.setString(8, Solder);
                comm.setString(9, mHeaterNumber.getText().toString());

                comm.setString(10, SelfDetermination.equals("V") ? "True" : "False");

                comm.setString(11, mBadReasons.getText().toString());

                comm.setString(12, now);
                comm.setString(13, EmployeeID);
                comm.setString(14, EmployeeName);
                comm.setString(15, now);
                comm.setString(16, EmployeeID);
                comm.setString(17, EmployeeName);
                comm.setString(18, now);

                comm.executeUpdate();

                ShowToast("新增成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask, 2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void UpDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        now = simpleDateFormat.format(date);
        Log.e("現在時間", now);
        try {
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection01_Modify =
                        "UPDATE tblOManufactureDailyDetailInspection15"+
                                " SET SelfDetermination = ?, BadReasons = ?, HeaterNumber = ,"+
                                " Thickness = ?, Solder = ?,"+
                                " ModifyDate = ?, ModifyUserNo = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                                " WHERE OrderID = " + OrderID +
                                " AND SerialNumber = " + SerialNumber +
                                " AND InspectionDate = '" + InspectionDate +
                                "' AND InspectionTime = '" + InspectionTime + "'";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection01_Modify);
                comm.setString(1, SelfDetermination.equals("V")? "True":"False");
                comm.setString(2, mBadReasons.getText().toString());
                comm.setString(3, mHeaterNumber.getText().toString());
                comm.setString(4, mThickness.getText().toString());
                comm.setString(5, Solder);
                comm.setString(6, now);
                comm.setString(7, EmployeeID);
                comm.setString(8, EmployeeName);
                comm.setString(9, now);

                Log.e("上傳資料:", "OrderID: " + OrderID + " SerialNumber: " + SerialNumber +" InspectionDate: "+ InspectionDate + " InspectionTime: "+ InspectionTime);
                Log.e("上傳資料:", " SelfDetermination: "+ (SelfDetermination.equals("V")? "True":"False"));
                Log.e("上傳資料:", "now: " + now + " EmployeeID: " + EmployeeID +" EmployeeName: "+ EmployeeName);
                comm.executeUpdate();

                ShowToast("修改成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask,2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private String GetStartDate(String Date){
        try{
            java.util.Date date;
            @SuppressLint("SimpleDateFormat") DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            return Stat.replace("-","/");
        } catch (NullPointerException | ParseException e){
            e.printStackTrace();
        }
        return null;
    }

    private void ShowToast(String Text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText(Text);
        toast.show();
    }

    private void OpenPopTime(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime, null, false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event)-> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> TimeFormat(hourOfDay, minute));
        Check.setOnClickListener(v12 -> {
            TimeJudgment();
            popupWindow.dismiss();
        });
    }

    private void TimeJudgment(){
        String [] StartTime = InputStartTime.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(StartTime[0]));
        calendar1.set(Calendar.MINUTE, Integer.parseInt(StartTime[1]));
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time1 = df.format(calendar1.getTime());
        Log.e("PASS1011", time1);
        Log.e("時間",calendar1.toString());
        Log.e("時間",calendar.toString());
        if(calendar.compareTo(calendar1) < 0){
            Log.e("時差", "增加一天");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar2 = Calendar.getInstance();
            try {
                Date date;
                date = dateFormat.parse(InputEndDate);
                calendar2.setTime(date);
                calendar2.add(Calendar.DATE, 1);
            }catch (ParseException e){
                e.printStackTrace();
            }
        }else{
            Log.e("時差", "不變");
        }
    }

    private void TimeFormat(int hourOfDay, int minute){
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar.getTime());
        Log.e("TimeFormat", time);
        mExamineTime.setText("");
        mExamineTime.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
    }
}