package com.sj.ligosystem.Passinspection;
/**
 * 2020/03/09 未完成
 */
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.Package.VerifyPassInspectionAddActivity;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;

public class PassInspection_1141Activity extends AppCompatActivity implements View.OnClickListener{
    Toolbar mToolBar;
    TextView mOrderNoBarCode, mManufactureOrderID, mProductsDescription, mProcess, mExamineDate,
            mWeldCurrentLowest, mWeldCurrentHighest,
            mDiskSpeedALowest, mDiskSpeedAHighest,
            mDiskSpeedBLowest, mDiskSpeedBHighest,
            mWirefeedSpeedLowest, mWirefeedSpeedHighest,
            mHighArgonALowest, mHighArgonAHighest,
            mHighArgonBLowest, mHighArgonBHighest,
            mMixedGasLowest, mMixedGasHighest,
            mInternalNitrogenLowest, mInternalNitrogenHighest,
            mExternalNitrogenLowest, mExternalNitrogenHighest;
    EditText mExamineTime,
            mHeaterNumber, mWeldCurrent, mDiskSpeedA,
            mDiskSpeedB, mWirefeedSpeed, mHighArgonA,
            mHighArgonB, mMixedGas, mInternalNitrogen,
            mExternalNitrogen, mPassword;
    ImageView mImgTimeView;
    RadioGroup mGroup_WeldVisualDetermination;
    RadioButton mRadioBtnV_WeldVisualDetermination, mRadioBtnX_WeldVisualDetermination;
    Button mCancelBtn, mDeleteBtn, mSaveBtn;
    Spinner spinner_Solder;

    ArrayAdapter<String> SolderIndex;

    List<String> SolderList = new ArrayList<>();

    int year, month, day, hour, min;
    //選擇類型
    String Type;
    //時間
    String InputStartTime, InputEndDate;
    //資料
    String OrderID, SerialNumber;
    String InputOrderNoBarCode, InputManufactureOrderID, InputProductsDescription, InputMachineID,
            InputProcessID, InputProcessDesc, InputWorkShiftype, InputStartDate,
            InputEndTime, InputStandardQty, InputProductsTypeID, InputStandardRequestID,
            InputSeamTypeID, InputRawJobTypeID, InputSaleTypeID, InputInspectionFormID, InputSizeID, InputSizeID2, InputThicknessID,
            InputCompletedFormID, InputMaterialFormID, InputDiagramFormID,PassWord;
    //人員與機台
    String EmployeeName, EmployeeID, MachineID, MachineName;
    String InspectionDate, InspectionTime, now;
    String WeldVisualDetermination = "V";
    String Solder, HeaterNumber;
    Float WeldCurrentSU, WeldCurrentSL, WeldCurrent;
    Float DiskSpeedASU, DiskSpeedASL, DiskSpeedA;
    Float DiskSpeedBSU, DiskSpeedBSL, DiskSpeedB;
    Float HighArgonASU, HighArgonASL, HighArgonA;
    Float HighArgonBSU, HighArgonBSL, HighArgonB;
    Float WirefeedSpeedSU, WirefeedSpeedSL, WirefeedSpeed;
    Float MixedGasSU, MixedGasSL, MixedGas;
    Float InternalNitrogenSU, InternalNitrogenSL, InternalNitrogen;
    Float ExternalNitrogenSU, ExternalNitrogenSL, ExternalNitrogen;

    Timer timer;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_inspection_1141);

        initView();
        initData();
    }

    private void initView(){
        //region Toolbar
        mToolBar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //spinner_Solder
        spinner_Solder = findViewById(R.id.spinner_Solder);
        spinner_Solder.setSelection(0, true);

        SolderList.add("308L");
        SolderList.add("316L");

        runOnUiThread(()->{
            SolderIndex = new ArrayAdapter<>(this, R.layout.item_spinner, SolderList);
            spinner_Solder.setAdapter(SolderIndex);
        });

        spinner_Solder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Solder = SolderList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //region TextView
        mOrderNoBarCode = findViewById(R.id.VPA_OrderNoBarCodeTextView);
        mManufactureOrderID = findViewById(R.id.VPA_ManufactureOrderIDTextView);
        mProductsDescription = findViewById(R.id.VPA_ProductsDescriptionTextView);
        mProcess = findViewById(R.id.VPA_ProcessTextView);
        mExamineDate = findViewById(R.id.text_examine_date);
        mWeldCurrentLowest = findViewById(R.id.text_WeldCurrent_lowest);
        mWeldCurrentHighest = findViewById(R.id.text_WeldCurrent_highest);
        mDiskSpeedALowest = findViewById(R.id.text_DiskSpeedA_lowest);
        mDiskSpeedAHighest = findViewById(R.id.text_DiskSpeedA_highest);
        mDiskSpeedBLowest = findViewById(R.id.text_DiskSpeedB_lowest);
        mDiskSpeedBHighest = findViewById(R.id.text_DiskSpeedB_highest);
        mWirefeedSpeedLowest = findViewById(R.id.text_WirefeedSpeed_lowest);
        mWirefeedSpeedHighest = findViewById(R.id.text_WirefeedSpeed_highest);
        mHighArgonALowest = findViewById(R.id.text_HighArgonA_lowest);
        mHighArgonAHighest = findViewById(R.id.text_HighArgonA_highest);
        mHighArgonBLowest = findViewById(R.id.text_HighArgonB_lowest);
        mHighArgonBHighest = findViewById(R.id.text_HighArgonB_highest);
        mMixedGasLowest = findViewById(R.id.text_MixedGas_lowest);
        mMixedGasHighest = findViewById(R.id.text_MixedGas_highest);
        mInternalNitrogenLowest = findViewById(R.id.text_InternalNitrogen_lowest);
        mInternalNitrogenHighest = findViewById(R.id.text_InternalNitrogen_highest);
        mExternalNitrogenLowest = findViewById(R.id.text_ExternalNitrogen_lowest);
        mExternalNitrogenHighest = findViewById(R.id.text_ExternalNitrogen_highest);
        //endregion

        //region EditText
        mExamineTime = findViewById(R.id.edit_examine_time);
        mHeaterNumber = findViewById(R.id.edit_HeaterNumber);
        mWeldCurrent = findViewById(R.id.edit_WeldCurrent);
        mDiskSpeedA = findViewById(R.id.edit_DiskSpeedA);
        mDiskSpeedB = findViewById(R.id.edit_DiskSpeedB);
        mWirefeedSpeed = findViewById(R.id.edit_WirefeedSpeed);
        mHighArgonA = findViewById(R.id.edit_HighArgonA);
        mHighArgonB = findViewById(R.id.edit_HighArgonB);
        mMixedGas = findViewById(R.id.edit_MixedGas);
        mInternalNitrogen = findViewById(R.id.edit_InternalNitrogen);
        mExternalNitrogen = findViewById(R.id.edit_ExternalNitrogen);
        mPassword = findViewById(R.id.edit_password);
        //endregion

        //region
        mImgTimeView = findViewById(R.id.img_time);
        mImgTimeView.setOnClickListener(this);
        //endregion

        //region RadioGroup
        mGroup_WeldVisualDetermination = findViewById(R.id.radioGroup_WeldVisualDetermination);
        mGroup_WeldVisualDetermination.setOnCheckedChangeListener((group, checkedId) ->{
            switch (checkedId){
                case R.id.radio_self_V:
                    WeldVisualDetermination = "V";
                    break;
                case R.id.radio_self_X:
                    WeldVisualDetermination = "X";
                    break;
            }
        });
        //endregion

        //region RadioButton
        mRadioBtnV_WeldVisualDetermination = findViewById(R.id.radio_WeldVisualDetermination_V);
        mRadioBtnX_WeldVisualDetermination = findViewById(R.id.radio_WeldVisualDetermination_X);
        //endregion

        //region Button
        mCancelBtn = findViewById(R.id.VPA_btnQuit);
        mDeleteBtn = findViewById(R.id.VPA_btnDelete);
        mSaveBtn = findViewById(R.id.VPA_btnSave);

        mCancelBtn.setOnClickListener(this);
        mDeleteBtn.setOnClickListener(this);
        mSaveBtn.setOnClickListener(this);
        //endregion
    }

    private void initData(){
        Intent intent = getIntent();
        Type = intent.getStringExtra("Type");
        if(intent.getStringExtra("InspectionDate") != null){
            InspectionDate = GetStartDate(intent.getStringExtra("InspectionDate"));
            InspectionTime = intent.getStringExtra("InspectionTime");
            Log.e("TAG", InspectionDate + " " + InspectionTime);
        }

        OrderID = intent.getStringExtra("OrderID");
        SerialNumber = intent.getStringExtra("SerialNumber");
        InputOrderNoBarCode = intent.getStringExtra("OrderNoBarCode");

        InputManufactureOrderID = intent.getStringExtra("ManufactureOrderID");
        InputProductsDescription = intent.getStringExtra("ProductsDescription");
        InputMachineID = intent.getStringExtra("MachineID");
        InputProcessID = intent.getStringExtra("ProcessID");

        InputWorkShiftype = intent.getStringExtra("WorkShiftype");
        InputStartDate = intent.getStringExtra("StartDate");
        InputStartTime = intent.getStringExtra("StartTime");
        InputEndDate = intent.getStringExtra("EndDate");
        InputEndTime = intent.getStringExtra("EndTime");
        InputProductsTypeID = intent.getStringExtra("ProductsTypeID");

        InputStandardRequestID = intent.getStringExtra("StandardRequestID");
        InputSeamTypeID = intent.getStringExtra("SeamTypeID");
        InputRawJobTypeID = intent.getStringExtra("RawJobTypeID");
        InputSaleTypeID = intent.getStringExtra("SaleTypeID");

        InputSizeID = intent.getStringExtra("SizeID");
        InputSizeID2 = intent.getStringExtra("SizeID2");
        InputThicknessID = intent.getStringExtra("ThicknessID");

        InputInspectionFormID = intent.getStringExtra("InspectionFormID");
        InputCompletedFormID = intent.getStringExtra("CompletedFormID");
        InputMaterialFormID = intent.getStringExtra("MaterialFormID");
        InputDiagramFormID = intent.getStringExtra("DiagramFormID");
        PassWord = intent.getStringExtra("PassWord");
        InputStandardQty = intent.getStringExtra("StandardQty");

        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeeID = intent.getStringExtra("EmployeeID");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");

        GetNotblOManufactureDailyDetailByID();

        runOnUiThread(()->{
            mOrderNoBarCode.setText(InputOrderNoBarCode.substring(0,8) +"-"+ InputOrderNoBarCode.substring(8,11) + "-" + InputOrderNoBarCode.substring(11));
            mManufactureOrderID.setText(InputManufactureOrderID);
            mProductsDescription.setText(InputProductsDescription);
            mExamineDate.setText(InputEndDate);
            mProcess.setText(InputProcessID + " " + InputProcessDesc);
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        //endregion

        //region 取得系統時間
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);
        TimeFormat(hour,min);
        mExamineTime.setText(String.format("%tR", calendar1.getTime()));
        //endregion
        TimeJudgment();

        if(Type.equals("編輯")){
            Log.e("TYPE", Type);
            mImgTimeView.setEnabled(false);
            mExamineTime.setEnabled(false);
            SearchOManufactureDailyDetailInspection();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.VPA_btnSave:
                clearFocus();
                if(mPassword.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "密碼請勿空白", Toast.LENGTH_SHORT).show();
                    mPassword.requestFocus();
                    return;
                }
                if(mPassword.getText().toString().equals(PassWord)){
                    if(Type.equals("新增")){
                        InspectionDate = mExamineDate.getText().toString();
                        InspectionTime = mExamineTime.getText().toString();
                        AddDate();
                    }else{
                        UpDate();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確!", Toast.LENGTH_LONG).show();
                    mPassword.requestFocus();
                }
                break;
            case R.id.VPA_btnDelete:
                if(mPassword.getText().toString().equals(PassWord)){
                    DeleteDate();
                }else{
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確!", Toast.LENGTH_LONG).show();
                    mPassword.requestFocus();
                }
                break;
            case R.id.VPA_btnQuit:
                GoBack();
                break;
            case R.id.img_time:
                OpenPopTime(v);
                break;
        }
    }

    void clearFocus(){mExamineTime.clearFocus();}

    void GoBack(){
        Intent intent = new Intent();
        intent.setClass(this, VerifyPassInspectionAddActivity.class);
        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",InputOrderNoBarCode);

        intent.putExtra("ManufactureOrderID",InputManufactureOrderID);
        intent.putExtra("ProductsDescription",InputProductsDescription);
        intent.putExtra("MachineID",InputMachineID);
        intent.putExtra("ProcessID",InputProcessID);

        intent.putExtra("ProcessDesc",InputProcessDesc);
        intent.putExtra("WorkShiftype",InputWorkShiftype);
        intent.putExtra("StartDate",InputStartDate);
        intent.putExtra("StartTime",InputStartTime);
        intent.putExtra("EndDate",InputEndDate);
        intent.putExtra("EndTime",InputEndTime);
        intent.putExtra("ProductsTypeID",InputProductsTypeID);

        intent.putExtra("StandardRequestID",InputStandardRequestID);
        intent.putExtra("SeamTypeID",InputSeamTypeID);
        intent.putExtra("RawJobTypeID",InputRawJobTypeID);
        intent.putExtra("SaleTypeID",InputSaleTypeID);

        intent.putExtra("SizeID", InputSizeID);
        intent.putExtra("SizeID2", InputSizeID2);
        intent.putExtra("ThicknessID", InputThicknessID);

        intent.putExtra("InspectionFormID",InputInspectionFormID);
        intent.putExtra("CompletedFormID",InputCompletedFormID);
        intent.putExtra("MaterialFormID",InputMaterialFormID);
        intent.putExtra("DiagramFormID",InputDiagramFormID);
        intent.putExtra("PassWord",PassWord);
        intent.putExtra("StandardQty",InputStandardQty);

        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        startActivity(intent);
        finish();
    }

    void GetNotblOManufactureDailyDetailByID(){
        Log.e("ManufactureDailyDetail", "取得開始");
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = "SELECT tblOManufactureDailyDetail.*, tblBProcess.ProcessDesc FROM tblOManufactureDailyDetail INNER JOIN tblBProcess ON tblOManufactureDailyDetail.ProcessID = tblBProcess.ProcessID WHERE tblOManufactureDailyDetail.OrderID=" + OrderID + " AND tblOManufactureDailyDetail.SerialNumber=" + SerialNumber ;
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            GetInspectionTolerance();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void GetInspectionTolerance(){
        Log.e("InspectionTolerance", "取得開始" + " " + InputProductsTypeID+ " " + InputSizeID);
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = " SELECT [tblB焊接作業條件標準資料表].*" +
                        " FROM [tblB焊接作業條件標準資料表]" +
                        " WHERE [tblB焊接作業條件標準資料表].MachineID='" + InputMachineID +
                        "' AND [tblB焊接作業條件標準資料表].ProcessID='" + InputProcessID +
                        "' AND [tblB焊接作業條件標準資料表].ProductsTypeID='" + InputProductsTypeID +
                        "' AND [tblB焊接作業條件標準資料表].ThicknessID='" + InputThicknessID +
                        "' AND [tblB焊接作業條件標準資料表].SizeID='" + InputSizeID + "'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            if (SingleResultSet.getString("焊道電流") != null) {
                                WeldCurrentSU = SingleResultSet.getFloat("焊道電流") + SingleResultSet.getFloat("焊道電流上限公差");
                                WeldCurrentSL = SingleResultSet.getFloat("焊道電流") - SingleResultSet.getFloat("焊道電流下限公差");
                            }
                            if (SingleResultSet.getString("圓盤速度A") != null) {
                                DiskSpeedASU = SingleResultSet.getFloat("圓盤速度A") + SingleResultSet.getFloat("圓盤速度A上限公差");
                                DiskSpeedASL = SingleResultSet.getFloat("圓盤速度A") - SingleResultSet.getFloat("圓盤速度A下限公差");
                            }
                            if (SingleResultSet.getString("圓盤速度B") != null) {
                                DiskSpeedBSU = SingleResultSet.getFloat("圓盤速度B") + SingleResultSet.getFloat("圓盤速度B上限公差");
                                DiskSpeedBSL = SingleResultSet.getFloat("圓盤速度B") - SingleResultSet.getFloat("圓盤速度B下限公差");
                            }
                            if (SingleResultSet.getString("送線速度") != null) {
                                WirefeedSpeedSU = SingleResultSet.getFloat("送線速度") + SingleResultSet.getFloat("送線速度上限公差");
                                WirefeedSpeedSL = SingleResultSet.getFloat("送線速度") - SingleResultSet.getFloat("送線速度下限公差");
                            }
                            if (SingleResultSet.getString("高氬A") != null) {
                                HighArgonASU = SingleResultSet.getFloat("高氬A") + SingleResultSet.getFloat("高氬A上限公差");
                                HighArgonASL = SingleResultSet.getFloat("高氬A") - SingleResultSet.getFloat("高氬A下限公差");
                            }
                            if (SingleResultSet.getString("高氬B") != null) {
                                HighArgonBSU = SingleResultSet.getFloat("高氬B") + SingleResultSet.getFloat("高氬B上限公差");
                                HighArgonBSL = SingleResultSet.getFloat("高氬B") - SingleResultSet.getFloat("高氬B下限公差");
                            }
                            if (SingleResultSet.getString("混合氣") != null) {
                                MixedGasSU = SingleResultSet.getFloat("混合氣") + SingleResultSet.getFloat("混合氣上限公差");
                                MixedGasSL = SingleResultSet.getFloat("混合氣") - SingleResultSet.getFloat("混合氣下限公差");
                            }
                            if (SingleResultSet.getString("內氮氣") != null) {
                                InternalNitrogenSU = SingleResultSet.getFloat("內氮氣") + SingleResultSet.getFloat("內氮氣上限公差");
                                InternalNitrogenSL = SingleResultSet.getFloat("內氮氣") - SingleResultSet.getFloat("內氮氣下限公差");
                            }
                            if (SingleResultSet.getString("外氮氣") != null) {
                                ExternalNitrogenSU = SingleResultSet.getFloat("外氮氣") + SingleResultSet.getFloat("外氮氣上限公差");
                                ExternalNitrogenSL = SingleResultSet.getFloat("外氮氣") - SingleResultSet.getFloat("外氮氣下限公差");
                            }
                            runOnUiThread(()->{
                                mWeldCurrentHighest.setText(String.valueOf(WeldCurrentSU));
                                mWeldCurrentLowest.setText(String.valueOf(WeldCurrentSL));
                                mDiskSpeedAHighest.setText(String.valueOf(DiskSpeedASU));
                                mDiskSpeedALowest.setText(String.valueOf(DiskSpeedASL));
                                mDiskSpeedBHighest.setText(String.valueOf(DiskSpeedBSU));
                                mDiskSpeedBLowest.setText(String.valueOf(DiskSpeedBSL));
                                mWirefeedSpeedHighest.setText(String.valueOf(WirefeedSpeedSU));
                                mWirefeedSpeedLowest.setText(String.valueOf(WirefeedSpeedSL));
                                mHighArgonAHighest.setText(String.valueOf(HighArgonASU));
                                mHighArgonALowest.setText(String.valueOf(HighArgonASL));
                                mHighArgonBHighest.setText(String.valueOf(HighArgonBSU));
                                mHighArgonBLowest.setText(String.valueOf(HighArgonBSL));
                                mMixedGasHighest.setText(String.valueOf(MixedGasSU));
                                mMixedGasLowest.setText(String.valueOf(MixedGasSL));
                                mInternalNitrogenHighest.setText(String.valueOf(InternalNitrogenSU));
                                mInternalNitrogenLowest.setText(String.valueOf(InternalNitrogenSL));
                                mExternalNitrogenHighest.setText(String.valueOf(ExternalNitrogenSU));
                                mExternalNitrogenLowest.setText(String.valueOf(ExternalNitrogenSL));
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void SearchOManufactureDailyDetailInspection(){
        Log.e("編輯", "有跑嗎?");
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = "SELECT tblOManufactureDailyDetailInspection14.*" +
                        " FROM tblOManufactureDailyDetailInspection14" +
                        " WHERE tblOManufactureDailyDetailInspection14.OrderID=" + OrderID +
                        " AND tblOManufactureDailyDetailInspection14.SerialNumber=" + SerialNumber +
                        " AND tblOManufactureDailyDetailInspection14.InspectionDate='" + InspectionDate +
                        "' AND tblOManufactureDailyDetailInspection14.InspectionTime='" + InspectionTime + "'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            if(SingleResultSet.getString("Solder") != null){
                                Solder = SingleResultSet.getString("Solder");
                            }
                            if(SingleResultSet.getString("HeaterNumber") != null){
                                HeaterNumber = SingleResultSet.getString("HeaterNumber");
                            }
                            if(SingleResultSet.getString("WeldCurrentSU") != null){
                                WeldCurrentSU = SingleResultSet.getFloat("WeldCurrentSU");
                            }
                            if(SingleResultSet.getString("WeldCurrentSL") != null){
                                WeldCurrentSL = SingleResultSet.getFloat("WeldCurrentSL");
                            }
                            if(SingleResultSet.getString("WeldCurrent") != null){
                                WeldCurrent = SingleResultSet.getFloat("WeldCurrent");
                            }
                            if(SingleResultSet.getString("DiskSpeedASU") != null){
                                DiskSpeedASU = SingleResultSet.getFloat("DiskSpeedASU");
                            }
                            if(SingleResultSet.getString("DiskSpeedASL") != null){
                                DiskSpeedASL = SingleResultSet.getFloat("DiskSpeedASL");
                            }
                            if(SingleResultSet.getString("DiskSpeedA") != null){
                                DiskSpeedA = SingleResultSet.getFloat("DiskSpeedA");
                            }
                            if(SingleResultSet.getString("DiskSpeedBSU") != null){
                                DiskSpeedBSU = SingleResultSet.getFloat("DiskSpeedBSU");
                            }
                            if(SingleResultSet.getString("DiskSpeedBSL") != null){
                                DiskSpeedBSL = SingleResultSet.getFloat("DiskSpeedBSL");
                            }
                            if(SingleResultSet.getString("DiskSpeedB") != null){
                                DiskSpeedB = SingleResultSet.getFloat("DiskSpeedB");
                            }
                            if(SingleResultSet.getString("WirefeedSpeedSU") != null){
                                WirefeedSpeedSU = SingleResultSet.getFloat("WirefeedSpeedSU");
                            }
                            if(SingleResultSet.getString("WirefeedSpeedSL") != null){
                                WirefeedSpeedSL = SingleResultSet.getFloat("WirefeedSpeedSL");
                            }
                            if(SingleResultSet.getString("WirefeedSpeed") != null){
                                WirefeedSpeed = SingleResultSet.getFloat("WirefeedSpeed");
                            }
                            if(SingleResultSet.getString("HighArgonASU") != null){
                                HighArgonASU = SingleResultSet.getFloat("HighArgonASU");
                            }
                            if(SingleResultSet.getString("HighArgonASL") != null){
                                HighArgonASL = SingleResultSet.getFloat("HighArgonASL");
                            }
                            if(SingleResultSet.getString("HighArgonA") != null){
                                HighArgonA = SingleResultSet.getFloat("HighArgonA");
                            }
                            if(SingleResultSet.getString("HighArgonBSU") != null){
                                HighArgonBSU = SingleResultSet.getFloat("HighArgonBSU");
                            }
                            if(SingleResultSet.getString("HighArgonBSL") != null){
                                HighArgonBSL = SingleResultSet.getFloat("HighArgonBSL");
                            }
                            if(SingleResultSet.getString("HighArgonB") != null){
                                HighArgonB = SingleResultSet.getFloat("HighArgonB");
                            }
                            if(SingleResultSet.getString("MixedGasSU") != null){
                                MixedGasSU = SingleResultSet.getFloat("MixedGasSU");
                            }
                            if(SingleResultSet.getString("MixedGasSL") != null){
                                MixedGasSL = SingleResultSet.getFloat("MixedGasSL");
                            }
                            if(SingleResultSet.getString("MixedGas") != null){
                                MixedGas = SingleResultSet.getFloat("MixedGas");
                            }
                            if(SingleResultSet.getString("InternalNitrogenSU") != null){
                                InternalNitrogenSU = SingleResultSet.getFloat("InternalNitrogenSU");
                            }
                            if(SingleResultSet.getString("InternalNitrogenSL") != null){
                                InternalNitrogenSL = SingleResultSet.getFloat("InternalNitrogenSL");
                            }
                            if(SingleResultSet.getString("InternalNitrogen") != null){
                                InternalNitrogen = SingleResultSet.getFloat("InternalNitrogen");
                            }
                            if(SingleResultSet.getString("ExternalNitrogenSU") != null){
                                ExternalNitrogenSU = SingleResultSet.getFloat("ExternalNitrogenSU");
                            }
                            if(SingleResultSet.getString("ExternalNitrogenSL") != null){
                                ExternalNitrogenSL = SingleResultSet.getFloat("ExternalNitrogenSL");
                            }
                            if(SingleResultSet.getString("ExternalNitrogen") != null){
                                ExternalNitrogen = SingleResultSet.getFloat("ExternalNitrogen");
                            }
                            runOnUiThread(()->{
                                for(int i = 0; i < SolderList.size(); i++){
                                    if(SolderList.get(i).equals(Solder)){
                                        spinner_Solder.setSelection(i);
                                    }
                                }
                                mHeaterNumber.setText(HeaterNumber);
                                mWeldCurrentHighest.setText(String.valueOf(WeldCurrentSU));
                                mWeldCurrentLowest.setText(String.valueOf(WeldCurrentSL));
                                mWeldCurrent.setText(String.valueOf(WeldCurrent));
                                mDiskSpeedAHighest.setText(String.valueOf(DiskSpeedASU));
                                mDiskSpeedALowest.setText(String.valueOf(DiskSpeedASL));
                                mDiskSpeedA.setText(String.valueOf(DiskSpeedA));
                                mDiskSpeedBHighest.setText(String.valueOf(DiskSpeedBSU));
                                mDiskSpeedBLowest.setText(String.valueOf(DiskSpeedBSL));
                                mDiskSpeedB.setText(String.valueOf(DiskSpeedB));
                                mWirefeedSpeedHighest.setText(String.valueOf(WirefeedSpeedSU));
                                mWirefeedSpeedLowest.setText(String.valueOf(WirefeedSpeedSL));
                                mWirefeedSpeed.setText(String.valueOf(WirefeedSpeed));
                                mHighArgonAHighest.setText(String.valueOf(HighArgonASU));
                                mHighArgonALowest.setText(String.valueOf(HighArgonASL));
                                mHighArgonA.setText(String.valueOf(HighArgonA));
                                mHighArgonBHighest.setText(String.valueOf(HighArgonBSU));
                                mHighArgonBLowest.setText(String.valueOf(HighArgonBSL));
                                mHighArgonB.setText(String.valueOf(HighArgonB));
                                mMixedGasHighest.setText(String.valueOf(MixedGasSU));
                                mMixedGasLowest.setText(String.valueOf(MixedGasSL));
                                mMixedGas.setText(String.valueOf(MixedGas));
                                mInternalNitrogenHighest.setText(String.valueOf(InternalNitrogenSU));
                                mInternalNitrogenLowest.setText(String.valueOf(InternalNitrogenSL));
                                mInternalNitrogen.setText(String.valueOf(InternalNitrogen));
                                mExternalNitrogenHighest.setText(String.valueOf(ExternalNitrogenSU));
                                mExternalNitrogenLowest.setText(String.valueOf(ExternalNitrogenSL));
                                mExternalNitrogen.setText(String.valueOf(ExternalNitrogen));
                            });
                            if(SingleResultSet.getString("WeldVisualDetermination") != null){
                                if(SingleResultSet.getString("WeldVisualDetermination").equals("1")){
                                    mGroup_WeldVisualDetermination.check(R.id.radio_WeldVisualDetermination_V);
                                    WeldVisualDetermination = "V";
                                }else{
                                    mGroup_WeldVisualDetermination.check(R.id.radio_WeldVisualDetermination_X);
                                    WeldVisualDetermination = "X";
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void DeleteDate(){
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single = "DELETE tblOManufactureDailyDetailInspection14 FROM tblOManufactureDailyDetailInspection14 WHERE OrderID = " + OrderID +
                        " AND SerialNumber = " + SerialNumber +
                        " AND InspectionDate = '" + InspectionDate +
                        "' AND InspectionTime = '" + InspectionTime +"'";
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                ShowToast("刪除成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask, 2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
            Log.e("SQL", "資料內容有誤");
        }
    }

    void AddDate(){
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e("SQL", "現在時間: " + now);

                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection01_Modify =
                        "INSERT into tblOManufactureDailyDetailInspection14(" +
                                "OrderID, SerialNumber, InspectionDate, InspectionTime," +
                                " WeldCurrentSU, WeldCurrentSL, WeldCurrent," +
                                " DiskSpeedASU, DiskSpeedASL, DiskSpeedA," +
                                " DiskSpeedBSU, DiskSpeedBSL, DiskSpeedB," +
                                " WirefeedSpeedSU, WirefeedSpeedSL, WirefeedSpeed," +
                                " HighArgonASU, HighArgonASL, HighArgonA," +
                                " HighArgonBSU, HighArgonBSL, HighArgonB," +
                                " MixedGasSU, MixedGasSL, MixedGas," +
                                " InternalNitrogenSU, InternalNitrogenSL, InternalNitrogen," +
                                " ExternalNitrogenSU, ExternalNitrogenSL, ExternalNitrogen," +
                                " WeldVisualDetermination, Solder, HeaterNumber," +
                                " CreateDate, CreateUserNo, CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate)" +
                                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection01_Modify, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1, OrderID);
                comm.setString(2, SerialNumber);
                comm.setString(3, InspectionDate);
                comm.setString(4, InspectionTime);

                comm.setString(5, String.valueOf(WeldCurrentSU));
                comm.setString(6, String.valueOf(WeldCurrentSL));
                comm.setString(7, mWeldCurrent.getText().toString());

                comm.setString(8, String.valueOf(DiskSpeedASU));
                comm.setString(9, String.valueOf(DiskSpeedASL));
                comm.setString(10, mDiskSpeedA.getText().toString());

                comm.setString(11, String.valueOf(DiskSpeedBSU));
                comm.setString(12, String.valueOf(DiskSpeedBSL));
                comm.setString(13, mDiskSpeedB.getText().toString());

                comm.setString(14, String.valueOf(WirefeedSpeedSU));
                comm.setString(15, String.valueOf(WirefeedSpeedSL));
                comm.setString(16, mWirefeedSpeed.getText().toString());

                comm.setString(17, String.valueOf(HighArgonASU));
                comm.setString(18, String.valueOf(HighArgonASL));
                comm.setString(19, mHighArgonA.getText().toString());

                comm.setString(20, String.valueOf(HighArgonBSU));
                comm.setString(21, String.valueOf(HighArgonBSL));
                comm.setString(22, mHighArgonB.getText().toString());

                comm.setString(23, String.valueOf(MixedGasSU));
                comm.setString(24, String.valueOf(MixedGasSL));
                comm.setString(25, mMixedGas.getText().toString());

                comm.setString(26, String.valueOf(InternalNitrogenSU));
                comm.setString(27, String.valueOf(InternalNitrogenSL));
                comm.setString(28, mInternalNitrogen.getText().toString());

                comm.setString(29, String.valueOf(ExternalNitrogenSU));
                comm.setString(30, String.valueOf(ExternalNitrogenSL));
                comm.setString(31, mExternalNitrogen.getText().toString());

                comm.setString(32, WeldVisualDetermination.equals("V") ? "True" : "False");
                comm.setString(33, Solder);
                comm.setString(34, mHeaterNumber.getText().toString());


                comm.setString(35, now);
                comm.setString(36, EmployeeID);
                comm.setString(37, EmployeeName);
                comm.setString(38, now);
                comm.setString(39, EmployeeID);
                comm.setString(40, EmployeeName);
                comm.setString(41, now);

                comm.executeUpdate();

                ShowToast("新增成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask, 2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void UpDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        now = simpleDateFormat.format(date);
        Log.e("現在時間", now);
        try {
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection01_Modify =
                        "UPDATE tblOManufactureDailyDetailInspection14"+
                                " SET WeldVisualDetermination = ?, Solder = ?, HeaterNumber = ?,"+
                                " WeldCurrent = ?, DiskSpeedA = ?, DiskSpeedB = ?, WirefeedSpeed = ?," +
                                " HighArgonA = ?, HighArgonB = ?, MixedGas = ?, InternalNitrogen = ?," +
                                " ExternalNitrogen = ?," +
                                " ModifyDate = ?, ModifyUserNo = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                                " WHERE OrderID = " + OrderID +
                                " AND SerialNumber = " + SerialNumber +
                                " AND InspectionDate = '" + InspectionDate +
                                "' AND InspectionTime = '" + InspectionTime + "'";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection01_Modify);
                comm.setString(1, WeldVisualDetermination.equals("V")? "True":"False");
                comm.setString(2, Solder);
                comm.setString(3, mHeaterNumber.getText().toString());
                comm.setString(4, mWeldCurrent.getText().toString());
                comm.setString(5, mDiskSpeedA.getText().toString());
                comm.setString(6, mDiskSpeedB.getText().toString());
                comm.setString(7, mWirefeedSpeed.getText().toString());
                comm.setString(8, mHighArgonA.getText().toString());
                comm.setString(9, mHighArgonB.getText().toString());
                comm.setString(10, mMixedGas.getText().toString());
                comm.setString(11, mInternalNitrogen.getText().toString());
                comm.setString(12, mExternalNitrogen.getText().toString());
                comm.setString(13, now);
                comm.setString(14, EmployeeID);
                comm.setString(15, EmployeeName);
                comm.setString(16, now);

                Log.e("上傳資料:", "OrderID: " + OrderID + " SerialNumber: " + SerialNumber +" InspectionDate: "+ InspectionDate + " InspectionTime: "+ InspectionTime);
                Log.e("上傳資料:", "now: " + now + " EmployeeID: " + EmployeeID +" EmployeeName: "+ EmployeeName);
                comm.executeUpdate();

                ShowToast("修改成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask,2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private String GetStartDate(String Date){
        try{
            java.util.Date date;
            @SuppressLint("SimpleDateFormat") DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            return Stat.replace("-","/");
        } catch (NullPointerException | ParseException e){
            e.printStackTrace();
        }
        return null;
    }

    private void ShowToast(String Text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText(Text);
        toast.show();
    }

    private void OpenPopTime(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime, null, false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event)-> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> TimeFormat(hourOfDay, minute));
        Check.setOnClickListener(v12 -> {
            TimeJudgment();
            popupWindow.dismiss();
        });
    }

    private void TimeJudgment(){
        String [] StartTime = InputStartTime.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(StartTime[0]));
        calendar1.set(Calendar.MINUTE, Integer.parseInt(StartTime[1]));
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time1 = df.format(calendar1.getTime());
        Log.e("PASS1011", time1);
        Log.e("時間",calendar1.toString());
        Log.e("時間",calendar.toString());
        if(calendar.compareTo(calendar1) < 0){
            Log.e("時差", "增加一天");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar2 = Calendar.getInstance();
            try {
                Date date;
                date = dateFormat.parse(InputEndDate);
                calendar2.setTime(date);
                calendar2.add(Calendar.DATE, 1);
            }catch (ParseException e){
                e.printStackTrace();
            }
        }else{
            Log.e("時差", "不變");
        }
    }

    private void TimeFormat(int hourOfDay, int minute){
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar.getTime());
        Log.e("TimeFormat", time);
        mExamineTime.setText("");
        mExamineTime.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
    }
}