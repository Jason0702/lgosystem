package com.sj.ligosystem.Passinspection;
/**
 * 2020/03/09 未完成
 */
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.content.Loader;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.Package.VerifyPassInspectionAddActivity;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;

public class PassInspection_1750Activity extends AppCompatActivity implements View.OnClickListener{

    Toolbar mToolBar;
    TextView mOrderNoBarCode, mManufactureOrderID, mProductsDescription, mProcess, mExamineDate,
            mWeldCurrentLowest, mWeldCurrentHighest,
            mDiskSpeedALowest, mDiskSpeedAHighest,
            mMixedGasLowest, mMixedGasHighest,
            mN2Lowest, mN2Highest;

    EditText mExamineTime, mTungstenRodOD, mWeldCurrent, mDiskSpeedA, mMixedGas, mN2, mPassword;
    ImageView mImgTimeView;
    RadioGroup mGroup_self;
    RadioButton mRadioBtnV_self, mRadioBtnX_self;
    Button mCancelBtn, mDeleteBtn, mSaveBtn;

    int year, month, day, hour, min;
    //選擇類型
    String Type;
    //時間
    String InputStartTime, InputEndDate;
    //資料
    String OrderID, SerialNumber;
    String InputOrderNoBarCode, InputManufactureOrderID, InputProductsDescription, InputMachineID,
            InputProcessID, InputProcessDesc, InputWorkShiftype, InputStartDate,
            InputEndTime, InputStandardQty, InputProductsTypeID, InputStandardRequestID,
            InputSeamTypeID, InputRawJobTypeID, InputSaleTypeID, InputInspectionFormID, InputSizeID, InputSizeID2, InputThicknessID,
            InputCompletedFormID, InputMaterialFormID, InputDiagramFormID,PassWord;
    //人員與機台
    String EmployeeName, EmployeeID, MachineID, MachineName;
    String InspectionDate, InspectionTime, now;
    String SelfDetermination = "V";
    Float TungstenRodOD;
    Float WeldCurrentSU, WeldCurrentSL, WeldCurrent;
    Float DiskSpeedASU, DiskSpeedASL, DiskSpeedA;
    Float MixedGasSU, MixedGasSL, MixedGas;
    Float N2SU, N2SL, N2;

    Timer timer;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_inspection_1750);

        initView();
        initData();
    }

    private void initView(){
        //region Toolbar
        mToolBar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        mOrderNoBarCode = findViewById(R.id.VPA_OrderNoBarCodeTextView);
        mManufactureOrderID = findViewById(R.id.VPA_ManufactureOrderIDTextView);
        mProductsDescription = findViewById(R.id.VPA_ProductsDescriptionTextView);
        mProcess = findViewById(R.id.VPA_ProcessTextView);
        mExamineDate = findViewById(R.id.text_examine_date);
        mWeldCurrentLowest = findViewById(R.id.text_WeldCurrent_lowest);
        mWeldCurrentHighest = findViewById(R.id.text_WeldCurrent_highest);
        mDiskSpeedALowest = findViewById(R.id.text_DiskSpeedA_lowest);
        mDiskSpeedAHighest = findViewById(R.id.text_DiskSpeedA_highest);
        mMixedGasLowest = findViewById(R.id.text_MixedGas_lowest);
        mMixedGasHighest = findViewById(R.id.text_MixedGas_highest);
        mN2Lowest = findViewById(R.id.text_N2_lowest);
        mN2Highest = findViewById(R.id.text_N2_highest);
        //endregion

        //region EditText
        mExamineTime = findViewById(R.id.edit_examine_time);
        mTungstenRodOD = findViewById(R.id.edit_TungstenRodOD);
        mWeldCurrent = findViewById(R.id.edit_WeldCurrent);
        mDiskSpeedA = findViewById(R.id.edit_DiskSpeedA);
        mMixedGas = findViewById(R.id.edit_MixedGas);
        mN2 = findViewById(R.id.edit_N2);
        mPassword = findViewById(R.id.edit_password);
        //endregion

        //region
        mImgTimeView = findViewById(R.id.img_time);
        mImgTimeView.setOnClickListener(this);
        //endregion

        //region RadioGroup
        mGroup_self = findViewById(R.id.radioGroup_self);
        mGroup_self.setOnCheckedChangeListener((group, checkedId) ->{
            switch (checkedId){
                case R.id.radio_self_V:
                    SelfDetermination = "V";
                    break;
                case R.id.radio_self_X:
                    SelfDetermination = "X";
                    break;
            }
        });
        //endregion

        //region RadioButton
        mRadioBtnV_self = findViewById(R.id.radio_self_V);
        mRadioBtnX_self = findViewById(R.id.radio_self_X);
        //endregion

        //region Button
        mCancelBtn = findViewById(R.id.VPA_btnQuit);
        mDeleteBtn = findViewById(R.id.VPA_btnDelete);
        mSaveBtn = findViewById(R.id.VPA_btnSave);

        mCancelBtn.setOnClickListener(this);
        mDeleteBtn.setOnClickListener(this);
        mSaveBtn.setOnClickListener(this);
        //endregion
    }

    private void initData(){
        Intent intent = getIntent();
        Type = intent.getStringExtra("Type");
        if(intent.getStringExtra("InspectionDate") != null){
            InspectionDate = GetStartDate(intent.getStringExtra("InspectionDate"));
            InspectionTime = intent.getStringExtra("InspectionTime");
            Log.e("TAG", InspectionDate + " " + InspectionTime);
        }

        OrderID = intent.getStringExtra("OrderID");
        SerialNumber = intent.getStringExtra("SerialNumber");
        InputOrderNoBarCode = intent.getStringExtra("OrderNoBarCode");

        InputManufactureOrderID = intent.getStringExtra("ManufactureOrderID");
        InputProductsDescription = intent.getStringExtra("ProductsDescription");
        InputMachineID = intent.getStringExtra("MachineID");
        InputProcessID = intent.getStringExtra("ProcessID");

        InputWorkShiftype = intent.getStringExtra("WorkShiftype");
        InputStartDate = intent.getStringExtra("StartDate");
        InputStartTime = intent.getStringExtra("StartTime");
        InputEndDate = intent.getStringExtra("EndDate");
        InputEndTime = intent.getStringExtra("EndTime");
        InputProductsTypeID = intent.getStringExtra("ProductsTypeID");

        InputStandardRequestID = intent.getStringExtra("StandardRequestID");
        InputSeamTypeID = intent.getStringExtra("SeamTypeID");
        InputRawJobTypeID = intent.getStringExtra("RawJobTypeID");
        InputSaleTypeID = intent.getStringExtra("SaleTypeID");

        InputSizeID = intent.getStringExtra("SizeID");
        InputSizeID2 = intent.getStringExtra("SizeID2");
        InputThicknessID = intent.getStringExtra("ThicknessID");

        InputInspectionFormID = intent.getStringExtra("InspectionFormID");
        InputCompletedFormID = intent.getStringExtra("CompletedFormID");
        InputMaterialFormID = intent.getStringExtra("MaterialFormID");
        InputDiagramFormID = intent.getStringExtra("DiagramFormID");
        PassWord = intent.getStringExtra("PassWord");
        InputStandardQty = intent.getStringExtra("StandardQty");

        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeeID = intent.getStringExtra("EmployeeID");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");

        GetNotblOManufactureDailyDetailByID();

        runOnUiThread(()->{
            mOrderNoBarCode.setText(InputOrderNoBarCode.substring(0,8) +"-"+ InputOrderNoBarCode.substring(8,11) + "-" + InputOrderNoBarCode.substring(11));
            mManufactureOrderID.setText(InputManufactureOrderID);
            mProductsDescription.setText(InputProductsDescription);
            mExamineDate.setText(InputEndDate);
            mProcess.setText(InputProcessID + " " + InputProcessDesc);
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        //endregion

        //region 取得系統時間
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);
        TimeFormat(hour,min);
        mExamineTime.setText(String.format("%tR", calendar1.getTime()));
        //endregion
        TimeJudgment();

        if(Type.equals("編輯")){
            Log.e("TYPE", Type);
            mImgTimeView.setEnabled(false);
            mExamineTime.setEnabled(false);
            SearchOManufactureDailyDetailInspection();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.VPA_btnSave:
                clearFocus();
                if(mPassword.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "密碼請勿空白", Toast.LENGTH_SHORT).show();
                    mPassword.requestFocus();
                    return;
                }
                if(mPassword.getText().toString().equals(PassWord)){
                    if(Type.equals("新增")){
                        InspectionDate = mExamineDate.getText().toString();
                        InspectionTime = mExamineTime.getText().toString();
                        AddDate();
                    }else{
                        UpDate();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確!", Toast.LENGTH_LONG).show();
                    mPassword.requestFocus();
                }
                break;
            case R.id.VPA_btnDelete:
                if(mPassword.getText().toString().equals(PassWord)){
                    DeleteDate();
                }else{
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確!", Toast.LENGTH_LONG).show();
                    mPassword.requestFocus();
                }
                break;
            case R.id.VPA_btnQuit:
                GoBack();
                break;
            case R.id.img_time:
                OpenPopTime(v);
                break;
        }
    }

    void clearFocus(){mExamineTime.clearFocus();}

    void GoBack(){
        Intent intent = new Intent();
        intent.setClass(this, VerifyPassInspectionAddActivity.class);
        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",InputOrderNoBarCode);

        intent.putExtra("ManufactureOrderID",InputManufactureOrderID);
        intent.putExtra("ProductsDescription",InputProductsDescription);
        intent.putExtra("MachineID",InputMachineID);
        intent.putExtra("ProcessID",InputProcessID);

        intent.putExtra("ProcessDesc",InputProcessDesc);
        intent.putExtra("WorkShiftype",InputWorkShiftype);
        intent.putExtra("StartDate",InputStartDate);
        intent.putExtra("StartTime",InputStartTime);
        intent.putExtra("EndDate",InputEndDate);
        intent.putExtra("EndTime",InputEndTime);
        intent.putExtra("ProductsTypeID",InputProductsTypeID);

        intent.putExtra("StandardRequestID",InputStandardRequestID);
        intent.putExtra("SeamTypeID",InputSeamTypeID);
        intent.putExtra("RawJobTypeID",InputRawJobTypeID);
        intent.putExtra("SaleTypeID",InputSaleTypeID);

        intent.putExtra("SizeID", InputSizeID);
        intent.putExtra("SizeID2", InputSizeID2);
        intent.putExtra("ThicknessID", InputThicknessID);

        intent.putExtra("InspectionFormID",InputInspectionFormID);
        intent.putExtra("CompletedFormID",InputCompletedFormID);
        intent.putExtra("MaterialFormID",InputMaterialFormID);
        intent.putExtra("DiagramFormID",InputDiagramFormID);
        intent.putExtra("PassWord",PassWord);
        intent.putExtra("StandardQty",InputStandardQty);

        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        startActivity(intent);
        finish();
    }

    void GetNotblOManufactureDailyDetailByID(){
        Log.e("ManufactureDailyDetail", "取得開始");
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = "SELECT tblOManufactureDailyDetail.*, tblBProcess.ProcessDesc FROM tblOManufactureDailyDetail INNER JOIN tblBProcess ON tblOManufactureDailyDetail.ProcessID = tblBProcess.ProcessID WHERE tblOManufactureDailyDetail.OrderID=" + OrderID + " AND tblOManufactureDailyDetail.SerialNumber=" + SerialNumber ;
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            GetInspectionTolerance();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void GetInspectionTolerance(){
        Log.e("InspectionTolerance", "取得開始" + " " + InputProductsTypeID+ " " + InputSizeID);
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = " SELECT [tblB焊接作業條件標準資料表].*" +
                        " FROM [tblB焊接作業條件標準資料表]" +
                        "WHERE [tblB焊接作業條件標準資料表].MachineID='" + InputMachineID + "' AND [tblB焊接作業條件標準資料表].ProcessID='" + InputProcessID + "' AND [tblB焊接作業條件標準資料表].ProductsTypeID='" + InputProductsTypeID + "' AND [tblB焊接作業條件標準資料表].ThicknessID='" + InputThicknessID + "' AND [tblB焊接作業條件標準資料表].SizeID='" + InputSizeID + "'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            if (SingleResultSet.getString("鎢棒口徑") != null) {
                                TungstenRodOD = SingleResultSet.getFloat("鎢棒口徑");
                            }
                            if (SingleResultSet.getString("焊道電流") != null) {
                                WeldCurrentSU = SingleResultSet.getFloat("焊道電流") + SingleResultSet.getFloat("焊道電流上限公差");
                                WeldCurrentSL = SingleResultSet.getFloat("焊道電流") - SingleResultSet.getFloat("焊道電流下限公差");
                            }
                            if (SingleResultSet.getString("圓盤速度A") != null) {
                                DiskSpeedASU = SingleResultSet.getFloat("圓盤速度A") + SingleResultSet.getFloat("圓盤速度A上限公差");
                                DiskSpeedASL = SingleResultSet.getFloat("圓盤速度A") - SingleResultSet.getFloat("圓盤速度A下限公差");
                            }
                            if (SingleResultSet.getString("混合氣") != null) {
                                MixedGasSU = SingleResultSet.getFloat("混合氣") + SingleResultSet.getFloat("混合氣上限公差");
                                MixedGasSL = SingleResultSet.getFloat("混合氣") - SingleResultSet.getFloat("混合氣下限公差");
                            }
                            if (SingleResultSet.getString("內氮氣") != null) {
                                N2SU = SingleResultSet.getFloat("內氮氣") + SingleResultSet.getFloat("內氮氣上限公差");
                                N2SL = SingleResultSet.getFloat("內氮氣") - SingleResultSet.getFloat("內氮氣下限公差");
                            }
                            runOnUiThread(()->{
                                mTungstenRodOD.setText(String.valueOf(TungstenRodOD));
                                mWeldCurrentHighest.setText(String.valueOf(WeldCurrentSU));
                                mWeldCurrentLowest.setText(String.valueOf(WeldCurrentSL));
                                mDiskSpeedAHighest.setText(String.valueOf(DiskSpeedASU));
                                mDiskSpeedALowest.setText(String.valueOf(DiskSpeedASL));
                                mMixedGasHighest.setText(String.valueOf(MixedGasSU));
                                mMixedGasLowest.setText(String.valueOf(MixedGasSL));
                                mN2Highest.setText(String.valueOf(N2SU));
                                mN2Lowest.setText(String.valueOf(N2SL));
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void SearchOManufactureDailyDetailInspection(){
        Log.e("編輯", "有跑嗎?");
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                Statement statement = connection.createStatement();
                String Single;
                //region
                Single = "SELECT tblOManufactureDailyDetailInspection75.*" +
                        " FROM tblOManufactureDailyDetailInspection75" +
                        " WHERE tblOManufactureDailyDetailInspection75.OrderID=" + OrderID +
                        " AND tblOManufactureDailyDetailInspection75.SerialNumber=" + SerialNumber +
                        " AND tblOManufactureDailyDetailInspection75.InspectionDate='" + InspectionDate +
                        "' AND tblOManufactureDailyDetailInspection75.InspectionTime='" + InspectionTime + "'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try{
                            if(SingleResultSet.getString("TungstenRodOD") != null){
                                TungstenRodOD = SingleResultSet.getFloat("TungstenRodOD");
                            }
                            if(SingleResultSet.getString("WeldCurrentSU") != null){
                                WeldCurrentSU = SingleResultSet.getFloat("WeldCurrentSU");
                            }
                            if(SingleResultSet.getString("WeldCurrentSL") != null){
                                WeldCurrentSL = SingleResultSet.getFloat("WeldCurrentSL");
                            }
                            if(SingleResultSet.getString("WeldCurrent") != null){
                                WeldCurrent = SingleResultSet.getFloat("WeldCurrent");
                            }
                            if(SingleResultSet.getString("DiskSpeedASU") != null){
                                DiskSpeedASU = SingleResultSet.getFloat("DiskSpeedASU");
                            }
                            if(SingleResultSet.getString("DiskSpeedASL") != null){
                                DiskSpeedASL = SingleResultSet.getFloat("DiskSpeedASL");
                            }
                            if(SingleResultSet.getString("DiskSpeedA") != null){
                                DiskSpeedA = SingleResultSet.getFloat("DiskSpeedA");
                            }
                            if(SingleResultSet.getString("MixedGasSU") != null){
                                MixedGasSU = SingleResultSet.getFloat("MixedGasSU");
                            }
                            if(SingleResultSet.getString("MixedGasSL") != null){
                                MixedGasSL = SingleResultSet.getFloat("MixedGasSL");
                            }
                            if(SingleResultSet.getString("MixedGas") != null){
                                MixedGas = SingleResultSet.getFloat("MixedGas");
                            }
                            if(SingleResultSet.getString("N2SU") != null){
                                N2SU = SingleResultSet.getFloat("N2SU");
                            }
                            if(SingleResultSet.getString("N2SL") != null){
                                N2SL = SingleResultSet.getFloat("N2SL");
                            }
                            if(SingleResultSet.getString("N2") != null){
                                N2 = SingleResultSet.getFloat("N2");
                            }
                            runOnUiThread(()->{
                                mTungstenRodOD.setText(String.valueOf(TungstenRodOD));
                                mWeldCurrentHighest.setText(String.valueOf(WeldCurrentSU));
                                mWeldCurrentLowest.setText(String.valueOf(WeldCurrentSL));
                                mWeldCurrent.setText(String.valueOf(WeldCurrent));
                                mDiskSpeedAHighest.setText(String.valueOf(DiskSpeedASU));
                                mDiskSpeedALowest.setText(String.valueOf(DiskSpeedASL));
                                mDiskSpeedA.setText(String.valueOf(DiskSpeedA));
                                mMixedGasHighest.setText(String.valueOf(MixedGasSU));
                                mMixedGasLowest.setText(String.valueOf(MixedGasSL));
                                mMixedGas.setText(String.valueOf(MixedGas));
                                mN2Highest.setText(String.valueOf(N2SU));
                                mN2Lowest.setText(String.valueOf(N2SL));
                                mN2.setText(String.valueOf(N2));
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void DeleteDate(){
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single = "DELETE tblOManufactureDailyDetailInspection75 FROM tblOManufactureDailyDetailInspection75 WHERE OrderID = " + OrderID +
                        " AND SerialNumber = " + SerialNumber +
                        " AND InspectionDate = '" + InspectionDate +
                        "' AND InspectionTime = '" + InspectionTime +"'";
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                ShowToast("刪除成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask, 2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
            Log.e("SQL", "資料內容有誤");
        }
    }

    void AddDate(){
        try{
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e("SQL", "現在時間: " + now);

                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection01_Modify =
                        "INSERT into tblOManufactureDailyDetailInspection75(" +
                                "OrderID, SerialNumber, InspectionDate, InspectionTime," +
                                " TungstenRodOD," +
                                " WeldCurrentSU, WeldCurrentSL, WeldCurrent," +
                                " DiskSpeedASU, DiskSpeedASL, DiskSpeedA," +
                                " MixedGasSU, MixedGasSL, MixedGas," +
                                " N2SU, N2SL, N2," +
                                " CreateDate, CreateUserNo, CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate)" +
                                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection01_Modify, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1, OrderID);
                comm.setString(2, SerialNumber);
                comm.setString(3, InspectionDate);
                comm.setString(4, InspectionTime);

                comm.setString(5, mTungstenRodOD.getText().toString());

                comm.setString(6, String.valueOf(WeldCurrentSU));
                comm.setString(7, String.valueOf(WeldCurrentSL));
                comm.setString(8, mWeldCurrent.getText().toString());

                comm.setString(9, String.valueOf(DiskSpeedASU));
                comm.setString(10, String.valueOf(DiskSpeedASL));
                comm.setString(11, mDiskSpeedA.getText().toString());

                comm.setString(12, String.valueOf(MixedGasSU));
                comm.setString(13, String.valueOf(MixedGasSL));
                comm.setString(14, mMixedGas.getText().toString());

                comm.setString(15, String.valueOf(N2SU));
                comm.setString(16, String.valueOf(N2SL));
                comm.setString(17, mN2.getText().toString());

                comm.setString(18, now);
                comm.setString(19, EmployeeID);
                comm.setString(20, EmployeeName);
                comm.setString(21, now);
                comm.setString(22, EmployeeID);
                comm.setString(23, EmployeeName);
                comm.setString(24, now);

                comm.executeUpdate();

                ShowToast("新增成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask, 2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void UpDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        now = simpleDateFormat.format(date);
        Log.e("現在時間", now);
        try {
            if(connection == null){
                Log.e("SQL", "connection NULL");
            }else{
                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection01_Modify =
                        "UPDATE tblOManufactureDailyDetailInspection75"+
                                " SET TungstenRodOD = ?,"+
                                " WeldCurrent = ?, DiskSpeedA = ?, MixedGas = ?, N2 = ?,"+
                                " ModifyDate = ?, ModifyUserNo = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                                " WHERE OrderID = " + OrderID +
                                " AND SerialNumber = " + SerialNumber +
                                " AND InspectionDate = '" + InspectionDate +
                                "' AND InspectionTime = '" + InspectionTime + "'";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection01_Modify);
                comm.setString(1, mTungstenRodOD.getText().toString());
                comm.setString(2, mWeldCurrent.getText().toString());
                comm.setString(3, mDiskSpeedA.getText().toString());
                comm.setString(4, mMixedGas.getText().toString());
                comm.setString(5, mN2.getText().toString());
                comm.setString(6, now);
                comm.setString(7, EmployeeID);
                comm.setString(8, EmployeeName);
                comm.setString(9, now);

                Log.e("上傳資料:", "OrderID: " + OrderID + " SerialNumber: " + SerialNumber +" InspectionDate: "+ InspectionDate + " InspectionTime: "+ InspectionTime);
                Log.e("上傳資料:", " SelfDetermination: "+ (SelfDetermination.equals("V")? "True":"False"));
                Log.e("上傳資料:", "now: " + now + " EmployeeID: " + EmployeeID +" EmployeeName: "+ EmployeeName);
                comm.executeUpdate();

                ShowToast("修改成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask,2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private String GetStartDate(String Date){
        try{
            java.util.Date date;
            @SuppressLint("SimpleDateFormat") DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            return Stat.replace("-","/");
        } catch (NullPointerException | ParseException e){
            e.printStackTrace();
        }
        return null;
    }

    private void ShowToast(String Text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText(Text);
        toast.show();
    }

    private void OpenPopTime(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime, null, false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event)-> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> TimeFormat(hourOfDay, minute));
        Check.setOnClickListener(v12 -> {
            TimeJudgment();
            popupWindow.dismiss();
        });
    }

    private void TimeJudgment(){
        String [] StartTime = InputStartTime.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(StartTime[0]));
        calendar1.set(Calendar.MINUTE, Integer.parseInt(StartTime[1]));
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time1 = df.format(calendar1.getTime());
        Log.e("PASS1011", time1);
        Log.e("時間",calendar1.toString());
        Log.e("時間",calendar.toString());
        if(calendar.compareTo(calendar1) < 0){
            Log.e("時差", "增加一天");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar2 = Calendar.getInstance();
            try {
                Date date;
                date = dateFormat.parse(InputEndDate);
                calendar2.setTime(date);
                calendar2.add(Calendar.DATE, 1);
            }catch (ParseException e){
                e.printStackTrace();
            }
        }else{
            Log.e("時差", "不變");
        }
    }

    private void TimeFormat(int hourOfDay, int minute){
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar.getTime());
        Log.e("TimeFormat", time);
        mExamineTime.setText("");
        mExamineTime.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
    }
}