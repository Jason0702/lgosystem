package com.sj.ligosystem.Control;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.FrameFormWork;
import com.sj.ligosystem.R;

import java.util.List;

public class item_start_frameAdapter extends RecyclerView.Adapter<item_start_frameAdapter.ViewHolder> {
    private OnItemClickListener onItemClickListener = null;
    private Context context;
    private List<FrameFormWork> frameFormWorkList;

    public item_start_frameAdapter(Context context, List<FrameFormWork> frameFormWorks){
        this.context = context;
        this.frameFormWorkList = frameFormWorks;
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public item_start_frameAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_startin_frame,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final FrameFormWork frameFormWork = frameFormWorkList.get(position);
        holder.ProcessID.setText("ID:" + frameFormWork.getProcessID());
        holder.ProcessDesc.setText(frameFormWork.getProcessDesc());
        holder.itemView.setOnClickListener(v->{
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() { return frameFormWorkList.size(); }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView ProcessID, ProcessDesc;
        ViewHolder(View itemView){
            super(itemView);
            ProcessID = itemView.findViewById(R.id.ISF_ProcessIDTextView);
            ProcessDesc = itemView.findViewById(R.id.ISF_ProcessDescTextView);
        }
    }
}
