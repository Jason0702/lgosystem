package com.sj.ligosystem.Control;

import com.sj.ligosystem.Model.item_start_card;

import java.util.Comparator;

public class NumberSort implements Comparator<item_start_card> {
    @Override
    public int compare(item_start_card o1, item_start_card o2) {
        return o2.getNumber().compareTo(o1.getNumber());
    }
}
