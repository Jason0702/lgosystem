package com.sj.ligosystem.Control;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.R;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> implements Filterable {
    private List Typeobject, Temp_Turbojet;
    TestFilter myFilter;

    private OnItemClickListener mOnItemClickListener;

    public SearchAdapter(List<String> Obj){
        this.Typeobject = Obj;
        Temp_Turbojet = Obj;
    }

    @Override
    public Filter getFilter() {
        if (myFilter == null){
            myFilter = new TestFilter();
        }
        return myFilter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_searchview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.OutputText.setText(Typeobject.get(position).toString());
        if (mOnItemClickListener != null){
            holder.itemView.setOnClickListener(v -> mOnItemClickListener.onItemClick(holder.itemView, holder.OutputText.getText().toString()));
        }
    }

    @Override
    public int getItemCount() {
        return Typeobject.size();
    }

    public void setmOnItemClickListener(OnItemClickListener listener){
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener{
        void onItemClick(View view, String OutputText);
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        TextView OutputText;

        public ViewHolder(View itemView){
            super(itemView);
            OutputText = itemView.findViewById(R.id.OutputText);
        }
    }
    class TestFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<String> new_String = new ArrayList<>();
            if (constraint != null && constraint.toString().trim().length() > 0){
                for (int i = 0; i< Temp_Turbojet.size(); i++){
                    String content = (String) Temp_Turbojet.get(i);
                    if (content.contains(constraint)){
                        new_String.add(content);
                    }
                }
            }else{
                new_String = Temp_Turbojet;
            }
            FilterResults filterResults = new FilterResults();
            filterResults.count = new_String.size();
            filterResults.values = new_String;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //這裡對OBJ進行過濾後重新賦值
            Typeobject = (List)results.values;
            //如果過濾後的反渾實的個數大於等於0的話，對Adpater的介面進行刷新
            if (results.count>0){
                notifyDataSetChanged();
            }else{
                //否則說明沒有任何過濾的結果，直接提示用戶"沒有符合條件的結果"
                Typeobject = new ArrayList(){};
                Typeobject.add("沒有符合條件的結果");
                notifyDataSetChanged();
            }
        }
    }
}
