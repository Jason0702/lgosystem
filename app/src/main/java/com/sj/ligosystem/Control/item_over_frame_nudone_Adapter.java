package com.sj.ligosystem.Control;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.item_frame_card;
import com.sj.ligosystem.R;

import java.util.List;

public class item_over_frame_nudone_Adapter extends RecyclerView.Adapter<item_over_frame_nudone_Adapter.ViewHolder> {
    private OnItemClickListener onItemClickListener = null;
    private Context context;
    private List<item_frame_card> itemOverCards;

    public item_over_frame_nudone_Adapter(Context context, List<item_frame_card> itemOverCards){
        this.context = context;
        this.itemOverCards = itemOverCards;
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_over_frame_nudone_card,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final item_frame_card item_over_card = itemOverCards.get(position);
        holder.ManufactureOrderIDTextView.setText(item_over_card.getEmployeeID() + item_over_card.getEmployeeName());//缺Name
        holder.SerialNumberTextView.setText(item_over_card.getSerialNumber());
        holder.MachineName.setText(item_over_card.getMachineID()+ " " + item_over_card.getMachineDesc());//缺機台名稱
        holder.ProcessIDView.setText(item_over_card.getProcessID()+ " " + item_over_card.getProcessDesc()); //缺道次名稱
        holder.StartTimeTextView.setText(item_over_card.getStartTime());
        holder.OverTimeTextView.setText("");

        holder.DemoldingView.setText(item_over_card.getBProductsTypeID()+ "-" + item_over_card.getBSizeID()+"("+ item_over_card.getBSizeCaption()  +")-"+ item_over_card.getBThicknessID());
        holder.FrameView.setText(item_over_card.getProductsTypeID()+ "-" + item_over_card.getSizeID()+"("+ item_over_card.getSizeCaption()  +")-"+ item_over_card.getThicknessID());

        holder.WorkingTimes.setText(item_over_card.getWorkingTimesB());
        holder.itemView.setOnClickListener(v -> {
            Log.e("TAG", item_over_card.getEmployeeName());
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() {
        return itemOverCards.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView ManufactureOrderIDTextView, SerialNumberTextView,MachineName, ProcessIDView, StartTimeTextView,OverTimeTextView, DemoldingView, FrameView, WorkingTimes;
        ImageView ChickImageView;
        ViewHolder(View itemView){
            super(itemView);
            ManufactureOrderIDTextView = itemView.findViewById(R.id.OF_ManufactureOrderIDTextView);
            SerialNumberTextView = itemView.findViewById(R.id.OF_SerialNumberTextView);
            MachineName = itemView.findViewById(R.id.OF_MachineNameTextView);
            ProcessIDView = itemView.findViewById(R.id.OF_ProcessIDView);

            StartTimeTextView = itemView.findViewById(R.id.OF_StartTimeTextView);
            OverTimeTextView = itemView.findViewById(R.id.OF_OverTimeTextView);

            DemoldingView = itemView.findViewById(R.id.OF_DemoldingView);
            FrameView = itemView.findViewById(R.id.OF_Frame_View);

            WorkingTimes = itemView.findViewById(R.id.OF_WorkingTimesView);
        }

    }
}
