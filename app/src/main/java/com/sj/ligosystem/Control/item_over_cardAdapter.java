package com.sj.ligosystem.Control;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.item_over_card;
import com.sj.ligosystem.R;

import java.util.List;

public class item_over_cardAdapter extends RecyclerView.Adapter<item_over_cardAdapter.ViewHolder> {
    private OnItemClickListener onItemClickListener = null;
    private Context context;
    private List<item_over_card> item_over_cards;
    public item_over_cardAdapter(Context context, List<item_over_card> itemOverCards){
        this.context = context;
        this.item_over_cards = itemOverCards;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public item_over_cardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_overin_cardview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final item_over_card item_over_card = item_over_cards.get(position);
        holder.ManufactureOrderIDTextView.setText(item_over_card.getManufactureOrderID());
        holder.ProductsDescriptionTextView.setText(item_over_card.getProductsDescription());
        holder.StartTimeTextView.setText(item_over_card.getStartTime());
        //holder.OverTimeTextView.setText(item_over_card.getEndTime());
        holder.SerialNumberTextView.setText(item_over_card.getSerialNumber());

        holder.StandardQty.setText(item_over_card.getStandardQty());

        holder.ProcessIDView.setText(item_over_card.getProcessID()+" "+item_over_card.getProcessDesc());

        holder.itemView.setOnClickListener(v -> {
            Log.e("TAG",item_over_cards.get(position).getManufactureOrderID());
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() {
        return item_over_cards.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView ManufactureOrderIDTextView,ProductsDescriptionTextView,StartTimeTextView,OverTimeTextView,SerialNumberTextView,StandardQty,ProcessIDView;
        ImageView ChickImageView;
        CardView cardView;
        ViewHolder(View itemView){
            super(itemView);
            ManufactureOrderIDTextView = itemView.findViewById(R.id.OC_ManufactureOrderIDTextView);
            ProductsDescriptionTextView = itemView.findViewById(R.id.OC_ProductsDescriptionTextView);
            SerialNumberTextView = itemView.findViewById(R.id.OC_SerialNumberTextView);
            StartTimeTextView = itemView.findViewById(R.id.OC_StartTimeTextView);
            OverTimeTextView = itemView.findViewById(R.id.OC_OverTimeTextView);
            ChickImageView = itemView.findViewById(R.id.OC_ChickImageView);
            cardView = itemView.findViewById(R.id.OC_CardView);

            ProcessIDView = itemView.findViewById(R.id.OC_ProcessIDView);

            StandardQty = itemView.findViewById(R.id.OC_StandardQty);
        }
    }
}
