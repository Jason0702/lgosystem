package com.sj.ligosystem.Control;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.item_start_card;
import com.sj.ligosystem.R;

import java.util.List;

public class item_start_cardAdapter extends RecyclerView.Adapter<item_start_cardAdapter.ViewHolder> {
    private Context context;
    private OnItemClickListener onItemClickListener = null;
    private List<item_start_card> itemStartCards;

    public item_start_cardAdapter(Context context, List<item_start_card> item_start_cards){
        this.context = context;
        this.itemStartCards = item_start_cards;
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }
    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }
    @NonNull
    @Override
    public item_start_cardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_startin_cardview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final item_start_card item_start_card = itemStartCards.get(position);
        holder.Number.setText(item_start_card.getNumber());
        holder.Leader.setText("組長: "+item_start_card.getLeader());
        holder.InputUser.setText("輸入: "+item_start_card.getInputUser());
        holder.Operator.setText("作業員: "+item_start_card.getOperator());

        if(position == SelectedNavItem.getSlectedNavItem()){
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.CardView_Color));
            holder.Number.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.Leader.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.InputUser.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.Operator.setTextColor(context.getResources().getColor(R.color.Text_Color));
        }else{
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.Number.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.Leader.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.InputUser.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.Operator.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
        }

        holder.itemView.setOnClickListener(v -> {
            Log.e("TAG",itemStartCards.get(position).getNumber());
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() {
        return itemStartCards.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView Number,Leader,InputUser,Operator;
        CardView cardView;
        ViewHolder(View itemView){
            super(itemView);
            Number = itemView.findViewById(R.id.SC_NumberTextView);
            Leader = itemView.findViewById(R.id.SC_LeaderTextView);
            InputUser = itemView.findViewById(R.id.SC_InputUserTextView);
            Operator = itemView.findViewById(R.id.SC_OperatorTextView);
            cardView = itemView.findViewById(R.id.SC_CardView);
        }
    }
}
