package com.sj.ligosystem.Control;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.PassInspection;
import com.sj.ligosystem.R;

import java.util.List;

public class item_over_passinspecion_complete_Adapter extends RecyclerView.Adapter<item_over_passinspecion_complete_Adapter.ViewHolder> {
    private OnItemClickListener onItemClickListener = null;
    private Context context;
    private List<PassInspection> passInspections;

    public item_over_passinspecion_complete_Adapter(Context context, List<PassInspection> passInspections){
        this.context = context;
        this.passInspections = passInspections;
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }
    @NonNull
    @Override
    public item_over_passinspecion_complete_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_overin_completed_cardview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull item_over_passinspecion_complete_Adapter.ViewHolder holder, int position) {
        final PassInspection passInspection = passInspections.get(position);
        holder.ManufactureOrderIDTextView.setText(passInspection.getManufactureOrderID());
        holder.ProductsDescriptionTextView.setText(passInspection.getProductsDescription());
        holder.StartTimeTextView.setText(passInspection.getStartTime());
        holder.OverTimeTextView.setText(passInspection.getEndTime());
        holder.SerialNumberTextView.setText(passInspection.getSerialNumber());

        holder.YieldQty.setText(passInspection.getYirldQty());
        holder.RestorationQty.setText(passInspection.getRestorationQty());
        holder.ScrapQty.setText(passInspection.getScrapQty());
        holder.YieldQtyA.setText(passInspection.getYieldQtyA());
        holder.YieldQtyB.setText(passInspection.getYieldQtyB());

        holder.ProcessIDView.setText(passInspection.getProcessID()+" "+passInspection.getProcessDesc());

        holder.StandardQty.setText(passInspection.getStandardQty());

        holder.itemView.setOnClickListener(v -> {
            Log.e("TAG", passInspections.get(position).getManufactureOrderID());
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() {
        return passInspections.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView ManufactureOrderIDTextView,ProductsDescriptionTextView,StartTimeTextView,OverTimeTextView,SerialNumberTextView,YieldQty, RestorationQty, ScrapQty, YieldQtyA, YieldQtyB, StandardQty, ProcessIDView;
        ImageView ChickImageView;
        CardView cardView;

        ViewHolder(View itemView){
            super(itemView);
            ManufactureOrderIDTextView = itemView.findViewById(R.id.OC_ManufactureOrderIDTextView);
            ProductsDescriptionTextView = itemView.findViewById(R.id.OC_ProductsDescriptionTextView);
            SerialNumberTextView = itemView.findViewById(R.id.OC_SerialNumberTextView);
            StartTimeTextView = itemView.findViewById(R.id.OC_StartTimeTextView);
            OverTimeTextView = itemView.findViewById(R.id.OC_OverTimeTextView);
            ChickImageView = itemView.findViewById(R.id.OC_ChickImageView);
            cardView = itemView.findViewById(R.id.OC_CardView);

            YieldQty = itemView.findViewById(R.id.OC_YieldQtyView);
            RestorationQty = itemView.findViewById(R.id.OC_RestorationQtyView);
            ScrapQty = itemView.findViewById(R.id.OC_ScrapQtyView);
            YieldQtyA = itemView.findViewById(R.id.OC_YieldQtyAView);
            YieldQtyB = itemView.findViewById(R.id.OC_YieldQtyBView);

            ProcessIDView = itemView.findViewById(R.id.OC_ProcessIDView);

            StandardQty = itemView.findViewById(R.id.OC_StandardQty);
        }
    }
}
