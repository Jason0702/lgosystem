package com.sj.ligosystem.Control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.item_pass_inspection;
import com.sj.ligosystem.R;


import java.util.List;

public class item_pass_inspection_details_Adapter extends RecyclerView.Adapter<item_pass_inspection_details_Adapter.ViewHolder> {
    private OnItemClickListener onItemClickListener = null;
    private Context context;
    private List<item_pass_inspection> inspectionList;

    public item_pass_inspection_details_Adapter(Context context, List<item_pass_inspection> item_pass_inspections){
        this.context = context;
        this.inspectionList = item_pass_inspections;
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public item_pass_inspection_details_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_pass_inspection_details,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull item_pass_inspection_details_Adapter.ViewHolder holder, int position) {
        final item_pass_inspection item_pass_inspection = inspectionList.get(position);
        holder.InspectionTimeTextView.setText(item_pass_inspection.getInspectionTime());
        holder.SelfDeterminationTextView.setText(item_pass_inspection.getSelfDetermination());
        if(position == SelectedNavItem.getSlectedNavItem()){
            holder.Card.setCardBackgroundColor(context.getResources().getColor(R.color.CardView_Color));
            holder.InspectionTimeTextView.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.textInspectionTime.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.SelfDeterminationTextView.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.textSelfDetermination.setTextColor(context.getResources().getColor(R.color.Text_Color));
        }else{
            holder.Card.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.InspectionTimeTextView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.textInspectionTime.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.SelfDeterminationTextView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.textSelfDetermination.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
        }


        holder.itemView.setOnClickListener(v ->{
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() {
        return inspectionList.size();
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView InspectionTimeTextView, SelfDeterminationTextView;
        TextView textInspectionTime, textSelfDetermination;
        CardView Card;
        ViewHolder(View itemView){
            super(itemView);
            InspectionTimeTextView = itemView.findViewById(R.id.VPA_InspectionTime);
            SelfDeterminationTextView = itemView.findViewById(R.id.VPA_SelfDetermination);
            Card = itemView.findViewById(R.id.OC_CardView);
            textInspectionTime = itemView.findViewById(R.id.text_VPA_InspectionTime);
            textSelfDetermination = itemView.findViewById(R.id.text_VPA_SelfDetermination);
        }

    }
}
