package com.sj.ligosystem.Control;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.item_operator;
import com.sj.ligosystem.R;

import java.util.List;

public class item_operator_Adapter extends RecyclerView.Adapter<item_operator_Adapter.ViewHolder> {
    private item_operator_Adapter.OnItemClickListener onItemClickListener = null;
    private Context context;
    private List<item_operator> itemOperators;

    public item_operator_Adapter(Context context, List<item_operator> item_operator){
        this.context = context;
        this.itemOperators = item_operator;
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }
    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public item_operator_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_operator,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final item_operator item_operator = itemOperators.get(position);
        holder.EmployeeID.setText(item_operator.getEmployeeID());
        holder.EmployeeName.setText(item_operator.getEmployeeName());
        if(position == SelectedNavItem.getSlectedNavItem()){
            holder.EmployeeID.setTextColor(context.getResources().getColor(R.color.CardView_Color));
            holder.EmployeeName.setTextColor(context.getResources().getColor(R.color.CardView_Color));
        }else{
            holder.EmployeeID.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.EmployeeName.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
        }
        holder.itemView.setOnClickListener(v -> {
            Log.e("TAG",item_operator.getEmployeeID());
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() {
        return itemOperators.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView EmployeeID, EmployeeName;
        ViewHolder(View itemView){
            super(itemView);
            EmployeeID = itemView.findViewById(R.id.EmployeeID);
            EmployeeName = itemView.findViewById(R.id.EmployeeName);
        }
    }
}
