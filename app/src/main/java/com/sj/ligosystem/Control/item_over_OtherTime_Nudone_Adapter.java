package com.sj.ligosystem.Control;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.item_OtherTime_card;
import com.sj.ligosystem.R;

import java.util.List;

public class item_over_OtherTime_Nudone_Adapter extends RecyclerView.Adapter<item_over_OtherTime_Nudone_Adapter.ViewHolder> {
    private OnItemClickListener onItemClickListener = null;
    private Context context;
    private List<item_OtherTime_card> itemOtherTimeCardList;

    public item_over_OtherTime_Nudone_Adapter(Context context, List<item_OtherTime_card> item_otherTime_cards){
        this.context = context;
        this.itemOtherTimeCardList = item_otherTime_cards;
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_over_other_time_nudone, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final item_OtherTime_card itemOtherTimeCard = itemOtherTimeCardList.get(position);
        holder.EmployeeNameView.setText(itemOtherTimeCard.getEmployeeID()+" "+ itemOtherTimeCard.getEmployeeName());
        holder.SerialNumberView.setText(itemOtherTimeCard.getSerialNumber());
        holder.WorkingHourView.setText(itemOtherTimeCard.getWorkingHourID() + "\n" + itemOtherTimeCard.getProcessDesc());
        holder.StartTimeView.setText(itemOtherTimeCard.getStartTime());
        holder.EndTimeView.setText("");
        if(itemOtherTimeCard.getWorkingHourID().equals("B01")){
           holder.MachineView.setVisibility(View.VISIBLE);
           holder.MachineNameView.setVisibility(View.VISIBLE);
           holder.MachineNameView.setText(itemOtherTimeCard.getMachineIDB() + "\n" +itemOtherTimeCard.getMachineDesc());
        }else{
            holder.MachineView.setVisibility(View.GONE);
            holder.MachineNameView.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            Log.e("TAG", itemOtherTimeCard.getEmployeeID());
            onItemClickListener.onItemClick(holder.itemView,position);
        });
    }

    @Override
    public int getItemCount() {
        return itemOtherTimeCardList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView EmployeeNameView, WorkingHourView, SerialNumberView, MachineView, MachineNameView, StartTimeView, EndTimeView;
        ViewHolder(View itemView){
            super(itemView);
            EmployeeNameView = itemView.findViewById(R.id.OOT_EmployeeNameView);
            WorkingHourView = itemView.findViewById(R.id.OOT_WorkingHourView);
            SerialNumberView = itemView.findViewById(R.id.OOT_SerialNumberView);
            MachineView = itemView.findViewById(R.id.OOT_MachineView);
            MachineNameView = itemView.findViewById(R.id.OOT_MachineNameView);
            StartTimeView = itemView.findViewById(R.id.OOT_StartTimeTextView);
            EndTimeView = itemView.findViewById(R.id.OOT_OverTimeTextView);
        }
    }
}
