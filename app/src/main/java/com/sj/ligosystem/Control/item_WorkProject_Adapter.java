package com.sj.ligosystem.Control;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Model.WorkingDescDetails;
import com.sj.ligosystem.R;

import java.util.List;

public class item_WorkProject_Adapter extends RecyclerView.Adapter<item_WorkProject_Adapter.ViewHolder> {
    private Context context;
    private OnItemClickListener onItemClickListener = null;
    private List<WorkingDescDetails> workingDescDetails;

    public item_WorkProject_Adapter(Context context, List<WorkingDescDetails> workingDescDetails){
        this.context = context;
        this.workingDescDetails = workingDescDetails;
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public item_WorkProject_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_workproject_card,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final WorkingDescDetails workingDescDetail = workingDescDetails.get(position);
        holder.ManufactureOrderID.setText("工作項目: "+workingDescDetail.getStandardIDItemNo() +" "+workingDescDetail.getWorkingDesc());
        holder.RateView.setText(workingDescDetail.getRate());
        holder.RateAView.setText(workingDescDetail.getRateA());
        holder.RateBView.setText(workingDescDetail.getRateB());
        if(position == SelectedNavItem.getSlectedNavItem()){
            holder.Card.setCardBackgroundColor(context.getResources().getColor(R.color.CardView_Color));
            holder.ManufactureOrderID.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.RateView.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.RateTextView.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.RateAView.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.RateATextView.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.RateBView.setTextColor(context.getResources().getColor(R.color.Text_Color));
            holder.RateBTextView.setTextColor(context.getResources().getColor(R.color.Text_Color));
        }else{
            holder.Card.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.ManufactureOrderID.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.RateView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.RateTextView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.RateAView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.RateATextView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.RateBView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
            holder.RateBTextView.setTextColor(context.getResources().getColor(R.color.Title_Text_Color));
        }
        holder.itemView.setOnClickListener(v -> {
            Log.e("TAG",workingDescDetail.getStandardIDItemNo());
            onItemClickListener.onItemClick(holder.itemView,position);
        });

    }

    @Override
    public int getItemCount() {
        return workingDescDetails.size();
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView ManufactureOrderID, RateAView, RateBView, RateView,
                RateTextView, RateBTextView, RateATextView;
        CardView Card;
        ViewHolder(View itemView){
            super(itemView);
            ManufactureOrderID = itemView.findViewById(R.id.WP_ManufactureOrderID);
            RateAView = itemView.findViewById(R.id.WP_RateAView);
            RateBView = itemView.findViewById(R.id.WP_RateBView);
            RateView = itemView.findViewById(R.id.WP_RateView);
            RateTextView = itemView.findViewById(R.id.WP_RateTextView);
            RateBTextView = itemView.findViewById(R.id.WP_RateBTextView);
            RateATextView = itemView.findViewById(R.id.WP_RateATextView);
            Card = itemView.findViewById(R.id.WP_cardView);
        }
    }
}
