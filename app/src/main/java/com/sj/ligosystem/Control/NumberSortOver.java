package com.sj.ligosystem.Control;

import com.sj.ligosystem.Model.item_over_card;

import java.util.Comparator;

public class NumberSortOver implements Comparator<item_over_card> {

    @Override
    public int compare(item_over_card o1, item_over_card o2) {
        return o2.getSerialNumber().compareTo(o1.getSerialNumber());
    }
}
