package com.sj.ligosystem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.sj.ligosystem.Control.ConnectionClass;
import com.sj.ligosystem.Control.PagerAdapter;
import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Model.Detail;
import com.sj.ligosystem.Model.Machine;
import com.sj.ligosystem.Model.Process;
import com.sj.ligosystem.Model.User;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.Package.CompletedActivity;
import com.sj.ligosystem.Package.OtherActivity;
import com.sj.ligosystem.Package.StartActivity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";
    public static Connection connection;

    private List<Fragment> fragments = new ArrayList<>();
    private List<String> titles = new ArrayList<>();
    private TabLayout mTableLayout;
    private ViewPager mViewPager;
    private StartActivity mStartFragment;
    private CompletedActivity mCompletedFragment;
    private OtherActivity mOtherFragment;

    TextView mTitle;
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        if(Util.AllManufacture.isEmpty() && Util.AllMachine.isEmpty() && Util.AllUserList.isEmpty()) {
            //連接資料庫
            ConnectionDataBase();
        }else{
            constraintLayout.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
        }
        initData();

    }
    void ConnectionDataBase(){
        ConnectionClass connectionClass = new ConnectionClass();
        //region 資料庫連線 與 測試
        try {
            connection = connectionClass.CONN();
            if(connection == null){
                Log.e(TAG,"connection NULL");
                ShowErrorAlertDialog();
            }else{
                BackgroundDownload backgroundDownload = new BackgroundDownload();
                backgroundDownload.execute(this);
                mViewPager.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //endregion
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SelectedNavItem.setSlectedNavItem(0);
        Log.e(TAG,"Resume");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    void initView(){

        constraintLayout = findViewById(R.id.LoadDataLayout);

        mTitle = findViewById(R.id.MainTitleTextView);
        mTitle.setText(getResources().getText(R.string.MainTitle));
        mTableLayout = findViewById(R.id.tabLayout);
        mViewPager = findViewById(R.id.vp_view);
        mViewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),MainActivity.this,fragments,titles));
        mTableLayout.setupWithViewPager(mViewPager);//此方法就是讓tablayout和ViewPager連動
    }
    void initData(){

        fragments.add(mStartFragment.newInstance(getResources().getString(R.string.StartTitle)));
        fragments.add(mCompletedFragment.newInstance(getResources().getString(R.string.CompletedTitle)));
        fragments.add(mOtherFragment.newInstance(getResources().getString(R.string.OtherTitle)));
        titles.add(getResources().getString(R.string.StartTitle));
        titles.add(getResources().getString(R.string.CompletedTitle));
        titles.add(getResources().getString(R.string.OtherTitle));
        mViewPager.getAdapter().notifyDataSetChanged();
    }
    void ShowErrorAlertDialog(){
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("錯誤")
                .setMessage("請確認網路連線正常!")
                .setPositiveButton("關閉視窗", (dialog, which) -> dialog.dismiss())
                .show();
    }

    class BackgroundDownload extends AsyncTask<Context,Void,Void>{

        final String TAG = "BackgroundDownload";

        //人員
        void EmployeeUser(){
            try{
                //region 人員
                String Userquery = "Select EmployeeID, EmployeeName,Password,TeamLeader From tblBEmployee";
                Statement statement = connection.createStatement();
                ResultSet UserResultSet = statement.executeQuery(Userquery);

                if(UserResultSet != null){
                    while (UserResultSet.next()){
                        try {
                            //Log.i(TAG,"ID: "+UserResultSet.getString("EmployeeID") +" "+ "Name: " + UserResultSet.getString("EmployeeName") + " " + UserResultSet.getString("Password"));
                            Util.AllUserList.add(new User(UserResultSet.getString("EmployeeID"),
                                    UserResultSet.getString("EmployeeName"),
                                    UserResultSet.getString("Password"),
                                    UserResultSet.getString("TeamLeader")));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                //endregion
            }catch (Exception e){e.printStackTrace();}
        }
        //機台
        void Machine(){
            try{
                //region 機台
                String Machinequery = "Select * From tblBMachine";
                Statement Machinest = connection.createStatement();
                ResultSet MachineResultSet = Machinest.executeQuery(Machinequery);

                if(MachineResultSet != null){
                    while (MachineResultSet.next()){
                        try{
                            //Log.i(TAG, "MachineID: "+ MachineResultSet.getString("MachineID") + " MachineName: "+ MachineResultSet.getString("MachineDesc"));
                            Util.AllMachine.add(new Machine(MachineResultSet.getString("MachineID"),
                                    MachineResultSet.getString("MachineDesc")));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                //endregion
            }catch (Exception e){e.printStackTrace();}
        }
        //道次
        void Process(){
            try{
                String Processquery = "Select * From tblBProcess";
                Statement ProcessConn = connection.createStatement();
                ResultSet ProcessResultSet = ProcessConn.executeQuery(Processquery);

                if(ProcessResultSet != null){
                    while (ProcessResultSet.next()){
                        try{
                            Util.AllProcess.add(new Process(ProcessResultSet.getString("ProcessID"),
                                    ProcessResultSet.getString("ProcessDesc"),
                                    ProcessResultSet.getString("WorkingHourTypeID")));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }catch (Exception e){e.printStackTrace();}
        }
        //工令號
        void Manufactur(){
            try{
                //region工令號
                String Manufacturequery = "Select * From [010工作通知單資料表]";
                Statement ManufactureConn = connection.createStatement();
                ResultSet ManufactureResultSet = ManufactureConn.executeQuery(Manufacturequery);

                if(ManufactureResultSet != null){
                    while (ManufactureResultSet.next()){
                        try{
                        Util.AllManufacture.add(new Detail(ManufactureResultSet.getString("工令編號"),
                                ManufactureResultSet.getString("總單工令編號序號"),
                                ManufactureResultSet.getString("產品類別編號"),
                                ManufactureResultSet.getString("生產別"),
                                ManufactureResultSet.getString("尺寸代碼"),
                                ManufactureResultSet.getString("SW"),
                                ManufactureResultSet.getString("獎金編號"),
                                ManufactureResultSet.getString("品名"),
                                ManufactureResultSet.getString("規格"),
                                ManufactureResultSet.getString("尺寸"),
                                ManufactureResultSet.getString("需求成品數"),
                                ManufactureResultSet.getString("規範編號")
                        ));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                //endregion
            }catch (Exception e){e.printStackTrace();}
        }
        //架模產品下拉
        void ProductsTypeID(){
            try{
                String ProductsTypeQuery = "SELECT tblBProductsType.ProductsTypeID, tblBProductsType.ProductsTypeDescription AS [Desc], tblBProductsType.SizeQty" +
                        " FROM tblBProductsType ORDER BY tblBProductsType.ProductsTypeID";
                Statement ProductsTypeConn = connection.createStatement();
                ResultSet ProductsTypeResultSet = ProductsTypeConn.executeQuery(ProductsTypeQuery);

                if(ProductsTypeResultSet != null){
                    while (ProductsTypeResultSet.next()){
                        try{
                            String ProductsTypeID = ProductsTypeResultSet.getString("ProductsTypeID").trim();
                            String ProductsTypeDesc = ProductsTypeResultSet.getString("Desc");
                            //Log.i(TAG, ProductsTypeID + " ("+ProductsTypeDesc+")");
                            Util.AllProductsType.add(ProductsTypeID + " ("+ProductsTypeDesc+")");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

            }catch (Exception e){e.printStackTrace();}
        }
        //尺寸下拉
        void SizeTypeID(){
            try{
                String SizeTypeQuery = "SELECT tblBProductsSizeCaption.SizeID, tblBProductsSizeCaption.SizeCaption AS [Desc], tblBProductsSizeCaption.SizeQty" +
                        " FROM tblBProductsSizeCaption ORDER BY tblBProductsSizeCaption.SizeQty, tblBProductsSizeCaption.SizeID";
                Statement SizeTypeConn = connection.createStatement();
                ResultSet SizeTypeResultSet = SizeTypeConn.executeQuery(SizeTypeQuery);

                if(SizeTypeResultSet != null){
                    while (SizeTypeResultSet.next()){
                        try{
                            String SizeTypeID = SizeTypeResultSet.getString("SizeID").trim();
                            String SizeTypeDesc = SizeTypeResultSet.getString("Desc");
                            String SizeQty = SizeTypeResultSet.getString("SizeQty");
                            //Log.i(TAG, SizeTypeID + " ("+SizeTypeDesc+")" + SizeQty);
                            Util.AllSize.add(SizeTypeID + " ("+SizeTypeDesc+")");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                //endregion
            }catch (Exception e){e.printStackTrace();}
        }
        //厚度下拉
        void ThicknessTypeID(){
            try{
                String ThicknessTypeQuery = "SELECT tblBProductsThicknessCaption.ThicknessID, tblBProductsThicknessCaption.ThicknessDescription AS [Desc]" +
                        " FROM tblBProductsThicknessCaption ORDER BY tblBProductsThicknessCaption.Priority, tblBProductsThicknessCaption.ThicknessID";
                Statement ThicknessTypeConn = connection.createStatement();
                ResultSet ThicknessTypeResultSet = ThicknessTypeConn.executeQuery(ThicknessTypeQuery);

                if(ThicknessTypeResultSet != null){
                    while (ThicknessTypeResultSet.next()){
                        try{
                            String ThicknessTypeID = ThicknessTypeResultSet.getString("ThicknessID").trim();
                            String ThicknessTypeDesc = ThicknessTypeResultSet.getString("Desc");
                            Log.i(TAG, ThicknessTypeID +" ("+ThicknessTypeDesc+")");
                            Util.AllThickness.add(ThicknessTypeID + " (" + ThicknessTypeDesc+")");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                //endregion
            }catch (Exception e){e.printStackTrace();}
        }


        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Context... contexts) {
            Log.e(TAG,"doInBackground");
            if(connection != null) {
                EmployeeUser();
                Machine();
                ProductsTypeID();
                SizeTypeID();
                ThicknessTypeID();
            }

            return null;
        }


        //背景處理完"後"做的事
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            constraintLayout.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
            Log.e(TAG,"onPostExecute");
        }
    }

}

