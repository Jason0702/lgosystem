package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.MainActivity;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.security.auth.login.LoginException;

import static com.sj.ligosystem.MainActivity.connection;
/**
 * 生產開工(新增明細)
 **/
@SuppressLint("UseSparseArrays")
public class Details_addedActivity extends AppCompatActivity {
    final String TAG = "Details_addedActivity";
    Toolbar toolbar;
    TextView WorkView,SingleNumberView,ItemsView,MachineView,
            ProductNameView,BonusNumberView,StandardQuantityView,InputPassNameView,InputOrder_QtyView,PassTextView,OrderTextView;
    EditText InputPassView,InputOrderNumberView,InputDayView,
            InputTimeView,InputPasswordView;
    Button CleanButton,LeaveButton,VerifyButton;
    ImageView CalenderView,TimeView,InputOrder_Check,InputPassView_Check;

    Map<Integer, String> Work = new HashMap<>();
    Timer timer;
    int year,month,day,hour,min;
    String PassWord,OrderNoBarCode,OrderID,SerialNumber,MachineID,MachineName,EmployeeName,EmployeeID
            ,ManufactureOrderID,ProductsDescription,ManufactureQty,BountyCode,ProcessID,ProcessDesc,WorkShiftType,
            ProductsTypeID,StandardRequestID,SeamTypeID,RawJobTypeID,PutDate,PutTime,now,Detail_ProductView,TableName, ImageFileName,
            SaleTypeID ,InspectionFormID, CompletedFormID,MaterialFormID,DiagramFormID;
    String StandardQty = "";
    boolean isProcess, isManufactur, isPassWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_added);
        //物件初始化
        initView();

        Work.put(0,"正常班");
        Work.put(1,"加班");

        initData();
        initControl();

    }

    @Override
    protected void onStart() {
        super.onStart();
        //region 取得日期

        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);

        //endregion
        InputTimeView.setText(String.format("%02d",hour) + ":" +String.format("%02d", min));

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar1.getTime());
        Log.e(TAG, time);
        PutTime = time;
        Log.e(TAG, PutTime);

    }

    private void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region UI物件
        //正常班或加班TextView
        WorkView = findViewById(R.id.DA_Work_View);
        //單號TextView
        SingleNumberView = findViewById(R.id.DA_single_number_View);
        //項次TextView
        ItemsView = findViewById(R.id.DA_Items_View);
        //機台TextView
        MachineView = findViewById(R.id.DA_Machine_View);
        //品名
        ProductNameView = findViewById(R.id.DA_Product_nameView);
        //獎金編號
        BonusNumberView = findViewById(R.id.DA_Bonus_NumberView);
        //標準量
        StandardQuantityView = findViewById(R.id.DA_Standard_quantity_View);
        //道次
        InputPassNameView = findViewById(R.id.DA_InputPassNameView);
        //道次TextView
        PassTextView = findViewById(R.id.textView17);
        //下料數
        InputOrder_QtyView = findViewById(R.id.DA_InputOrder_Qty);

        //道次EditText
        InputPassView = findViewById(R.id.DA_InputPassView);
        //工令號EditText
        InputOrderNumberView = findViewById(R.id.DA_InputOrder_number_View);
        //工令號TextView
        OrderTextView = findViewById(R.id.textView18);
        //日期EditText
        InputDayView = findViewById(R.id.DA_InputDayView);
        InputDayView.setEnabled(false);
        //時間EditText
        InputTimeView = findViewById(R.id.DA_InputTimeView);
        InputTimeView.setEnabled(false);
        //密碼確認EditText
        InputPasswordView = findViewById(R.id.DA_InputPasswordView);

        //清除Button
        CleanButton = findViewById(R.id.DA_Clean_Button);
        //離開Button
        LeaveButton = findViewById(R.id.DA_Leave_Button);
        //儲存Button
        VerifyButton = findViewById(R.id.DA_Verify_Button);
        //ImageView
        InputPassView_Check = findViewById(R.id.DA_InputPassView_Check_imageView);
        InputOrder_Check = findViewById(R.id.DA_InputOrder_Check_imageView);
        InputOrder_Check.setVisibility(View.GONE);
        InputPassView_Check.setVisibility(View.GONE);
        //日期選擇
        CalenderView = findViewById(R.id.DA_Calendar_View);
        //時間選擇
        TimeView = findViewById(R.id.DA_Time_View);

        InputDayView.setEnabled(false);
        InputTimeView.setEnabled(false);

        CleanButton.setOnClickListener(Click);
        LeaveButton.setOnClickListener(Click);
        VerifyButton.setOnClickListener(Click);
        CalenderView.setOnClickListener(Click);
        TimeView.setOnClickListener(Click);
        //endregion

        //region 輸入監聽
        runOnUiThread(()->{
            InputPassView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                    InputPassView_Check.setVisibility(View.GONE);
                    InputPassNameView.setText("");
                    InputPassView.setError(null,null);
                }else{
                    if(!Process(InputPassView.getText().toString().toUpperCase().trim()) || InputPassView.getText().toString().isEmpty()){
                        Log.e(TAG,InputPassView.getText().toString().toUpperCase().trim() + InputPassView.getText().toString().isEmpty());
                        if(!InputPassView.getText().toString().isEmpty()){
                            InputPassView_Check.setVisibility(View.GONE);
                            InputPassView.setError("不正確道次編號，請重新輸入！");
                            isProcess = false;
                        }else{
                            InputPassView_Check.setVisibility(View.GONE);
                            isProcess = false;
                        }
                    }else{
                        InputPassView_Check.setVisibility(View.VISIBLE);
                        InputPassView.setText(ProcessID);
                        InputPassView.setSelection(ProcessID.length());
                        InputPassNameView.setText(ProcessDesc.trim());
                        InputPassView.setError(null,null);
                        isProcess = true;
                    }
                }
            });
            InputOrderNumberView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                    InputOrder_Check.setVisibility(View.GONE);
                    BonusNumberView.setText("");
                    InputOrder_QtyView.setText("");
                    ProductNameView.setText("");
                    InputOrderNumberView.setError(null,null);
                }else{
                    if(!Manufactur(InputOrderNumberView.getText().toString().toUpperCase().trim()) || InputOrderNumberView.getText().toString().isEmpty()){
                        if(!InputOrderNumberView.getText().toString().isEmpty()){
                            InputOrder_Check.setVisibility(View.GONE);
                            InputOrderNumberView.setError("不正確工令編號或已作廢，請重新輸入！");
                            isManufactur = false;
                        }else{
                            InputOrder_Check.setVisibility(View.GONE);
                        }
                    }else{
                        GetStandardQty();
                        InputOrder_Check.setVisibility(View.VISIBLE);
                        InputOrderNumberView.setText(ManufactureOrderID);
                        InputOrderNumberView.setSelection(ManufactureOrderID.length());
                        ProductNameView.setText(Detail_ProductView);
                        BonusNumberView.setText(BountyCode);
                        InputOrder_QtyView.setText(ManufactureQty);
                        InputOrderNumberView.setError(null,null);
                        isManufactur = true;

                    }
                }
            });
            InputPasswordView.setOnFocusChangeListener((v, hasFocus)->{
                if(hasFocus){

                }else {
                    if(!InputPasswordView.getText().toString().trim().equals(PassWord) || InputPasswordView.getText().toString().isEmpty()){
                        if (InputPasswordView.getText().toString().isEmpty()){
                            InputPasswordView.setError("密碼不可為空");
                            isPassWord = false;
                        }else{
                            InputPasswordView.setError("密碼輸入錯誤，請重新檢查!");
                            isPassWord = false;
                        }
                    }else{
                        isPassWord = true;
                        InputPasswordView.setError(null,null);
                    }
                }
            });
        });
        //endregion
    }

    private void initData(){
        Intent intent = getIntent();
        PutDate = getIntent().getStringExtra("DayTime");
        InputDayView.setText(PutDate);
        int WorkType = intent.getIntExtra("StartWork",0);
        //寫入資料庫用
        WorkShiftType = String.valueOf(WorkType);

        String SingleNumber = intent.getStringExtra("SingleNumber");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");
        PassWord = intent.getStringExtra("Password");
        OrderNoBarCode = intent.getStringExtra("OrderNoBarCode");
        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeeID = intent.getStringExtra("EmployeeID");

        Log.e(TAG,"OrderNoBarCode"+OrderNoBarCode);
        OrderID = String.valueOf(intent.getIntExtra("OrderID",0));

        GetSerialNumber();
        runOnUiThread(()->{
            //班別
            WorkView.setText(Work.get(WorkType));
            //單號
            SingleNumberView.setText(SingleNumber);
            //機台
            MachineView.setText(MachineID+" "+MachineName);
        });
    }
    void initControl(){
        if(SelectedNavItem.getSlectedNavItem()!= 0){
            SelectedNavItem.setSlectedNavItem(0);
        }
    }
    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.DA_Calendar_View:
                OpenPopCalender(v);
                break;
            case R.id.DA_Time_View:
                OpenPopTime(v);
                break;
            case R.id.DA_Clean_Button:
                CleanView();
                break;
            case R.id.DA_Leave_Button:
                GoBack();
                break;
            case R.id.DA_Verify_Button:
                InputPassView.clearFocus();
                InputOrderNumberView.clearFocus();
                InputPasswordView.clearFocus();
                Log.e(TAG, "密碼: "+PassWord + "輸入碼: "+InputPasswordView.getText().toString());
                if(isPassWord && isManufactur && isProcess) {
                    AddDetails();
                }else if(!isProcess){
                    InputPassView.requestFocus();
                    ShowToast("請輸入道次");
                }else if(!isManufactur){
                    InputOrderNumberView.requestFocus();
                    ShowToast("請輸入工令號");
                }else if(!isPassWord){
                    InputPasswordView.requestFocus();
                    ShowToast("請確認密碼是否輸入正確!");
                }
                break;
        }
    };
    private void GoBack(){
        Intent CallBackView = new Intent();
        CallBackView.setClass(this, MainActivity.class);
        startActivity(CallBackView);
        finish();
    }
    private void OpenPopCalender(View v){
        hideInput(getApplicationContext(),v);
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_popcalendar,null,false);
        CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            InputDayView.setText("");
            Toast.makeText(Details_addedActivity.this,"選擇的是: "+ year + "/" + (month+1) + "/" + dayOfMonth,Toast.LENGTH_LONG).show();
            InputDayView.setText(String.format("%04d",year)  + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",dayOfMonth));
            DayTime(year,month,dayOfMonth);

            popupWindow.dismiss();
        });
    }
    private void DayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        PutDate = sdf.format(new Date(eventOccursOn));
        Log.e(TAG,"Date: " + PutDate);
    }
    @SuppressLint("SetTextI18n")
    private void OpenPopTime(View v){
        CalenderView.setEnabled(false);

        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime,null,false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);


        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
            InputTimeView.setText("");
            InputTimeView.setText( String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));

            Calendar calendar = Calendar.getInstance();
            calendar.set(1899,11,30,hourOfDay,minute);
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            String time = df.format(calendar.getTime());
            Log.e(TAG, time);
            PutTime = time;
        });
        Check.setOnClickListener(v12 -> {
            CalenderView.setEnabled(true);
            popupWindow.dismiss();
        });
    }

    private void CleanView(){
        InputPassView.setError(null,null);
        InputOrderNumberView.setError(null,null);
        InputPassView_Check.setVisibility(View.INVISIBLE);
        InputOrder_Check.setVisibility(View.INVISIBLE);
        ProductNameView.setText("");
        BonusNumberView.setText("");
        StandardQuantityView.setText("");
        InputPassView.setText("");
        InputOrderNumberView.setText("");
        InputPassNameView.setText("");
        InputOrder_QtyView.setText("");
        InputPassView.requestFocus();
        isManufactur = false;
        isProcess = false;
    }


    //region取得 SerialNumber
    private void GetSerialNumber(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT tblOManufactureDailyDetail.*" +
                                " FROM tblOManufactureDailyDetail" +
                                " WHERE tblOManufactureDailyDetail.OrderID = "+ OrderID;
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            Log.e(TAG,SingleResultSet.getString("StartTime"));
                            SerialNumber = SingleResultSet.getString("SerialNumber");

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    if(SerialNumber == null){
                        SerialNumber = "1";
                    }else{
                        SerialNumber = String.valueOf(Integer.valueOf(SerialNumber) + 1);
                    }
                    //項次
                    ItemsView.setText(SerialNumber);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    //endregion

    private void GetStandardQty(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得

                Log.e(TAG, "BountyCode: "+ BountyCode);
                Log.e(TAG, "MachineID: " + MachineID);
                Log.e(TAG, "ProcessID" + ProcessID);

                Single =
                        "SELECT tblBMachineProcessBounty.StandardQty FROM tblBMachineProcessBounty"+
                        " WHERE BountyCode ='" +BountyCode+ "'" +
                        " AND MachineID ='" +MachineID+ "' AND ProcessID ='" +ProcessID+ "' ORDER BY OperationTypeID";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            Log.e(TAG,"標準量: "+SingleResultSet.getString("StandardQty"));
                            StandardQty = SingleResultSet.getString("StandardQty");
                            StandardQuantityView.setText(StandardQty);
                            if(StandardQuantityView.getText().toString().isEmpty()){
                                StandardQty = "";
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

    }



    //region 新增明細
    private void AddDetails(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dt = new Date();
        now = sdf.format(dt);
        Log.e(TAG, "現在時間: " + now);
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Add
                Single = "INSERT into"+
                        " tblOManufactureDailyDetail ("+
                        " OrderID, SerialNumber, OrderNoBarCode, ManufactureOrderID, ProductsDescription,"+
                        " ManufactureQty, BountyCode, MachineID, ProcessID, OperationTypeID, WorkingHourID,"+
                        " WorkShiftType, StartDate, StartTime, EndDate, EndTime, WorkingTimes, YieldQty,"+
                        " RestorationQty, YieldQtyA, YieldQtyB, ScrapQty, StandardQty, ProductsTypeID,"+
                        " StandardRequestID, SeamTypeID, RawJobTypeID, SaleTypeID, InspectionFormID,"+
                        " CompletedFormID, MaterialFormID, DiagramFormID, TableName, ImageFileName," +
                        " CreateDate, CreateUserNo, CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate )"+
                        " VALUES(?,?,?,?,?,?,?,?,?,?,"+"?,?,?,?,?,?,?,?,?,?,"+"?,?,?,?,?,?,?,?,?,?,"+"?,?,?,?,?,?,?,?,?,?,?)";
                String OrderBarCord = OrderNoBarCode + String.format("%02d",Integer.valueOf(SerialNumber));
                //OrderNoBarCode += String.format("%02d",Integer.valueOf(SerialNumber));
                Log.e(TAG,OrderNoBarCode);

                Log.e(TAG," StandardQty: " + StandardQty+
                        "\n ProductsTypeID: " + ProductsTypeID+
                        "\n StandardRequestID: " + StandardRequestID+
                        "\n SeamTypeID: " + SeamTypeID+
                        "\n RawJobTypeID: " + RawJobTypeID+
                        "\n SaleTypeID: " + SaleTypeID);

                Log.e(TAG," InspectionFormID: " + InspectionFormID+
                        "\n CompletedFormID: " + CompletedFormID+
                        "\n MaterialFormID: " + MaterialFormID+
                        "\n DiagramFormID: " + DiagramFormID+
                        "\n TableName: " + TableName+
                        "\n ImageFileName: " + ImageFileName);
                comm = connection.prepareStatement(Single);
                comm.setString(1, OrderID);// OrderID
                comm.setString(2, SerialNumber);// SerialNumber
                comm.setString(3, OrderBarCord);// [OrderNoBarCode] + [SerialNumber], "00"
                comm.setString(4, ManufactureOrderID);// ManufactureOrderID
                comm.setString(5, ProductsDescription);// ProductsDescription
                comm.setString(6, ManufactureQty);// ManufactureQty
                comm.setString(7, BountyCode);// BountyCode
                comm.setString(8, MachineID);// MachineID
                comm.setString(9, ProcessID);//ProcessID
                comm.setString(10, "0");// 0
                comm.setString(11, ProcessID);//ProcessID
                comm.setString(12, WorkShiftType);//WorkShiftType
                comm.setString(13, PutDate);// StartDate
                comm.setString(14, PutTime);// StartTime
                comm.setString(15, PutDate);// EndDate
                comm.setString(16, PutTime);// EndTime
                comm.setString(17, "0");// WorkingTimes = 0
                comm.setString(18, "0");// YieldQty = 0
                comm.setString(19, "0");// RestorationQty = 0
                comm.setString(20, "0");// YieldQtyA = 0
                comm.setString(21, "0");//YieldQtyB = 0
                comm.setString(22, "0");// ScrapQty = 0

                comm.setString(23, StandardQty.isEmpty()? "0" : StandardQty);// StandardQty 0
                comm.setString(24, ProductsTypeID);// ProductsTypeID
                comm.setString(25, StandardRequestID);// StandardRequestID
                comm.setString(26, SeamTypeID);// SeamTypeID
                comm.setString(27, RawJobTypeID);// RawJobTypeID
                comm.setString(28, SaleTypeID);// SaleTypeID
                comm.setString(29, InspectionFormID.isEmpty() ? "0" : InspectionFormID);//InspectionFormID 0
                comm.setString(30, CompletedFormID.isEmpty() ? "0" : CompletedFormID);//CompletedFormID 0
                comm.setString(31, MaterialFormID.isEmpty() ? "0" : MaterialFormID);//MaterialFormID 0
                comm.setString(32, DiagramFormID.isEmpty() ? "0" : DiagramFormID);//DiagramFormID 0
                comm.setString(33,TableName);//TableName
                comm.setString(34,ImageFileName); //ImageFileName


                comm.setString(35, now);// now
                comm.setString(36, EmployeeID);// EmployeeID
                comm.setString(37, EmployeeName);// EmployeeName
                comm.setString(38, now);// now
                comm.setString(39, EmployeeID); //EmployeeID
                comm.setString(40, EmployeeName);// EmployeeName
                comm.setString(41, now);// now

                comm.executeUpdate();
                ShowToast("新增成功!");
                runOnUiThread(() -> {
                    CleanView();
                    GetSerialNumber();

                    InputPassView.requestFocus();
                });
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }
    }
    //endregion

    //region Toast
    private void ShowToast(String Text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText(Text);
        toast.show();
    }
    //endregion

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }
    //region 隱藏鍵盤
    private void hideInput(Context context, View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
    }
    //endregion
    private Boolean Process(String InputProcessID){
        Boolean isProcessID = false;
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT ProcessID, ProcessDesc, WorkingHourTypeID" +
                                " FROM tblBProcess" +
                                " WHERE tblBProcess.ProcessID = '"+ InputProcessID +"' AND tblBProcess.WorkingHourTypeID = '01'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            if(SingleResultSet.getString("ProcessID")!= null){
                                Log.e(TAG,SingleResultSet.getString("ProcessID")+SingleResultSet.getString("ProcessDesc") + SingleResultSet.getString("WorkingHourTypeID"));
                                ProcessID = SingleResultSet.getString("ProcessID");
                                ProcessDesc = SingleResultSet.getString("ProcessDesc");
                                isProcessID = true;
                            }else{
                                isProcessID = false;
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
            isProcessID = false;
        }
        return isProcessID;
    }

    private Boolean Manufactur(String InputManufacturID){
        Boolean isManufacturID = false;
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT * FROM [010工作通知單資料表]  WHERE 工令編號='" + InputManufacturID + "' AND 作廢 = 'False'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            if(SingleResultSet.getString("工令編號")!= null){
                                Log.e(TAG,SingleResultSet.getString("工令編號")+ " " +
                                        SingleResultSet.getString("總單工令編號序號")+  " " +
                                        SingleResultSet.getString("產品類別編號")+  " " +
                                        SingleResultSet.getString("生產別")+  " " +
                                        SingleResultSet.getString("尺寸代碼")+  " " +
                                        SingleResultSet.getString("SW")+  " " +
                                        SingleResultSet.getString("獎金編號")+  " " +
                                        SingleResultSet.getString("品名")+  " " +
                                        SingleResultSet.getString("規格")+  " " +
                                        SingleResultSet.getString("尺寸")+  " " +
                                        SingleResultSet.getString("需求成品數")+  " " +
                                        SingleResultSet.getString("規範編號") + " " +
                                        SingleResultSet.getString("ProductsTypeID"));
                                Detail_ProductView = SingleResultSet.getString("品名")+"\n"+SingleResultSet.getString("規格")+" "+SingleResultSet.getString("尺寸");
                                String [] QtySplit = SingleResultSet.getString("需求成品數").split("\\.");
                                //寫入資料庫部分
                                ProductsDescription = Detail_ProductView;
                                ManufactureQty = QtySplit[0].trim();
                                ManufactureOrderID = SingleResultSet.getString("工令編號").trim();
                                BountyCode = SingleResultSet.getString("獎金編號").trim();
                                //ProductsTypeID = SingleResultSet.getString("產品類別編號").trim();
                                //StandardRequestID = SingleResultSet.getString("生產別").trim();
                                //SeamTypeID = SingleResultSet.getString("SW").trim();
                                //RawJobTypeID = (SingleResultSet.getString("規範編號").trim());
                                ProductsTypeID = SingleResultSet.getString("ProductsTypeID").trim();
                                StandardRequestID = SingleResultSet.getString("StandardRequestID").trim();
                                SeamTypeID = SingleResultSet.getString("SeamTypeID").trim();
                                RawJobTypeID = (SingleResultSet.getString("RawJobTypeID").trim());
                                SaleTypeID = SingleResultSet.getString("SaleTypeID").trim();
                                isManufacturID = true;
                                ProductsTypeProcessID(ProcessID);
                            }else{
                                isManufacturID = false;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
            isManufacturID = false;
        }
        return isManufacturID;
    }

    private Boolean ProductsTypeProcessID(String ProcessID){
        Log.e("ProductsTypeProcessID","取得開始 "+ ProcessID + " " + ProductsTypeID);
        Boolean isProductsTypeProcessID = false;
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT * FROM tblBProductsTypeProcess WHERE tblBProductsTypeProcess.ProcessID = '" + ProcessID +
                                "' AND tblBProductsTypeProcess.ProductsTypeID = '" + ProductsTypeID + "'";
                                /*"' AND tblBProductsTypeProcess.StandardRequestID='" + StandardRequestID +
                                "' AND tblBProductsTypeProcess.SeamTypeID='" + SeamTypeID +
                                "' AND tblBProductsTypeProcess.RawJobTypeID='" + RawJobTypeID +
                                "' AND tblBProductsTypeProcess.SaleTypeID='" + SaleTypeID + "'"*/
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            Log.e("InspectionFormID", SingleResultSet.getString("InspectionFormID"));
                            if(SingleResultSet.getString("InspectionFormID") != null){
                                Log.e(TAG," InspectionFormID: " + SingleResultSet.getString("InspectionFormID")+
                                        "\n CompletedFormID: " + SingleResultSet.getString("CompletedFormID")+
                                        "\n MaterialFormID: " + SingleResultSet.getString("MaterialFormID")+
                                        "\n DiagramFormID: " + SingleResultSet.getString("DiagramFormID")+
                                        "\n TableName: " + SingleResultSet.getString("TableName")+
                                        "\n ImageFileName: " + SingleResultSet.getString("ImageFileName"));
                                //寫入資料庫部分
                                InspectionFormID = SingleResultSet.getString("InspectionFormID");
                                CompletedFormID = SingleResultSet.getString("CompletedFormID");
                                MaterialFormID = SingleResultSet.getString("MaterialFormID");
                                DiagramFormID = SingleResultSet.getString("DiagramFormID");
                                TableName = SingleResultSet.getString("TableName");
                                ImageFileName = SingleResultSet.getString("ImageFileName");
                                isProductsTypeProcessID = true;
                            }else{
                                isProductsTypeProcessID = false;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
            isProductsTypeProcessID = false;
        }
        return isProductsTypeProcessID;
    }
}
