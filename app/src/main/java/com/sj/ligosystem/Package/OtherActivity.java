package com.sj.ligosystem.Package;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sj.ligosystem.Control.BaseFragment;
import com.sj.ligosystem.R;

import ru.katso.livebutton.LiveButton;

public class OtherActivity extends BaseFragment {
    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    LiveButton Operator_Button,Inspection_Button;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.activity_other,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();
        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null){
            parent.removeView(mView);
        }
        return mView;
    }
    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        Operator_Button = mView.findViewById(R.id.Operator_LVbutton);
        Inspection_Button = mView.findViewById(R.id.Inspection_LVbutton);

        Operator_Button.setOnClickListener(Click);
        Inspection_Button.setOnClickListener(Click);
    }

    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }
    public static OtherActivity newInstance(String param){
        OtherActivity otherActivity = new OtherActivity();
        Bundle args = new Bundle();
        args.putString("agrs1", param);
        otherActivity.setArguments(args);
        return otherActivity;
    }
    View.OnClickListener Click = v -> {
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.Operator_LVbutton:
                intent.setClass(mView.getContext(),Operator_AddedActivity.class);
                break;
            case R.id.Inspection_LVbutton:
                intent.setClass(mView.getContext(),PassInspectionActivity.class);
                break;
        }
        startActivity(intent);
    };
}
