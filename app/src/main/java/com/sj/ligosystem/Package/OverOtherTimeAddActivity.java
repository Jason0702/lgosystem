package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;
/**
 * 其他工時完工回報(明細輸入)
 **/
public class OverOtherTimeAddActivity extends AppCompatActivity {
    final String TAG = OverOtherTimeAddActivity.class.getSimpleName();

    Toolbar toolbar;
    TextView OrderNoBarCodeView, SerialNumberView, EmployeeIDView, EmployeeNameView, WorkHourTimeTypeView, WorkHourTypeNameView, MachineIDView, MachineNameView, MachineTextView, StartTimeView,EndDateView, EndTimeView;
    EditText EmployeePassWordView, WorkingTimeView;
    Button Leave_Button, Verify_Button, Delete_Button;
    ImageView TimeView;

    int year,month,day,hour,min;
    String StartDataTime, PutTime, PutDate, WorkingTimes, now;
    String OrderID, SerialNumber, OrderNoBarCode, EmployeeID, EmployeeName, WorkHourTimeID, WorkHourTimeName, InputStartDate, InputStartTime, PassWord, MachineIDB, MachineName, WorkShiftType, MachineID;
    boolean isWorkingTime;
    Timer timer;
    Intent intent = new Intent();
    Calendar calendar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_over_other_time_add);
        initView();
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
        day  = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.e(TAG,"HR:"+hour);
        //endregion

        //region 取得系統時間
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);
        TimeFormat(hour,min);

        TimeJudgment();

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar1.getTime());
        Log.e(TAG, time);
        PutTime = time;
        Log.e(TAG, PutTime);
        //endregion

        String EndDate = EndDateView.getText().toString() + " " +EndTimeView.getText().toString();
        WorkingTime(StartDataTime,EndDate);
    }

    void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        OrderNoBarCodeView = findViewById(R.id.OOA_OrderNoBarCodeView);
        SerialNumberView = findViewById(R.id.OOA_SerialNumberView);
        EmployeeIDView = findViewById(R.id.OOA_EmployeeIDView);
        EmployeeNameView = findViewById(R.id.OOA_EmployeeNameView);
        WorkHourTimeTypeView = findViewById(R.id.OOA_WorkHourTimeTypeView);
        WorkHourTypeNameView = findViewById(R.id.OOA_WorkHourTypeNameView);
        MachineIDView = findViewById(R.id.OOA_MachineIDView);
        MachineNameView = findViewById(R.id.OOA_MachineNameView);
        MachineTextView = findViewById(R.id.OOA_MachineTextView);
        //endregion

        //region EditText
        StartTimeView = findViewById(R.id.OOA_StartTimeView);
        StartTimeView.setEnabled(false);
        EmployeePassWordView = findViewById(R.id.OOA_EmployeePassWordView);
        EndDateView = findViewById(R.id.OOA_EndDateView);
        EndDateView.setOnClickListener(Click);
        EndTimeView = findViewById(R.id.OOA_EndTimeView);
        WorkingTimeView = findViewById(R.id.OOA_WorkingTimeView);

        WorkingTimeView.setOnKeyListener((v, keyCode, event) ->{
            if(keyCode == KeyEvent.KEYCODE_DEL){
                isWorkingTime = false;
            }
            return false;
        });

        WorkingTimeView.setOnFocusChangeListener((v,hasFocus) -> {
            if(!hasFocus && !WorkingTimeView.getText().toString().isEmpty()) {
                PushBackWorkingTime(StartTimeView.getText().toString(), WorkingTimeView.getText().toString());
            }else if(WorkingTimeView.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(),"請輸入工作分鐘", Toast.LENGTH_LONG).show();
                WorkingTimeView.setText("0");
                WorkingTimeView.requestFocus();
            }
        });
        EmployeePassWordView = findViewById(R.id.OOA_EmployeePassWordView);


        //endregion
        //region Button
        Leave_Button = findViewById(R.id.OOA_Leave_Button);
        Leave_Button.setOnClickListener(Click);
        Verify_Button = findViewById(R.id.OOA_Verify_Button);
        Verify_Button.setOnClickListener(Click);
        Delete_Button = findViewById(R.id.OOA_Delete_Button);
        Delete_Button.setOnClickListener(Click);
        //endregion
        //region ImageView
        TimeView = findViewById(R.id.OOA_TimeView);
        TimeView.setOnClickListener(Click);
        //endregion
    }

    void initData(){
        Intent GetbeforeData = getIntent();
        OrderID = GetbeforeData.getStringExtra("OrderID");
        SerialNumber = GetbeforeData.getStringExtra("SerialNumber");
        OrderNoBarCode = GetbeforeData.getStringExtra("OrderNoBarCode");
        EmployeeID = GetbeforeData.getStringExtra("EmployeeID");
        EmployeeName = GetbeforeData.getStringExtra("EmployeeName");
        WorkHourTimeID = GetbeforeData.getStringExtra("WorkHourTimeID");
        WorkHourTimeName = GetbeforeData.getStringExtra("WorkHourTimeName");
        InputStartDate = GetStartDate(GetbeforeData.getStringExtra("StartDate"));
        InputStartTime = GetbeforeData.getStringExtra("StartTime");
        PassWord = GetbeforeData.getStringExtra("PassWord");
        WorkShiftType = GetbeforeData.getStringExtra("WorkShiftType");
        MachineID = GetbeforeData.getStringExtra("MachineID");

        if(WorkHourTimeID.equals("B01")){
            MachineIDB = GetbeforeData.getStringExtra("MachineIDB");
            MachineName = GetbeforeData.getStringExtra("MachineName");
            MachineIDView.setText(MachineIDB);
            MachineNameView.setText(MachineName);
            MachineTextView.setVisibility(View.VISIBLE);
        }else{
            MachineIDView.setVisibility(View.GONE);
            MachineNameView.setVisibility(View.GONE);
            MachineTextView.setVisibility(View.GONE);
        }
        OrderNoBarCodeView.setText(OrderNoBarCode);
        SerialNumberView.setText(SerialNumber);
        EmployeeIDView.setText(EmployeeID);
        EmployeeNameView.setText(EmployeeName);
        WorkHourTimeTypeView.setText(WorkHourTimeID);
        WorkHourTypeNameView.setText(WorkHourTimeName);
        StartDataTime = InputStartDate+ " " + InputStartTime;
        StartTimeView.setText(StartDataTime);

        //WorkingTimeView.clearFocus();
    }
    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.OOA_TimeView:
                OpenPopTime(v);
                break;
            case R.id.OOA_Leave_Button:
                finish();
                break;
            case R.id.OOA_Verify_Button:
                Log.e(TAG, WorkingTimeView.getText().toString());
                Log.e(TAG, String.valueOf(isWorkingTime));

                if(!WorkingTimeView.getText().toString().isEmpty() && !WorkingTimeView.getText().toString().equals("0") && !EmployeePassWordView.getText().toString().isEmpty() && isWorkingTime && PassWord.equals(EmployeePassWordView.getText().toString())) {
                    UpDataOtherTime();
                } else if(!PassWord.equals(EmployeePassWordView.getText().toString())){
                    Toast.makeText(getApplicationContext(),"請確認密碼是否輸入正確!",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.OOA_Delete_Button:
                AlertDialog();
                break;
            case R.id.OOA_EndDateView:
                OpenPopCalender(v);
                break;
        }
    };

    private void OpenPopCalender(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_popcalendar,null,false);
        CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);
        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            EndDateView.setText("");
            EndDateView.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",dayOfMonth));
            DayTime(year,month,dayOfMonth);
            WorkingTime(StartDataTime,EndDateView.getText().toString() + " " + EndTimeView.getText().toString());
            popupWindow.dismiss();
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    private void DayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        PutDate = sdf.format(new Date(eventOccursOn));
        Log.e(TAG,"Date: " + PutDate);
    }
    private void WorkingTime(String StartTime, String EndTime){
        //region 計算時差
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try{
            Date d1 = dateFormat.parse(StartTime);
            Date d2 = dateFormat.parse(EndTime);
            long diff = d2.getTime() - d1.getTime();
            Log.e("VPA","LongTime:" + diff);
            long min = diff/ (1000 * 60);
            Log.e(TAG,"分鐘:"+min);
            int outTime = (int)min;
            if(outTime<0){
                WorkingTimeView.setText("0");
                WorkingTimeView.requestFocus();
                WorkingTimeView.clearFocus();
                Toast.makeText(getApplicationContext(),"輸入時間不正確!",Toast.LENGTH_LONG).show();
            }else {
                WorkingTimes = String.valueOf(outTime);
                WorkingTimeView.setText(WorkingTimes);
                isWorkingTime = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //endregion
    }
    private void OpenPopTime(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime,null,false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);


        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
            TimeFormat(hourOfDay,minute);
        });
        Check.setOnClickListener(v12 -> {
            TimeJudgment();
            popupWindow.dismiss();
        });
    }


    private void TimeFormat(int hourOfDay, int minute){
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE,minute);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar.getTime());
        Log.e(TAG, time);
        EndTimeView.setText("");
        EndTimeView.setText( String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));
        PutTime = time;
        isWorkingTime = true;
    }
    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    private void PushBackWorkingTime(String StartTime, String InputTime){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date d1 = dateFormat.parse(StartTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d1);
            calendar.add(Calendar.MINUTE, Integer.parseInt(InputTime));
            int Year = calendar.get(Calendar.YEAR);
            int Month = calendar.get(Calendar.MONTH) + 1;
            int Day = calendar.get(Calendar.DAY_OF_MONTH);

            EndDateView.setText(String.format("%04d",Year)+ "/" +String.format("%02d",Month)+ "/" +String.format("%02d",Day));
            TimeFormat(calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void TimeJudgment(){
        String [] StartTime = InputStartTime.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY,Integer.valueOf(StartTime[0]));
        calendar1.set(Calendar.MINUTE,Integer.valueOf(StartTime[1]));
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time1 = df.format(calendar1.getTime());
        Log.e(TAG,time1);
        Log.e("時間", String.valueOf(calendar1.get(Calendar.YEAR)+calendar1.get(Calendar.MINUTE)+calendar1.get(Calendar.DAY_OF_MONTH)+calendar1.get(Calendar.HOUR_OF_DAY)+calendar1.get(Calendar.MONTH)));
        Log.e("時間",String.valueOf(calendar.get(Calendar.YEAR)+calendar.get(Calendar.MINUTE)+calendar.get(Calendar.DAY_OF_MONTH)+calendar.get(Calendar.HOUR_OF_DAY)+calendar.get(Calendar.MONTH)));

        if(calendar.compareTo(calendar1) == -1){
            Log.e("時差","增加一天");
            // 對 calendar 設定時間的方法
            // 設定傳入的時間格式
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar2 = Calendar.getInstance();
            // 指定一個日期
            try {
                Date date;
                date = dateFormat.parse(InputStartDate);
                calendar2.setTime(date);
                calendar2.add(Calendar.DATE,1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // 對 calendar 設定為 date 所定的日期
            String time2 = dateFormat.format(calendar2.getTime());
            Log.e("TAG",time2);
            EndDateView.setText(time2);
            WorkingTime(StartTimeView.getText().toString(),time2 + " " +EndTimeView.getText().toString());
        }else{
            Log.e("時差","不變");
            EndDateView.setText(InputStartDate);
            WorkingTime(StartTimeView.getText().toString(),InputStartDate+" "+EndTimeView.getText().toString());
        }
    }
    private String GetStartDate(String Date){
        try{
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            String newStat = Stat.replace("-","/");
            return newStat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    private void UpDataOtherTime(){
        Log.e("上傳日期",PutTime);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dt = new Date();
        now = sdf.format(dt);
        Log.e(TAG, "現在時間: " + now);

        try {
            if (connection == null) {
                Log.e(TAG, "connection NULL");
            } else {
                PreparedStatement comm;
                String Single;
                Single = " UPDATE tblOManufactureDailyOther" +
                        " SET EmployeeID = ?, WorkingHourID = ?, MachineID = ?, ProcessID = ?, HourMoney = ?," +
                        " WorkShiftType = ?, EndDate = ?, EndTime = ?, WorkingTimes = ?, MachineIDB = ?," +
                        " ModifyDate = ?, ModifyUserNo = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                        " WHERE OrderID = "+ OrderID +" AND SerialNumber = " + SerialNumber;
                comm = connection.prepareStatement(Single);
                comm.setString(1,EmployeeID);
                comm.setString(2,WorkHourTimeID);
                comm.setString(3,MachineID);
                comm.setString(4,WorkHourTimeID);
                comm.setString(5,"0");
                comm.setString(6,WorkShiftType);
                comm.setString(7,EndDateView.getText().toString());
                comm.setString(8,PutTime);
                comm.setString(9,WorkingTimes);
                comm.setString(10,MachineIDB);

                comm.setString(11,now);
                comm.setString(12,EmployeeID);
                comm.setString(13,EmployeeName);
                comm.setString(14,now);
                comm.executeUpdate();
                ShowToast();
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Log.e(TAG,"run方法所在的線程: "+Thread.currentThread().getName());
                        GoBack();
                    }
                };

                timer.schedule(timerTask,2000);
                //endregion
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //region Toast
    private void ShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("新增成功!");
        toast.show();
    }
    //endregion

    //region刪除對話框
    private void AlertDialog(){
        new AlertDialog.Builder(OverOtherTimeAddActivity.this)
                .setTitle("刪除")
                .setMessage("您確定要刪除此報工單號明細？ 項次號："+SerialNumber)
                .setPositiveButton("確定",((dialog, which) -> {
                    DeleteDetails();
                    dialog.dismiss();
                }))
                .setNegativeButton("取消",((dialog, which) -> dialog.dismiss()))
                .show();
    }
    //endregion

    void DeleteDetails(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single ="DELETE tblOManufactureDailyOther FROM tblOManufactureDailyOther WHERE OrderID = '" + OrderID + "' AND SerialNumber = '" + SerialNumber + "'";
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                DeleteShowToast();
                finish();
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }
    }

    //region DeleteToast
    private void DeleteShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("刪除成功!");
        toast.show();
    }
    //endregion

    private void GoBack(){
        intent.setClass(OverOtherTimeAddActivity.this,VerifyOtherTimeActivity.class);
        intent.putExtra("OrderID", Integer.valueOf(OrderID));
        Log.e("PutOrderID",OrderID);
        intent.putExtra("OrderNoBarCode", Util.OrderNoBarCode);
        intent.putExtra("EmployeeID",Util.EmployeeID);
        intent.putExtra("EmployeePassWord",Util.EmployeePassWord);
        intent.putExtra("MachineID",Util.MachineID);
        intent.putExtra("MachineName",Util.MachineName);
        intent.putExtra("EmployeeName",Util.EmployeeName);
        startActivity(intent);
        finish();
    }
}
