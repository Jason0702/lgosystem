package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;

/**
 * 道次檢驗(明細輸入)
 * */
public class VerifyPassInspectionInputActivity extends AppCompatActivity {
    final String TAG = VerifyPassInspectionInputActivity.class.getSimpleName();

    //取得
    String OrderID, SerialNumber,WorkingTimes;
    String InputOrderNoBarCode, InputManufactureOrderID, InputProductsDescription, InputMachineID,
            InputProcessID, InputProcessDesc, InputWorkShiftype, InputStartDate, InputStartTime,
            InputEndDate, InputEndTime, InputStandardQty, InputProductsTypeID, InputStandardRequestID,
            InputSeamTypeID, InputRawJobTypeID, InputSaleTypeID, InputInspectionFormID, InputCompletedFormID,
            InputMaterialFormID, InputDiagramFormID,PassWord;

    String EmployeeName,EmployeeID,MachineID,MachineName;
    String InspectionDate, InspectionTime, now;
    String BadReasons, CauseAnalysis, CorrectivePrecautions, SelfDetermination;
    String Type;

    int year, month, day, hour, min;

    Toolbar toolbar;
    TextView OrderNoBarCodeTextView,ManufactureOrderIDTextView,ProductsDescriptionTextView,ProcessTextView;
    TextView InspectionDateTextView;
    ImageView ImageTimeView;
    EditText InspectionTimeEditView, BadReasonsEditView, CauseAnalysisEditView, CorrectivePrecautionsEditView,ConfirmPasswordEditView;
    RadioGroup mRadioGroup;
    RadioButton mRadioBtnV, mRadioBtnX;
    Button btnQuit, btnDelete, btnSave;

    Timer timer;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_pass_inspection_input);

        initView();
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();

        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
        day  = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.e(TAG,"HR:"+hour);
        //endregion

        //region 取得系統時間
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);
        TimeFormat(hour,min);
        InspectionTimeEditView.setText(String.format("%tR",calendar1.getTime()));
        //endregion
        TimeJudgment();

        if(Type.equals("編輯")) {
            Log.e(TAG,Type);
            ImageTimeView.setEnabled(false);
            InspectionTimeEditView.setEnabled(false);
            SearchOManufactureDailyDetailInspection();
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        OrderNoBarCodeTextView = findViewById(R.id.VPA_OrderNoBarCodeTextView);
        ManufactureOrderIDTextView = findViewById(R.id.VPA_ManufactureOrderIDTextView);
        ProductsDescriptionTextView = findViewById(R.id.VPA_ProductsDescriptionTextView);
        ProcessTextView = findViewById(R.id.VPA_ProcessTextView);
        InspectionDateTextView = findViewById(R.id.VPA_InspectionDate);

        //endregion

        //region EditView
        InspectionTimeEditView = findViewById(R.id.VPA_InspectionTime);
        InspectionTimeEditView.setEnabled(false);
        BadReasonsEditView = findViewById(R.id.VPA_BadReasons);
        CauseAnalysisEditView = findViewById(R.id.VPA_CauseAnalysis);
        CorrectivePrecautionsEditView = findViewById(R.id.VPA_CorrectivePrecautions);
        ConfirmPasswordEditView = findViewById(R.id.VPA_ConfirmPassword);
        //endregion

        //region RadioGroup
        mRadioGroup = findViewById(R.id.mRadioGroup);
        mRadioBtnV = findViewById(R.id.mRadioBtnV);
        mRadioBtnX = findViewById(R.id.mRadioBtnX);
        //endregion

        //region Button
        btnQuit = findViewById(R.id.VPA_btnQuit);
        btnQuit.setOnClickListener(Click);
        btnDelete = findViewById(R.id.VPA_btnDelete);
        btnDelete.setOnClickListener(Click);
        btnSave = findViewById(R.id.VPA_btnSave);
        btnSave.setOnClickListener(Click);
        //endregion

        //region ImageView
        ImageTimeView = findViewById(R.id.VPA_TimeImageView);
        ImageTimeView.setOnClickListener(Click);
        //endregion

        //region Edit監聽
        InspectionTimeEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus){

            }
        });
        BadReasonsEditView.setOnFocusChangeListener((v, hasFocus)->{
            if(!hasFocus){
                BadReasons = BadReasonsEditView.getText().toString();
            }
        });
        CauseAnalysisEditView.setOnFocusChangeListener((v, hasFocus)->{
            if(!hasFocus){
                CauseAnalysis = CauseAnalysisEditView.getText().toString();
            }
        });
        CorrectivePrecautionsEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus){
                CorrectivePrecautions = CorrectivePrecautionsEditView.getText().toString();
            }
        });
        //endregion
        //region 單選監聽器
        mRadioGroup.setOnCheckedChangeListener(radGrpRegionOnCheckedChange);
        //endregion
    }
    private RadioGroup.OnCheckedChangeListener radGrpRegionOnCheckedChange = (group, checkedId) -> {
        switch (checkedId){
            case R.id.mRadioBtnV:
                SelfDetermination = "V";
                Log.e(TAG, "V");
                break;
            case R.id.mRadioBtnX:
                SelfDetermination = "X";
                Log.e(TAG, "X");
                break;
        }
    };

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.VPA_btnQuit:
                GoBack();
                break;
            case R.id.VPA_btnDelete:
                if(ConfirmPasswordEditView.getText().toString().equals(PassWord)) {
                    DeleteDate();
                }else{
                    Toast.makeText(getApplicationContext(),"請確認密碼是否輸入正確!",Toast.LENGTH_LONG).show();
                    ConfirmPasswordEditView.requestFocus();
                }
                break;
            case R.id.VPA_btnSave:
                clearFocus();
                if(ConfirmPasswordEditView.getText().toString().equals(PassWord)) {
                    if(Type.equals("新增")){
                        InspectionDate = InspectionDateTextView.getText().toString();
                        InspectionTime = InspectionTimeEditView.getText().toString();
                        if(SelfDetermination == null){
                            mRadioGroup.check(R.id.mRadioBtnV);
                            SelfDetermination = "V";
                        }
                        AddDate();
                    }else{
                        UpDate();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確!",Toast.LENGTH_LONG).show();
                    ConfirmPasswordEditView.requestFocus();
                }
                break;
            case R.id.VPA_TimeImageView:
                OpenPopTime(v);
                break;
        }
    };
    void clearFocus(){
        InspectionTimeEditView.clearFocus();
        BadReasonsEditView.clearFocus();
        CauseAnalysisEditView.clearFocus();
        CorrectivePrecautionsEditView.clearFocus();
    }

    void initData(){
        Intent intent = getIntent();
        Type = intent.getStringExtra("Type");
        if(intent.getStringExtra("InspectionDate") != null) {
            InspectionDate = GetStartDate(intent.getStringExtra("InspectionDate"));
            InspectionTime = intent.getStringExtra("InspectionTime");
            Log.e(TAG, InspectionDate + " " + InspectionTime);
        }

        OrderID = intent.getStringExtra("OrderID");
        SerialNumber = intent.getStringExtra("SerialNumber");
        InputOrderNoBarCode = intent.getStringExtra("OrderNoBarCode");

        InputManufactureOrderID = intent.getStringExtra("ManufactureOrderID");
        InputProductsDescription = intent.getStringExtra("ProductsDescription");
        InputMachineID = intent.getStringExtra("MachineID");
        InputProcessID = intent.getStringExtra("ProcessID");

        InputProcessDesc = intent.getStringExtra("ProcessDesc");
        InputWorkShiftype = intent.getStringExtra("WorkShiftType");
        InputStartDate =intent.getStringExtra("StartDate");
        InputStartTime = intent.getStringExtra("StartTime");
        InputEndDate = intent.getStringExtra("EndDate");
        InputEndTime = intent.getStringExtra("EndTime");
        InputProductsTypeID = intent.getStringExtra("ProductsTypeID");

        InputStandardRequestID = intent.getStringExtra("StandardRequestID");
        InputSeamTypeID = intent.getStringExtra("SeamTypeID");
        InputRawJobTypeID = intent.getStringExtra("RawJobTypeID");
        InputSaleTypeID = intent.getStringExtra("SaleTypeID");

        InputInspectionFormID = intent.getStringExtra("InspectionFormID");
        InputCompletedFormID = intent.getStringExtra("CompletedFormID");
        InputMaterialFormID = intent.getStringExtra("MaterialFormID");
        InputDiagramFormID = intent.getStringExtra("DiagramFormID");
        PassWord = intent.getStringExtra("PassWord");
        InputStandardQty = intent.getStringExtra("StandardQty");

        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeeID = intent.getStringExtra("EmployeeID");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");

        runOnUiThread(()->{
            OrderNoBarCodeTextView.setText(InputOrderNoBarCode.substring(0,8) +"-"+ InputOrderNoBarCode.substring(8,11) + "-" + InputOrderNoBarCode.substring(11));
            ManufactureOrderIDTextView.setText(InputManufactureOrderID);
            ProductsDescriptionTextView.setText(InputProductsDescription);
            InspectionDateTextView.setText(InputEndDate);
            ProcessTextView.setText(InputProcessID + " " + InputProcessDesc);
        });

    }

    void GoBack(){
        Intent intent = new Intent();
        intent.setClass(this, VerifyPassInspectionAddActivity.class);
        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",InputOrderNoBarCode);

        intent.putExtra("ManufactureOrderID",InputManufactureOrderID);
        intent.putExtra("ProductsDescription",InputProductsDescription);
        intent.putExtra("MachineID",InputMachineID);
        intent.putExtra("ProcessID",InputProcessID);

        intent.putExtra("ProcessDesc",InputProcessDesc);
        intent.putExtra("WorkShiftype",InputWorkShiftype);
        intent.putExtra("StartDate",InputStartDate);
        intent.putExtra("StartTime",InputStartTime);
        intent.putExtra("EndDate",InputEndDate);
        intent.putExtra("EndTime",InputEndTime);
        intent.putExtra("ProductsTypeID",InputProductsTypeID);

        intent.putExtra("StandardRequestID",InputStandardRequestID);
        intent.putExtra("SeamTypeID",InputSeamTypeID);
        intent.putExtra("RawJobTypeID",InputRawJobTypeID);
        intent.putExtra("SaleTypeID",InputSaleTypeID);

        intent.putExtra("InspectionFormID",InputInspectionFormID);
        intent.putExtra("CompletedFormID",InputCompletedFormID);
        intent.putExtra("MaterialFormID",InputMaterialFormID);
        intent.putExtra("DiagramFormID",InputDiagramFormID);
        intent.putExtra("PassWord",PassWord);
        intent.putExtra("StandardQty",InputStandardQty);

        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        startActivity(intent);
        finish();
    }

    void DeleteDate(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single ="DELETE tblOManufactureDailyDetailInspection FROM tblOManufactureDailyDetailInspection WHERE OrderID = " + OrderID +
                        " AND SerialNumber = " + SerialNumber +
                        " AND InspectionDate = '" + InspectionDate +
                        "' AND InspectionTime = '" + InspectionTime +"'";
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                ShowToast("刪除成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask,2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }
    }

    void SearchOManufactureDailyDetailInspection() {
        try {
            Log.e("編輯","有跑嗎?");
            Statement statement = connection.createStatement();
            String qryAddtblOManufactureDailyDetailInspection_Modify =
                    "SELECT tblOManufactureDailyDetailInspection.*" +
                            " FROM tblOManufactureDailyDetailInspection" +
                            " WHERE OrderID = " + OrderID +
                            " AND SerialNumber = " + SerialNumber +
                            " AND InspectionDate = '" + InspectionDate +
                            "' AND InspectionTime = '" + InspectionTime +"'";
            ResultSet SingleResultSet = statement.executeQuery(qryAddtblOManufactureDailyDetailInspection_Modify);
            if (SingleResultSet != null) {
                while (SingleResultSet.next()) {
                    Log.e(TAG, SingleResultSet.getString("InspectionTime"));
                    InspectionTimeEditView.setText(SingleResultSet.getString("InspectionTime"));
                    BadReasonsEditView.setText(SingleResultSet.getString("BadReasons"));
                    CauseAnalysisEditView.setText(SingleResultSet.getString("CauseAnalysis"));
                    CorrectivePrecautionsEditView.setText(SingleResultSet.getString("CorrectivePrecautions"));
                    BadReasons = SingleResultSet.getString("BadReasons");
                    CauseAnalysis = SingleResultSet.getString("CauseAnalysis");
                    CorrectivePrecautions = SingleResultSet.getString("CorrectivePrecautions");

                    Log.e(TAG,SingleResultSet.getString("SelfDetermination"));
                    if(SingleResultSet.getString("SelfDetermination").equals("1")){
                        mRadioGroup.check(R.id.mRadioBtnV);
                        SelfDetermination = "V";
                    }else{
                        mRadioGroup.check(R.id.mRadioBtnX);
                        SelfDetermination = "X";
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void AddDate(){
        try {
            if (connection == null) {
                Log.e(TAG, "connection NULL");
            } else {
                //Change below query according to your own database.
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e(TAG, "現在時間: " + now);

                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection_Modify =
                        "INSERT into tblOManufactureDailyDetailInspection(" +
                                "OrderID, SerialNumber, InspectionDate, InspectionTime," +
                                "BadReasons, CauseAnalysis, CorrectivePrecautions, SelfDetermination," +
                                "CreateDate, CreateUserNo, CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate)" +
                                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection_Modify, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1,OrderID);//OrderID
                comm.setString(2,SerialNumber);//SerialNumber
                comm.setString(3,InspectionDate);//InspectionDate
                comm.setString(4,InspectionTime);//InspectionTime

                comm.setString(5,BadReasons);//BadReasons
                comm.setString(6,CauseAnalysis);//CauseAnalysis
                comm.setString(7,CorrectivePrecautions);//CorrectivePrecautions
                comm.setString(8,SelfDetermination.equals("V")? "True":"False");//SelfDetermination

                comm.setString(9,now);//CreateDate
                comm.setString(10,EmployeeID);//CreateUserNo
                comm.setString(11,EmployeeName);//CreateUserName
                comm.setString(12,now);//ModifyDate
                comm.setString(13,EmployeeID);//ModifyUserNo
                comm.setString(14,EmployeeName);//ModifyUserName
                comm.setString(15,now);//SystemModifyDate


               /* Log.e("上傳資料:", "OrderID: " + OrderID + " SerialNumber: " + SerialNumber +" InspectionDate: "+ InspectionDate + " InspectionTime: "+ InspectionTime);
                Log.e("上傳資料:", "BadReasons: " + BadReasons + " CauseAnalysis: " + CauseAnalysis +" CorrectivePrecautions: "+ CorrectivePrecautions + " SelfDetermination: "+ (SelfDetermination.equals("V")? "True":"False"));
                Log.e("上傳資料:", "now: " + now + " EmployeeID: " + EmployeeID +" EmployeeName: "+ EmployeeName);*/
                comm.executeUpdate();

                ShowToast("新增成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask,2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void UpDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dt = new Date();
        now = sdf.format(dt);
        Log.e(TAG, "現在時間: " + now);
        try {
            if (connection == null) {
                Log.e(TAG, "connection NULL");
            } else {
                PreparedStatement comm;
                String qryAddtblOManufactureDailyDetailInspection_Modify =
                        "UPDATE tblOManufactureDailyDetailInspection" +
                                " SET BadReasons = ?, CauseAnalysis = ?, CorrectivePrecautions = ?, SelfDetermination = ?," +
                                " ModifyDate = ?, ModifyUserNo = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                                " WHERE OrderID = " + OrderID +
                                " AND SerialNumber = " + SerialNumber +
                                " AND InspectionDate = '" + InspectionDate +
                                "' AND InspectionTime = '" + InspectionTime + "'";
                comm = connection.prepareStatement(qryAddtblOManufactureDailyDetailInspection_Modify);
                comm.setString(1,BadReasons);
                comm.setString(2,CauseAnalysis);
                comm.setString(3,CorrectivePrecautions);
                comm.setString(4,SelfDetermination.equals("V")? "True":"False");

                comm.setString(5,now);
                comm.setString(6,EmployeeID);
                comm.setString(7,EmployeeName);
                comm.setString(8,now);

                Log.e("上傳資料:", "OrderID: " + OrderID + " SerialNumber: " + SerialNumber +" InspectionDate: "+ InspectionDate + " InspectionTime: "+ InspectionTime);
                Log.e("上傳資料:", "BadReasons: " + BadReasons + " CauseAnalysis: " + CauseAnalysis +" CorrectivePrecautions: "+ CorrectivePrecautions + " SelfDetermination: "+ (SelfDetermination.equals("V")? "True":"False"));
                Log.e("上傳資料:", "now: " + now + " EmployeeID: " + EmployeeID +" EmployeeName: "+ EmployeeName);

                comm.executeUpdate();

                ShowToast("修改成功!");
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        GoBack();
                    }
                };
                timer.schedule(timerTask,2000);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private String GetStartDate(String Date){
        try{
            java.util.Date date;
            @SuppressLint("SimpleDateFormat") DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            return Stat.replace("-","/");
        } catch (NullPointerException | ParseException e){
            e.printStackTrace();
        }
        return null;
    }

    //region Toast
    private void ShowToast(String Text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText(Text);
        toast.show();
    }
    //endregion

    @SuppressLint("ClickableViewAccessibility")
    private void OpenPopTime(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime,null,false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);


        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
                TimeFormat(hourOfDay,minute);
        });
        Check.setOnClickListener(v12 -> {
            TimeJudgment();
            popupWindow.dismiss();
        });
    }

    private void TimeJudgment(){
        String [] StartTime = InputStartTime.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY,Integer.parseInt(StartTime[0]));
        calendar1.set(Calendar.MINUTE,Integer.parseInt(StartTime[1]));
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time1 = df.format(calendar1.getTime());
        Log.e(TAG,time1);
        Log.e("時間",calendar1.toString());
        Log.e("時間",calendar.toString());

        if(calendar.compareTo(calendar1) == -1){
            Log.e("時差","增加一天");
            // 對 calendar 設定時間的方法
            // 設定傳入的時間格式
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar2 = Calendar.getInstance();
            // 指定一個日期
            try {
                Date date;
                date = dateFormat.parse(InputEndDate);
                calendar2.setTime(date);
                calendar2.add(Calendar.DATE,1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // 對 calendar 設定為 date 所定的日期
            String time2 = dateFormat.format(calendar2.getTime());
            Log.e("TAG",time2);
            InspectionDateTextView.setText(time2);
        }else{
            Log.e("時差","不變");
            InspectionDateTextView.setText(InputEndDate);
        }
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    private void TimeFormat(int hourOfDay, int minute){
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE,minute);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar.getTime());
        Log.e(TAG, time);
        InspectionTimeEditView.setText("");
        InspectionTimeEditView.setText( String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));

    }
}
