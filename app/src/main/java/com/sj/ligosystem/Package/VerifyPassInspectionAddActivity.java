package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_pass_inspection_details_Adapter;
import com.sj.ligosystem.Model.item_pass_inspection;
import com.sj.ligosystem.Passinspection.PassInspection_01Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1011Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1020Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1030Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1040Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1060Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1070Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1080Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1081Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1090Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1091Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1092Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1093Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1094Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1100Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1101Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1102Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1103Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1110Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1111Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1112Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1113Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1120Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1130Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1140Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1141Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1142Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1150Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1151Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1152Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1160Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1170Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1180Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1190Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1191Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1200Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1201Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1210Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1211Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1212Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1213Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1214Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1215Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1220Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1700Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1701Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1703Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1704Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1705Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1706Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1707Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1708Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1709Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1710Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1711Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1740Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1741Activity;
import com.sj.ligosystem.Passinspection.PassInspection_1750Activity;
import com.sj.ligosystem.R;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;


public class VerifyPassInspectionAddActivity extends AppCompatActivity {
    final String TAG = "VerifyPassInspection";
    Toolbar toolbar;
    TextView OrderNoBarCodeTextView,ManufactureOrderIDTextView,ProductsDescriptionTextView,ProcessTextView,
            ManufactureQtyTextView,StandardQtyTextView,SuggestedQtyTextView, EndDataTextView, StartDateTimeTextView;
    Button btnQuit,btnEdit,btnAdd;

    RecyclerView recyclerView;
    item_pass_inspection_details_Adapter adapter;

    int year,month,day,hour;
    String PutDate;
    String OrderID, SerialNumber,WorkingTimes;
    //取得
    String InputOrderNoBarCode, InputManufactureOrderID, InputProductsDescription, InputMachineID,
            InputProcessID, InputProcessDesc, InputWorkShiftype, InputStartDate, InputStartTime,
            InputEndDate, InputEndTime, InputStandardQty, InputProductsTypeID, InputStandardRequestID,
            InputSeamTypeID, InputRawJobTypeID, InputSaleTypeID, InputInspectionFormID, InputCompletedFormID,InputSizeID,InputSizeID2,InputThicknessID, InputThicknessID2, InputInoc,
            InputMaterialFormID, InputDiagramFormID,PassWord;

    String InspectionTime, BadReasons, SelfDetermination, InspectionDate;

    String EmployeeName,EmployeeID,MachineID,MachineName;

    List<item_pass_inspection> itemPassInspections = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_pass_inspection_add);
        initView();
        initData();
    }
    void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        OrderNoBarCodeTextView = findViewById(R.id.VPA_OrderNoBarCodeTextView);
        ManufactureOrderIDTextView = findViewById(R.id.VPA_ManufactureOrderIDTextView);
        ProductsDescriptionTextView = findViewById(R.id.VPA_ProductsDescriptionTextView);
        ProcessTextView = findViewById(R.id.VPA_ProcessTextView);
        ManufactureQtyTextView = findViewById(R.id.VPA_ManufactureQtyTextView);
        StandardQtyTextView = findViewById(R.id.VPA_StandardQtyTextView);
        SuggestedQtyTextView = findViewById(R.id.VPA_SuggestedQtyTextView);
        EndDataTextView = findViewById(R.id.VPA_EndDataTextView);
        StartDateTimeTextView = findViewById(R.id.VPA_StartDateTimeTextView);
        //endregion

        //region Button
        btnQuit = findViewById(R.id.VPA_btnQuit);
        btnQuit.setOnClickListener(Click);
        btnEdit = findViewById(R.id.VPA_btnEdit);
        btnEdit.setOnClickListener(Click);
        btnAdd = findViewById(R.id.VPA_btnAdd);
        btnAdd.setOnClickListener(Click);
        //endregion

        //region 列表
        recyclerView = findViewById(R.id.VPA_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        adapter = new item_pass_inspection_details_Adapter(this, itemPassInspections);
        adapter.setOnItemClickListener((view, position) -> {
            //ClickPosition = position;

            InspectionDate = itemPassInspections.get(position).getInspectionDate();
            InspectionTime = itemPassInspections.get(position).getInspectionTime();
            SelfDetermination = itemPassInspections.get(position).getSelfDetermination();

            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();
        });
        //endregion
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.VPA_btnQuit:
                GoBack();
                break;
            case R.id.VPA_btnEdit:
                GoEdit();
                break;
            case R.id.VPA_btnAdd:
                GoAdd();
                break;
        }
    };
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }
    void initData(){
        Intent intent = getIntent();
        OrderID = intent.getStringExtra("OrderID");
        SerialNumber = intent.getStringExtra("SerialNumber");
        InputOrderNoBarCode = intent.getStringExtra("OrderNoBarCode");

        InputManufactureOrderID = intent.getStringExtra("ManufactureOrderID");
        InputProductsDescription = intent.getStringExtra("ProductsDescription");
        InputMachineID = intent.getStringExtra("MachineID");
        InputProcessID = intent.getStringExtra("ProcessID");

        InputProcessDesc = intent.getStringExtra("ProcessDesc");
        InputWorkShiftype = intent.getStringExtra("WorkShiftType");
        InputStartDate =intent.getStringExtra("StartDate");
        InputStartTime = intent.getStringExtra("StartTime");
        InputEndDate = intent.getStringExtra("EndDate");
        InputEndTime = intent.getStringExtra("EndTime");
        InputProductsTypeID = intent.getStringExtra("ProductsTypeID");

        InputStandardRequestID = intent.getStringExtra("StandardRequestID");
        InputSeamTypeID = intent.getStringExtra("SeamTypeID");
        InputRawJobTypeID = intent.getStringExtra("RawJobTypeID");
        InputSaleTypeID = intent.getStringExtra("SaleTypeID");

        InputSizeID = intent.getStringExtra("SizeID");
        InputSizeID2 = intent.getStringExtra("SizeID2");
        InputThicknessID = intent.getStringExtra("ThicknessID");
        InputThicknessID2 = intent.getStringExtra("ThicknessID2");

        InputInspectionFormID = intent.getStringExtra("InspectionFormID");
        InputCompletedFormID = intent.getStringExtra("CompletedFormID");
        InputMaterialFormID = intent.getStringExtra("MaterialFormID");
        InputDiagramFormID = intent.getStringExtra("DiagramFormID");
        PassWord = intent.getStringExtra("PassWord");
        InputStandardQty = intent.getStringExtra("StandardQty");

        InputInoc = intent.getStringExtra("Inoc");

        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeeID = intent.getStringExtra("EmployeeID");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");

        Log.e("InputInspectionFormID", InputInspectionFormID);

        runOnUiThread(()->{
            OrderNoBarCodeTextView.setText(InputOrderNoBarCode.substring(0,8) +"-"+ InputOrderNoBarCode.substring(8,11) + "-" + InputOrderNoBarCode.substring(11));
            ManufactureOrderIDTextView.setText(InputManufactureOrderID);
            ProductsDescriptionTextView.setText(InputProductsDescription);
            ProcessTextView.setText(InputProcessID + " " + InputProcessDesc);

            EndDataTextView.setText(InputEndDate + " " + InputEndTime);
            StartDateTimeTextView.setText(InputStartDate + " " + InputStartTime);

            GetInspection();
        });
    }
    void GoBack(){
        Intent intent = new Intent();
        intent.setClass(this, VerifyPassInspectionActivity.class);
        intent.putExtra("OrderID",Integer.valueOf(OrderID));
        intent.putExtra("OrderNoBarCode",InputOrderNoBarCode);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeePassWord",PassWord);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        startActivity(intent);
        finish();
    }
    void GoAdd(){
        Intent intent = new Intent();
        switch (InputInspectionFormID) {
            case "1010":
                intent.setClass(this, PassInspection_01Activity.class);
                break;
            case "1011":
                intent.setClass(this, PassInspection_1011Activity.class);
                break;
            case "1020":
                intent.setClass(this, PassInspection_1020Activity.class);
                break;
            case "1030":
                intent.setClass(this, PassInspection_1030Activity.class);
                break;
            case "1040":
                intent.setClass(this, PassInspection_1040Activity.class);
                break;
            case "1060":
                intent.setClass(this, PassInspection_1060Activity.class);
                break;
            case "1070":
                intent.setClass(this, PassInspection_1070Activity.class);
                break;
            case "1080":
                intent.setClass(this, PassInspection_1080Activity.class);
                break;
            case "1081":
                intent.setClass(this, PassInspection_1081Activity.class);
                break;
            case "1090":
                intent.setClass(this, PassInspection_1090Activity.class);
                break;
            case "1091":
                intent.setClass(this, PassInspection_1091Activity.class);
                break;
            case "1092":
                intent.setClass(this, PassInspection_1092Activity.class);
                break;
            case "1093":
                intent.setClass(this, PassInspection_1093Activity.class);
                break;
            case "1094":
                intent.setClass(this, PassInspection_1094Activity.class);
                break;
            case "1100":
                intent.setClass(this, PassInspection_1100Activity.class);
                break;
            case "1101":
                intent.setClass(this, PassInspection_1101Activity.class);
                break;
            case "1102":
                intent.setClass(this, PassInspection_1102Activity.class);
                break;
            case "1103":
                intent.setClass(this, PassInspection_1103Activity.class);
                break;
            case "1110":
                intent.setClass(this, PassInspection_1110Activity.class);
                break;
            case "1111":
                intent.setClass(this, PassInspection_1111Activity.class);
                break;
            case "1112":
                intent.setClass(this, PassInspection_1112Activity.class);
                break;
            case "1113":
                intent.setClass(this, PassInspection_1113Activity.class);
                break;
            case "1120":
                intent.setClass(this, PassInspection_1120Activity.class);
                break;
            case "1130":
                intent.setClass(this, PassInspection_1130Activity.class);
                break;
            case "1140":
                intent.setClass(this, PassInspection_1140Activity.class);
                break;
            case "1141":
                intent.setClass(this, PassInspection_1141Activity.class);
                break;
            case "1142":
                intent.setClass(this, PassInspection_1142Activity.class);
                break;
            case "1150":
                intent.setClass(this, PassInspection_1150Activity.class);
                break;
            case "1151":
                intent.setClass(this, PassInspection_1151Activity.class);
                break;
            case "1152":
                intent.setClass(this, PassInspection_1152Activity.class);
                break;
            case "1160":
                intent.setClass(this, PassInspection_1160Activity.class);
                break;
            case "1170":
                intent.setClass(this, PassInspection_1170Activity.class);
                break;
            case "1180":
                intent.setClass(this, PassInspection_1180Activity.class);
                break;
            case "1190":
                intent.setClass(this, PassInspection_1190Activity.class);
                break;
            case "1191":
                intent.setClass(this, PassInspection_1191Activity.class);
                break;
            case "1200":
                intent.setClass(this, PassInspection_1200Activity.class);
                break;
            case "1201":
                intent.setClass(this, PassInspection_1201Activity.class);
                break;
            case "1210":
                intent.setClass(this, PassInspection_1210Activity.class);
                break;
            case "1211":
                intent.setClass(this, PassInspection_1211Activity.class);
                break;
            case "1212":
                intent.setClass(this, PassInspection_1212Activity.class);
                break;
            case "1213":
                intent.setClass(this, PassInspection_1213Activity.class);
                break;
            case "1214":
                intent.setClass(this, PassInspection_1214Activity.class);
                break;
            case "1215":
                intent.setClass(this, PassInspection_1215Activity.class);
                break;
            case "1220":
                intent.setClass(this, PassInspection_1220Activity.class);
                break;
            case "1700":
                intent.setClass(this, PassInspection_1700Activity.class);
                break;
            case "1701":
                intent.setClass(this, PassInspection_1701Activity.class);
                break;
            case "1703":
                intent.setClass(this, PassInspection_1703Activity.class);
                break;
            case "1704":
                intent.setClass(this, PassInspection_1704Activity.class);
                break;
            case "1705":
                intent.setClass(this, PassInspection_1705Activity.class);
                break;
            case "1706":
                intent.setClass(this, PassInspection_1706Activity.class);
                break;
            case "1707":
                intent.setClass(this, PassInspection_1707Activity.class);
                break;
            case "1708":
                intent.setClass(this, PassInspection_1708Activity.class);
                break;
            case "1709":
                intent.setClass(this, PassInspection_1709Activity.class);
                break;
            case "1710":
                intent.setClass(this, PassInspection_1710Activity.class);
                break;
            case "1711":
                intent.setClass(this, PassInspection_1711Activity.class);
                break;
            case "1740":
                intent.setClass(this, PassInspection_1740Activity.class);
                break;
            case "1741":
                intent.setClass(this, PassInspection_1741Activity.class);
                break;
            case "1750":
                intent.setClass(this, PassInspection_1750Activity.class);
                break;
            default:
                Toast.makeText(getApplicationContext(), "未有此道次檢驗", Toast.LENGTH_LONG).show();
                return;
        }

        intent.putExtra("Type","新增");

        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",InputOrderNoBarCode);

        intent.putExtra("ManufactureOrderID",InputManufactureOrderID);
        intent.putExtra("ProductsDescription",InputProductsDescription);
        intent.putExtra("MachineID",InputMachineID);
        intent.putExtra("ProcessID",InputProcessID);

        intent.putExtra("ProcessDesc",InputProcessDesc);
        intent.putExtra("WorkShiftype",InputWorkShiftype);
        intent.putExtra("StartDate",InputStartDate);
        intent.putExtra("StartTime",InputStartTime);
        intent.putExtra("EndDate",InputEndDate);
        intent.putExtra("EndTime",InputEndTime);
        intent.putExtra("ProductsTypeID",InputProductsTypeID);

        intent.putExtra("StandardRequestID",InputStandardRequestID);
        intent.putExtra("SeamTypeID",InputSeamTypeID);
        intent.putExtra("RawJobTypeID",InputRawJobTypeID);
        intent.putExtra("SaleTypeID",InputSaleTypeID);

        intent.putExtra("SizeID", InputSizeID);
        intent.putExtra("SizeID2", InputSizeID2);
        intent.putExtra("ThicknessID", InputThicknessID);
        intent.putExtra("ThicknessID2", InputThicknessID2);

        intent.putExtra("InspectionFormID",InputInspectionFormID);
        intent.putExtra("CompletedFormID",InputCompletedFormID);
        intent.putExtra("MaterialFormID",InputMaterialFormID);
        intent.putExtra("DiagramFormID",InputDiagramFormID);
        intent.putExtra("PassWord",PassWord);
        intent.putExtra("StandardQty",InputStandardQty);

        intent.putExtra("Inoc",InputInoc);

        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        startActivity(intent);
        finish();
    }
    void GoEdit(){
        Intent intent = new Intent();
        switch (InputInspectionFormID) {
            case "1010":
                intent.setClass(this, PassInspection_01Activity.class);
                break;
            case "1011":
                intent.setClass(this, PassInspection_1011Activity.class);
                break;
            case "1020":
                intent.setClass(this, PassInspection_1020Activity.class);
                break;
            case "1030":
                intent.setClass(this, PassInspection_1030Activity.class);
                break;
            case "1040":
                intent.setClass(this, PassInspection_1040Activity.class);
                break;
            case "1060":
                intent.setClass(this, PassInspection_1060Activity.class);
                break;
            case "1070":
                intent.setClass(this, PassInspection_1070Activity.class);
                break;
            case "1080":
                intent.setClass(this, PassInspection_1080Activity.class);
                break;
            case "1081":
                intent.setClass(this, PassInspection_1081Activity.class);
                break;
            case "1090":
                intent.setClass(this, PassInspection_1090Activity.class);
                break;
            case "1091":
                intent.setClass(this, PassInspection_1091Activity.class);
                break;
            case "1092":
                intent.setClass(this, PassInspection_1092Activity.class);
                break;
            case "1093":
                intent.setClass(this, PassInspection_1093Activity.class);
                break;
            case "1094":
                intent.setClass(this, PassInspection_1094Activity.class);
                break;
            case "1100":
                intent.setClass(this, PassInspection_1100Activity.class);
                break;
            case "1101":
                intent.setClass(this, PassInspection_1101Activity.class);
                break;
            case "1102":
                intent.setClass(this, PassInspection_1102Activity.class);
                break;
            case "1103":
                intent.setClass(this, PassInspection_1103Activity.class);
                break;
            case "1110":
                intent.setClass(this, PassInspection_1110Activity.class);
                break;
            case "1111":
                intent.setClass(this, PassInspection_1111Activity.class);
                break;
            case "1112":
                intent.setClass(this, PassInspection_1112Activity.class);
                break;
            case "1113":
                intent.setClass(this, PassInspection_1113Activity.class);
                break;
            case "1120":
                intent.setClass(this, PassInspection_1120Activity.class);
                break;
            case "1130":
                intent.setClass(this, PassInspection_1130Activity.class);
                break;
            case "1140":
                intent.setClass(this, PassInspection_1140Activity.class);
                break;
            case "1141":
                intent.setClass(this, PassInspection_1141Activity.class);
                break;
            case "1142":
                intent.setClass(this, PassInspection_1142Activity.class);
                break;
            case "1150":
                intent.setClass(this, PassInspection_1150Activity.class);
                break;
            case "1151":
                intent.setClass(this, PassInspection_1151Activity.class);
                break;
            case "1152":
                intent.setClass(this, PassInspection_1152Activity.class);
                break;
            case "1160":
                intent.setClass(this, PassInspection_1160Activity.class);
                break;
            case "1170":
                intent.setClass(this, PassInspection_1170Activity.class);
                break;
            case "1180":
                intent.setClass(this, PassInspection_1180Activity.class);
                break;
            case "1190":
                intent.setClass(this, PassInspection_1190Activity.class);
                break;
            case "1191":
                intent.setClass(this, PassInspection_1191Activity.class);
                break;
            case "1200":
                intent.setClass(this, PassInspection_1200Activity.class);
                break;
            case "1201":
                intent.setClass(this, PassInspection_1201Activity.class);
                break;
            case "1210":
                intent.setClass(this, PassInspection_1210Activity.class);
                break;
            case "1211":
                intent.setClass(this, PassInspection_1211Activity.class);
                break;
            case "1212":
                intent.setClass(this, PassInspection_1212Activity.class);
                break;
            case "1213":
                intent.setClass(this, PassInspection_1213Activity.class);
                break;
            case "1214":
                intent.setClass(this, PassInspection_1214Activity.class);
                break;
            case "1215":
                intent.setClass(this, PassInspection_1215Activity.class);
                break;
            case "1220":
                intent.setClass(this, PassInspection_1220Activity.class);
                break;
            case "1700":
                intent.setClass(this, PassInspection_1700Activity.class);
                break;
            case "1701":
                intent.setClass(this, PassInspection_1701Activity.class);
                break;
            case "1703":
                intent.setClass(this, PassInspection_1703Activity.class);
                break;
            case "1704":
                intent.setClass(this, PassInspection_1704Activity.class);
                break;
            case "1705":
                intent.setClass(this, PassInspection_1705Activity.class);
                break;
            case "1706":
                intent.setClass(this, PassInspection_1706Activity.class);
                break;
            case "1707":
                intent.setClass(this, PassInspection_1707Activity.class);
                break;
            case "1708":
                intent.setClass(this, PassInspection_1708Activity.class);
                break;
            case "1709":
                intent.setClass(this, PassInspection_1709Activity.class);
                break;
            case "1710":
                intent.setClass(this, PassInspection_1710Activity.class);
                break;
            case "1711":
                intent.setClass(this, PassInspection_1711Activity.class);
                break;
            case "1740":
                intent.setClass(this, PassInspection_1740Activity.class);
                break;
            case "1741":
                intent.setClass(this, PassInspection_1741Activity.class);
                break;
            case "1750":
                intent.setClass(this, PassInspection_1750Activity.class);
                break;
            default:
                Toast.makeText(getApplicationContext(), "未有此道次檢驗", Toast.LENGTH_LONG).show();
                return;
        }

        if(itemPassInspections != null && itemPassInspections.size() != 0){
            if(InspectionDate == null || InspectionDate.isEmpty()){
                InspectionDate = itemPassInspections.get(0).getInspectionDate();
            }
            if(InspectionTime == null || InspectionTime.isEmpty()){
                InspectionTime = itemPassInspections.get(0).getInspectionTime();
            }
        }

        intent.putExtra("Type","編輯");
        intent.putExtra("InspectionDate", InspectionDate);
        intent.putExtra("InspectionTime", InspectionTime);

        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",InputOrderNoBarCode);

        intent.putExtra("ManufactureOrderID",InputManufactureOrderID);
        intent.putExtra("ProductsDescription",InputProductsDescription);
        intent.putExtra("MachineID",InputMachineID);
        intent.putExtra("ProcessID",InputProcessID);

        intent.putExtra("ProcessDesc",InputProcessDesc);
        intent.putExtra("WorkShiftype",InputWorkShiftype);
        intent.putExtra("StartDate",InputStartDate);
        intent.putExtra("StartTime",InputStartTime);
        intent.putExtra("EndDate",InputEndDate);
        intent.putExtra("EndTime",InputEndTime);
        intent.putExtra("ProductsTypeID",InputProductsTypeID);

        intent.putExtra("StandardRequestID",InputStandardRequestID);
        intent.putExtra("SeamTypeID",InputSeamTypeID);
        intent.putExtra("RawJobTypeID",InputRawJobTypeID);
        intent.putExtra("SaleTypeID",InputSaleTypeID);

        intent.putExtra("SizeID", InputSizeID);
        intent.putExtra("SizeID2", InputSizeID2);
        intent.putExtra("ThicknessID", InputThicknessID);
        intent.putExtra("ThicknessID2", InputThicknessID2);

        intent.putExtra("InspectionFormID",InputInspectionFormID);
        intent.putExtra("CompletedFormID",InputCompletedFormID);
        intent.putExtra("MaterialFormID",InputMaterialFormID);
        intent.putExtra("DiagramFormID",InputDiagramFormID);
        intent.putExtra("PassWord",PassWord);
        intent.putExtra("StandardQty",InputStandardQty);

        intent.putExtra("Inoc",InputInoc);

        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        startActivity(intent);
        finish();
    }

    void GetInspection(){
        try {
            Statement statement = connection.createStatement();
            String qryGettblOManufactureDailyDetail_Unfinished = null;

            switch (InputInspectionFormID) {
                case "1010":
                case "1011":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection01" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1020":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection02" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1030":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection03" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1040":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection04" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1060":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection06" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1070":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection07" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1080":
                case "1081":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection08" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1090":
                case "1091":
                case "1094":
                case "1093":
                case "1092":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection09" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1100":
                case "1101":
                case "1102":
                case "1103":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection10" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1110":
                case "1111":
                case "1112":
                case "1113":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection11" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1120":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection12" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1130":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection13" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1140":
                case "1141":
                case "1142":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection14" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1150":
                case "1151":
                case "1152":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection15" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1160":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection16" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1170":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection17" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1180":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection18" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1190":
                case "1191":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection19" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1200":
                case "1201":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection20" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1210":
                case "1211":
                case "1212":
                case "1213":
                case "1214":
                case "1215":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection21" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1220":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection22" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1700":
                case "1701":
                case "1703":
                case "1704":
                case "1705":
                case "1706":
                case "1707":
                case "1708":
                case "1709":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection70" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1710":
                case "1711":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection71" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1740":
                case "1741":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection74" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                case "1750":
                    qryGettblOManufactureDailyDetail_Unfinished =
                            "SELECT * FROM tblOManufactureDailyDetailInspection75" +
                                    " WHERE OrderID= " + OrderID + " AND SerialNumber = " + SerialNumber;
                    break;
                default:
                    Toast.makeText(getApplicationContext(), "未有此道次檢驗", Toast.LENGTH_LONG).show();
                    return;
            }

            ResultSet SingleResultSet = statement.executeQuery(qryGettblOManufactureDailyDetail_Unfinished);
            if (SingleResultSet != null) {
                while (SingleResultSet.next()) {

                    String InspectionDate = SingleResultSet.getString("InspectionDate");
                    String InspectionTime = SingleResultSet.getString("InspectionTime");
                    String ModifyUserName = SingleResultSet.getString("ModifyUserName");

                    Log.e(TAG,GetStartDate(InspectionDate) + "  " + InspectionTime);

                    itemPassInspections.add(new item_pass_inspection(InspectionDate,InspectionTime,ModifyUserName));
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    private String GetStartDate(String Date){
        try{
            java.util.Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            String newStat = Stat.replace("-","/");
            return newStat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
