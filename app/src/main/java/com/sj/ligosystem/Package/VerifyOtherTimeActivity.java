package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.sj.ligosystem.Control.PagerAdapter;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.R;

import java.util.ArrayList;
import java.util.List;

public class VerifyOtherTimeActivity extends AppCompatActivity {
    private final String TAG = VerifyOtherTimeActivity.class.getSimpleName();
    private List<Fragment> fragments = new ArrayList<>();
    private List<String> titles = new ArrayList<>();
    private TabLayout mTableLayout;
    private ViewPager mViewPager;


    Toolbar toolbar;
    TextView EmployeeTextView, MachineIDTextView, OrderNoBarCodeTextView;
    Button Leave_Button;

     String OrderNoBarCode, EmployeeID, EmployeeName, EmployeePassWord, MachineID, MachineName,OrderID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_other_time);
        initView();
        initData();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    void initView(){

        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        EmployeeTextView = findViewById(R.id.VO_EmployeeTextView);
        MachineIDTextView = findViewById(R.id.VO_MachineIDTextView);
        OrderNoBarCodeTextView = findViewById(R.id.VO_OrderNoBarCodeTextView);
        //endregion

        //region Button
        Leave_Button = findViewById(R.id.VO_LeaveButton);
        Leave_Button.setOnClickListener(Click);
        //endregion

        mTableLayout = findViewById(R.id.vfp_tabLayout);
        mViewPager = findViewById(R.id.vfp_view);
        mViewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), VerifyOtherTimeActivity.this, fragments, titles));
        mTableLayout.setupWithViewPager(mViewPager);
    }
    void initData(){
        Intent intent = getIntent();
        OrderNoBarCode = intent.getStringExtra("OrderNoBarCode");
        EmployeeID = intent.getStringExtra("EmployeeID");
        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeePassWord = intent.getStringExtra("EmployeePassWord");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");

        Util.EmployeeID = EmployeeID;
        Util.EmployeeName = EmployeeName;
        Util.EmployeePassWord = EmployeePassWord;
        Util.OrderNoBarCode = OrderNoBarCode;
        Util.MachineID = MachineID;
        Util.MachineName = MachineName;

        Log.e(TAG,"OrderID"+intent.getIntExtra("OrderID",0));

        OrderID = String.valueOf(intent.getIntExtra("OrderID",0));


        OrderNoBarCodeTextView.setText(OrderNoBarCode);
        EmployeeTextView.setText(EmployeeID +" "+EmployeeName);
        MachineIDTextView.setText(MachineID +" "+MachineName);

        fragments.add(OverNudoneOtherTimeActivity.newInstance("未完成",OrderID,OrderNoBarCode,EmployeePassWord));
        fragments.add(OverCompleteOtherTimeActivity.newInstance("已完成",OrderID,OrderNoBarCode,EmployeePassWord));
        titles.add("未完成");
        titles.add("已完成");
        mViewPager.getAdapter().notifyDataSetChanged();
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.VP_LeaveButton:
                finish();
                break;
        }
    };
}
