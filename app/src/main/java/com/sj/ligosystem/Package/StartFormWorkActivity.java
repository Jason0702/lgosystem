package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;

import com.sj.ligosystem.Control.SpacesItemDecoration;
import com.sj.ligosystem.Control.item_start_frameAdapter;
import com.sj.ligosystem.Model.FrameFormWork;
import com.sj.ligosystem.R;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;

public class StartFormWorkActivity extends AppCompatActivity {
    final String TAG = "StartFormWork_Activity";
    //region UI物件
    Toolbar toolbar;
    RecyclerView recyclerView;
    Button Cancel_Button;
    //endregion
    List<FrameFormWork> frameFormWorkList = new ArrayList<>();
    item_start_frameAdapter adapter;

    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_form_work);
        initView();
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
    private void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion
        //region 列表
        recyclerView = findViewById(R.id.SF_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));
        adapter = new item_start_frameAdapter(this,frameFormWorkList);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(((view, position) -> {
            GoSingNumberSelect(position);
        }));
        //endregion
        //region 按鈕
        Cancel_Button = findViewById(R.id.SF_Cancel_Button);
        Cancel_Button.setOnClickListener(v -> finish());
        //endregion
    }
    private void initData(){
        Frameformwork();
    }
    //架模別
    void Frameformwork(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                String Frameformwork_query="SELECT tblBProcess.ProcessID, tblBProcess.ProcessDesc FROM tblBProcess WHERE (((tblBProcess.WorkHoursStatisticsTypeID)= 'B' Or (tblBProcess.WorkHoursStatisticsTypeID)= 'C')) ORDER BY tblBProcess.ProcessID";
                Statement statement = connection.createStatement();
                ResultSet FrameformworkResultSet = statement.executeQuery(Frameformwork_query);

                if(FrameformworkResultSet != null){
                    while (FrameformworkResultSet.next()){
                        try {
                            Log.i(TAG,"ID: "+FrameformworkResultSet.getString("ProcessID") +" "+ "Name: " + FrameformworkResultSet.getString("ProcessDesc"));
                            frameFormWorkList.add(new FrameFormWork(FrameformworkResultSet.getString("ProcessID").trim(),
                                    FrameformworkResultSet.getString("ProcessDesc").trim()));
                            adapter.notifyDataSetChanged();
                            recyclerView.setAdapter(adapter);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }

        }catch (Exception e){e.printStackTrace();}
    }

    void GoSingNumberSelect(int position){
        intent.setClass(StartFormWorkActivity.this, StartFormWorkSingNumberActivity.class);
        intent.putExtra("Type","1");
        intent.putExtra("ProcessID",frameFormWorkList.get(position).getProcessID());
        intent.putExtra("ProcessDesc",frameFormWorkList.get(position).getProcessDesc());
        intent.putExtra("SelectWorkingHourID",frameFormWorkList.get(position).getProcessID());
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
