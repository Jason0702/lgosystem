package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;
/**
 * 架模完工回報(明細輸入)
 **/
public class OverFrameAddActivity extends AppCompatActivity {
    final String TAG = "OverFrameAddActivity";
    Toolbar toolbar;
    TextView SingleNumberView, MachineView, SerialNumberView;
    TextView EmployeeView, PassView,
            OrderNumberView, FrameProductView, FrameSizeView, FrameThicknessView,
            DemoldingView, DemoldingProductView, DemoldingSizeView,DemoldingThicknessView;
    TextView StartDateTimeView, EndDataView, EndTimeView, WorkProjectsView;
    EditText WorkingTimesEditView, ConfirmPasswordEditView;
    Button LeaveBtn,DeleteBtn,VerifyBtn, WorkProjectsBtn;
    ImageView TimeImageView;

    Timer timer;

    String OrderNoBarCode, EmployeeID, EmployeeName, MachineID,  MachineDesc;
    String OrderID, SerialNumber, PassID, PassDesc, ManufactureOrderID, ProductsTypeID,
            SizeID, ThicknessID, BManufactureOrderID, BProductsTypeID, BSizeID, BThicknessID, InputStartDate,
            InputStartTime, InputEndDate, InputEndTime, PassWord, Rate, WorkingTimes, Type, WorkingDescID;

    String WorkingHourID, WorkShiftType, StandardID, SizeQty, BSizeQty, WorkingTimesB, Amount, StandardIDItemNo, BStandardIDItemNo, AmendDate, WorkHoursStatisticsTypeID;

    String PutTime,PutDate,now;
    int year,month,day,hour,min;

    boolean isWorkingTime;

    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_over_frame_add);
        initView();
        initData();
        initControl();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
        day  = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.e(TAG,"HR:"+hour);
        //endregion
        UnfinishedTime();


        ShowWorkingTime();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    void initView(){
        //region Tootle
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion
        //region TextView
        SingleNumberView = findViewById(R.id.OFA_SingleNumberView);
        MachineView = findViewById(R.id.OFA_MachineView);
        SerialNumberView = findViewById(R.id.OFA_SerialNumberView);

        EmployeeView = findViewById(R.id.OFA_EmployeeView);
        PassView = findViewById(R.id.OFA_PassView);
        OrderNumberView = findViewById(R.id.OFA_OrderNumberView);
        FrameProductView = findViewById(R.id.OFA_FrameProductView);
        FrameSizeView = findViewById(R.id.OFA_FrameSizeView);
        FrameThicknessView = findViewById(R.id.OFA_FrameThicknessView);
        DemoldingView = findViewById(R.id.OFA_DemoldingView);
        DemoldingProductView = findViewById(R.id.OFA_DemoldingProductView);
        DemoldingSizeView = findViewById(R.id.OFA_DemoldingSizeView);
        DemoldingThicknessView = findViewById(R.id.OFA_DemoldingThicknessView);

        StartDateTimeView = findViewById(R.id.OFA_StartDateTimeView);
        EndDataView = findViewById(R.id.OFA_EndDataView);
        EndTimeView = findViewById(R.id.OFA_EndTimeEditView);
        WorkProjectsView = findViewById(R.id.OFA_WorkProjectsView);
        //endregion

        //region EditView
        WorkingTimesEditView = findViewById(R.id.OFA_WorkingTimesEditView);
        ConfirmPasswordEditView = findViewById(R.id.OFA_ConfirmPasswordEditView);
        //endregion

        //region Button
        LeaveBtn = findViewById(R.id.OFA_LeaveButton);
        DeleteBtn = findViewById(R.id.OFA_DeleteButton);
        VerifyBtn = findViewById(R.id.OFA_VerifyButton);
        WorkProjectsBtn = findViewById(R.id.OFA_WorkProjectsBtn);
        //endregion

        //region
        TimeImageView = findViewById(R.id.OFA_TimeImageView);
        //endregion
    }
    @SuppressLint("SetTextI18n")
    void initData(){
        Intent intent = getIntent();
        Type = intent.getStringExtra("Type");
        OrderID = intent.getStringExtra("OrderID");
        SerialNumber = intent.getStringExtra("SerialNumber");
        OrderNoBarCode = intent.getStringExtra("OrderNoBarCode");
        MachineID = intent.getStringExtra("MachineID");
        MachineDesc = intent.getStringExtra("MachineDesc");

        EmployeeID = intent.getStringExtra("EmployeeID");
        EmployeeName = intent.getStringExtra("EmployeeName");
        PassID = intent.getStringExtra("PassID");
        PassDesc = intent.getStringExtra("PassDesc");

        ManufactureOrderID = intent.getStringExtra("ManufactureOrderID");
        ProductsTypeID = intent.getStringExtra("ProductsTypeID");
        SizeID = intent.getStringExtra("SizeID");
        ThicknessID = intent.getStringExtra("ThicknessID");

        BManufactureOrderID = intent.getStringExtra("BManufactureOrderID");
        BProductsTypeID = intent.getStringExtra("BProductsTypeID");
        BSizeID = intent.getStringExtra("BSizeID");
        BThicknessID = intent.getStringExtra("BThicknessID");

        InputStartDate = GetStartDate(intent.getStringExtra("StartDate"));
        InputStartTime = intent.getStringExtra("StartTime");
        InputEndDate = intent.getStringExtra("EndDate");
        InputEndTime = intent.getStringExtra("EndTime");

        WorkingTimes = intent.getStringExtra("WorkingTimes");
        if (intent.getStringExtra("Rate") == null){
            Rate = "0.0";
        }else{
            Rate = intent.getStringExtra("Rate");
        }
        Log.e(TAG, Rate);

        WorkingDescID = intent.getStringExtra("WorkingDescID");

        PassWord = intent.getStringExtra("PassWord");

        WorkingHourID = intent.getStringExtra("WorkingHourID");
        WorkShiftType = intent.getStringExtra("WorkShiftType");
        StandardID = intent.getStringExtra("StandardID");
        SizeQty = intent.getStringExtra("SizeQty");
        BSizeQty = intent.getStringExtra("BSizeQty");

        WorkingTimesB = intent.getStringExtra("WorkingTimesB");
        Amount = intent.getStringExtra("Amount");
        StandardIDItemNo = intent.getStringExtra("StandardIDItemNo");
        BStandardIDItemNo = intent.getStringExtra("BStandardIDItemNo");
        AmendDate = intent.getStringExtra("AmendDate");

        WorkHoursStatisticsTypeID = intent.getStringExtra("WorkHoursStatisticsTypeID");

        SerialNumberView.setText(SerialNumber);
        SingleNumberView.setText(OrderNoBarCode);
        MachineView.setText(MachineID + " "+ MachineDesc);

        EmployeeView.setText(EmployeeID +" "+EmployeeName);
        PassView.setText(PassID +" "+ PassDesc);

        OrderNumberView.setText(ManufactureOrderID);
        runOnUiThread(()->{

            Log.e("ProductsTypeID",ProductsTypeID);
            Log.e("SizeID",SizeID);
            Log.e("ThicknessID",ThicknessID);
            Log.e("BProductsTypeID",BProductsTypeID);
            Log.e("BSizeID",BSizeID);
            Log.e("BThicknessID",BThicknessID);

            for(String Products : Util.AllProductsType) {
                String [] Cut = Products.split("\\(");
                if(Cut[0].trim().equals(ProductsTypeID)) {
                    FrameProductView.setText(Products);
                    break;
                }
            }
            for(String Size: Util.AllSize){
                String [] Cut = Size.split("\\(");
                if(Cut[0].trim().equals(SizeID)){
                    FrameSizeView.setText(Size);
                    break;
                }
            }

            for(String Thickness: Util.AllThickness) {
                String [] Cut = Thickness.split("\\(");
                if(Cut[0].trim().equals(ThicknessID)){
                    FrameThicknessView.setText(Thickness);
                    break;
                }
            }

            DemoldingView.setText(BManufactureOrderID);
            for(String Products : Util.AllProductsType) {
                String[] Cut = Products.split("\\(");
                if (Cut[0].trim().equals(BProductsTypeID)) {
                    DemoldingProductView.setText(Products);
                    break;
                }
            }

            for(String Size: Util.AllSize) {
                String[] Cut = Size.split("\\(");
                if (Cut[0].trim().equals(BSizeID)) {
                    DemoldingSizeView.setText(Size);
                    break;
                }
            }

            for(String Thickness: Util.AllThickness) {
                String[] Cut = Thickness.split("\\(");
                if (Cut[0].trim().equals(ThicknessID)) {
                    DemoldingThicknessView.setText(Thickness);
                    break;
                }
            }
        });


        WorkProjectsView.setText("完工百分比: " + Float.valueOf(Rate) + " %");

        WorkingTimesEditView.setText(WorkingTimes);

        StartDateTimeView.setText(InputStartDate + " " +InputStartTime);

        if(Type.equals("未完工")) {
            Log.e(TAG,Type);
        }else if(Type.equals("已完工")){
            Log.e(TAG,Type);
            //EndDataView.setText(GetStartDate(InputEndDate));
            EndTimeView.setText(InputEndTime);
            ShowWorkingTime();
        }

    }
    void initControl(){

        //region Button
        LeaveBtn.setOnClickListener(Click);
        DeleteBtn.setOnClickListener(Click);
        VerifyBtn.setOnClickListener(Click);
        WorkProjectsBtn.setOnClickListener(Click);
        //endregion

        //region ImageView
        TimeImageView.setOnClickListener(Click);
        //endregion

        WorkingTimesEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus && !WorkingTimesEditView.getText().toString().isEmpty()){
                PushBackWorkingTime(StartDateTimeView.getText().toString(), WorkingTimesEditView.getText().toString());
            }else if(WorkingTimesEditView.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(),"請輸入工作分鐘", Toast.LENGTH_LONG).show();
                WorkingTimesEditView.setText("0");
                WorkingTimesEditView.requestFocus();
            }
        });
    }
    private void PushBackWorkingTime(String StartTime, String InputTime){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date d1 = dateFormat.parse(StartTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d1);
            calendar.add(Calendar.MINUTE, Integer.parseInt(InputTime));
            int Year = calendar.get(Calendar.YEAR);
            int Month = calendar.get(Calendar.MONTH) + 1;
            int Day = calendar.get(Calendar.DAY_OF_MONTH);

            EndDataView.setText(String.format("%04d",Year)+ "/" +String.format("%02d",Month)+ "/" +String.format("%02d",Day));
            TimeFormat(calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    View.OnClickListener Click = v -> {
        switch(v.getId()){
            case R.id.OFA_LeaveButton:
                GoBack();
                break;
            case R.id.OFA_DeleteButton:
                if(ConfirmPasswordEditView.getText().toString().equals(PassWord) && !ConfirmPasswordEditView.getText().toString().isEmpty()) {
                    DeleteFunc();
                }else if(ConfirmPasswordEditView.getText().toString().isEmpty()){
                    ShowErrorToast("密碼不可為空!");
                    ConfirmPasswordEditView.requestFocus();
                }else if(!ConfirmPasswordEditView.getText().toString().equals(PassWord)){
                    ShowErrorToast("密碼錯誤，請檢查密碼是否正確!");
                    ConfirmPasswordEditView.requestFocus();
                }
                break;
            case R.id.OFA_VerifyButton:
                if(ConfirmPasswordEditView.getText().toString().equals(PassWord) && !ConfirmPasswordEditView.getText().toString().isEmpty()) {
                    SaveFunc();
                }else if(ConfirmPasswordEditView.getText().toString().isEmpty()){
                    ShowErrorToast("密碼不可為空!");
                    ConfirmPasswordEditView.requestFocus();
                }else if(!ConfirmPasswordEditView.getText().toString().equals(PassWord)){
                    ShowErrorToast("密碼錯誤，請檢查密碼是否正確!");
                    ConfirmPasswordEditView.requestFocus();
                }
                break;
            case R.id.OFA_WorkProjectsBtn:
                GoWorkProjects();
                break;
            case R.id.OFA_TimeImageView:
                OpenPopTime(v);
                break;
        }
    };
    void ShowErrorToast(String Text){
        Toast.makeText(this,Text,Toast.LENGTH_LONG).show();
    }

    void GoBack(){
        Intent intent = new Intent();
        intent.setClass(this, VerifyFrameActivity.class);
        intent.putExtra("OrderID",Integer.valueOf(OrderID));
        intent.putExtra("OrderNoBarCode",OrderNoBarCode);
        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("EmployeePassWord",PassWord);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName", MachineDesc);

        startActivity(intent);
        this.finish();
    }

    void GoWorkProjects(){
        Intent intent = new Intent();
        intent.setClass(this, WorkProjectsActivity.class);
        intent.putExtra("Type",Type);
        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",OrderNoBarCode);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineDesc",MachineDesc);

        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("PassID",PassID);
        intent.putExtra("PassDesc",PassDesc);

        intent.putExtra("ManufactureOrderID", ManufactureOrderID);
        intent.putExtra("ProductsTypeID", ProductsTypeID);
        intent.putExtra("SizeID", SizeID);
        intent.putExtra("ThicknessID", ThicknessID);

        intent.putExtra("BManufactureOrderID", BManufactureOrderID);
        intent.putExtra("BProductsTypeID", BProductsTypeID);
        intent.putExtra("BSizeID", BSizeID);
        intent.putExtra("BThicknessID", BThicknessID);

        intent.putExtra("StartDate", InputStartDate);
        intent.putExtra("StartTime", InputStartTime);
        intent.putExtra("EndDate",InputEndDate);
        intent.putExtra("EndTime",InputEndTime);

        intent.putExtra("WorkingTimes",WorkingTimes);

        intent.putExtra("Rate", Rate);

        intent.putExtra("WorkingDescID",WorkingDescID);

        intent.putExtra("PassWord", PassWord);

        intent.putExtra("WorkingHourID",WorkingHourID);
        intent.putExtra("WorkShiftType",WorkShiftType);
        intent.putExtra("StandardID",StandardID);
        intent.putExtra("SizeQty",SizeQty);
        intent.putExtra("BSizeQty",BSizeQty);

        intent.putExtra("WorkingTimesB", WorkingTimesB);
        intent.putExtra("Amount",Amount);
        intent.putExtra("StandardIDItemNo", StandardIDItemNo);
        intent.putExtra("BStandardIDItemNo", BStandardIDItemNo);
        intent.putExtra("AmendDate", AmendDate);

        intent.putExtra("WorkHoursStatisticsTypeID",WorkHoursStatisticsTypeID);


        startActivity(intent);
        this.finish();
    }

    private String GetStartDate(String Date){
        try{
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            String newStat = Stat.replace("-","/");
            return newStat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private void WorkingTime(String StartTime, String EndTime){
        //region 計算時差
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try{
            Date d1 = dateFormat.parse(StartTime);
            Date d2 = dateFormat.parse(EndTime);
            long diff = d2.getTime() - d1.getTime();
            Log.e("VPA","LongTime:" + diff);
            long min = diff/ (1000 * 60);
            Log.e(TAG,"分鐘:"+min);
            int outTime = (int)min;
            WorkingTimes = String.valueOf(outTime);
            WorkingTimesEditView.setText(WorkingTimes);
            isWorkingTime = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        //endregion
    }

    void UnfinishedTime(){
        //region 取得系統時間
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);

        Log.e(TAG, hour + "  " + min);
        TimeFormat(hour,min);

        TimeJudgment();

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar1.getTime());
        Log.e(TAG, time);
        PutTime = time;
        Log.e("放入時間", PutTime);
        //endregion
    }
    void ShowWorkingTime(){
        String EndDate = EndDataView.getText().toString() + " " +EndTimeView.getText().toString();
        WorkingTime(StartDateTimeView.getText().toString(),EndDate);
    }
    private void OpenPopTime(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime,null,false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);


        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
            TimeFormat(hourOfDay,minute);
        });
        Check.setOnClickListener(v12 -> {
            TimeJudgment();
            popupWindow.dismiss();
        });
    }


    private void DeleteFunc(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single = "DELETE tblOManufactureDailyExchangeMold FROM tblOManufactureDailyExchangeMold WHERE tblOManufactureDailyExchangeMold.OrderID="+OrderID+" AND tblOManufactureDailyExchangeMold.SerialNumber="+SerialNumber;
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                DeleteShowToast();
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Log.e(TAG,"run方法所在的線程: "+Thread.currentThread().getName());
                        GoBack();
                    }
                };
                timer.schedule(timerTask,2000);
                //endregion
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }
    }
    //region DeleteToast
    private void DeleteShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("刪除成功!");
        toast.show();
    }
    //endregion
    private void SaveFunc(){
        Log.e("上傳時間",  EndDataView.getText().toString()+" "+PutTime);
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e(TAG, "現在時間: " + now);

                PreparedStatement comm;

                String Single;

                //Add
                Single = "UPDATE tblOManufactureDailyExchangeMold SET" +
                        " OrderID = ?, SerialNumber = ?, EmployeeID = ?, WorkingHourID = ?, ManufactureOrderID = ?," +
                        " MachineID = ?, ProcessID = ?, WorkShiftType = ?," +
                        " EndDate = ?, EndTime = ?, WorkingTimes = ?, ProductsTypeID = ?, SizeID = ?," +
                        " ThicknessID = ?, BProductsTypeID = ?, BSizeID = ?, BThicknessID = ?, BManufactureOrderID = ?," +
                        " Rate = ?, WorkingDescID = ?, StandardID = ?, SizeQty = ?, BSizeQty = ?," +
                        " WorkingTimesB = ?, Amount = ?, StandardIDItemNo = ?, BStandardIDItemNo = ?, AmendDate = ?," +
                        " WorkHoursStatisticsTypeID = ?, CreateDate = ?, CreateUserNo = ?, CreateUserName = ?, ModifyDate = ?," +
                        " ModifyUserNo = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                        " WHERE tblOManufactureDailyExchangeMold.OrderID = "+OrderID+" AND tblOManufactureDailyExchangeMold.SerialNumber = "+SerialNumber;
                comm = connection.prepareStatement(Single, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1, OrderID);//OrderID
                comm.setString(2, SerialNumber);//SerialNumber
                comm.setString(3, EmployeeID);//EmployeeID
                comm.setString(4, WorkingHourID);//WorkingHourID
                comm.setString(5, ManufactureOrderID);//ManufactureOrderID

                comm.setString(6, MachineID);//MachineID
                comm.setString(7, PassID);//ProcessID
                comm.setString(8, WorkShiftType);//WorkShiftType

                comm.setString(9, EndDataView.getText().toString());//EndDate
                comm.setString(10, PutTime);//EndTime
                comm.setString(11, WorkingTimes);//WorkingTimes
                comm.setString(12, ProductsTypeID);//ProductsTypeID
                comm.setString(13, SizeID);//SizeID

                comm.setString(14,ThicknessID);//ThicknessID
                comm.setString(15,BProductsTypeID);//BProductsTypeID
                comm.setString(16,BSizeID);//BSizeID
                comm.setString(17,BThicknessID);//BThicknessID
                comm.setString(18,BManufactureOrderID);//BManufactureOrderID

                comm.setString(19,Rate);//Rate
                comm.setString(20,WorkingDescID);//WorkingDescID
                comm.setString(21,StandardID);//StandardID
                comm.setString(22,SizeQty);//SizeQty
                comm.setString(23,BSizeQty);//BSizeQty

                comm.setString(24,WorkingTimesB);//WorkingTimesB
                comm.setString(25,Amount);//Amount
                comm.setString(26,StandardIDItemNo);//StandardIDItemNo
                comm.setString(27,BStandardIDItemNo);//BStandardIDItemNo
                comm.setString(28,AmendDate);//AmendDate

                comm.setString(29,WorkHoursStatisticsTypeID);//WorkHoursStatisticsTypeID
                comm.setString(30,now);//CreateDate
                comm.setString(31,EmployeeID); // CreateUserNo
                comm.setString(32,EmployeeName); // CreateUserName
                comm.setString(33,now); //ModifyDate

                comm.setString(34,EmployeeID); //ModifyUserNo
                comm.setString(35,EmployeeName); //ModifyUserName
                comm.setString(36,now);//SystemModifyDate
                comm.executeUpdate();
                ShowToast();
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Log.e(TAG,"run方法所在的線程: "+Thread.currentThread().getName());
                        GoBack();
                    }
                };

                timer.schedule(timerTask,2000);
                //endregion
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    //region Toast
    private void ShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("新增成功!");
        toast.show();
    }
    //endregion

    private void TimeJudgment(){
        String [] StartTime = InputStartTime.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY,Integer.valueOf(StartTime[0]));
        calendar1.set(Calendar.MINUTE,Integer.valueOf(StartTime[1]));
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time1 = df.format(calendar1.getTime());
        Log.e(TAG,time1);
        Log.e("時間", String.valueOf(calendar1.get(Calendar.YEAR)+calendar1.get(Calendar.MINUTE)+calendar1.get(Calendar.DAY_OF_MONTH)+calendar1.get(Calendar.HOUR_OF_DAY)+calendar1.get(Calendar.MONTH)));
        Log.e("時間",String.valueOf(calendar.get(Calendar.YEAR)+calendar.get(Calendar.MINUTE)+calendar.get(Calendar.DAY_OF_MONTH)+calendar.get(Calendar.HOUR_OF_DAY)+calendar.get(Calendar.MONTH)));

        if(calendar.compareTo(calendar1) == -1){
            Log.e("時差","增加一天");
            // 對 calendar 設定時間的方法
            // 設定傳入的時間格式
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar2 = Calendar.getInstance();
            // 指定一個日期
            try {
                Date date;
                date = dateFormat.parse(InputStartDate);
                calendar2.setTime(date);
                calendar2.add(Calendar.DATE,1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // 對 calendar 設定為 date 所定的日期
            String time2 = dateFormat.format(calendar2.getTime());
            Log.e("TAG",time2);
            EndDataView.setText(time2);
            WorkingTime(StartDateTimeView.getText().toString(),time2 + " " +EndTimeView.getText().toString());
        }else{
            Log.e("時差","不變");
            EndDataView.setText(InputStartDate);
            WorkingTime(StartDateTimeView.getText().toString(),InputStartDate+" "+EndTimeView.getText().toString());
        }
    }

    private void TimeFormat(int hourOfDay, int minute){
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE,minute);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar.getTime());
        Log.e(TAG, time);
        EndTimeView.setText("");
        EndTimeView.setText( String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));
        PutTime = time;
        isWorkingTime = true;
    }
}
