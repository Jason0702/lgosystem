package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sj.ligosystem.Model.User;
import com.sj.ligosystem.Model.Util;

import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import static com.sj.ligosystem.MainActivity.connection;

/**
 * 新增報工單號
 * **/
public class AddSingNumberActivity extends AppCompatActivity {
    final String TAG = "AddSingNumberActivity";
    Toolbar toolbar;
    EditText InputTeam_leader,InputPassword;
    TextView UserText, MachineText,DateText,TeamLeaderText;
    Button CleanBtn,LeaveBtn,VerifyBtn;
    Timer timer;
    Intent intent = new Intent();
    String MachineID, MachineName, UserID, UserName, SetDate, UserPassword,Type;
    Boolean isTeamLeader = false;
    ArrayList<String> OrderSN = new ArrayList<>();
    String OrderNONumber = "";
    String now;
    ImageView Team_leader_Check_imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sing_number);
        initView();
        initData();
    }
    private void initView(){

        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        UserText = findViewById(R.id.AS_User_Text);
        MachineText = findViewById(R.id.AS_Machine_Text);
        DateText = findViewById(R.id.AS_Date_Text);
        TeamLeaderText = findViewById(R.id.AS_TeamLaderName_TextView);
        //endregion

        //region
        Team_leader_Check_imageView = findViewById(R.id.AS_Team_leader_Check_imageView);
        Team_leader_Check_imageView.setVisibility(View.GONE);
        //endregion

        //region EditText
        try {
            InputTeam_leader = findViewById(R.id.AS_Team_leader_View);
            /*InputTeam_leader.setOnKeyListener((v, keyCode, event) -> {
                Clean();
                Log.e(TAG,"點擊");
                return false;
            });*/


            InputTeam_leader.setOnFocusChangeListener((v, hasFocus) -> {
                if (!hasFocus) {
                    runOnUiThread(() -> {
                        Log.e(TAG, hasFocus + InputTeam_leader.getText().toString());
                        CheckTeamLeader(InputTeam_leader.getText().toString());
                    });
                }else{
                    Clean();
                }
            });
            InputPassword = findViewById(R.id.AS_Password_View);
            InputPassword.setOnFocusChangeListener((v, hasFocus) -> {
                if (!hasFocus) {
                    runOnUiThread(() -> {
                        Log.e(TAG, hasFocus + InputPassword.getText().toString() + " "+ UserPassword);
                        if (!InputPassword.getText().toString().trim().equals(UserPassword)) {
                            InputPassword.setError("密碼錯誤");
                            InputPassword.setSelected(true);
                        }
                    });
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
        //endregion

        //region 按鈕
        CleanBtn = findViewById(R.id.AS_Clean_Button);
        LeaveBtn = findViewById(R.id.AS_Leave_Button);
        VerifyBtn = findViewById(R.id.AS_Verify_Button);

        CleanBtn.setOnClickListener(Click);
        LeaveBtn.setOnClickListener(Click);
        VerifyBtn.setOnClickListener(Click);
        //endregion

    }
    private void CheckTeamLeader(String TeamLeaderID){
        String TeamLeaderQuery = "SELECT * FROM tblBEmployee WHERE EmployeeID ='"+TeamLeaderID+"' AND TeamLeader = 1";
        try {
            Statement statement = connection.createStatement();
            ResultSet UserResultSet = statement.executeQuery(TeamLeaderQuery);
            if(UserResultSet != null){
                while (UserResultSet.next()) {
                    String GetLeaderID = UserResultSet.getString("EmployeeID");
                    String GetLeaderName = UserResultSet.getString("EmployeeName");
                    if (!GetLeaderID.isEmpty() && !GetLeaderName.isEmpty()) {
                        Team_leader_Check_imageView.setVisibility(View.VISIBLE);
                        TeamLeaderText.setText(GetLeaderName);
                        InputTeam_leader.setError(null, null);
                        isTeamLeader = true;
                    }
                    Log.e(TAG, GetLeaderID + GetLeaderName + isTeamLeader);
                }
                if (TeamLeaderText.getText().toString().isEmpty()){
                    Team_leader_Check_imageView.setVisibility(View.GONE);
                    InputTeam_leader.setError("此員工並非組長，請重新輸入");
                    Toast.makeText(getApplicationContext(),"此員工並非組長，請重新輸入",Toast.LENGTH_LONG).show();
                    InputTeam_leader.setSelected(true);
                    isTeamLeader = false;
                }
                Log.e(TAG, String.valueOf(isTeamLeader));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initData(){
        try {
            Intent Data = getIntent();
            //Type 1 = 生產 2 = 架模 3 = 其他工時
            Type = Data.getStringExtra("Type");
            UserID = Data.getStringExtra("UserID");
            UserName = Data.getStringExtra("UserName");
            MachineID = Data.getStringExtra("MachineID");
            MachineName = Data.getStringExtra("MachineName");
            //SetDate = Data.getStringExtra("Date");
            SetDate = Data.getStringExtra("DayTime");
            UserPassword = Data.getStringExtra("Password");
            runOnUiThread(()->{
                //region 放入資料
                UserText.setText("卡號: " + UserID + " " + UserName);
                MachineText.setText("機台: " + MachineID + " " +MachineName);
                DateText.setText(SetDate);
                //endregion
            });
            UPloadToDB();
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.AS_Clean_Button:
                Clean();
                break;
            case R.id.AS_Leave_Button:
                GoBack();
                break;
            case R.id.AS_Verify_Button:
                Log.e(TAG, String.valueOf(isTeamLeader));
                if(InputPassword.getText().toString().trim().equals(UserPassword) && isTeamLeader){
                    VerifyBtn.setEnabled(false);
                    if(!OrderSN.isEmpty()){
                        AddOrderSNAddition();
                    }else{
                        NoOrderSNAddition(OrderNONumber,"200");
                    }
                }else{
                    if(!isTeamLeader){
                        InputTeam_leader.requestFocus();
                    }else{
                        InputPassword.requestFocus();
                    }
                    ErrorToast();
                }
                break;
        }
    };
    //region
    void CloseFocus(){
        runOnUiThread(()->{
            InputTeam_leader.clearFocus();
            InputPassword.clearFocus();
        });
    }
    //endregion
    //region 清除欄位
    private void Clean(){
        runOnUiThread(()->{
            isTeamLeader = false;
            InputTeam_leader.setText("");
            TeamLeaderText.setError(null,null);
            TeamLeaderText.setText("");
            Team_leader_Check_imageView.setVisibility(View.GONE);
        });
    }
    //endregion

    //region 取得當日是否有單號
    private void UPloadToDB(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                Log.e(TAG,"Date: "+SetDate);
                OrderNONumber = OrderNo(SetDate);
                //region 取得
                    Single =
                            "SELECT"+
                                    " tblOManufactureDaily.OrderID, tblOManufactureDaily.OrderNo, tblOManufactureDaily.OrderSN, tblOManufactureDaily.OrderNoBarCode, tblOManufactureDaily.ManufactureDate, tblOManufactureDaily.ManufactureDailyType, tblOManufactureDaily.TeamLeaderEmployeeID, tblOManufactureDaily.WorkShiftType, tblOManufactureDaily.MachineID, tblOManufactureDaily.ProcessID, tblOManufactureDaily.CreateUserName"+
                                    " FROM tblOManufactureDaily"+
                                    " WHERE tblOManufactureDaily.OrderNo = '"+ OrderNONumber +"'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            String SingleOrderSN = SingleResultSet.getString("OrderSN");
                            Log.e(TAG,"OrderNo: "+SingleResultSet.getString("OrderNo")+" OrderSN: "+ SingleOrderSN +" OrderID: "+ SingleResultSet.getString("OrderID"));
                            OrderSN.add(SingleOrderSN);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }
    //endregion
    private void NoOrderSNAddition(String OrderNo,String OrderSN){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e(TAG, "現在時間: " + now);


                //Statement statement = connection.createStatement();
                PreparedStatement comm;

                String Single;
                String TeamLeaderID = InputTeam_leader.getText().toString();
                //region 取得
                Single =
                        "INSERT into" +
                                " tblOManufactureDaily (" +
                                " OrderNo, OrderSN, OrderNoBarCode, ManufactureDate, ManufactureDailyType," +
                                " TeamLeaderEmployeeID, WorkShiftType, MachineID, CreateDate, CreateUserID," +
                                " CreateUserNo, CreateUserName, ModifyDate, ModifyUserID, ModifyUserNo, ModifyUserName," +
                                " SystemModifyDate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                //endregion

                comm = connection.prepareStatement(Single, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1, OrderNo);// OrderNo                        填入: 20200220
                comm.setString(2, OrderSN);// OrderSN                        填入: 200
                comm.setString(3, (OrderNo + OrderSN));// OrderNoBarCode     填入: OrderNo + OrderSN
                comm.setString(4, SetDate);// ManufactureDate                填入: 2020/2/20
                comm.setString(5, "0");// ManufactureDailyType           填入: 0
                comm.setString(6, TeamLeaderID);// TeamLeaderEmployeeID     填入: InputLeaderID
                comm.setInt(7, 0);// WorkShiftType                  填入: 0
                comm.setString(8, MachineID);// MachineID                   填入: MachineID
                comm.setString(9, now);// CreateDate                         填入: NowTime
                comm.setString(10, UserID);// CreateUserID                   填入: EmployeeID
                comm.setString(11, UserID);// CreateUserNo                   填入: EmployeeID
                comm.setString(12, UserName);// CreateUserName               填入: EmployeeName
                comm.setString(13, now); // ModifyDate                       填入: Now
                comm.setString(14, UserID);// ModifyUserID                   填入: EmployeeID
                comm.setString(15, UserID);// ModifyUserNo                   填入: EmployeeID
                comm.setString(16, UserName);// ModifyUserName               填入: EmployeeName
                comm.setString(17, now);// SystemModifyDate                  填入: NowTime

                comm.executeUpdate();
                ResultSet rs = comm.getGeneratedKeys();
                if(rs.next()){
                    Log.e(TAG,"OrderID: "+rs.getInt(1));
                    int Index = rs.getInt(1);
                    comm.close();

                    //創建Timer定時器
                    timer = new Timer();
                    //創建一個TimerTask
                    TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Log.e(TAG,"run方法所在的線程: "+Thread.currentThread().getName());
                        AddEmployee(Index);
                        }
                    };
                    timer.schedule(timerTask,1000);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }

    @SuppressLint("NewApi")
    private void AddOrderSNAddition(){
        try {
            String At_last = "";
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                OrderSN.sort(String::compareTo);
            }else {
                Collections.sort(OrderSN, (o1, o2) -> {
                    Integer Number1 = Integer.valueOf(o1);
                    Integer Number2 = Integer.valueOf(o2);
                    return Number1.compareTo(Number2);
                });
            }

            for(String SN : OrderSN){
                At_last = SN;
                Log.e(TAG,At_last);
            }
            int LastIndex = Integer.parseInt(At_last) + 1;
            Log.e(TAG,"最後一單:"+LastIndex);
            NoOrderSNAddition(OrderNONumber,String.valueOf(LastIndex));
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }

    //只要給OrderID就能上傳作業員
    private void AddEmployee(int OrderID){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                PreparedStatement comm;
                String Single;
                //region 取得
                Single =
                        "INSERT into" +
                                " tblOManufactureDailyEmployee (" +
                                " OrderID, EmployeeID, EmployeeName, CreateDate, CreateUserID," +
                                " CreateUserNo, CreateUserName, ModifyDate, ModifyUserID, ModifyUserNo, ModifyUserName," +
                                " SystemModifyDate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                //endregion

                comm = connection.prepareStatement(Single);
                comm.setInt(1, OrderID);// OrderID
                comm.setString(2, UserID);// EmployeeID
                comm.setString(3, UserName);// EmployeeName
                comm.setString(4, now);// Now
                comm.setString(5, UserID);// EmployeeID
                comm.setString(6, UserID);// EmployeeID
                comm.setString(7, UserName);// EmployeeName
                comm.setString(8, now);// Now
                comm.setString(9, UserID);//EmployeeID
                comm.setString(10, UserID);//EmployeeID
                comm.setString(11, UserName);//EmployeeName
                comm.setString(12, now);//Now
                comm.executeUpdate();
                //region 計時器
                runOnUiThread(this::ShowToast);
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Log.e(TAG,"run方法所在的線程: "+Thread.currentThread().getName());
                        GoBack();
                    }
                };

                timer.schedule(timerTask,2000);
                //endregion
            }
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }

    void GoBack(){
        if(Type.equals("1")) {
            intent.setClass(AddSingNumberActivity.this, StartIn_Activity.class);
            intent.putExtra("Type","2");
            intent.putExtra("UserID", UserID);
            intent.putExtra("MachineID", MachineID);
            intent.putExtra("DayTime",SetDate);
            startActivity(intent);
            finish();
        }else if(Type.equals("2")){
            intent.setClass(AddSingNumberActivity.this, StartFormWorkSingNumberActivity.class);
            intent.putExtra("Type","2");
            intent.putExtra("UserID", UserID);
            intent.putExtra("MachineID", MachineID);
            intent.putExtra("DayTime",SetDate);
            startActivity(intent);
            finish();
        }else if(Type.equals("3")){
            intent.setClass(AddSingNumberActivity.this, StartOtherTimeActivity.class);
            intent.putExtra("Type","2");
            intent.putExtra("UserID", UserID);
            intent.putExtra("MachineID", MachineID);
            intent.putExtra("DayTime",SetDate);
            startActivity(intent);
            finish();
        }
    }

    //region 新增失敗Toast
    void ErrorToast(){
        try {
            //region Toast
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
            TextView text = layout.findViewById(R.id.text);

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            text.setText("新增失敗，請檢查組長與密碼是否輸入錯誤。");
            toast.show();
            //endregion
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }
    //endregion
    //region 新增成功Toast
    void ShowToast(){
        try {
            //region Toast
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
            TextView text = layout.findViewById(R.id.text);

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            text.setText("新增成功!");
            toast.show();
            //endregion
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }
    //endregion

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"Destroy");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    private String OrderNo(String mDay){
        try {
            String[] array = mDay.split("/");
            Log.e(TAG, "Year: " + array[0] + "month: " + array[1] + "day: " + array[2]);
            String year = String.format("%04d", Integer.valueOf(array[0]));
            String month = String.format("%02d", Integer.valueOf(array[1]));
            String day = String.format("%02d", Integer.valueOf(array[2]));
            String AllTime = year + month + day;
            Log.e(TAG, AllTime);
            return AllTime;
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
        return null;
    }
    //region 對話框
    private void ErrorAlertDialog(Exception error){
        try {
            new AlertDialog.Builder(AddSingNumberActivity.this)
                    .setTitle("Error")
                    .setMessage(error.getMessage())
                    .setPositiveButton("關閉視窗", (dialog, which) -> dialog.dismiss())
                    .show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //endregion
}
