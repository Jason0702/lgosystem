package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.sj.ligosystem.Control.PagerAdapter;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.R;

import java.util.ArrayList;
import java.util.List;

public class VerifyFrameActivity extends AppCompatActivity {
    private final String TAG = "VerifyFrameActivity";
    private List<Fragment> fragments = new ArrayList<>();
    private List<String> titles = new ArrayList<>();
    private TabLayout mTableLayout;
    private ViewPager mViewPager;

    Toolbar toolbar;
    TextView EmployeeTextView, MachineIDTextView, OrderNoBarCodeTextView;
    Button Leave_Button;

    private String OrderNoBarCode, EmployeeID, EmployeeName, EmployeePassWord, MachineID, MachineName,OrderID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_frame);

        initView();
        initData();
    }
    void initView(){

        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        EmployeeTextView = findViewById(R.id.VF_EmployeeTextView);
        MachineIDTextView = findViewById(R.id.VF_MachineIDTextView);
        OrderNoBarCodeTextView = findViewById(R.id.VF_OrderNoBarCodeTextView);
        //endregion

        //region Button
        Leave_Button = findViewById(R.id.VF_LeaveButton);
        Leave_Button.setOnClickListener(Click);
        //endregion

        mTableLayout = findViewById(R.id.vff_tabLayout);
        mViewPager = findViewById(R.id.vff_view);
        mViewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), VerifyFrameActivity.this, fragments, titles));
        mTableLayout.setupWithViewPager(mViewPager);
    }
    void initData(){
        Intent intent = getIntent();
        OrderNoBarCode = intent.getStringExtra("OrderNoBarCode");
        EmployeeID = intent.getStringExtra("EmployeeID");
        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeePassWord = intent.getStringExtra("EmployeePassWord");
        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");

        Log.e(TAG,"OrderID"+intent.getIntExtra("OrderID",0));

        OrderID = String.valueOf(intent.getIntExtra("OrderID",0));


        OrderNoBarCodeTextView.setText(OrderNoBarCode);
        EmployeeTextView.setText(EmployeeID +" "+EmployeeName);
        MachineIDTextView.setText(MachineID +" "+MachineName);

        fragments.add(OverNudoneFrameActivity.newInstance("未完成",OrderID,OrderNoBarCode,EmployeePassWord));
        fragments.add(OverCompleteFrameActivity.newInstance("已完成",OrderID,OrderNoBarCode,EmployeePassWord));
        titles.add("未完成");
        titles.add("已完成");
        mViewPager.getAdapter().notifyDataSetChanged();
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.VF_LeaveButton:
                finish();
                break;
        }
    };
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
