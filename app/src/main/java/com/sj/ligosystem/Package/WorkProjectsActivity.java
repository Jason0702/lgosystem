package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_WorkProject_Adapter;
import com.sj.ligosystem.Model.WorkingDescDetails;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;

//架模完工回報(工作項目明細輸入)
public class WorkProjectsActivity extends AppCompatActivity {
    final String TAG = "WorkProjects";
    Toolbar toolbar;
    TextView SingleNumberView, SerialNumberView, MachineView, EmployeeView, WorkProjectsView, DegreeOfCompletionView;
    Spinner WorkProjectsSp;
    EditText DegreeOfCompletionEdit,ConfirmPasswordEditView;
    Button LeaveButton, DeleteButton, VerifyButton;
    RecyclerView recyclerView;

    String OrderNoBarCode, EmployeeID, EmployeeName, MachineID,  MachineDesc;
    String OrderID, SerialNumber, PassID, PassDesc, ManufactureOrderID, ProductsTypeID,
            SizeID, ThicknessID, BManufactureOrderID, BProductsTypeID, BSizeID, BThicknessID, StartDate,
            StartTime, EndDate, EndTime, PassWord, Rate, WorkingTimes, Type, WorkingDescID;
    String WorkingHourID, WorkShiftType, StandardID, SizeQty, BSizeQty, WorkingTimesB, Amount, StandardIDItemNo, BStandardIDItemNo, AmendDate, WorkHoursStatisticsTypeID;
     String now;
    List<String> WorkProjectContentDisplay = new ArrayList<>();
    List<WorkingDescDetails> WorkProjectContent = new ArrayList<>();
    ArrayAdapter<String> WorkProjectSPContent;
    List<WorkingDescDetails> RecyclerWorkProject = new ArrayList<>();
    item_WorkProject_Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_projects);

        SelectedNavItem.setSlectedNavItem(0);

        initView();
        initData();
        initControl();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    void initView() {
        //region Tootle
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion
        //region TextView
        SingleNumberView = findViewById(R.id.WP_SingleNumberView);
        SerialNumberView = findViewById(R.id.WP_SerialNumberView);
        MachineView = findViewById(R.id.WP_MachineView);
        EmployeeView = findViewById(R.id.WP_EmployeeView);
        WorkProjectsView = findViewById(R.id.WP_WorkProjectsView);
        DegreeOfCompletionView = findViewById(R.id.WP_DegreeOfCompletionView);
        //endregion
        //region EditView
        DegreeOfCompletionEdit = findViewById(R.id.WP_DegreeOfCompletionEdit);
        ConfirmPasswordEditView = findViewById(R.id.WP_ConfirmPasswordEditView);
        //endregion
        //region Button
        LeaveButton = findViewById(R.id.WP_LeaveButton);
        DeleteButton = findViewById(R.id.WP_DeleteButton);
        VerifyButton = findViewById(R.id.WP_VerifyButton);
        //endregion
        //region Spinner
        WorkProjectsSp = findViewById(R.id.WP_WorkProjectsSp);
        //endregion

        //region
        recyclerView = findViewById(R.id.WP_recyclerView);
        //endregion
    }
    void initData(){
        Intent intent = getIntent();
        Type = intent.getStringExtra("Type");
        OrderID = intent.getStringExtra("OrderID");
        SerialNumber = intent.getStringExtra("SerialNumber");
        OrderNoBarCode = intent.getStringExtra("OrderNoBarCode");
        MachineID = intent.getStringExtra("MachineID");
        MachineDesc = intent.getStringExtra("MachineDesc");

        EmployeeID = intent.getStringExtra("EmployeeID");
        EmployeeName = intent.getStringExtra("EmployeeName");
        PassID = intent.getStringExtra("PassID");
        PassDesc = intent.getStringExtra("PassDesc");

        ManufactureOrderID = intent.getStringExtra("ManufactureOrderID");
        ProductsTypeID = intent.getStringExtra("ProductsTypeID");
        SizeID = intent.getStringExtra("SizeID");
        ThicknessID = intent.getStringExtra("ThicknessID");

        BManufactureOrderID = intent.getStringExtra("BManufactureOrderID");
        BProductsTypeID = intent.getStringExtra("BProductsTypeID");
        BSizeID = intent.getStringExtra("BSizeID");
        BThicknessID = intent.getStringExtra("BThicknessID");

        StartDate = intent.getStringExtra("StartDate");
        StartTime = intent.getStringExtra("StartTime");
        EndDate = intent.getStringExtra("EndDate");
        EndTime = intent.getStringExtra("EndTime");

        WorkingTimes = intent.getStringExtra("WorkingTimes");

        WorkingDescID = intent.getStringExtra("WorkingDescID");

        PassWord = intent.getStringExtra("PassWord");


        WorkingHourID = intent.getStringExtra("WorkingHourID");
        WorkShiftType = intent.getStringExtra("WorkShiftType");
        StandardID = intent.getStringExtra("StandardID");
        SizeQty = intent.getStringExtra("SizeQty");
        BSizeQty = intent.getStringExtra("BSizeQty");

        WorkingTimesB = intent.getStringExtra("WorkingTimesB");
        Amount = intent.getStringExtra("Amount");
        StandardIDItemNo = intent.getStringExtra("StandardIDItemNo");
        BStandardIDItemNo = intent.getStringExtra("BStandardIDItemNo");
        AmendDate = intent.getStringExtra("AmendDate");

        WorkHoursStatisticsTypeID = intent.getStringExtra("WorkHoursStatisticsTypeID");


        SingleNumberView.setText(OrderNoBarCode);
        SerialNumberView.setText(SerialNumber);
        MachineView.setText(MachineID + " " + MachineDesc);
        EmployeeView.setText(EmployeeID + " " + EmployeeName);

    }

    void initControl(){
        //region Button
        LeaveButton.setOnClickListener(Click);
        DeleteButton.setOnClickListener(Click);
        VerifyButton.setOnClickListener(Click);
        //endregion
        GetSpinner();
        GetRecyclerContent();
        WorkProjectsSp.setSelection(0,true);
        if(WorkProjectContentDisplay.size()!=0){
            WorkProjectsView.setText(String.valueOf(Float.valueOf(WorkProjectContent.get(0).getRate())));
            DegreeOfCompletionView.setText(String.valueOf(Float.valueOf(WorkProjectContent.get(0).getRate())));
            StandardIDItemNo = WorkProjectContent.get(0).getStandardIDItemNo();
        }

        WorkProjectsSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                WorkProjectsView.setText(String.valueOf(Float.valueOf(WorkProjectContent.get(position).getRate())));
                DegreeOfCompletionView.setText(String.valueOf(Float.valueOf(WorkProjectContent.get(position).getRate())));
                StandardIDItemNo = WorkProjectContent.get(position).getStandardIDItemNo();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //region
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        adapter = new item_WorkProject_Adapter(this, RecyclerWorkProject);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(((view, position) -> {
            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();
        }));
        //endregion
    }

    void GoBack(){
        Intent intent = new Intent();
        intent.setClass(this, OverFrameAddActivity.class);
        intent.putExtra("Type",Type);
        intent.putExtra("OrderID",OrderID);
        intent.putExtra("SerialNumber",SerialNumber);
        intent.putExtra("OrderNoBarCode",OrderNoBarCode);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineDesc",MachineDesc);

        intent.putExtra("EmployeeID",EmployeeID);
        intent.putExtra("EmployeeName",EmployeeName);
        intent.putExtra("PassID",PassID);
        intent.putExtra("PassDesc",PassDesc);

        intent.putExtra("ManufactureOrderID", ManufactureOrderID);
        intent.putExtra("ProductsTypeID", ProductsTypeID);
        intent.putExtra("SizeID", SizeID);
        intent.putExtra("ThicknessID", ThicknessID);

        intent.putExtra("BManufactureOrderID", BManufactureOrderID);
        intent.putExtra("BProductsTypeID", BProductsTypeID);
        intent.putExtra("BSizeID", BSizeID);
        intent.putExtra("BThicknessID", BThicknessID);

        intent.putExtra("StartDate", StartDate);
        intent.putExtra("StartTime", StartTime);
        intent.putExtra("EndDate",EndDate);
        intent.putExtra("EndTime",EndTime);

        intent.putExtra("WorkingTimes",WorkingTimes);

        intent.putExtra("Rate", Rate);

        intent.putExtra("PassWord", PassWord);

        intent.putExtra("WorkingHourID",WorkingHourID);
        intent.putExtra("WorkShiftType",WorkShiftType);
        intent.putExtra("StandardID",StandardID);
        intent.putExtra("SizeQty",SizeQty);
        intent.putExtra("BSizeQty",BSizeQty);

        intent.putExtra("WorkingTimesB", WorkingTimesB);
        intent.putExtra("Amount",Amount);
        intent.putExtra("StandardIDItemNo", StandardIDItemNo);
        intent.putExtra("BStandardIDItemNo", BStandardIDItemNo);
        intent.putExtra("AmendDate", AmendDate);

        intent.putExtra("WorkHoursStatisticsTypeID",WorkHoursStatisticsTypeID);

        startActivity(intent);
        this.finish();
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.WP_VerifyButton:
                VerifyButtonCheck();
                break;
            case R.id.WP_DeleteButton:
                DeleteButtonCheck();
                break;
            case R.id.WP_LeaveButton:
                GoBack();
                break;
        }
    };
    void VerifyButtonCheck(){
        if(!ConfirmPasswordEditView.getText().toString().isEmpty() && ConfirmPasswordEditView.getText().toString().equals(PassWord) && !DegreeOfCompletionEdit.getText().toString().isEmpty() && !DegreeOfCompletionEdit.getText().toString().equals("0")) {
            AddNewFunc();
        }else if(DegreeOfCompletionEdit.getText().toString().equals("0")){
            ShowErrorToast("完工程度不可為0!");
            DegreeOfCompletionEdit.requestFocus();
        }else if(DegreeOfCompletionEdit.getText().toString().isEmpty()){
            ShowErrorToast("請輸入完工程度!");
            DegreeOfCompletionEdit.requestFocus();
        }else if(ConfirmPasswordEditView.getText().toString().isEmpty()){
            ShowErrorToast("密碼不可為空!");
            ConfirmPasswordEditView.requestFocus();
        }else if(!ConfirmPasswordEditView.getText().toString().equals(PassWord)){
            ShowErrorToast("密碼錯誤，請確認密碼是否輸入正確!");
            ConfirmPasswordEditView.requestFocus();
        }
    }
    void DeleteButtonCheck(){
        if(!ConfirmPasswordEditView.getText().toString().isEmpty() && ConfirmPasswordEditView.getText().toString().equals(PassWord) && !DegreeOfCompletionEdit.getText().toString().isEmpty() && !DegreeOfCompletionEdit.getText().toString().equals("0")) {
            DeleteFunc();
        }else if(DegreeOfCompletionEdit.getText().toString().equals("0")){
            ShowErrorToast("完工程度不可為0!");
            DegreeOfCompletionEdit.requestFocus();
        }else if(DegreeOfCompletionEdit.getText().toString().isEmpty()){
            ShowErrorToast("請輸入完工程度!");
            DegreeOfCompletionEdit.requestFocus();
        }else if(ConfirmPasswordEditView.getText().toString().isEmpty()){
            ShowErrorToast("密碼不可為空!");
            ConfirmPasswordEditView.requestFocus();
        }else if(!ConfirmPasswordEditView.getText().toString().equals(PassWord)){
            ShowErrorToast("密碼錯誤，請確認密碼是否輸入正確!");
            ConfirmPasswordEditView.requestFocus();
        }
    }
    void ShowErrorToast(String Text){
        Toast.makeText(this,Text,Toast.LENGTH_LONG).show();
    }

    void AddNewFunc(){
        String InputSerialNumber = "";
        try {
            Statement statement = connection.createStatement();
            String Single =
            "SELECT tblOManufactureDailyExchangeMoldWorkDesc.*"+
            " FROM tblOManufactureDailyExchangeMoldWorkDesc"+
            " WHERE tblOManufactureDailyExchangeMoldWorkDesc.OrderID = " + OrderID + " AND tblOManufactureDailyExchangeMoldWorkDesc.SerialNumber = " + SerialNumber +" AND tblOManufactureDailyExchangeMoldWorkDesc.StandardIDItemNo = " + StandardIDItemNo;
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){
                    InputSerialNumber = SingleResultSet.getString("SerialNumber");
                    Log.e(TAG,InputSerialNumber);
                }
            }
            if(InputSerialNumber.isEmpty()){
                AddNewInsert();
                Log.e(TAG, "新增");
            }else{
                AddNewUpData();
                Log.e(TAG,"更新");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }


    }
    void AddNewInsert(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e(TAG, "現在時間: " + now);

                PreparedStatement comm;

                String Single;
                //Add
                Single = "INSERT into tblOManufactureDailyExchangeMoldWorkDesc( OrderID, SerialNumber, StandardIDItemNo, WorkingDescID," +
                        " Rate, RateA, RateB, CreateDate, CreateUserNo, CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate" +
                        ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                comm = connection.prepareStatement(Single, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1, OrderID);//OrderID
                comm.setString(2, SerialNumber);//SerialNumber
                comm.setString(3, StandardIDItemNo);//StandardIDItemNo
                comm.setString(4, WorkingDescID);//WorkingDescID
                comm.setString(5, DegreeOfCompletionView.getText().toString().trim());//Rate
                comm.setString(6, WorkProjectsView.getText().toString().trim());//RateA
                comm.setString(7, DegreeOfCompletionEdit.getText().toString().trim());//RateB
                comm.setString(8, now);//CreateDate
                comm.setString(9, EmployeeID);//CreateUserNo
                comm.setString(10, EmployeeName);//CreateUserName
                comm.setString(11, now);//ModifyDate
                comm.setString(12, EmployeeID);//ModifyUserNo
                comm.setString(13, EmployeeName);//ModifyUserName
                comm.setString(14, now);//SystemModifyDate
                comm.executeUpdate();
                RecyclerWorkProject.clear();
                ShowToast();
                DegreeOfCompletionEdit.setText("1.0");
                GetRecyclerContent();
                adapter.notifyDataSetChanged();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    void AddNewUpData(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dt = new Date();
        now = sdf.format(dt);
        Log.e(TAG, "現在時間: " + now);
        try{
            if(connection == null){
                Log.e(TAG, "connection NULL");
            }else{
                PreparedStatement comm;
                String Single = "UPDATE tblOManufactureDailyExchangeMoldWorkDesc SET" +
                        " WorkingDescID = ?, Rate = ?, RateA = ?, RateB = ?, ModifyDate = ?, ModifyUserNo = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                        " WHERE tblOManufactureDailyExchangeMoldWorkDesc.OrderID = " + OrderID + " AND tblOManufactureDailyExchangeMoldWorkDesc.SerialNumber = "+ SerialNumber +" AND tblOManufactureDailyExchangeMoldWorkDesc.StandardIDItemNo =" + StandardIDItemNo;
                comm = connection.prepareStatement(Single);
                comm.setString(1, WorkingDescID);
                comm.setString(2, DegreeOfCompletionView.getText().toString().trim());
                comm.setString(3, WorkProjectsView.getText().toString().trim());
                comm.setString(4, DegreeOfCompletionEdit.getText().toString().trim());
                comm.setString(5, now);
                comm.setString(6, EmployeeID);
                comm.setString(7, EmployeeName);
                comm.setString(8, now);
                comm.executeUpdate();
                RecyclerWorkProject.clear();
                ShowToast();
                DegreeOfCompletionEdit.setText("1.0");
                GetRecyclerContent();
                adapter.notifyDataSetChanged();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    void DeleteFunc(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single = "DELETE tblOManufactureDailyExchangeMoldWorkDesc FROM tblOManufactureDailyExchangeMoldWorkDesc WHERE OrderID =" +OrderID+ " AND SerialNumber =" + SerialNumber + " AND StandardIDItemNo =" + StandardIDItemNo;
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                DeleteShowToast();
                RecyclerWorkProject.clear();
                GetRecyclerContent();
                adapter.notifyDataSetChanged();
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }
    }
    void GetSpinner(){
        try{
            Statement statement = connection.createStatement();
            String Single = "SELECT tblBExchangeMoldWorkingDescDetails.StandardIDItemNo, tblBExchangeMoldWorkingDescDetails.StandardIDItemNo, tblBExchangeMoldWorkingDescDetails.WorkingDesc, tblBExchangeMoldWorkingDescDetails.Rate" +
                    " FROM tblBExchangeMoldWorkingDescDetails" +
                    " WHERE tblBExchangeMoldWorkingDescDetails.WorkingDescID = '"+ WorkingDescID +"'"+
                    " ORDER BY tblBExchangeMoldWorkingDescDetails.StandardIDItemNo";
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){
                    String InputStandardIDItemNo = SingleResultSet.getString("StandardIDItemNo");
                    String InputWorkingDesc = SingleResultSet.getString("WorkingDesc");
                    String InputRate = SingleResultSet.getString("Rate");
                    Log.e(TAG,InputStandardIDItemNo);
                    WorkingDescDetails workingDescDetails = new WorkingDescDetails(InputStandardIDItemNo,InputWorkingDesc,InputRate);
                    WorkProjectContentDisplay.add(workingDescDetails.getStandardIDItemNo() + " " + workingDescDetails.getWorkingDesc());
                    WorkProjectContent.add(workingDescDetails);
                }
            }
            runOnUiThread(()->{
                WorkProjectSPContent = new ArrayAdapter<String>(this,R.layout.item_spinner,WorkProjectContentDisplay);
                WorkProjectsSp.setAdapter(WorkProjectSPContent);
            });
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    //region Toast
    private void ShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("新增成功!");
        toast.show();
    }
    //endregion
    //region DeleteToast
    private void DeleteShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("刪除成功!");
        toast.show();
    }
    //endregion
    void GetRecyclerContent(){
        try{
            Statement statement = connection.createStatement();
            String Single = "SELECT tblOManufactureDailyExchangeMoldWorkDesc.*, tblBExchangeMoldWorkingDescDetails.WorkingDesc" +
                    " FROM tblOManufactureDailyExchangeMoldWorkDesc INNER JOIN tblBExchangeMoldWorkingDescDetails ON (tblOManufactureDailyExchangeMoldWorkDesc.StandardIDItemNo = tblBExchangeMoldWorkingDescDetails.StandardIDItemNo) AND (tblOManufactureDailyExchangeMoldWorkDesc.WorkingDescID = tblBExchangeMoldWorkingDescDetails.WorkingDescID)" +
                    " WHERE tblOManufactureDailyExchangeMoldWorkDesc.OrderID = "+OrderID+" AND tblOManufactureDailyExchangeMoldWorkDesc.SerialNumber = "+SerialNumber+
                    " ORDER BY tblOManufactureDailyExchangeMoldWorkDesc.StandardIDItemNo;";
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){
                    String InputStandardIDItemNo = SingleResultSet.getString("StandardIDItemNo");
                    String InputWorkingDesc = SingleResultSet.getString("WorkingDesc");
                    Rate = String.valueOf(SingleResultSet.getFloat("Rate"));
                    String InputRateA = String.valueOf(SingleResultSet.getFloat("RateA"));
                    String InputRateB = String.valueOf(SingleResultSet.getFloat("RateB"));
                    Log.e(TAG,Rate);
                    RecyclerWorkProject.add(new WorkingDescDetails(InputStandardIDItemNo,InputWorkingDesc,Rate,InputRateA,InputRateB));
                }
                runOnUiThread(()->{
                    recyclerView.setAdapter(adapter);
                });
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}