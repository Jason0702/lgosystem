package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.sj.ligosystem.Control.SearchAdapter;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.R;

public class SearchViewActivity extends AppCompatActivity {
    SearchView searchView;
    Toolbar toolbar;
    RecyclerView recycler;
    String Type;
    SearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);
        InitView();
        InitData();
    }
    private void InitView()
    {
        searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText.toUpperCase());
                return false;
            }
        });
        recycler = findViewById(R.id.SV_recycler);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

    }
    private void InitData(){
        Intent getType = getIntent();
        Type = getType.getStringExtra("Type");
        Log.e("TAG", Type);
        if (Type.equals("FrameProduct")){
            adapter = new SearchAdapter(Util.AllProductsType);
            recycler.setAdapter(adapter);
        }else if (Type.equals("FrameSize")){
            adapter = new SearchAdapter(Util.AllSize);
            recycler.setAdapter(adapter);
        }else if(Type.equals("DemoldingProduct")){
            adapter = new SearchAdapter(Util.AllProductsType);
            recycler.setAdapter(adapter);
        }else if(Type.equals("DemoldingSize")){
            adapter = new SearchAdapter(Util.AllSize);
            recycler.setAdapter(adapter);
        }
        adapter.setmOnItemClickListener((view, OutputText) -> {
            Log.e("TAG",OutputText);
            if (Type.equals("FrameProduct")){
                FormWorks_addedActivity.FrameProductText = OutputText;
            }else if (Type.equals("FrameSize")){
                FormWorks_addedActivity.FrameSizeText = OutputText;
            }else if(Type.equals("DemoldingProduct")){
                FormWorks_addedActivity.DemoldingProductText = OutputText;
            }else if(Type.equals("DemoldingSize")){
                FormWorks_addedActivity.DemoldingSizeText = OutputText;
            }

            this.finish();
        });
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
