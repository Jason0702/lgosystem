package com.sj.ligosystem.Package;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sj.ligosystem.Control.BaseFragment;
import com.sj.ligosystem.R;

import ru.katso.livebutton.LiveButton;

public class CompletedActivity extends BaseFragment {
    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    LiveButton StartIn_Button, FrameIn_Button, OtherIn_Button;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.activity_completed,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();
        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null){
            parent.removeView(mView);
        }
        return mView;
    }
    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        StartIn_Button = mView.findViewById(R.id.Start_LVbutton);
        FrameIn_Button = mView.findViewById(R.id.Frame_LVbutton);
        OtherIn_Button = mView.findViewById(R.id.Other_LVbutton);

        StartIn_Button.setOnClickListener(Click);
        FrameIn_Button.setOnClickListener(Click);
        OtherIn_Button.setOnClickListener(Click);
    }

    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }
    public static CompletedActivity newInstance(String param){
        CompletedActivity completedActivity = new CompletedActivity();
        Bundle args = new Bundle();
        args.putString("agrs1", param);
        completedActivity.setArguments(args);
        return completedActivity;
    }
    View.OnClickListener Click = v -> {
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.Start_LVbutton:
                intent.setClass(mView.getContext(),OverInActivity.class);
                break;
            case R.id.Frame_LVbutton:
                intent.setClass(mView.getContext(),OverFormWorkActivity.class);
                break;
            case  R.id.Other_LVbutton:
                intent.setClass(mView.getContext(),OverOtherTimeActivity.class);
                break;
        }
        startActivity(intent);
    };
}
