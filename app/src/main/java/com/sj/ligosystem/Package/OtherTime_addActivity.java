package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.MainActivity;
import com.sj.ligosystem.Model.Machine;
import com.sj.ligosystem.Model.Process;
import com.sj.ligosystem.Model.User;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;
/**
 * 其他工時開工(明細新增)
 * */
public class OtherTime_addActivity extends AppCompatActivity {
    final String TAG = "OtherTime_addActivity";
    //region UI物件
    Toolbar toolbar;
    Button LeaveButton, CleanButton, VerifyButton;
    TextView WorkView, SingNumberView, ItemsView, MachineView,TextViewMachine,WorkingHoursNameView,UserNameView,MachineNameView;
    ImageView CalendarView, TimeView, EmployeeCheckView, WorkingHoursCheckView,MachineCheckView;
    EditText InputEmployeeView, InputWorkingHoursView, InputDayView, InputTimeView, InputPasswordView,InputMachineView;
    //endregion

    //region
    Map<Integer, String> Work = new HashMap<>();
    Timer timer;
    String PutDate,PutTime,MachineID,MachineName,PassWord,OrderNoBarCode,EmployeeName,EmployeeID,SerialNumber,OrderID,now,ProcessID,MachineIDB,WorkShiftType,ProcessDesc;
    int year,month,day,hour,min;
    Boolean isEmployee,isWorkHour,isMachine;
    //endregion
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_other_in);
        initView();
        initData();
        initControl();
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    protected void onStart() {
        super.onStart();
        //region 取得日期
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);
        //endregion

        //region 顯示年月日
        InputTimeView.setText(String.format("%02d",hour) + ":" +String.format("%02d", min));

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar1.getTime());
        Log.e(TAG, time);
        PutTime = time;
        Log.e(TAG, PutTime);
        //endregion
    }

    void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region Button
        LeaveButton = findViewById(R.id.SOI_Leave_Button);
        CleanButton = findViewById(R.id.SOI_Clean_Button);
        VerifyButton = findViewById(R.id.SOI_Verify_Button);
        //endregion

        //region TextView
        WorkView = findViewById(R.id.SOI_Work_View);
        SingNumberView = findViewById(R.id.SOI_single_number_View);
        ItemsView = findViewById(R.id.SOI_Items_View);
        MachineView = findViewById(R.id.SOI_Machine_View);
        UserNameView = findViewById(R.id.SOI_UserNameView);
        WorkingHoursNameView = findViewById(R.id.SOI_WorkName_View);

        MachineNameView = findViewById(R.id.SOI_MachineName_View);
        TextViewMachine = findViewById(R.id.textView40);
        //endregion

        //region EditText
        InputEmployeeView = findViewById(R.id.SOI_InputEmployeeView);
        InputWorkingHoursView = findViewById(R.id.SOI_InputWorkingHoursView);
        InputDayView = findViewById(R.id.SOI_InputDayView);
        InputTimeView = findViewById(R.id.SOI_InputTimeView);
        InputPasswordView = findViewById(R.id.SOI_InputPasswordView);

        InputMachineView = findViewById(R.id.SOI_InputMachineView);

        InputTimeView.setEnabled(false);
        InputDayView.setEnabled(false);
        //endregion

        //region ImageView
        CalendarView = findViewById(R.id.SOI_Calendar_View);
        TimeView = findViewById(R.id.SOI_Time_View);
        EmployeeCheckView = findViewById(R.id.SOI_InputEmployee_Check_imageView);
        WorkingHoursCheckView = findViewById(R.id.SOI_InputWorkingHours_Check_imageView);

        MachineCheckView = findViewById(R.id.SOI_InputMachine_Check_imageView);

        EmployeeCheckView.setVisibility(View.GONE);
        WorkingHoursCheckView.setVisibility(View.GONE);
        MachineCheckView.setVisibility(View.GONE);
        //endregion

        //region 機台(B)
        BMachine(false);
        //endregion
    }
    void initControl(){
        //region Button
        LeaveButton.setOnClickListener(Click);
        CleanButton.setOnClickListener(Click);
        VerifyButton.setOnClickListener(Click);
        //endregion
        //region EditText
        runOnUiThread(()->{
            InputEmployeeView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                    isEmployee = false;
                }else{
                    for(User user : Util.AllUserList)
                    {
                        if(!InputEmployeeView.getText().toString().toUpperCase().equals(user.getUserID()) || InputEmployeeView.getText().toString().isEmpty()){
                            InputEmployeeView.setError("作業員卡號空白或不正確，請重新輸入!");
                            EmployeeCheckView.setVisibility(View.GONE);
                            isEmployee = false;
                        }else{
                            Log.e(TAG,"選中的員工: " + user.getUserName());
                            InputEmployeeView.setText(user.getUserID());
                            UserNameView.setText(user.getUserName());

                            EmployeeCheckView.setVisibility(View.VISIBLE);
                            InputEmployeeView.setError(null,null);
                            isEmployee = true;
                            break;
                        }
                    }
                }
            });
            InputWorkingHoursView.setOnKeyListener((v, keyCode, event) -> {
                if(keyCode == KeyEvent.KEYCODE_DEL){
                    WorkingHoursCheckView.setVisibility(View.GONE);
                    WorkingHoursNameView.setText("");
                    isWorkHour = false;
                }
                return false;
            });
            InputWorkingHoursView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                    isWorkHour = false;
                    BMachine(false);
                }else{
                    if (!Process(InputWorkingHoursView.getText().toString().toUpperCase().trim())  || InputWorkingHoursView.getText().toString().isEmpty()){
                        InputWorkingHoursView.setError("不正確工時別編號，請重新輸入！");
                        WorkingHoursCheckView.setVisibility(View.GONE);
                        WorkingHoursNameView.setText("");
                        isWorkHour = false;
                    }else{
                        if(InputWorkingHoursView.getText().toString().toUpperCase().equals("B01")){BMachine(true);}
                        Log.e(TAG, "選中工時別: " + ProcessID);
                        InputWorkingHoursView.setText(ProcessID);
                        WorkingHoursNameView.setText(ProcessDesc);

                        WorkingHoursCheckView.setVisibility(View.VISIBLE);
                        InputWorkingHoursView.setError(null, null);
                        isWorkHour = true;
                    }
                }
            });
            InputMachineView.setOnKeyListener((v, keyCode, event) -> {
                if(keyCode == KeyEvent.KEYCODE_DEL){
                    MachineCheckView.setVisibility(View.GONE);
                    MachineNameView.setText("");
                    isMachine = false;
                }
                return false;
            });
            InputMachineView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                    isMachine = false;
                }else{
                    if(!InputMachineView.getText().toString().toUpperCase().equals(MachineID)) {
                        for (Machine machine : Util.AllMachine) {
                            if (!InputMachineView.getText().toString().toUpperCase().equals(machine.getMachineID()) || InputMachineView.getText().toString().isEmpty()) {
                                InputMachineView.setError("不正確機台(B)編號，請重新輸入！");
                                MachineCheckView.setVisibility(View.GONE);
                                MachineNameView.setText("");
                                isMachine = false;
                            } else {
                                Log.e(TAG, "選中的機台: " + machine.getMachineName());
                                MachineIDB = machine.getMachineID();
                                InputMachineView.setText(machine.getMachineID());
                                MachineNameView.setText(machine.getMachineName());

                                MachineCheckView.setVisibility(View.VISIBLE);
                                InputMachineView.setError(null, null);
                                isMachine = true;
                                break;
                            }
                        }
                    }else{
                        InputMachineView.setError("機台(B)不能與 機台 相同，請重新輸入！");
                        isMachine = false;
                    }
                }
            });
        });
        //endregion

        //region ImageView
        CalendarView.setOnClickListener(Click);
        TimeView.setOnClickListener(Click);
        //endregion

        InputEmployeeView.requestFocus();
        InputEmployeeView.setText(EmployeeID);
        InputEmployeeView.clearFocus();
    }
    void initData(){
        Work.put(0,"正常班");
        Work.put(1,"加班");
        Intent intent = getIntent();
        PutDate = getIntent().getStringExtra("DayTime");
        InputDayView.setText(PutDate);
        int WorkType = intent.getIntExtra("StartWork",0);
        WorkShiftType = String.valueOf(WorkType);
        String SingleNumber = intent.getStringExtra("SingleNumber");

        MachineID = intent.getStringExtra("MachineID");
        MachineName = intent.getStringExtra("MachineName");
        PassWord = intent.getStringExtra("Password");
        OrderNoBarCode = intent.getStringExtra("OrderNoBarCode");
        EmployeeName = intent.getStringExtra("EmployeeName");
        EmployeeID = intent.getStringExtra("EmployeeID");
        OrderID = String.valueOf(intent.getIntExtra("OrderID",0));
        GetSerialNumber();
        runOnUiThread(()->{
            //班別
            WorkView.setText(Work.get(WorkType));
            //單號
            SingNumberView.setText(SingleNumber);
            //機台
            MachineView.setText(MachineID+" "+MachineName);
        });
        MachineIDB = "";
    }
    void BMachine(Boolean OPEN){
        if(!OPEN) {
            TextViewMachine.setVisibility(View.INVISIBLE);
            InputMachineView.setVisibility(View.INVISIBLE);
            MachineCheckView.setVisibility(View.INVISIBLE);
            MachineNameView.setVisibility(View.INVISIBLE);
            isMachine = true;
        }else{
            TextViewMachine.setVisibility(View.VISIBLE);
            InputMachineView.setVisibility(View.VISIBLE);
            MachineNameView.setVisibility(View.VISIBLE);
        }
    }
    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.SOI_Calendar_View:
                OpenPopCalender(v);
                break;
            case R.id.SOI_Time_View:
                OpenPopTime(v);
                break;
            case R.id.SOI_Leave_Button:
                GoBack();
                break;
            case R.id.SOI_Clean_Button:
                Clean();
                break;
            case R.id.SOI_Verify_Button:
                InputMachineView.clearFocus();
                InputPasswordView.clearFocus();
                InputEmployeeView.clearFocus();
                InputWorkingHoursView.clearFocus();
                if(PassWord.equals(InputPasswordView.getText().toString()) && isMachine) {
                    UpData();
                }else if(!PassWord.equals(InputPasswordView.getText().toString())){
                    InputPasswordView.requestFocus();
                    Toast.makeText(getApplicationContext(),"請確認密碼是否輸入正確!",Toast.LENGTH_LONG).show();
                }else if(!isMachine){
                    InputMachineView.requestFocus();
                    Toast.makeText(getApplicationContext(),"請確認機台(B)是否輸入正確!",Toast.LENGTH_LONG).show();
                }
                break;
        }
    };

    void UpData(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dt = new Date();
        now = sdf.format(dt);
        Log.e(TAG, "現在時間: " + now);
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Add
                Single = "INSERT into"+
                        " tblOManufactureDailyOther ("+
                        " OrderID, SerialNumber, EmployeeID, WorkingHourID, MachineID,"+
                        " ProcessID, HourMoney, WorkShiftType, StartDate, StartTime,"+
                        " EndDate, EndTime, WorkingTimes, MachineIDB, CreateDate,"+
                        " CreateUserNo, CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate)"+
                        " VALUES(?,?,?,?,?,?,?,?,?,?,"+"?,?,?,?,?,?,?,?,?,?,"+"?)";
                comm = connection.prepareStatement(Single);
                comm.setString(1,OrderID);
                comm.setString(2,SerialNumber);
                comm.setString(3,EmployeeID);
                comm.setString(4,ProcessID);
                comm.setString(5,MachineID);

                comm.setString(6,ProcessID);
                comm.setString(7,"0");
                comm.setString(8,WorkShiftType);
                comm.setString(9,PutDate);
                comm.setString(10,PutTime);

                comm.setString(11,PutDate);
                comm.setString(12,PutTime);
                comm.setString(13,"0");
                comm.setString(14,MachineIDB);
                comm.setString(15,now);

                comm.setString(16,EmployeeID);
                comm.setString(17,EmployeeName);
                comm.setString(18,now);
                comm.setString(19,EmployeeID);
                comm.setString(20,EmployeeName);

                comm.setString(21,now);

                comm.executeUpdate();
                ShowToast();
                runOnUiThread(()->{
                    Clean();
                    GetSerialNumber();
                    InputWorkingHoursView.requestFocus();
                });

            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
            VerifyButton.setEnabled(true);
        }
    }
    //region Clean
    void Clean(){
        InputWorkingHoursView.setText("");
        InputMachineView.setText("");
        MachineCheckView.setVisibility(View.GONE);
        WorkingHoursCheckView.setVisibility(View.GONE);
        MachineNameView.setText("");
        WorkingHoursNameView.setText("");
        BMachine(false);
    }
    //endregion
    //region取得 SerialNumber
    private void GetSerialNumber(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT tblOManufactureDailyOther.*" +
                                " FROM tblOManufactureDailyOther" +
                                " WHERE tblOManufactureDailyOther.OrderID = "+ OrderID;
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            Log.e(TAG,SingleResultSet.getString("StartTime"));
                            SerialNumber = SingleResultSet.getString("SerialNumber");

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    if(SerialNumber == null){
                        SerialNumber = "1";
                    }else{
                        SerialNumber = String.valueOf(Integer.valueOf(SerialNumber) + 1);
                    }
                    ItemsView.setText(SerialNumber);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    //endregion
    //region Toast
    private void ShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("新增成功!");
        toast.show();
    }
    //endregion
    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    private void OpenPopCalender(View v){
        hideInput(getApplicationContext(),v);
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_popcalendar,null,false);
        android.widget.CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            InputDayView.setText("");
            InputDayView.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",dayOfMonth));
            DayTime(year,month,dayOfMonth);

            popupWindow.dismiss();
        });
    }
    private void DayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        PutDate = sdf.format(new Date(eventOccursOn));
        Log.e(TAG,"Date: " + PutDate);
    }
    @SuppressLint("SetTextI18n")
    private void OpenPopTime(View v){
        VerifyButton.setEnabled(false);

        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime,null,false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);


        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
            InputTimeView.setText("");
            String minuteFormat = String.format("%02d",Integer.valueOf(minute));
            InputTimeView.setText( hourOfDay + ":" + minuteFormat);

            Calendar calendar = Calendar.getInstance();
            calendar.set(1899,11,30,hourOfDay,minute);
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            String time = df.format(calendar.getTime());
            Log.e(TAG, time);
            PutTime = time;
        });
        Check.setOnClickListener(v12 -> {
            VerifyButton.setEnabled(true);
            popupWindow.dismiss();
        });
    }
    //region 隱藏鍵盤
    private void hideInput(Context context, View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
    }
    //endregion
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }
    //region 道次比對
    private Boolean Process(String InputProcessID){
        Boolean isProcessID = false;
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT ProcessID, ProcessDesc, WorkingHourTypeID" +
                                " FROM tblBProcess" +
                                " WHERE tblBProcess.ProcessID = '"+ InputProcessID +"' AND tblBProcess.WorkingHourTypeID <> '01'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            if(SingleResultSet.getString("ProcessID")!= null){
                                Log.e(TAG,SingleResultSet.getString("ProcessID")+SingleResultSet.getString("ProcessDesc") + SingleResultSet.getString("WorkingHourTypeID"));
                                ProcessID = SingleResultSet.getString("ProcessID");
                                ProcessDesc = SingleResultSet.getString("ProcessDesc");
                                isProcessID = true;
                            }else{
                                isProcessID = false;
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
            isProcessID = false;
        }
        return isProcessID;
    }
    //endregion
    void GoBack(){
        Intent CallBackView = new Intent();
        CallBackView.setClass(this, MainActivity.class);
        startActivity(CallBackView);
        finish();
    }
}
