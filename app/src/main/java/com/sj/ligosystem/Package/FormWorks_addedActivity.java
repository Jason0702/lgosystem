package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Size;
import android.view.Gravity;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sj.ligosystem.MainActivity;
import com.sj.ligosystem.Model.User;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.KeyEvent.*;
import static com.sj.ligosystem.MainActivity.connection;

/**
 * 開工架模(明細新增)
 */
public class FormWorks_addedActivity extends AppCompatActivity {
    final String TAG = "FormWorks_addedActivity";
    Intent intent = new Intent();
    //region UI物件
    Toolbar toolbar;
    Button LeaveButton, CleanButton, VerifyButton;
    TextView WorkView, SingNumberView, ItemsView, MachineView,InputPassNameView, EmployeeNameView;
    ImageView CalendarView, TimeView, EmployeeCheckView, PassCheckView, OrderCheckView, DemoldingCheckView;
    Spinner /*FrameProduct, FrameSize,*/ FrameThickness, /*DemoldingProduct, DemoldingSize,*/ DemoldingThickness;
    EditText InputEmployeeView, InputPassView, InputOrderNumberView, InputDemoldingView, InputDayView, InputTimeView, InputPasswordView;
    TextView FrameProduct, FrameSize, DemoldingProduct, DemoldingSize;
    //endregion
    //region
    Map<Integer, String> Work = new HashMap<>();
    Timer timer;
    int year,month,day,hour,min;
    String PassWord,OrderNoBarCode,OrderID,SerialNumber,MachineID,MachineName,EmployeeName,EmployeeID
            ,ManufactureOrderID,BManufactureOrderID,ProductsTypeID,SizeID,ThicknessID,ProcessID,WorkShiftType
            ,BProductsTypeID,BSizeID,BThicknessID,PutDate,PutTime,now,TeamleaderID,SizeQty,BSizeQty,SelectWorkingHourID,ProcessDesc;

    String  WorkingDescID, StandardID,WorkingTimesB,Amount,StandardIDItemNo,BStandardIDItemNo,AmendDate;

    Boolean isEmployee,isPass,isOrderNumber,isDemolding;

    public static String FrameProductText, FrameSizeText, DemoldingProductText, DemoldingSizeText;

    ArrayAdapter<String> ProductsTypeAdapter, SizeAdapter, ThiclnessAdapter;
    //endregion
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_works_added);
        initView();
        initData();
        initControl();
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    protected void onStart() {
        super.onStart();
        //region 取得日期
        Calendar calendar = Calendar.getInstance();
        /*year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day  = calendar.get(Calendar.DAY_OF_MONTH);*/

        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);
        //endregion

        //region
        InputTimeView.setText(String.format("%02d",hour) + ":" +String.format("%02d", min));

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar1.getTime());
        Log.e(TAG, time);
        PutTime = time;
        Log.e(TAG, PutTime);
        //endregion

        FrameProduct.setText(FrameProductText);
        FrameSize.setText(FrameSizeText);
        DemoldingProduct.setText(DemoldingProductText);
        DemoldingSize.setText(DemoldingSizeText);

        ClearFocus();
        FrameProduct.didTouchFocusSelect();

    }

    void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region Button
        LeaveButton = findViewById(R.id.FWA_Leave_Button);
        CleanButton = findViewById(R.id.FWA_Clean_Button);
        VerifyButton = findViewById(R.id.FWA_Verify_Button);
        //endregion

        //region TextView
        WorkView = findViewById(R.id.FWA_Work_View);
        SingNumberView = findViewById(R.id.FWA_single_number_View);
        ItemsView = findViewById(R.id.FWA_Items_View);
        MachineView = findViewById(R.id.FWA_Machine_View);
        InputPassNameView = findViewById(R.id.FWA_InputPassNameView);
        EmployeeNameView = findViewById(R.id.FWA_EmployeeNameView);
        //endregion

        //region EditText
        InputEmployeeView = findViewById(R.id.FWA_InputEmployeeView);
        InputPassView = findViewById(R.id.FWA_InputPassView);
        InputOrderNumberView = findViewById(R.id.FWA_InputOrder_number_View);
        InputDemoldingView = findViewById(R.id.FWA_InputDemoldingView);
        InputDayView  = findViewById(R.id.FWA_InputDayView);
        InputTimeView = findViewById(R.id.FWA_InputTimeView);
        InputPasswordView = findViewById(R.id.FWA_InputPasswordView);
        //endregion

        //region ImageView
        CalendarView = findViewById(R.id.FWA_Calendar_View);
        TimeView = findViewById(R.id.FWA_Time_View);
        EmployeeCheckView = findViewById(R.id.FWA_InputEmployee_Check_imageView);
        PassCheckView = findViewById(R.id.FWA_InputPassView_Check_imageView);
        OrderCheckView = findViewById(R.id.FWA_InputOrder_Check_imageView);
        DemoldingCheckView = findViewById(R.id.FWA_InputDemolding_Check_imageView);
        //endregion

        //region Spinner
        FrameProduct = findViewById(R.id.FWA_FrameProduct_spinner);
        FrameSize = findViewById(R.id.FWA_FrameSize_spinner);
        FrameThickness = findViewById(R.id.FWA_FrameThickness_spinner);
        DemoldingProduct = findViewById(R.id.FWA_DemoldingProduct_spinner);
        DemoldingSize = findViewById(R.id.FWA_DemoldingSize_spinner);
        DemoldingThickness = findViewById(R.id.FWA_DemoldingThickness_spinner);

        /*FrameProduct.setSelection(0,true);
        FrameSize.setSelection(0,true);*/
        FrameThickness.setSelection(0,true);
        /*DemoldingProduct.setSelection(0,true);
        DemoldingSize.setSelection(0,true);*/
        DemoldingThickness.setSelection(0,true);

        FrameProduct.setOnClickListener(Click);
        FrameSize.setOnClickListener(Click);
        DemoldingProduct.setOnClickListener(Click);
        DemoldingSize.setOnClickListener(Click);

        FrameProductText = Util.AllProductsType.get(0);
        FrameSizeText =Util.AllSize.get(0);
        DemoldingProductText = Util.AllProductsType.get(0);
        DemoldingSizeText = Util.AllSize.get(0);

        /*FrameProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG,FrameProduct.getItemAtPosition(position).toString());
                ProductsTypeID = FrameProduct.getItemAtPosition(position).toString();
                hideInput(getApplicationContext(),view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        FrameSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG,FrameSize.getItemAtPosition(position).toString());
                SizeID = FrameSize.getItemAtPosition(position).toString();
                hideInput(getApplicationContext(),view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        FrameThickness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG,FrameThickness.getItemAtPosition(position).toString());
                ThicknessID = FrameThickness.getItemAtPosition(position).toString();
                hideInput(getApplicationContext(),view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

       /* DemoldingProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG,DemoldingProduct.getItemAtPosition(position).toString());
                BProductsTypeID = DemoldingProduct.getItemAtPosition(position).toString();
                hideInput(getApplicationContext(),view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        DemoldingSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG,DemoldingSize.getItemAtPosition(position).toString());
                BSizeID = DemoldingSize.getItemAtPosition(position).toString();
                hideInput(getApplicationContext(),view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        DemoldingThickness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG,DemoldingThickness.getItemAtPosition(position).toString());
                BThicknessID = DemoldingThickness.getItemAtPosition(position).toString();
                hideInput(getApplicationContext(),view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion

    }
    void initControl(){
        //region Button
        LeaveButton.setOnClickListener(Click);
        CleanButton.setOnClickListener(Click);
        VerifyButton.setOnClickListener(Click);
        //endregion

        //region EditText


        runOnUiThread(()->{
            InputEmployeeView.setOnKeyListener((v, keyCode, event) ->{
                if(keyCode == KEYCODE_DEL){
                    EmployeeCheckView.setVisibility(View.GONE);
                    InputEmployeeView.setText("");
                    EmployeeNameView.setText("");
                    InputEmployeeView.setError(null,null);
                    isEmployee = false;
                }
                return false;
            });

            InputEmployeeView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                }else{
                    for(User user : Util.AllUserList){
                        if(!InputEmployeeView.getText().toString().toUpperCase().equals(user.getUserID()) || InputEmployeeView.getText().toString().trim().isEmpty()){
                            InputEmployeeView.setError("作業員卡號空白或不正確，請重新輸入!");
                            EmployeeCheckView.setVisibility(View.GONE);
                            EmployeeNameView.setText("");
                            isEmployee = false;
                        }else {
                            Log.e(TAG,"選中的員工:" + user.getUserName());
                            InputEmployeeView.setText(user.getUserID());
                            InputEmployeeView.setSelection(user.getUserID().length());
                            EmployeeNameView.setText(user.getUserName());
                            EmployeeCheckView.setVisibility(View.VISIBLE);
                            InputEmployeeView.setError(null,null);
                            isEmployee = true;
                            break;
                        }
                    }
                }
            });

            InputPassView.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    isPass = false;
                    OrderCheckView.setVisibility(View.GONE);
                    InputOrderNumberView.setText("");
                    /*FrameProduct.setSelection(0,true);
                    FrameSize.setSelection(0,true);*/
                    FrameThickness.setSelection(0,true);
                    isOrderNumber = false;
                    DemoldingCheckView.setVisibility(View.GONE);
                    InputDemoldingView.setText("");
                    /*DemoldingProduct.setSelection(0,true);
                    DemoldingSize.setSelection(0,true);*/
                    DemoldingThickness.setSelection(0,true);
                    isDemolding = false;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            InputPassView.setOnFocusChangeListener((v,hasFocus)->{
                if(!hasFocus) {
                    if (!Process(InputPassView.getText().toString().toUpperCase().trim()) || InputPassView.getText().toString().trim().isEmpty()) {
                        InputPassView.setError("道次空白或不正確，請重新輸入！");
                        InputPassNameView.setText("");
                        PassCheckView.setVisibility(View.GONE);
                        isPass = false;
                    } else {
                        InputPassView.setText(ProcessID);
                        InputPassNameView.setText(ProcessDesc.trim());
                        InputPassView.setSelection(ProcessID.length());
                        PassCheckView.setVisibility(View.VISIBLE);
                        InputPassView.setError(null, null);
                        isPass = true;
                    }
                }
            });

            InputOrderNumberView.setOnKeyListener((v, keyCode, event) -> {
                if(keyCode == KEYCODE_DEL){
                    OrderCheckView.setVisibility(View.GONE);
                    InputOrderNumberView.setText("");
                    /*FrameProduct.setSelection(0,true);
                    FrameSize.setSelection(0,true);*/
                    FrameThickness.setSelection(0,true);
                    isOrderNumber = false;
                }
                return false;
            });

            InputOrderNumberView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                    isOrderNumber = false;
                }else {
                    GetOrderNumber();
                }
            });

            InputDemoldingView.setOnKeyListener((v, keyCode, event) -> {
                if(keyCode == KEYCODE_DEL){
                    DemoldingCheckView.setVisibility(View.GONE);
                    InputDemoldingView.setText("");
                    /*DemoldingProduct.setSelection(0,true);
                    DemoldingSize.setSelection(0,true);*/
                    DemoldingThickness.setSelection(0,true);
                    isDemolding = false;
                }
                return false;
            });

            InputDemoldingView.setOnFocusChangeListener((v,hasFocus)->{
                if(hasFocus){
                    isDemolding = false;
                }else{
                   GetDemolding();
                }
            });
        });
        //endregion

        //region ImageView
        EmployeeCheckView.setVisibility(View.GONE);
        PassCheckView.setVisibility(View.GONE);
        OrderCheckView.setVisibility(View.GONE);
        DemoldingCheckView.setVisibility(View.GONE);

        CalendarView.setOnClickListener(Click);
        TimeView.setOnClickListener(Click);

        //endregion

        //region Spinner

        ProductsTypeAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, Util.AllProductsType);
        SizeAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, Util.AllSize);
        ThiclnessAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, Util.AllThickness);

        /*FrameProduct.setAdapter(ProductsTypeAdapter);
        FrameSize.setAdapter(SizeAdapter);*/
        FrameThickness.setAdapter(ThiclnessAdapter);
       /* DemoldingProduct.setAdapter(ProductsTypeAdapter);
        DemoldingSize.setAdapter(SizeAdapter);*/
        DemoldingThickness.setAdapter(ThiclnessAdapter);


        //endregion

        InputEmployeeView.requestFocus();
        InputEmployeeView.setText(EmployeeID);
        InputEmployeeView.clearFocus();
    }
    void initData(){
        Intent intent = getIntent();
        String Type =intent.getStringExtra("Type");

            WorkingDescID = "";
            StandardID = "";
            WorkingTimesB = "";
            Amount = "";
            StandardIDItemNo = "";
            BStandardIDItemNo = "";
            AmendDate = "";

            isEmployee = false;
            isDemolding = false;
            isOrderNumber = false;
            isPass = false;


            Work.put(0, "正常班");
            Work.put(1, "加班");


            int WorkType = intent.getIntExtra("StartWork", 0);
            //寫入資料庫用
            WorkShiftType = String.valueOf(WorkType);

            String SingleNumber = intent.getStringExtra("SingleNumber");
            MachineID = intent.getStringExtra("MachineID");
            MachineName = intent.getStringExtra("MachineName");
            PassWord = intent.getStringExtra("Password");
            OrderNoBarCode = intent.getStringExtra("OrderNoBarCode");
            EmployeeName = intent.getStringExtra("EmployeeName");
            EmployeeID = intent.getStringExtra("EmployeeID");
            TeamleaderID = intent.getStringExtra("TeamleaderID");
            SelectWorkingHourID = intent.getStringExtra("SelectWorkingHourID");
            PutDate = intent.getStringExtra("DayTime");
            InputDayView.setText(PutDate);

            Log.e(TAG, "OrderNoBarCode" + OrderNoBarCode);
            Log.e(TAG, "SelectWorkingHourID" + SelectWorkingHourID);
            OrderID = String.valueOf(intent.getIntExtra("OrderID", 0));
            Log.e(TAG, OrderID);
            //GetSerialNumber();
            runOnUiThread(() -> {
                //班別
                WorkView.setText(Work.get(WorkType));
                //單號
                SingNumberView.setText(SingleNumber);
                //機台
                MachineView.setText(MachineID + " " + MachineName);
                //項次
                GetSerialNumber();
            });
    }

    //region取得 SerialNumber
    private void GetSerialNumber(){
        Log.e(TAG,"SerialNumber有呼叫");
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT tblOManufactureDailyExchangeMold.SerialNumber" +
                                " FROM tblOManufactureDailyExchangeMold" +
                                " WHERE OrderID = "+ OrderID;
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            Log.e(TAG,"SerialNumber"+SingleResultSet.getString("SerialNumber"));
                            SerialNumber = SingleResultSet.getString("SerialNumber");

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    if(SerialNumber == null){
                        SerialNumber = "1";
                    }else{
                        SerialNumber = String.valueOf(Integer.valueOf(SerialNumber) + 1);
                    }
                    ItemsView.setText(SerialNumber);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    //endregion

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.FWA_FrameProduct_spinner:
                intent.setClass(this, SearchViewActivity.class);
                intent.putExtra("Type","FrameProduct");
                startActivity(intent);
                break;
            case R.id.FWA_FrameSize_spinner:
                intent.setClass(this, SearchViewActivity.class);
                intent.putExtra("Type","FrameSize");
                startActivity(intent);
                break;
            case R.id.FWA_DemoldingProduct_spinner:
                intent.setClass(this, SearchViewActivity.class);
                intent.putExtra("Type","DemoldingProduct");
                startActivity(intent);
                break;
            case R.id.FWA_DemoldingSize_spinner:
                intent.setClass(this, SearchViewActivity.class);
                intent.putExtra("Type","DemoldingSize");
                startActivity(intent);
                break;
            case R.id.FWA_Calendar_View:
                OpenPopCalender(v);
                break;
            case R.id.FWA_Time_View:
                OpenPopTime(v);
                break;
            case R.id.FWA_Leave_Button:
                GoBack();
                break;
            case R.id.FWA_Clean_Button:
                Clean();
                break;
            case R.id.FWA_Verify_Button:
                ClearFocus();
                if(isEmployee && isPass && isOrderNumber && isDemolding && InputPasswordView.getText().toString().equals(PassWord)){
                    ProductsTypeID = ProductsTypeID == null ? Util.AllProductsType.get(0) : ProductsTypeID;
                    SizeID = SizeID == null ? Util.AllSize.get(0) : SizeID;
                    ThicknessID = ThicknessID == null ? Util.AllThickness.get(0) : ThicknessID;
                    BProductsTypeID = BProductsTypeID == null ? Util.AllProductsType.get(0) : BProductsTypeID;
                    BSizeID = BSizeID == null ? Util.AllSize.get(0) : BSizeID;
                    BThicknessID = BThicknessID == null ? Util.AllThickness.get(0) : BThicknessID;
                    NotblBExchangeMoldStandard(MachineID,ProcessID,ProductsTypeID,SizeID,ThicknessID,BProductsTypeID,BSizeID,BThicknessID);
                }else if(!isEmployee){
                    Toast.makeText(getApplicationContext(),"作業員卡號錯誤，請重新輸入！",Toast.LENGTH_LONG).show();
                    InputEmployeeView.requestFocus();
                }else if(!isPass){
                    Toast.makeText(getApplicationContext(),"道次錯誤，請重新輸入！",Toast.LENGTH_LONG).show();
                    InputPassView.requestFocus();
                }else if(!isOrderNumber){
                    Toast.makeText(getApplicationContext(),"不正確架模工令編號或已作廢，請重新輸入！",Toast.LENGTH_LONG).show();
                    InputOrderNumberView.requestFocus();
                }else if(!isDemolding){
                    Toast.makeText(getApplicationContext(),"不正確拆模工令編號或已作廢，請重新輸入！",Toast.LENGTH_LONG).show();
                    InputDemoldingView.requestFocus();
                }else if(!InputPasswordView.getText().toString().equals(PassWord)){
                    Toast.makeText(getApplicationContext(),"密碼不正確，請重新輸入！",Toast.LENGTH_LONG).show();
                    InputPasswordView.requestFocus();
                }
                break;
        }
    };

    private void ClearFocus(){
        InputEmployeeView.clearFocus();
        InputPassView.clearFocus();
        InputOrderNumberView.clearFocus();
        InputDemoldingView.clearFocus();
    }

    private void GoBack(){
        Intent CallBackView = new Intent();
        CallBackView.setClass(this, MainActivity.class);
        startActivity(CallBackView);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }
    //region 隱藏鍵盤
    private void hideInput(Context context, View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
    }
    //endregion
    void Clean(){
        InputPassView.setText("");
        InputOrderNumberView.setText("");
        InputDemoldingView.setText("");
        InputPassNameView.setText("");

        PassCheckView.setVisibility(View.GONE);
        OrderCheckView.setVisibility(View.GONE);
        DemoldingCheckView.setVisibility(View.GONE);

        /*FrameProduct.setSelection(0,true);
        FrameSize.setSelection(0,true);*/
        FrameThickness.setSelection(0,true);
        /*DemoldingProduct.setSelection(0,true);
        DemoldingSize.setSelection(0,true);*/
        DemoldingThickness.setSelection(0,true);
        FrameProductText = Util.AllProductsType.get(0);
        FrameProduct.setText(FrameProductText);
        FrameSizeText = Util.AllSize.get(0);
        FrameSize.setText(FrameSizeText);

        DemoldingProductText = Util.AllProductsType.get(0);
        DemoldingProduct.setText(DemoldingProductText);
        DemoldingSizeText = Util.AllSize.get(0);
        DemoldingSize.setText(DemoldingSizeText);
    }
    void NoManufactureOrderID(String ManufactureOrderID){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT [010工作通知單資料表].工令編號, [010工作通知單資料表].產品類別編號, [010工作通知單資料表].尺寸代碼, tblBProductsType.ProductsTypeID, tblBProductsType.SizeQty" +
                        " FROM [010工作通知單資料表] LEFT JOIN tblBProductsType ON [010工作通知單資料表].[產品類別編號] = [tblBProductsType].[ProductsTypeID]  WHERE (([010工作通知單資料表].[工令編號])='" +ManufactureOrderID+ "') AND (([010工作通知單資料表].[作廢])='0')";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            String GetProductsTypeID = SingleResultSet.getString("ProductsTypeID").trim();
                            String GetSizeID = GetSizeToString(SingleResultSet.getString("尺寸代碼").trim());
                            String GetThickness = GetThicknessToString(SingleResultSet.getString("SizeQty").trim());
                            SizeQty = SingleResultSet.getString("SizeQty");
                            Log.e(TAG,SingleResultSet.getString("產品類別編號"));
                            Log.e(TAG,"尺寸代碼"+SingleResultSet.getString("尺寸代碼"));
                            Log.e(TAG,SingleResultSet.getString("ProductsTypeID"));
                            Log.e(TAG,SingleResultSet.getString("SizeQty"));
                            runOnUiThread(()->{
                                for(int i = 0; i<Util.AllProductsType.size();i++){
                                    String InProductsTypeID = Util.AllProductsType.get(i);
                                    String [] InProductsTypeIDCut = InProductsTypeID.split("\\(");
                                    if(InProductsTypeIDCut[0].trim().equals(GetProductsTypeID)){
                                        ProductsTypeID = GetProductsTypeID;
                                        Log.e(TAG,"ProductsTypeID: "+ ProductsTypeID);
                                        FrameProductText = InProductsTypeID;
                                        FrameProduct.setText(InProductsTypeID);
                                        //FrameProduct.setSelection(i,true);
                                        break;
                                    }
                                }
                                for(int i = 0; i<Util.AllSize.size(); i++){
                                    String InSizeID = Util.AllSize.get(i);
                                    String [] SizeCut = InSizeID.split("\\(");
                                    if(SizeCut[0].trim().equals(GetSizeID)){
                                        SizeID = GetSizeID;
                                        Log.e(TAG,"SizeID: "+ SizeID);
                                        FrameSizeText = InSizeID;
                                        FrameSize.setText(InSizeID);
                                        //FrameSize.setSelection(i,true);
                                        break;
                                    }
                                }
                                if(MachineID.equals("F19")){

                                }
                               /* for (int i = 0; i<Util.AllThickness.size(); i++){
                                    String Thickness = Util.AllThickness.get(i);
                                    if(Thickness.equals(GetThickness)){
                                        ThicknessID = GetThickness;
                                        Log.e(TAG,"ThicknessID: "+ ThicknessID);
                                        FrameThickness.setSelection(i,true);
                                        break;
                                    }
                                }*/
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    void NoBManufactureOrderID(String ManufactureOrderID){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT [010工作通知單資料表].工令編號, [010工作通知單資料表].產品類別編號, [010工作通知單資料表].尺寸代碼, tblBProductsType.ProductsTypeID, tblBProductsType.SizeQty" +
                        " FROM [010工作通知單資料表] LEFT JOIN tblBProductsType ON [010工作通知單資料表].[產品類別編號] = [tblBProductsType].[ProductsTypeID]  WHERE (([010工作通知單資料表].[工令編號])='" +ManufactureOrderID+ "') AND (([010工作通知單資料表].[作廢])='0')";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            String GetProductsTypeID = SingleResultSet.getString("ProductsTypeID").trim();
                            String GetSizeID = GetSizeToString(SingleResultSet.getString("尺寸代碼").trim());
                            String GetThickness = GetThicknessToString(SingleResultSet.getString("SizeQty").trim());
                            BSizeQty = SingleResultSet.getString("SizeQty").trim();
                            Log.e(TAG,SingleResultSet.getString("產品類別編號"));
                            Log.e(TAG,SingleResultSet.getString("尺寸代碼"));
                            Log.e(TAG,SingleResultSet.getString("ProductsTypeID"));
                            Log.e(TAG,"Thickness"+GetThickness);
                            runOnUiThread(()->{
                                for(int i = 0; i<Util.AllProductsType.size();i++){
                                    String InProductsTypeID = Util.AllProductsType.get(i);
                                    String [] InProductsTypeIDCut = InProductsTypeID.split("\\(");
                                    if(InProductsTypeIDCut[0].trim().equals(GetProductsTypeID)){
                                        BProductsTypeID = GetProductsTypeID;
                                        DemoldingProductText = InProductsTypeID;
                                        DemoldingProduct.setText(InProductsTypeID);
                                        //DemoldingProduct.setSelection(i,true);
                                        break;
                                    }
                                }
                                for(int i = 0; i<Util.AllSize.size(); i++){
                                    String SizeID = Util.AllSize.get(i);
                                    String [] SizeCut = SizeID.split("\\(");
                                    if(SizeCut[0].trim().equals(GetSizeID)){
                                        BSizeID = GetSizeID;
                                        Log.e(TAG, "BSize: "+ GetSizeID);
                                        DemoldingSizeText = SizeID;
                                        DemoldingSize.setText(SizeID);
                                        //DemoldingSize.setSelection(i,true);
                                        break;
                                    }
                                }
                                /*for (int i = 0; i<Util.AllThickness.size(); i++){
                                    String Thickness = Util.AllThickness.get(i);
                                    if(Thickness.contains(GetThickness)){
                                        BThicknessID = GetThickness;
                                        DemoldingThickness.setSelection(i,true);
                                        break;
                                    }
                                }*/
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    private String GetSizeToString(String Size){
        String returnSize;
        returnSize = Size.substring(1);
        Log.e(TAG,"Size: "+returnSize);
        return returnSize;
    }
    private String GetThicknessToString(String Thickness){
        String returnThickness = "";
        switch(Thickness.charAt(0)) {
            case '1': case '3': case '7': case '9':
                returnThickness = "10S";
                break;
            case 'C': case 'D': case 'I': case 'J':
                returnThickness = "20S";
                break;
            case '2': case '4': case '8': case '0': case 'H':
                returnThickness = "40S";
                break;
            case '5': case '6': case 'A': case 'B':
                returnThickness = "5S";
                break;
            case 'E': case 'F': case 'G': case 'K': case 'L':
                returnThickness = "80S";
                break;
            case 'Q':
                returnThickness = "20";
                break;
            case 'R':
                returnThickness = "30";
                break;
            case 'S':
                returnThickness = "40";
                break;
            case 'T': case 'U': case 'V':
                returnThickness = "160S";
                break;
        }
        return returnThickness;
    }
    private String GetWorkHoursStatisticsTypeID(String SelectWorkingHourID){
        String WorkHoursStatisticsTypeID ="";
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT tblBProcess.ProcessID, tblBProcess.WorkHoursStatisticsTypeID"+
                                " FROM tblBProcess"+
                                " WHERE ((tblBProcess.ProcessID)= '"+ SelectWorkingHourID +"')";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            WorkHoursStatisticsTypeID = SingleResultSet.getString("WorkHoursStatisticsTypeID");
                            Log.e(TAG,"OrderNo: "+SingleResultSet.getString("WorkHoursStatisticsTypeID"));
                            return WorkHoursStatisticsTypeID;
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return WorkHoursStatisticsTypeID;
    }

    private void UpData(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dt = new Date();
                now = sdf.format(dt);
                Log.e(TAG, "現在時間: " + now);

                //Statement statement = connection.createStatement();
                PreparedStatement comm;

                String Single;
                //region 取得
                Single =
                        "INSERT into" +
                                " tblOManufactureDailyExchangeMold (" +
                                " OrderID, SerialNumber, EmployeeID, WorkingHourID, ManufactureOrderID," +
                                " MachineID, ProcessID, HourAmount, WorkShiftType, StartDate," +
                                " StartTime, EndDate, EndTime, WorkingTimes, ProductsTypeID," +
                                " SizeID, ThicknessID, BProductsTypeID, BSizeID, BThicknessID," +
                                " BManufactureOrderID, Rate, WorkingDescID, StandardID, SizeQty," +
                                " BSizeQty, WorkingTimesB, Amount, StandardIDItemNo, BStandardIDItemNo," +
                                " AmendDate, WorkHoursStatisticsTypeID, CreateDate, CreateUserNo, CreateUserName," +
                                " ModifyDate, ModifyUserNo, ModifyUserName, SystemModifyDate" +
                                ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                //endregion

                Log.e(TAG,OrderID+" "+ SerialNumber +" "+ EmployeeID +" "+ SelectWorkingHourID +" "+ ManufactureOrderID);

                Log.e(TAG,MachineID+" "+ ProcessID +" "+ "0" +" "+ WorkShiftType +" "+ PutDate);

                Log.e(TAG,PutTime+" "+ PutDate +" "+ PutTime +" "+ "0" +" "+ CutString(ProductsTypeID)[0]);

                Log.e(TAG,CutString(SizeID)[0]+" "+ CutString(ThicknessID)[0] +" "+ CutString(BProductsTypeID)[0] +" "+ CutString(BSizeID)[0] +" "+ CutString(BThicknessID)[0]);

                Log.e(TAG,BManufactureOrderID+ " " + (WorkingDescID.isEmpty() ? "0" : WorkingDescID) +" "+ (Amount.isEmpty() ? "0" : Amount) +" "+ (StandardIDItemNo.isEmpty() ? "0" : StandardIDItemNo) +" "+ (BStandardIDItemNo.isEmpty() ? "0" : BStandardIDItemNo));

                comm = connection.prepareStatement(Single, Statement.RETURN_GENERATED_KEYS);
                comm.setString(1, OrderID);//OrderID
                comm.setString(2, SerialNumber);//SerialNumber
                comm.setString(3, EmployeeID);//TeamleaderID
                comm.setString(4, SelectWorkingHourID);//SelectWorkingHourID
                comm.setString(5, ManufactureOrderID);//ManufactureOrderID

                comm.setString(6, MachineID);//MachineID
                comm.setString(7, ProcessID);//ProcessID
                comm.setString(8, "0");//HourAmount
                comm.setString(9, WorkShiftType);//WorkShiftType
                comm.setString(10, PutDate);//StartDate StartDate

                comm.setString(11, PutTime);//StartTime StartTime
                comm.setString(12, PutDate);//EndDate StartDate
                comm.setString(13, PutTime);//EndTime StartTime
                comm.setString(14, "0");//WorkingTimes 0
                comm.setString(15, CutString(ProductsTypeID)[0]);//ProductsTypeID

                comm.setString(16, CutString(SizeID)[0]);//SizeID
                comm.setString(17, CutString(ThicknessID)[0]);//ThicknessID
                comm.setString(18, CutString(BProductsTypeID)[0]);//BProductsTypeID
                comm.setString(19, CutString(BSizeID)[0]);//BSizeID
                comm.setString(20, CutString(BThicknessID)[0]);//BThicknessID

                comm.setString(21, BManufactureOrderID);//BManufactureOrderID
                comm.setString(22, "0");//0
                comm.setString(23, WorkingDescID.isEmpty() ? "0" : WorkingDescID);//WorkingDescID
                comm.setString(24, StandardID.isEmpty() ? "0" : StandardID);//Nz(Me![StandardID], 0)
                comm.setString(25, SizeQty);//Nz(Me![SizeQty], 0)

                comm.setString(26, BSizeQty);//BSizeQty
                comm.setString(27, WorkingTimesB.isEmpty() ? "0" : WorkingTimesB);//WorkingTimesB
                comm.setString(28, Amount.isEmpty() ? "0" : Amount);//Amount
                comm.setString(29, StandardIDItemNo.isEmpty() ? "0" : StandardIDItemNo);//StandardIDItemNo
                comm.setString(30, BStandardIDItemNo.isEmpty() ? "0" : BStandardIDItemNo);//BStandardIDItemNo

                comm.setString(31, AmendDate.isEmpty()? "1/1/1900" : AmendDate);//AmendDate
                comm.setString(32, GetWorkHoursStatisticsTypeID(SelectWorkingHourID));//WorkHoursStatisticsTypeID
                comm.setString(33, now);//
                comm.setString(34, EmployeeID);//EmployeeID
                comm.setString(35, EmployeeName);//EmployeeName

                comm.setString(36, now);
                comm.setString(37, EmployeeID);//EmployeeID
                comm.setString(38, EmployeeName);//EmployeeName
                comm.setString(39, now);
                comm.executeUpdate();
                ShowToast();

                runOnUiThread(()->{
                    Clean();
                    GetSerialNumber();
                    InputPassView.requestFocus();
                });
            }
        }catch (SQLException e){
            e.printStackTrace();
        }


    }
    private void NotblBExchangeMoldStandard(String MachineID, String ProcessID, String ProductsTypeID, String SizeID, String ThicknessID, String BProductsTypeID, String BSizeID, String BThicknessID){
        Log.e(TAG,"MachineID:" + MachineID + " ProcessID: " + ProcessID + " ProductsTypeID: " + CutString(ProductsTypeID)[0] + " SizeID: " + CutString(SizeID)[0] + " ThicknessID: "+ ThicknessID + " BProductsTypeID: " + BProductsTypeID +
                " BSizeID: " + BSizeID + " BThicknessID: " + BThicknessID);
        try {
            String Single = "SELECT tblBExchangeMoldStandard.*" +
                    " FROM tblBExchangeMoldStandard" +
                    " WHERE MachineID='" + MachineID + "' AND ProcessID='" + ProcessID + "' AND ProductsTypeID='" + CutString(ProductsTypeID)[0] + "' AND SizeID='" + CutString(SizeID)[0] + "' AND ThicknessID='" + CutString(ThicknessID)[0] + "' AND BProductsTypeID='" + CutString(BProductsTypeID)[0] + "' AND" +
                    " BSizeID='" + CutString(BSizeID)[0] + "' AND BThicknessID='" + CutString(BThicknessID)[0] + "'";
            Statement Conn = connection.createStatement();
            ResultSet d1Set = Conn.executeQuery(Single);
            if (d1Set != null) {
                while (d1Set.next()) {
                    WorkingDescID = d1Set.getString("WorkingDescID");
                    StandardID = d1Set.getString("StandardID");
                    WorkingTimesB = d1Set.getString("WorkingTimes");
                    Amount = d1Set.getString("Amount");
                    StandardIDItemNo = d1Set.getString("StandardIDItemNo");
                    BStandardIDItemNo = d1Set.getString("BStandardIDItemNo");
                    AmendDate = d1Set.getString("AmendDate");
                    Log.e(TAG, "WorkingDescID"+WorkingDescID);
                    Log.e(TAG, "StandardID"+StandardID);
                    Log.e(TAG, "WorkingTimesB"+WorkingTimesB);
                    Log.e(TAG, "Amount"+Amount);
                    Log.e(TAG, "StandardIDItemNo"+StandardIDItemNo);
                    Log.e(TAG, "BStandardIDItemNo"+BStandardIDItemNo);
                    Log.e(TAG, "AmendDate"+AmendDate);

                }
            }
            UpData();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    private String[] CutString(String mContent){
        return mContent.trim().split("\\(");
    }
    private void OpenPopCalender(View v){
        hideInput(getApplicationContext(),v);
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_popcalendar,null,false);
        android.widget.CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            InputDayView.setText("");
            Toast.makeText(FormWorks_addedActivity.this,"選擇的是: "+ year + "/" + (month+1) + "/" + dayOfMonth,Toast.LENGTH_LONG).show();
            InputDayView.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",dayOfMonth));
            DayTime(year,month,dayOfMonth);

            popupWindow.dismiss();
        });
    }
    private void DayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        PutDate = sdf.format(new Date(eventOccursOn));
        Log.e(TAG,"Date: " + PutDate);
    }
    private void OpenPopTime(View v){
        VerifyButton.setEnabled(false);
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime,null,false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);


        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
            InputTimeView.setText("");
            String minuteFormat = String.format("%02d",Integer.valueOf(minute));
            InputTimeView.setText( hourOfDay + ":" + minuteFormat);

            Calendar calendar = Calendar.getInstance();
            calendar.set(1899,11,30,hourOfDay,minute);
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            String time = df.format(calendar.getTime());
            Log.e(TAG, time);
            PutTime = time;
        });
        Check.setOnClickListener(v12 -> {
            VerifyButton.setEnabled(true);
            popupWindow.dismiss();
        });
    }
    //region Toast
    private void ShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("新增成功!");
        toast.show();
    }
    //endregion
    private Boolean Process(String InputProcessID){
        Boolean isProcessID = false;
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else {
                //Change below query according to your own database.
                Statement statement = connection.createStatement();
                String Single;
                //region 取得
                Single =
                        "SELECT ProcessID, ProcessDesc, WorkingHourTypeID" +
                                " FROM tblBProcess" +
                                " WHERE tblBProcess.ProcessID = '"+ InputProcessID +"' AND tblBProcess.WorkingHourTypeID = '01'";
                //endregion
                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            if(SingleResultSet.getString("ProcessID")!= null){
                                Log.e(TAG,SingleResultSet.getString("ProcessID")+SingleResultSet.getString("ProcessDesc") + SingleResultSet.getString("WorkingHourTypeID"));
                                ProcessID = SingleResultSet.getString("ProcessID");
                                ProcessDesc = SingleResultSet.getString("ProcessDesc");
                                isProcessID = true;
                            }else{
                                isProcessID = false;
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
            isProcessID = false;
        }
        return isProcessID;
    }


    private void GetOrderNumber(){
        try{
            //region工令號
            String Manufacturequery = "Select * From [010工作通知單資料表] WHERE 工令編號 = '"+InputOrderNumberView.getText().toString().toUpperCase().trim()+"'";
            Statement ManufactureConn = connection.createStatement();
            ResultSet ManufactureResultSet = ManufactureConn.executeQuery(Manufacturequery);

            if(ManufactureResultSet != null){
                while (ManufactureResultSet.next()){
                    try{
                        String Order_Number = ManufactureResultSet.getString("工令編號");

                        if(!InputOrderNumberView.getText().toString().toUpperCase().trim().equals(Order_Number) || InputOrderNumberView.getText().toString().trim().isEmpty()){
                            InputOrderNumberView.setError("架模工令編號空白或不正確架模工令編號，請重新輸入！");
                            OrderCheckView.setVisibility(View.GONE);
                            isOrderNumber = false;
                        }else{
                            InputOrderNumberView.setText(Order_Number);
                            InputOrderNumberView.setSelection(Order_Number.length());
                            ManufactureOrderID = Order_Number;
                            OrderCheckView.setVisibility(View.VISIBLE);
                            NoManufactureOrderID(Order_Number);
                            InputOrderNumberView.setError(null,null);
                            isOrderNumber = true;
                            break;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            //endregion
        }catch (Exception e){e.printStackTrace();}
    }

    private void GetDemolding(){
        try{
            //region工令號
            String Manufacturequery = "Select * From [010工作通知單資料表] WHERE 工令編號 = '"+InputDemoldingView.getText().toString().toUpperCase().trim()+"'";
            Statement ManufactureConn = connection.createStatement();
            ResultSet ManufactureResultSet = ManufactureConn.executeQuery(Manufacturequery);

            if(ManufactureResultSet != null){
                while (ManufactureResultSet.next()){
                    try{
                        String Order_Number = ManufactureResultSet.getString("工令編號");

                        if(!InputDemoldingView.getText().toString().toUpperCase().trim().equals(Order_Number) || InputDemoldingView.getText().toString().trim().isEmpty()){
                            InputDemoldingView.setError("架模工令編號空白或不正確架模工令編號，請重新輸入！");
                            DemoldingCheckView.setVisibility(View.GONE);
                            isOrderNumber = false;
                        }else{
                            InputDemoldingView.setText(Order_Number);
                            InputDemoldingView.setSelection(Order_Number.length());
                            BManufactureOrderID = Order_Number;
                            DemoldingCheckView.setVisibility(View.VISIBLE);
                            NoBManufactureOrderID(Order_Number);
                            InputDemoldingView.setError(null,null);
                            isDemolding = true;
                            break;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            //endregion
        }catch (Exception e){e.printStackTrace();}
    }
}
