package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.Model.item_over_card;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;
/**
 * 生產完工回報(明細輸入)
 **/
public class VerifyProduceAddActivity extends AppCompatActivity {
    final String TAG = "VPAddActivity";
    Toolbar toolbar;
    TextView OrderNoBarCodeTextView,ManufactureOrderIDTextView,ProductsDescriptionTextView,ProcessTextView,
            ManufactureQtyTextView,StandardQtyTextView,SuggestedQtyTextView, EndDataEditView, StartDateTimeEditView, EndTimeEditView;
    EditText WorkingTimesEditView;
    EditText YieldQtyEditView,RestorationQtyEditView,ScrapQtyEditView,YieldQtyAEditView,YieldQtyBEditView;
    EditText ConfirmPasswordEditView;
    ImageView TimeImageView;
    Button btnQuit,btnDelete,btnSave;

    Timer timer;
    int year,month,day,hour,min;
    String PutDate,PutTime,StartDataTime,now;
    String OrderID, SerialNumber,WorkingTimes;
    //取得
    String InputOrderNoBarCode, InputManufactureOrderID, InputProductsDescription, InputManufactureQty, InputBountyCode,
            InputMachineID, InputProcessID, InputProcessDesc, InputWorkShiftype, InputStartDate,
            InputStartTime, InputEndDate, InputEndTime, InputWorkingTimes, InputYieldQty,
            InputRestorationQty,InputScrapQty, InputYieldQtyB, InputStandardQty, InputYieldQtyA,
            InputProductsTypeID, InputStandardRequestID, InputSeamTypeID, InputRawJobTypeID, InputSaleTypeID,
            InputInspectionFormID, InputCompletedFormID, InputMaterialFormID, InputDiagramFormID,InputEmployeeName,PassWord;

    Intent intent = new Intent();

    int Qty;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_produce_add);
        initView();
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
        day  = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.e(TAG,"HR:"+hour);
        //endregion

        //region 取得系統時間
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1899,11,30);
        hour = calendar1.get(Calendar.HOUR_OF_DAY);
        min = calendar1.get(Calendar.MINUTE);

        Log.e(TAG, hour + "  " + min);
        TimeFormat(hour,min);

        TimeJudgment();

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar1.getTime());
        Log.e(TAG, time);
        PutTime = time;
        Log.e(TAG, PutTime);
        //endregion

    }

    void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion

        //region TextView
        OrderNoBarCodeTextView = findViewById(R.id.VPA_OrderNoBarCodeTextView);
        ManufactureOrderIDTextView = findViewById(R.id.VPA_ManufactureOrderIDTextView);
        ProductsDescriptionTextView = findViewById(R.id.VPA_ProductsDescriptionTextView);
        ProcessTextView = findViewById(R.id.VPA_ProcessTextView);
        ManufactureQtyTextView = findViewById(R.id.VPA_ManufactureQtyTextView);
        StandardQtyTextView = findViewById(R.id.VPA_StandardQtyTextView);
        SuggestedQtyTextView = findViewById(R.id.VPA_SuggestedQtyTextView);
        //endregion

        //region EditView
        StartDateTimeEditView = findViewById(R.id.VPA_StartDateTimeEditView);
        StartDateTimeEditView.setEnabled(false);
        EndDataEditView = findViewById(R.id.VPA_EndDataEditView);
        EndDataEditView.setOnClickListener(Click);
        EndTimeEditView = findViewById(R.id.VPA_EndTimeEditView);
        EndTimeEditView.setEnabled(false);
        WorkingTimesEditView = findViewById(R.id.VPA_WorkingTimesEditView);
        WorkingTimesEditView.setOnFocusChangeListener((v,hasFocus) -> {
            if(!hasFocus && !WorkingTimesEditView.getText().toString().isEmpty()){
                PushBackWorkingTime(StartDateTimeEditView.getText().toString(),WorkingTimesEditView.getText().toString());
            }else if(WorkingTimesEditView.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(),"請輸入工作分鐘", Toast.LENGTH_LONG).show();
                WorkingTimesEditView.setText("0");
                WorkingTimesEditView.requestFocus();
                AllZeroing();
            }else if(WorkingTimesEditView.getText().toString().equals("0")){
                AllZeroing();
            }
        });
        YieldQtyEditView = findViewById(R.id.VPA_YieldQtyEditView);
        YieldQtyEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus && YieldQtyEditView.getText().toString().isEmpty()){
                YieldQtyEditView.setText("0");
            }
        });
        RestorationQtyEditView = findViewById(R.id.VPA_RestorationQtyEditView);
        RestorationQtyEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus && RestorationQtyEditView.getText().toString().isEmpty()){
                RestorationQtyEditView.setText("0");
            }
        });
        ScrapQtyEditView = findViewById(R.id.VPA_ScrapQtyEditView);
        ScrapQtyEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus && ScrapQtyEditView.getText().toString().isEmpty()){
                ScrapQtyEditView.setText("0");
            }
        });
        YieldQtyAEditView = findViewById(R.id.VPA_YieldQtyAEditView);
        YieldQtyAEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus && YieldQtyAEditView.getText().toString().isEmpty()){
                YieldQtyAEditView.setText("0");
            }
        });
        YieldQtyBEditView = findViewById(R.id.VPA_YieldQtyBEditView);
        YieldQtyBEditView.setOnFocusChangeListener((v,hasFocus)->{
            if(!hasFocus && YieldQtyBEditView.getText().toString().isEmpty()){
                YieldQtyBEditView.setText("0");
            }
        });
        ConfirmPasswordEditView = findViewById(R.id.VPA_ConfirmPasswordEditView);
        //endregion

        //region Button
        btnDelete = findViewById(R.id.VPA_btnDelete);
        btnQuit = findViewById(R.id.VPA_btnQuit);
        btnSave = findViewById(R.id.VPA_btnSave);
        btnQuit.setOnClickListener(Click);
        btnDelete.setOnClickListener(Click);
        btnSave.setOnClickListener(Click);

        //endregion

        //region ImageView
        TimeImageView = findViewById(R.id.VPA_TimeImageView);
        TimeImageView.setOnClickListener(Click);
        //endregion
    }
    void AllZeroing(){
        YieldQtyEditView.setText("0");
        RestorationQtyEditView.setText("0");
        ScrapQtyEditView.setText("0");
        YieldQtyAEditView.setText("0");
        YieldQtyBEditView.setText("0");
    }
    void initData(){
        Intent GetbeforeData = getIntent();
        InputEmployeeName = GetbeforeData.getStringExtra("EmployeeName");
        OrderID = GetbeforeData.getStringExtra("OrderID");
        SerialNumber = GetbeforeData.getStringExtra("SerialNumber");
        Qty = GetbeforeData.getIntExtra("Qty",0);
        SuggestedQtyTextView.setText(GetbeforeData.getStringExtra("StandardQty"));
        InputData(OrderID,SerialNumber);
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.VPA_btnQuit:
                GoBack();
                break;
            case R.id.VPA_btnDelete:
                AlertDialog();
                break;
            case R.id.VPA_btnSave:
                int YieldQty = Integer.parseInt(YieldQtyEditView.getText().toString());
                int Restoration = Integer.parseInt(RestorationQtyEditView.getText().toString());
                int ScrapQty = Integer.parseInt(ScrapQtyEditView.getText().toString());
                int YieldQtyA = Integer.parseInt(YieldQtyAEditView.getText().toString());
                int YieldQtyB = Integer.parseInt(YieldQtyBEditView.getText().toString());
                int total = YieldQty+Restoration+ScrapQty+YieldQtyA+YieldQtyB;
                Log.e("QTY",Qty + "   " + total);
                if(OverInActivity.UserPassword.equals(ConfirmPasswordEditView.getText().toString()) && Qty>=total && total != 0 && !WorkingTimesEditView.getText().toString().equals("0")){
                    UpData(OrderID, SerialNumber);
                }else if(Qty<total){
                    Toast.makeText(getApplicationContext(),"輸入數量不正確，請重新輸入",Toast.LENGTH_LONG).show();
                    YieldQtyEditView.requestFocus();
                }else if(total == 0){
                    Toast.makeText(getApplicationContext(),"輸入數量不可為0，請重新輸入",Toast.LENGTH_LONG).show();
                    YieldQtyEditView.requestFocus();
                }else if(!OverInActivity.UserPassword.equals(ConfirmPasswordEditView.getText().toString())){
                    Log.e(TAG, "密碼錯誤");
                    Toast.makeText(getApplicationContext(), "請確認密碼是否輸入正確",Toast.LENGTH_LONG).show();
                    ConfirmPasswordEditView.requestFocus();
                }
                break;
            case R.id.VPA_TimeImageView:
                OpenPopTime(v);
                break;
            case R.id.VPA_EndDataEditView:
                OpenPopCalender(v);
                break;
        }
    };

    private void OpenPopCalender(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_popcalendar,null,false);
        CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        //popupWindow.showAsDropDown(v,0,0);
        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);
        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            EndDataEditView.setText("");
            EndDataEditView.setText(String.format("%04d/%02d/%02d",year,(month+1),dayOfMonth));
            DayTime(year,month,dayOfMonth);
            WorkingTime(StartDataTime,EndDataEditView.getText().toString() + " " + EndTimeEditView.getText().toString());
            popupWindow.dismiss();
        });
    }

    //region刪除對話框
    private void AlertDialog(){
        new AlertDialog.Builder(VerifyProduceAddActivity.this)
                .setTitle("刪除")
                .setMessage("您確定要刪除此報工單號明細？ 項次號："+SerialNumber)
                .setPositiveButton("確定",((dialog, which) -> {
                    DeleteDetails();
                    dialog.dismiss();
                }))
                .setNegativeButton("取消",((dialog, which) -> dialog.dismiss()))
                .show();
    }
    //endregion

    void DeleteDetails(){
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single ="DELETE tblOManufactureDailyDetail FROM tblOManufactureDailyDetail WHERE OrderID = '" + OrderID + "' AND SerialNumber = '" + SerialNumber + "'";
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                DeleteShowToast();
                finish();
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }
    }

    void InputData(String OrderID, String SerialNumber){
        try {
            Statement statement = connection.createStatement();
            String Single = "SELECT tblOManufactureDailyDetail.*, tblBProcess.ProcessDesc FROM  tblOManufactureDailyDetail INNER JOIN tblBProcess ON tblOManufactureDailyDetail.ProcessID = tblBProcess.ProcessID  WHERE tblOManufactureDailyDetail.OrderID= '"+OrderID+"' AND tblOManufactureDailyDetail.SerialNumber= '"+SerialNumber+"'";
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){
                    InputOrderNoBarCode = SingleResultSet.getString("OrderNoBarCode");
                    InputManufactureOrderID = SingleResultSet.getString("ManufactureOrderID");
                    InputProductsDescription = SingleResultSet.getString("ProductsDescription");
                    InputManufactureQty = SingleResultSet.getString("ManufactureQty");
                    InputBountyCode = SingleResultSet.getString("BountyCode");

                    InputMachineID = SingleResultSet.getString("MachineID");
                    InputProcessID = SingleResultSet.getString("ProcessID");
                    InputProcessDesc = SingleResultSet.getString("ProcessDesc");
                    InputWorkShiftype = SingleResultSet.getString("WorkShiftType");
                    InputStartDate = GetStartDate(SingleResultSet.getString("StartDate"));

                    InputStartTime = GetStartTime(SingleResultSet.getString("StartTime"));
                    InputEndDate = SingleResultSet.getString("EndDate");
                    InputEndTime = SingleResultSet.getString("EndTime");
                    InputWorkingTimes = SingleResultSet.getString("WorkingTimes");
                    InputYieldQty = SingleResultSet.getString("YieldQty");

                    InputRestorationQty = SingleResultSet.getString("RestorationQty");
                    InputScrapQty = SingleResultSet.getString("ScrapQty");
                    InputYieldQtyA = SingleResultSet.getString("YieldQtyA");
                    InputYieldQtyB = SingleResultSet.getString("YieldQtyB");
                    InputStandardQty = SingleResultSet.getString("StandardQty");

                    InputProductsTypeID = SingleResultSet.getString("ProductsTypeID");
                    InputStandardRequestID = SingleResultSet.getString("StandardRequestID");
                    InputSeamTypeID = SingleResultSet.getString("SeamTypeID");
                    InputRawJobTypeID = SingleResultSet.getString("RawJobTypeID");
                    InputSaleTypeID = SingleResultSet.getString("SaleTypeID");

                    InputInspectionFormID = SingleResultSet.getString("InspectionFormID");
                    InputCompletedFormID = SingleResultSet.getString("CompletedFormID");
                    InputMaterialFormID = SingleResultSet.getString("MaterialFormID");
                    InputDiagramFormID = SingleResultSet.getString("DiagramFormID");

                    Log.e(TAG,InputOrderNoBarCode);

                    String OutPutFormat = String.format("%1$s-%2$s-%3$s",InputOrderNoBarCode.substring(0,8),InputOrderNoBarCode.substring(8,11),InputOrderNoBarCode.substring(11));
                    int ManufactureQty = Math.round(Float.parseFloat(InputManufactureQty));
                    int StandardQty = Math.round(Float.parseFloat(InputStandardQty));
                    Log.e(TAG, String.valueOf(ManufactureQty));
                    OrderNoBarCodeTextView.setText(OutPutFormat);
                    ManufactureOrderIDTextView.setText(InputManufactureOrderID);
                    ProductsDescriptionTextView.setText(InputProductsDescription);
                    ManufactureQtyTextView.setText(String.valueOf(ManufactureQty));
                    ProcessTextView.setText(InputProcessID+" "+InputProcessDesc);
                    StandardQtyTextView.setText(String.valueOf(StandardQty));
                    StartDataTime = InputStartDate+" "+InputStartTime;
                    StartDateTimeEditView.setText(StartDataTime);


                    String [] splitYQ = InputYieldQty.split("\\.");
                    String [] splitRQ = InputRestorationQty.split("\\.");
                    String [] splitSQ = InputScrapQty.split("\\.");
                    String [] splitYQA = InputYieldQtyA.split("\\.");
                    String [] splitYQB = InputYieldQtyB.split("\\.");

                    YieldQtyEditView.setText(splitYQ[0]);
                    RestorationQtyEditView.setText(splitRQ[0]);
                    ScrapQtyEditView.setText(splitSQ[0]);
                    YieldQtyAEditView.setText(splitYQA[0]);
                    YieldQtyBEditView.setText(splitYQB[0]);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //region 新增明細
    private void UpData(String UpOrderID, String UpSerialNumber){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dt = new Date();
        now = sdf.format(dt);
        Log.e(TAG, "現在時間: " + now);
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Add
                Single = " UPDATE tblOManufactureDailyDetail"+
                " SET OrderNoBarCode = ?, ManufactureOrderID = ?, ProductsDescription = ?, ManufactureQty = ?, BountyCode = ?, "+
                " MachineID = ?, ProcessID = ?, OperationTypeID = ?, WorkingHourID = ?, WorkShiftType = ?," +
                " StartDate = ?, StartTime = ?, EndDate = ?, EndTime = ?, WorkingTimes = ?," +
                " YieldQty = ?, RestorationQty = ?, YieldQtyA = ?, YieldQtyB = ?, ScrapQty = ?,"+
                " StandardQty = ?, ProductsTypeID = ?, StandardRequestID = ?, SeamTypeID = ?, RawJobTypeID = ?,"+
                " SaleTypeID = ?, InspectionFormID = ?, CompletedFormID = ?, MaterialFormID = ?, DiagramFormID = ?,"+
                " ModifyDate = ?, ModifyUserName = ?, SystemModifyDate = ?" +
                " WHERE OrderID = "+UpOrderID+" AND SerialNumber="+UpSerialNumber;
                comm = connection.prepareStatement(Single);
                //region
                comm.setString(1,InputOrderNoBarCode);
                comm.setString(2,InputManufactureOrderID);
                comm.setString(3,InputProductsDescription);
                comm.setString(4,InputManufactureQty);
                comm.setString(5,InputBountyCode);
                //endregion
                //region
                comm.setString(6,InputMachineID);
                comm.setString(7,InputProcessID);
                comm.setString(8,"0");
                comm.setString(9,InputProcessID);
                comm.setString(10,InputWorkShiftype);
                //endregion
                //region
                comm.setString(11,InputStartDate);
                comm.setString(12,InputStartTime);

                Calendar calendar = Calendar.getInstance();
                calendar.set(1899,11,30,Integer.valueOf(EndTimeEditView.getText().toString().substring(0,2)),Integer.valueOf(EndTimeEditView.getText().toString().substring(3,4)));
                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                String time = df.format(calendar.getTime());
                Log.e(TAG, EndDataEditView.getText().toString());
                Log.e(TAG, EndTimeEditView.getText().toString().substring(0,2) + EndTimeEditView.getText().toString().substring(3,4));

                comm.setString(13,EndDataEditView.getText().toString());
                comm.setString(14,PutTime);
                comm.setString(15,WorkingTimesEditView.getText().toString());
                //endregion
                //region
                comm.setString(16,YieldQtyEditView.getText().toString());
                comm.setString(17,RestorationQtyEditView.getText().toString());
                comm.setString(18,YieldQtyAEditView.getText().toString());
                comm.setString(19,YieldQtyBEditView.getText().toString());
                comm.setString(20,ScrapQtyEditView.getText().toString());
                //endregion
                //region
                comm.setString(21,InputStandardQty);
                comm.setString(22,InputProductsTypeID);
                comm.setString(23,InputStandardRequestID);
                comm.setString(24,InputSeamTypeID);
                comm.setString(25,InputRawJobTypeID);
                //endregion
                //region
                comm.setString(26,InputSaleTypeID);
                comm.setString(27,InputInspectionFormID);
                comm.setString(28,InputCompletedFormID);
                comm.setString(29,InputMaterialFormID);
                comm.setString(30,InputDiagramFormID);
                //endregion
                //region
                comm.setString(31,now);
                comm.setString(32,InputEmployeeName);
                comm.setString(33,now);
                //endregion
                comm.executeUpdate();
                ShowToast();
                //創建Timer定時器
                timer = new Timer();
                //創建一個TimerTask
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Log.e(TAG,"run方法所在的線程: "+Thread.currentThread().getName());
                        intent.setClass(VerifyProduceAddActivity.this,VerifyProduceActivity.class);
                        intent.putExtra("OrderID", Integer.valueOf(OrderID));
                        intent.putExtra("OrderNoBarCode",Util.OrderNoBarCode);
                        intent.putExtra("EmployeeID",Util.EmployeeID);
                        intent.putExtra("EmployeePassWord",Util.EmployeePassWord);
                        intent.putExtra("MachineID",Util.MachineID);
                        intent.putExtra("MachineName",Util.MachineName);
                        intent.putExtra("EmployeeName",Util.EmployeeName);
                        startActivity(intent);
                        finish();
                    }
                };

                timer.schedule(timerTask,2000);
                //endregion
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }
    }
    //endregion

    //region Toast
    private void ShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("新增成功!");
        toast.show();
    }
    //endregion
    //region DeleteToast
    private void DeleteShowToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("刪除成功!");
        toast.show();
    }
    //endregion

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            GoBack();
        }
        return super.onOptionsItemSelected(item);
    }

    private void DayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        PutDate = sdf.format(new Date(eventOccursOn));
        Log.e(TAG,"Date: " + PutDate);
    }
    private String GetStartTime(String Time){
        try {
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Time);
            String Stat = String.format("%tR", date);
            return Stat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    private String GetStartDate(String Date){
        try{
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            String newStat = Stat.replace("-","/");
            return newStat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private void OpenPopTime(View v){
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_poptime,null,false);
        TimePicker TimeView = view.findViewById(R.id.TimerpickerView);
        Button Check = view.findViewById(R.id.TimeCheck);


        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAtLocation(v, Gravity.CENTER,0,0);

        TimeView.setIs24HourView(true);

        TimeView.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
            TimeFormat(hourOfDay,minute);
        });
        Check.setOnClickListener(v12 -> {
            TimeJudgment();
            popupWindow.dismiss();
        });
    }

    private void WorkingTime(String StartTime, String EndTime){
        //region 計算時差
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try{
            Date d1 = dateFormat.parse(StartTime);
            Date d2 = dateFormat.parse(EndTime);
            long diff = d2.getTime() - d1.getTime();
            Log.e("VPA","LongTime:" + diff);
            long min = diff/ (1000 * 60);
            Log.e(TAG,"分鐘:"+min);
            int outTime = (int)min;
            if(outTime<0){
                WorkingTimesEditView.setText("0");
                WorkingTimesEditView.requestFocus();
                WorkingTimesEditView.clearFocus();
                Toast.makeText(getApplicationContext(),"輸入時間不正確!",Toast.LENGTH_LONG).show();
                AllZeroing();
            }else {
                WorkingTimes = String.valueOf(outTime);
                WorkingTimesEditView.setText(WorkingTimes);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //endregion
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void PushBackWorkingTime(String StartTime, String InputTime){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date d1 = dateFormat.parse(StartTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d1);
            calendar.add(Calendar.MINUTE, Integer.parseInt(InputTime));
            Log.e(TAG, String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            int Year = calendar.get(Calendar.YEAR);
            int Month = calendar.get(Calendar.MONTH) +1;
            int Day = calendar.get(Calendar.DAY_OF_MONTH);

            EndDataEditView.setText(String.format("%04d",Year)+ "/" +String.format("%02d",Month)+ "/" +String.format("%02d",Day));
            TimeFormat(calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    void GoBack(){
        intent.setClass(VerifyProduceAddActivity.this,VerifyProduceActivity.class);
        intent.putExtra("OrderID", Integer.valueOf(OrderID));
        Log.e("PutOrderID",OrderID);
        intent.putExtra("OrderNoBarCode",Util.OrderNoBarCode);
        intent.putExtra("EmployeeID",Util.EmployeeID);
        intent.putExtra("EmployeePassWord",Util.EmployeePassWord);
        intent.putExtra("MachineID",Util.MachineID);
        intent.putExtra("MachineName",Util.MachineName);
        intent.putExtra("EmployeeName",Util.EmployeeName);
        startActivity(intent);
        finish();
    }


    private void TimeJudgment(){
        String [] StartTime = InputStartTime.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY,Integer.valueOf(StartTime[0]));
        calendar1.set(Calendar.MINUTE,Integer.valueOf(StartTime[1]));
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time1 = df.format(calendar1.getTime());
        Log.e(TAG,time1);
        Log.e("時間", String.valueOf(calendar1.get(Calendar.YEAR)+calendar1.get(Calendar.MINUTE)+calendar1.get(Calendar.DAY_OF_MONTH)+calendar1.get(Calendar.HOUR_OF_DAY)+calendar1.get(Calendar.MONTH)));
        Log.e("時間",String.valueOf(calendar.get(Calendar.YEAR)+calendar.get(Calendar.MINUTE)+calendar.get(Calendar.DAY_OF_MONTH)+calendar.get(Calendar.HOUR_OF_DAY)+calendar.get(Calendar.MONTH)));

        if(calendar.compareTo(calendar1) == -1){
            Log.e("時差","增加一天");
            // 對 calendar 設定時間的方法
            // 設定傳入的時間格式
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar2 = Calendar.getInstance();
            // 指定一個日期
            try {
                Date date;
                date = dateFormat.parse(InputStartDate);
                calendar2.setTime(date);
                calendar2.add(Calendar.DATE,1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // 對 calendar 設定為 date 所定的日期
            String time2 = dateFormat.format(calendar2.getTime());
            Log.e("TAG",time2);
            EndDataEditView.setText(time2);
            WorkingTime(StartDateTimeEditView.getText().toString(),time2 + " " +EndTimeEditView.getText().toString());
        }else{
            Log.e("時差","不變");
            EndDataEditView.setText(InputStartDate);
            WorkingTime(StartDateTimeEditView.getText().toString(),InputStartDate+" "+EndTimeEditView.getText().toString());
        }
    }
    private void TimeFormat(int hourOfDay, int minute){
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE,minute);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String time = df.format(calendar.getTime());
        Log.e(TAG, time);
        EndTimeEditView.setText("");
        EndTimeEditView.setText( String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));
        PutTime = time;
    }
}
