package com.sj.ligosystem.Package;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Control.BaseFragment;
import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_over_OtherTime_Complete_Adapter;

import com.sj.ligosystem.Model.item_OtherTime_card;
import com.sj.ligosystem.R;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;

public class OverCompleteOtherTimeActivity extends BaseFragment {

    final String TAG = OverCompleteOtherTimeActivity.class.getSimpleName();
    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    private String Single;
    private int ClickPosition;
    List<item_OtherTime_card> Over_Cards = new ArrayList<>();
    item_over_OtherTime_Complete_Adapter adapter;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.activity_over_complete_other_time,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();
        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null){
            parent.removeView(mView);
        }
        return mView;
    }

    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        String OrderID = bundle.getString("OrderID");
        String OrderNoBarCode = bundle.getString("OrderNoBarCode");
        String PassWord = bundle.getString("PassWord");
        //region 列表
        recyclerView = mView.findViewById(R.id.ON_RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new item_over_OtherTime_Complete_Adapter(getContext(),Over_Cards);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((view, position) -> {
            Log.e("TAG", String.valueOf(position));
            ClickPosition = position;
            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();
            Intent intent = new Intent();
            intent.setClass(getContext(),OverOtherTimeAddActivity.class);
            intent.putExtra("OrderID",Over_Cards.get(position).getOrderID());
            intent.putExtra("SerialNumber",Over_Cards.get(position).getSerialNumber());
            intent.putExtra("OrderNoBarCode",OrderNoBarCode);
            intent.putExtra("EmployeeID",Over_Cards.get(position).getEmployeeID());
            intent.putExtra("EmployeeName",Over_Cards.get(position).getEmployeeName());
            intent.putExtra("WorkHourTimeID",Over_Cards.get(position).getWorkingHourID());
            intent.putExtra("WorkHourTimeName",Over_Cards.get(position).getProcessDesc());
            intent.putExtra("StartDate",Over_Cards.get(position).getStartDate());
            intent.putExtra("StartTime",Over_Cards.get(position).getStartTime());
            intent.putExtra("EndDate",Over_Cards.get(position).getEndDate());
            intent.putExtra("EndTime",Over_Cards.get(position).getEndTime());
            intent.putExtra("PassWord",PassWord);
            intent.putExtra("WorkShiftType",Over_Cards.get(position).getWorkShiftType());
            intent.putExtra("MachineID",Over_Cards.get(position).getMachineID());

            if(Over_Cards.get(position).getWorkingHourID().equals("B01")){
                intent.putExtra("MachineIDB",Over_Cards.get(position).getMachineIDB());
                intent.putExtra("MachineName",Over_Cards.get(position).getMachineDesc());
            }
            startActivity(intent);
            getActivity().finish();

        });
        //endregion
        Getfinished(OrderID);
    }
    void Getfinished(String OrderID){
        try {
            Statement statement = connection.createStatement();

            Single = "SELECT tblOManufactureDailyOther.*, tblBProcess.ProcessDesc, tblBEmployee.EmployeeName, tblBMachine.MachineDesc"+
                    " FROM tblOManufactureDailyOther INNER JOIN tblBEmployee ON tblOManufactureDailyOther.EmployeeID = tblBEmployee.EmployeeID" +
                    " INNER JOIN tblBProcess ON tblOManufactureDailyOther.WorkingHourID = tblBProcess.ProcessID LEFT JOIN tblBMachine ON tblOManufactureDailyOther.MachineIDB = tblBMachine.MachineID"+
                    " WHERE tblOManufactureDailyOther.WorkingTimes > 0 AND tblOManufactureDailyOther.OrderID ="+OrderID+
                    " ORDER BY tblOManufactureDailyOther.SerialNumber";
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){
                    String InputOrderID = SingleResultSet.getString("OrderID");
                    String InputSerialNumber = SingleResultSet.getString("SerialNumber");
                    String InputWorkingHourID = SingleResultSet.getString("WorkingHourID");
                    String InputEmployeeID = SingleResultSet.getString("EmployeeID");
                    String InputEmployeeName = SingleResultSet.getString("EmployeeName");
                    String InputProcessDesc = SingleResultSet.getString("ProcessDesc");
                    String InputStartDate = SingleResultSet.getString("StartDate");
                    String InputStartTime = SingleResultSet.getString("StartTime");
                    String InputEndDate = SingleResultSet.getString("EndDate");
                    String InputEndTime = SingleResultSet.getString("EndTime");
                    String InputWorkShiftType = SingleResultSet.getString("WorkShiftType");
                    String InputMachineID = SingleResultSet.getString("MachineID");
                    String InputWorkingTime = SingleResultSet.getString("WorkingTimes");
                    Log.e(TAG,"StartTime: " + GetStartTime(InputStartTime) );
                    if(InputWorkingHourID.equals("B01")){
                        String InputMachineIDB = SingleResultSet.getString("MachineIDB");
                        String InputMachineDesc = SingleResultSet.getString("MachineDesc");
                        Over_Cards.add(new item_OtherTime_card(InputOrderID,
                                InputSerialNumber,
                                InputWorkingHourID,
                                InputEmployeeID,
                                InputEmployeeName,
                                InputProcessDesc,
                                InputStartDate,
                                GetStartTime(InputStartTime),
                                InputEndDate,
                                GetStartTime(InputEndTime),
                                InputMachineIDB,
                                InputMachineDesc,
                                InputWorkShiftType,
                                InputMachineID,
                                InputWorkingTime));
                        Log.v(TAG,InputWorkingHourID);
                    }else{
                        Over_Cards.add(new item_OtherTime_card(InputOrderID,
                                InputSerialNumber,
                                InputWorkingHourID,
                                InputEmployeeID,
                                InputEmployeeName,
                                InputProcessDesc,
                                InputStartDate,
                                GetStartTime(InputStartTime),
                                InputEndDate,
                                GetStartTime(InputEndTime),
                                InputWorkShiftType,
                                InputMachineID,
                                InputWorkingTime));
                        Log.v(TAG,InputWorkingHourID);
                    }

                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }

    public static OverCompleteOtherTimeActivity newInstance(String param, String OrderID, String OrderNoBarCode, String PassWord){
        OverCompleteOtherTimeActivity overCompleteOtherTimeActivity = new OverCompleteOtherTimeActivity();
        Bundle args = new Bundle();
        args.putString("args1", param);
        args.putString("OrderID",OrderID);
        args.putString("OrderNoBarCode",OrderNoBarCode);
        args.putString("PassWord",PassWord);
        overCompleteOtherTimeActivity.setArguments(args);
        return overCompleteOtherTimeActivity;
    }


    private String GetStartTime(String Time){
        try {
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Time);
            return String.format("%tR", date);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
