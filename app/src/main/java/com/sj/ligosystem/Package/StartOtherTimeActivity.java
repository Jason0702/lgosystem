package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.sj.ligosystem.Control.NumberSort;
import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_start_cardAdapter;
import com.sj.ligosystem.Model.Machine;
import com.sj.ligosystem.Model.User;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.Model.item_start_card;
import com.sj.ligosystem.R;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;
//其他工時開工(單號選擇)
public class StartOtherTimeActivity extends AppCompatActivity {

    final String TAG = "Start_Other_Time";

    //region UI物件
    Toolbar toolbar;
    ImageView Calendar_View,UserCheck,MachineCheck,AddSingle_number_View;
    EditText InputUser,InputMachine;
    EditText InputDay;
    RecyclerView recyclerView;
    Button Leave_Button,Overtime_Button,Ordinary_Button;
    TextView Date_TextView, CardID_TextView, Machine_TextView, UserNameView, MachineNameView;
    Button Search_Button;
    //endregion

    //region
    Intent intent = new Intent();
    int year,month,day,hour;
    boolean isUserBeing, isMachineBeing;
    int ClickPosition;
    String MachineID, MachineName, UserID, UserName, UserPassWord;
    String PutMachineID, PutCardID, PutDate;
    List<item_start_card> Single_number_cards = new ArrayList();
    item_start_cardAdapter adapter;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_other_time);
        initView();
        initControl();
    }
    @Override
    protected void onStart() {
        super.onStart();
        //region 取得系統日期
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
        day  = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.e(TAG,"HR:"+hour);
        //endregion
        //region 顯示年月日
        if(hour == 6) {
            int LessDay = day - 1;
            InputDay.setText(String.format("%04d",year) + "/" + String.format("%02d",month) + "/" + String.format("%02d",LessDay));
            DayTime(year, month - 1, LessDay);
        }else{
            InputDay.setText(String.format("%04d",year) + "/" + String.format("%02d",month) + "/" + String.format("%02d",day));
            DayTime(year, month - 1, day);
        }
        //endregion

        //region 取得新增單號內容
        Intent GetAddIndex = getIntent();
        if(GetAddIndex.getExtras()!= null){
           if(GetAddIndex.getStringExtra("Type").equals("2")){
                Log.e(TAG, "In");
                PutMachineID = GetAddIndex.getStringExtra("MachineID");
                PutDate = GetAddIndex.getStringExtra("DayTime");
                InputDay.setText(PutDate);
                PutCardID = GetAddIndex.getStringExtra("UserID");
                InputUser.requestFocus();
                InputUser.setText(PutCardID);
                InputMachine.requestFocus();
                InputMachine.setText(PutMachineID);
                InputUser.clearFocus();
                InputMachine.clearFocus();
                SearchRefreshCardView();
            }
        }
        //endregion
    }
    private void initView(){
        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion
        //region TextView
        Date_TextView = findViewById(R.id.SO_Date_TextView);
        CardID_TextView = findViewById(R.id.SO_CardID_TextView);
        Machine_TextView = findViewById(R.id.SO_Machine_TextView);
        UserNameView = findViewById(R.id.SO_UserName_View);
        MachineNameView = findViewById(R.id.SO_MachineName_View);
        //endregion

        //region CheckImage
        UserCheck = findViewById(R.id.SO_UserCheck_imageView);
        MachineCheck = findViewById(R.id.SO_MachineCheck_imageView);
        UserCheck.setVisibility(View.GONE);
        MachineCheck.setVisibility(View.GONE);
        //endregion

        //region 日曆
        Calendar_View = findViewById(R.id.SO_Calendar_View);
        Calendar_View.setOnClickListener(Click);
        //endregion

        //region 輸入框
        //region 日期
        InputDay = findViewById(R.id.SO_InputDay_View);
        InputDay.setEnabled(false);
        //endregion

        //region 卡號
        //判斷是否有員工
        isUserBeing = false;

        InputUser = findViewById(R.id.SO_InputUser_View);

        InputUser.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL){
                isUserBeing = false;
                ClearEditView();

            }
            return false;
        });

        InputUser.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                UserCheck.setVisibility(View.GONE);
                UserNameView.setText("");
                ClearCardView();
            } else {
                for(User user : Util.AllUserList) {
                    if (!InputUser.getText().toString().toUpperCase().trim().equals(user.getUserID()) || InputUser.getText().toString().isEmpty()) {
                        if(!InputUser.getText().toString().isEmpty()){
                            InputUser.setError("無此員工");
                        }else{
                            InputUser.setError("請輸入卡號");
                        }
                    } else {
                        Log.e(TAG,"選中的員工: "+ user.getUserID() + " " +user.getUserName());
                        InputUser.setText(user.getUserID());
                        InputUser.setSelection(user.getUserID().length());
                        UserNameView.setText(user.getUserName());

                        UserID = user.getUserID();
                        UserName = user.getUserName();
                        UserPassWord = user.getUserPassWord();
                        PutCardID = user.getUserID();

                        isUserBeing = true;
                        //關閉Error提示
                        InputUser.setError(null,null);
                        UserCheck.setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }
        });

        //endregion

        //region 機台
        //判斷是否有機台
        isMachineBeing = false;

        InputMachine = findViewById(R.id.SO_InputMachine_View);

        InputMachine.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                MachineCheck.setVisibility(View.GONE);
                MachineNameView.setText("");
                ClearCardView();
            } else {
                for(Machine machine : Util.AllMachine){
                    if(!InputMachine.getText().toString().toUpperCase().trim().equals(machine.getMachineID()) || InputMachine.getText().toString().isEmpty()){
                        if(!InputMachine.getText().toString().isEmpty()) {
                            InputMachine.setError("無此機台");
                            MachineNameView.setText("");
                            isMachineBeing = false;
                        }else{
                            InputMachine.setError("請輸入機台編號");
                            MachineNameView.setText("");
                            isMachineBeing = false;
                        }
                    } else {
                        InputMachine.setText(machine.getMachineID());
                        InputMachine.setSelection(machine.getMachineID().length());
                        MachineNameView.setText(machine.getMachineName());

                        MachineID = machine.getMachineID();
                        MachineName = machine.getMachineName();
                        PutMachineID = machine.getMachineID();

                        isMachineBeing = true;
                        //關閉Error提示
                        InputMachine.setError(null,null);
                        MachineCheck.setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }
        });
        //endregion
        //endregion

        //region 列表
        recyclerView = findViewById(R.id.Start_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        adapter = new item_start_cardAdapter(this,Single_number_cards);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((view, position) -> {
            ClickPosition = position;
            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();
        });

        //endregion

        //region 按鈕
        Leave_Button = findViewById(R.id.SO_Leave_Button);
        Ordinary_Button = findViewById(R.id.SO_Ordinary_Button);
        Overtime_Button = findViewById(R.id.SO_Overtime_Button);

        Leave_Button.setOnClickListener(Click);
        Ordinary_Button.setOnClickListener(Click);
        Overtime_Button.setOnClickListener(Click);


        //endregion

        //region ImageButton
        Search_Button = findViewById(R.id.SO_Search_Button);
        Search_Button.setOnClickListener(Click);
        //endregion

        //region 新增單號
        AddSingle_number_View = findViewById(R.id.SO_Add_single_number_View);
        AddSingle_number_View.setOnClickListener(Click);
        //endregion

    }
    void initControl(){
        ClickPosition = 0;
        if(SelectedNavItem.getSlectedNavItem()!= 0){
            SelectedNavItem.setSlectedNavItem(0);
        }
    }
    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.SO_Calendar_View:
                OpenPopCalender(v);
                break;
            case R.id.SO_Add_single_number_View:
                InputMachine.clearFocus();
                InputUser.clearFocus();
                if((isMachineBeing && isUserBeing)) {
                    GoAddSingnumber();
                }else{
                    if(!isUserBeing){
                        InputUser.requestFocus();
                        Toast.makeText(this, "請確認卡號是否輸入正確!", Toast.LENGTH_SHORT).show();
                    }else{
                        InputMachine.requestFocus();
                        Toast.makeText(this, "請確認機台是否輸入正確!", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.SO_Leave_Button:
                finish();
                break;
            case R.id.SO_Ordinary_Button:
                //正常班
                InputMachine.clearFocus();
                InputUser.clearFocus();
                if((isMachineBeing && isUserBeing) && isUserInSingle()) {
                    GoOrdinary();
                }else if(!isUserBeing){
                    InputUser.requestFocus();
                    Toast.makeText(this, "請確認卡號是否輸入正確!", Toast.LENGTH_SHORT).show();
                }else if(!isMachineBeing){
                    InputMachine.requestFocus();
                    Toast.makeText(this, "請確認機台是否輸入正確!", Toast.LENGTH_SHORT).show();
                }else if(!isUserInSingle()){
                    Toast.makeText(getApplicationContext(),"卡號不屬於此報工單號作業員，請重新選擇輸入！",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.SO_Overtime_Button:
                InputMachine.clearFocus();
                InputUser.clearFocus();
                if((isMachineBeing && isUserBeing) && isUserInSingle()) {
                    GoOvertime();
                }else if(!isUserBeing){
                    InputUser.requestFocus();
                    Toast.makeText(this, "請確認卡號是否輸入正確!", Toast.LENGTH_SHORT).show();
                }else if(!isMachineBeing){
                    InputMachine.requestFocus();
                    Toast.makeText(this, "請確認機台是否輸入正確!", Toast.LENGTH_SHORT).show();
                }else if(!isUserInSingle()){
                    Toast.makeText(getApplicationContext(),"卡號不屬於此報工單號作業員，請重新選擇輸入！",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.SO_Search_Button:
                hideInput(getApplicationContext(),v);
                ClearCardView();
                InputMachine.clearFocus();
                InputUser.clearFocus();
                SearchRefreshCardView();
                break;
        }
    };
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    private void OpenPopCalender(View v){
        hideInput(getApplicationContext(),v);
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_popcalendar,null,false);
        CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAsDropDown(v,0,0);

        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            InputDay.setText("");
            InputDay.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",dayOfMonth));
            DayTime(year,month,dayOfMonth);

            popupWindow.dismiss();
        });
    }
    private void DayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        PutDate = sdf.format(new Date(eventOccursOn));
        Log.e(TAG,"Date: " + PutDate);
    }

    private void GoAddSingnumber(){
        intent.setClass(this,AddSingNumberActivity.class);
        intent.putExtra("DayTime",PutDate);
        intent.putExtra("Type","3");
        intent.putExtra("UserID",UserID);
        intent.putExtra("UserName", UserName);
        intent.putExtra("MachineID",MachineID);
        intent.putExtra("MachineName",MachineName);
        intent.putExtra("Date",InputDay.getText().toString());
        intent.putExtra("Password",UserPassWord);
        startActivity(intent);
        finish();
    }
    private void GoOrdinary(){
        intent.setClass(this,OtherTime_addActivity.class);
        //日期
        intent.putExtra("DayTime",PutDate);
        //User
        intent.putExtra("EmployeeID",UserID);
        intent.putExtra("EmployeeName",UserName);
        //班別
        intent.putExtra("StartWork", 0);
        //單號
        intent.putExtra("SingleNumber", Single_number_cards.get(ClickPosition).getNumber());
        //機台
        intent.putExtra("MachineID", InputMachine.getText().toString());
        intent.putExtra("MachineName", MachineName);
        //密碼
        intent.putExtra("Password", UserPassWord);
        //OrderNoBarCode
        intent.putExtra("OrderNoBarCode", Single_number_cards.get(ClickPosition).getOrderNoBarCode());
        //OrderID
        intent.putExtra("OrderID", Single_number_cards.get(ClickPosition).getOrderID());

        startActivity(intent);

        finish();
    }
    private void GoOvertime(){
        intent.setClass(this, OtherTime_addActivity.class);
        //日期
        intent.putExtra("DayTime",PutDate);
        //User
        intent.putExtra("EmployeeID",UserID);
        intent.putExtra("EmployeeName",UserName);
        //班別
        intent.putExtra("StartWork", 1);
        //單號
        intent.putExtra("SingleNumber", Single_number_cards.get(ClickPosition).getNumber());
        //機台
        intent.putExtra("MachineID", InputMachine.getText().toString());
        intent.putExtra("MachineName", MachineName);
        //密碼
        intent.putExtra("Password", UserPassWord);
        //OrderNoBarCode
        intent.putExtra("OrderNoBarCode", Single_number_cards.get(ClickPosition).getOrderNoBarCode());
        //OrderID
        intent.putExtra("OrderID", Single_number_cards.get(ClickPosition).getOrderID());

        startActivity(intent);

        finish();
    }

    //region 報工單號取得與顯示
    private void GetCardViewcontent(){
        try {
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                Log.e(TAG,"CAR: "+PutCardID+" MAC: "+PutMachineID+ "DAT" +PutDate);
                //Change below query according to your own database.
                Statement statement = connection.createStatement();

                String Single;

                //region 取得OrderID
                if(InputMachine.getText().toString().equals("00")){
                    Log.e(TAG,"這是機台00");
                    Single =
                            "SELECT"+
                                    " tblOManufactureDaily.OrderID, tblOManufactureDaily.OrderID, tblOManufactureDaily.OrderNo, tblOManufactureDaily.OrderSN, tblOManufactureDaily.OrderNoBarCode, tblOManufactureDaily.ManufactureDate, tblOManufactureDaily.ManufactureDailyType, tblOManufactureDaily.TeamLeaderEmployeeID, tblOManufactureDaily.WorkShiftType, tblOManufactureDaily.MachineID, tblOManufactureDaily.ProcessID, tblOManufactureDaily.CreateUserNo, tblOManufactureDaily.CreateUserName"+
                                    " FROM tblOManufactureDaily"+
                                    " WHERE (((tblOManufactureDaily.ManufactureDate) = '"+ PutDate +"') And ((tblOManufactureDaily.MachineID) = '"+PutMachineID+"')) AND tblOManufactureDaily.CreateUserNo='"+PutCardID+"'";
                }else{
                    Log.e(TAG,"這是其他機台");
                    Single =
                            "SELECT"+
                                    " tblOManufactureDaily.OrderID, tblOManufactureDaily.OrderID, tblOManufactureDaily.OrderNo, tblOManufactureDaily.OrderSN, tblOManufactureDaily.OrderNoBarCode, tblOManufactureDaily.ManufactureDate, tblOManufactureDaily.ManufactureDailyType, tblOManufactureDaily.TeamLeaderEmployeeID, tblOManufactureDaily.WorkShiftType, tblOManufactureDaily.MachineID, tblOManufactureDaily.ProcessID, tblOManufactureDaily.CreateUserNo, tblOManufactureDaily.CreateUserName"+
                                    " FROM tblOManufactureDaily"+
                                    " WHERE (((tblOManufactureDaily.ManufactureDate) = '"+ PutDate +"') And ((tblOManufactureDaily.MachineID) = '"+PutMachineID+"'))";
                }
                //endregion

                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            ArrayList<String> list = new ArrayList<>();
                            Log.e(TAG,"ID: "+ SingleResultSet.getString("OrderID"));
                            int OrderID = SingleResultSet.getInt("OrderID");
                            String OrderNO = SingleResultSet.getString("OrderNo");
                            String OrderSN = SingleResultSet.getString("OrderSN");
                            String OrderNoBarCode = SingleResultSet.getString("OrderNoBarCode");
                            String SingNumber = OrderNO + "-" + OrderSN;
                            String InputUserID = SingleResultSet.getString("CreateUserNo");
                            String InputUser = SingleResultSet.getString("CreateUserName");
                            Log.e(TAG,"單號: "+ SingNumber + "輸入: " + InputUser);
                            //region 取得作業員
                            Statement statementE = connection.createStatement();
                            ResultSet getEmployeeSet = statementE.executeQuery("SELECT"+
                                    " tblOManufactureDailyEmployee.EmployeeID, tblBEmployee.EmployeeName"+
                                    " FROM tblOManufactureDailyEmployee INNER JOIN tblBEmployee ON tblOManufactureDailyEmployee.EmployeeID = tblBEmployee.EmployeeID"+
                                    " WHERE (((tblOManufactureDailyEmployee.OrderID)="+ SingleResultSet.getString("OrderID") +"))"+
                                    " ORDER BY tblOManufactureDailyEmployee.EmployeeID");
                            //region 取得組長
                            String TeamLeader = GetTeamLeader(SingleResultSet.getString("TeamLeaderEmployeeID"));
                            //endregion
                            if(getEmployeeSet != null){
                                while (getEmployeeSet.next()){
                                    try {
                                        Log.e(TAG,"作業員: "+getEmployeeSet.getString("EmployeeID") + "-" + getEmployeeSet.getString("EmployeeName"));
                                        list.add(getEmployeeSet.getString("EmployeeName").trim());
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        ErrorAlertDialog(e);
                                    }
                                }
                                Single_number_cards.add(new item_start_card(
                                        OrderID,
                                        OrderNO,
                                        OrderSN,
                                        SingNumber,
                                        OrderNoBarCode,
                                        TeamLeader,
                                        InputUserID,
                                        InputUser,
                                        list));
                                Collections.sort(Single_number_cards,new NumberSort());
                                adapter.notifyDataSetChanged();
                                recyclerView.setAdapter(adapter);
                            }

                            //endregion

                        }catch (Exception e){
                            e.printStackTrace();
                            ErrorAlertDialog(e);
                        }
                    }
                    if(Single_number_cards.size() != 0){

                    }else{

                        ShowToast();
                    }
                }

            }
        }catch (Exception e){
            e.printStackTrace();
            ErrorAlertDialog(e);
        }
    }
    //endregion

    //region 取得組長
    private String GetTeamLeader(String teamleader){
        for(User user : Util.AllUserList){
            if(teamleader.equals(user.getUserID())){
                Log.e(TAG,"組長: "+user.getUserName());
                return user.getUserName();
            }
        }
        return null;
    }
    //endregion
    //region 隱藏鍵盤
    private void hideInput(Context context, View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
    }
    //endregion
    //region 搜尋單號
    private void SearchRefreshCardView(){
        if(!InputUser.getText().toString().isEmpty() && !InputMachine.getText().toString().isEmpty() && !InputDay.getText().toString().isEmpty() && isMachineBeing && isUserBeing){
            GetCardViewcontent();
        }else if(!isUserBeing){
            Toast.makeText(getApplicationContext(),"請確認卡號是否輸入正確!",Toast.LENGTH_LONG).show();
            InputUser.requestFocus();
        }else if(!isMachineBeing){
            Toast.makeText(getApplicationContext(),"請確認機台是否輸入正確!",Toast.LENGTH_LONG).show();
            InputMachine.requestFocus();
        }
    }
    //endregion
    //region 清除單號選擇列表
    private void ClearCardView(){
        Single_number_cards.removeAll(Single_number_cards);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }
    //endregion
    //region 清除輸入框
    void ClearEditView(){
        InputUser.setText("");
        InputMachine.setText("");
        UserCheck.setVisibility(View.GONE);
        MachineCheck.setVisibility(View.GONE);
        MachineNameView.setText("");
        UserNameView.setText("");
    }
    //endregion
    //region 驗證
    private boolean isUserInSingle(){
        boolean isUserIn =false;
        try {
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                Log.e(TAG,"RID: "+Single_number_cards.get(ClickPosition).getOrderID());
                int OrderID = Single_number_cards.get(ClickPosition).getOrderID();
                //Change below query according to your own database.
                Statement statement = connection.createStatement();

                String Single;

                Single = "SELECT"+
                        " tblOManufactureDailyEmployee.EmployeeID, tblBEmployee.EmployeeName"+
                        " FROM tblOManufactureDailyEmployee INNER JOIN tblBEmployee ON tblOManufactureDailyEmployee.EmployeeID = tblBEmployee.EmployeeID"+
                        " WHERE tblOManufactureDailyEmployee.OrderID="+ OrderID +
                        " ORDER BY tblOManufactureDailyEmployee.EmployeeID";

                ResultSet SingleResultSet = statement.executeQuery(Single);
                if(SingleResultSet != null){
                    while (SingleResultSet.next()){
                        try {
                            if(SingleResultSet.getString("EmployeeID").equals(PutCardID)){
                                isUserIn = true;
                                break;
                            }else{
                                isUserIn = false;
                            }
                        }catch (IndexOutOfBoundsException e){
                            e.printStackTrace();

                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return isUserIn;
    }
    //endregion
    //region 對話框
    private void ErrorAlertDialog(Exception error){
        new AlertDialog.Builder(StartOtherTimeActivity.this)
                .setTitle("Error")
                .setMessage(error.getMessage())
                .setPositiveButton("關閉視窗", (dialog, which) -> dialog.dismiss())
                .show();
    }
    //endregion
    //region Toast
    void ShowToast(){
        //region Toast
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("目前沒有單號!");
        toast.show();
        //endregion
    }
    //endregion
}
