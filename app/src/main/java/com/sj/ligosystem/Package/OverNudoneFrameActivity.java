package com.sj.ligosystem.Package;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Control.BaseFragment;
import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_over_frame_nudone_Adapter;
import com.sj.ligosystem.Model.item_frame_card;
import com.sj.ligosystem.R;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;

public class OverNudoneFrameActivity extends BaseFragment {
    final String TAG = "OverNudoneFrame";

    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    private String Single,InputRate;
    private int ClickPosition;
    List<item_frame_card> Over_Cards = new ArrayList<>();
    item_over_frame_nudone_Adapter adapter;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mView == null){
            //需要inflate一個次局文件,填充Fragment
            mView = inflater.inflate(R.layout.over_frame_nudone,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();
        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if(parent != null){
            parent.removeView(mView);
        }
        return mView;
    }
    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        String OrderID = bundle.getString("OrderID");
        String OrderNoBarCode = bundle.getString("OrderNoBarCode");
        String PassWord = bundle.getString("PassWord");


        //region 列表
        recyclerView = mView.findViewById(R.id.ON_RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new item_over_frame_nudone_Adapter(getContext(),Over_Cards);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((view, position) -> {
            ClickPosition = position;
            Log.e("Nudone", String.valueOf(ClickPosition));
            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();
            Intent intent = new Intent();
            intent.setClass(getContext(),OverFrameAddActivity.class);
            intent.putExtra("Type","未完工");
            intent.putExtra("OrderID", Over_Cards.get(position).getOrderID());
            intent.putExtra("SerialNumber", Over_Cards.get(position).getSerialNumber());
            intent.putExtra("OrderNoBarCode", OrderNoBarCode);
            intent.putExtra("MachineID", Over_Cards.get(position).getMachineID());
            intent.putExtra("MachineDesc", Over_Cards.get(position).getMachineDesc());

            intent.putExtra("EmployeeID", Over_Cards.get(position).getEmployeeID());
            intent.putExtra("EmployeeName", Over_Cards.get(position).getEmployeeName());
            intent.putExtra("PassID", Over_Cards.get(position).getProcessID());
            intent.putExtra("PassDesc", Over_Cards.get(position).getProcessDesc());

            intent.putExtra("ManufactureOrderID", Over_Cards.get(position).getManufactureOrderID().trim());
            intent.putExtra("ProductsTypeID", Over_Cards.get(position).getProductsTypeID().trim());
            intent.putExtra("SizeID", Over_Cards.get(position).getSizeID().trim());
            intent.putExtra("ThicknessID", Over_Cards.get(position).getThicknessID().trim());

            intent.putExtra("BManufactureOrderID", Over_Cards.get(position).getBManufactureOrderID().trim());
            intent.putExtra("BProductsTypeID", Over_Cards.get(position).getBProductsTypeID().trim());
            intent.putExtra("BSizeID", Over_Cards.get(position).getBSizeID().trim());
            intent.putExtra("BThicknessID", Over_Cards.get(position).getBThicknessID().trim());

            intent.putExtra("StartDate", Over_Cards.get(position).getStartDate());
            intent.putExtra("StartTime", Over_Cards.get(position).getStartTime());
            intent.putExtra("EndDate", Over_Cards.get(position).getEndDate());
            intent.putExtra("EndTime", Over_Cards.get(position).getEndTime());

            intent.putExtra("Rate", Over_Cards.get(position).getRate());
            intent.putExtra("WorkingTimes",Over_Cards.get(position).getWorkingTimes());
            intent.putExtra("StandardIDItemNo",Over_Cards.get(position).getWorkingTimes());
            intent.putExtra("WorkingDescID",Over_Cards.get(position).getWorkingDescID());

            intent.putExtra("PassWord",PassWord);

            Log.e(TAG, "WorkingHourID"+Over_Cards.get(position).getWorkingHourID());
            Log.e(TAG, "WorkShiftType"+Over_Cards.get(position).getWorkShiftType());
            Log.e(TAG, "StandardID"+Over_Cards.get(position).getStandardID());
            Log.e(TAG, "SizeQty"+Over_Cards.get(position).getSizeQty());
            Log.e(TAG, "BSizeQty"+Over_Cards.get(position).getBSizeQty());

            Log.e(TAG, "WorkingTimesB"+ Over_Cards.get(position).getWorkingTimesB());
            Log.e(TAG, "Amount"+Over_Cards.get(position).getAmount());
            Log.e(TAG, "StandardIDItemNo"+ Over_Cards.get(position).getStandardIDItemNo());
            Log.e(TAG, "BStandardIDItemNo"+ Over_Cards.get(position).getBStandardIDItemNo());
            Log.e(TAG, "AmendDate"+ Over_Cards.get(position).getAmendDate());

            Log.e(TAG, "WorkHoursStatisticsTypeID"+ Over_Cards.get(position).getWorkHoursStatisticsTypeID());

            intent.putExtra("WorkingHourID",Over_Cards.get(position).getWorkingHourID());
            intent.putExtra("WorkShiftType",Over_Cards.get(position).getWorkShiftType());
            intent.putExtra("StandardID",Over_Cards.get(position).getStandardID());
            intent.putExtra("SizeQty",Over_Cards.get(position).getSizeQty());
            intent.putExtra("BSizeQty",Over_Cards.get(position).getBSizeQty());

            intent.putExtra("WorkingTimesB", Over_Cards.get(position).getWorkingTimesB());

            intent.putExtra("Amount",Over_Cards.get(position).getAmount());
            intent.putExtra("StandardIDItemNo", Over_Cards.get(position).getStandardIDItemNo());
            intent.putExtra("BStandardIDItemNo", Over_Cards.get(position).getBStandardIDItemNo());
            intent.putExtra("AmendDate", Over_Cards.get(position).getAmendDate());

            intent.putExtra("WorkHoursStatisticsTypeID", Over_Cards.get(position).getWorkHoursStatisticsTypeID());


            Log.e(TAG, "EmployeeName" + Over_Cards.get(position).getEmployeeName());
            startActivity(intent);
            getActivity().finish();
        });

        //endregion
        GetUnfinished(OrderID);
    }
    void GetRecyclerContent(String OrderID, String SerialNumber){
        try{
            Statement statement = connection.createStatement();
            String Single = "SELECT tblOManufactureDailyExchangeMoldWorkDesc.*, tblBExchangeMoldWorkingDescDetails.WorkingDesc" +
                    " FROM tblOManufactureDailyExchangeMoldWorkDesc INNER JOIN tblBExchangeMoldWorkingDescDetails ON (tblOManufactureDailyExchangeMoldWorkDesc.StandardIDItemNo = tblBExchangeMoldWorkingDescDetails.StandardIDItemNo) AND (tblOManufactureDailyExchangeMoldWorkDesc.WorkingDescID = tblBExchangeMoldWorkingDescDetails.WorkingDescID)" +
                    " WHERE tblOManufactureDailyExchangeMoldWorkDesc.OrderID = "+OrderID+" AND tblOManufactureDailyExchangeMoldWorkDesc.SerialNumber = "+SerialNumber+
                    " ORDER BY tblOManufactureDailyExchangeMoldWorkDesc.StandardIDItemNo;";
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){
                    InputRate = String.valueOf(SingleResultSet.getFloat("Rate"));
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    void GetUnfinished(String OrderID){
        try {
            Statement statement = connection.createStatement();
            Single ="SELECT tblOManufactureDailyExchangeMold.*, tblBProcess.ProcessDesc, tblBEmployee.EmployeeName, tblBMachine.MachineDesc, tblBProductsSizeCaption.SizeCaption, tblBProductsSizeCaption_1.SizeCaption AS BSizeCaption"+
            " FROM ((tblBMachine INNER JOIN (tblBProcess INNER JOIN (tblBEmployee INNER JOIN tblOManufactureDailyExchangeMold ON tblBEmployee.EmployeeID = tblOManufactureDailyExchangeMold.EmployeeID) ON tblBProcess.ProcessID = tblOManufactureDailyExchangeMold.ProcessID) ON tblBMachine.MachineID = tblOManufactureDailyExchangeMold.MachineID) LEFT JOIN tblBProductsSizeCaption ON (tblOManufactureDailyExchangeMold.SizeQty = tblBProductsSizeCaption.SizeQty) AND (tblOManufactureDailyExchangeMold.SizeID = tblBProductsSizeCaption.SizeID)) LEFT JOIN tblBProductsSizeCaption AS tblBProductsSizeCaption_1 ON (tblOManufactureDailyExchangeMold.BSizeQty = tblBProductsSizeCaption_1.SizeQty) AND (tblOManufactureDailyExchangeMold.BSizeID = tblBProductsSizeCaption_1.SizeID)"+
            " WHERE tblOManufactureDailyExchangeMold.WorkingTimes = 0 AND tblOManufactureDailyExchangeMold.OrderID ="+OrderID+
            " ORDER BY tblOManufactureDailyExchangeMold.SerialNumber";

            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){

                    String InputOrderID = SingleResultSet.getString("OrderID");
                    String InputSerialNumber = SingleResultSet.getString("SerialNumber");
                    GetRecyclerContent(InputOrderID,InputSerialNumber);
                    String InputEmployeeID = SingleResultSet.getString("EmployeeID");
                    String InputWorkingHourID = SingleResultSet.getString("WorkingHourID");
                    String InputManufactureOrderID = SingleResultSet.getString("ManufactureOrderID");
                    String InputMachineID = SingleResultSet.getString("MachineID");
                    String InputProcessID = SingleResultSet.getString("ProcessID");
                    String InputHourAmounnt = SingleResultSet.getString("HourAmount");
                    String InputWorkShiftype = SingleResultSet.getString("WorkShiftType");
                    String InputStartDate = SingleResultSet.getString("StartDate");
                    String InputStartTime = SingleResultSet.getString("StartTime");

                    String InputEndDate = SingleResultSet.getString("EndDate");
                    String InputEndTime = SingleResultSet.getString("EndTime");
                    String InputWorkingTimes = SingleResultSet.getString("WorkingTimes");
                    String InputRestTimes = SingleResultSet.getString("RestTimes");
                    String InputProductsTypeID = SingleResultSet.getString("ProductsTypeID");
                    String InputSizeID = SingleResultSet.getString("SizeID");

                    String InputThicknessID = SingleResultSet.getString("ThicknessID");
                    String InputBProductsTypeID = SingleResultSet.getString("BProductsTypeID");
                    String InputBSizeID = SingleResultSet.getString("BSizeID");
                    String InputBThicknessID = SingleResultSet.getString("BThicknessID");
                    String InputBManufactureOrderID = SingleResultSet.getString("BManufactureOrderID");

                    String InputWorkingDescID = SingleResultSet.getString("WorkingDescID");
                    String InputStandardID = SingleResultSet.getString("StandardID");
                    String InputSizeQty = SingleResultSet.getString("SizeQty");
                    String InputBSizeQty = SingleResultSet.getString("BSizeQty");

                    String InputWorkingTimesB = SingleResultSet.getString("WorkingTimesB");
                    Log.e("WorkingTimesB",InputWorkingTimesB);
                    String InputAmount = SingleResultSet.getString("Amount");
                    String InputStandardIDItemNo = SingleResultSet.getString("StandardIDItemNo");
                    String InputBStandardIDItemNo = SingleResultSet.getString("BStandardIDItemNo");
                    String InputAmendDate = SingleResultSet.getString("AmendDate");

                    String InputWorkHoursStatisticsTypeID = SingleResultSet.getString("WorkHoursStatisticsTypeID");

                    String InputProcessDesc = SingleResultSet.getString("ProcessDesc");
                    String InputEmployeeName = SingleResultSet.getString("EmployeeName");
                    String InputMachineDesc = SingleResultSet.getString("MachineDesc");
                    String InputSizeCaption = SingleResultSet.getString("SizeCaption");
                    String InputBSizeCaption = SingleResultSet.getString("BSizeCaption");

                    Over_Cards.add(new item_frame_card(
                            InputOrderID,
                            InputSerialNumber,
                            InputEmployeeID,
                            InputWorkingHourID,
                            InputManufactureOrderID,
                            InputMachineID,
                            InputProcessID,
                            InputWorkShiftype,
                            InputStartDate,
                            GetStartTime(InputStartTime),
                            InputEndDate,
                            GetStartTime(InputEndTime),
                            InputWorkingTimes,
                            InputProductsTypeID,
                            InputSizeID,
                            InputThicknessID,
                            InputBProductsTypeID,
                            InputBSizeID,
                            InputBThicknessID,
                            InputBManufactureOrderID,
                            InputRate,
                            InputWorkingDescID,
                            InputStandardID,
                            InputSizeQty,
                            InputBSizeQty,
                            InputWorkingTimesB,
                            InputAmount,
                            InputStandardIDItemNo,
                            InputBStandardIDItemNo,
                            InputAmendDate,
                            InputWorkHoursStatisticsTypeID,
                            InputEmployeeName,
                            InputProcessDesc,
                            InputMachineDesc,
                            InputSizeCaption,
                            InputBSizeCaption
                    ));
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static OverNudoneFrameActivity newInstance(String param, String OrderID, String OrderNoBarCode, String PassWord){
        OverNudoneFrameActivity overNudoneFrameActivity = new OverNudoneFrameActivity();
        Bundle args = new Bundle();
        args.putString("args1", param);
        args.putString("OrderID",OrderID);
        args.putString("OrderNoBarCode",OrderNoBarCode);
        args.putString("PassWord",PassWord);
        overNudoneFrameActivity.setArguments(args);
        return overNudoneFrameActivity;
    }

    private String GetStartTime(String Time){
        try {
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Time);
            String Stat = String.format("%tR", date);
            return Stat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }
}
