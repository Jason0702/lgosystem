package com.sj.ligosystem.Package;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_operator_Adapter;
import com.sj.ligosystem.Control.item_start_cardAdapter;
import com.sj.ligosystem.Model.User;
import com.sj.ligosystem.Model.Util;
import com.sj.ligosystem.Model.item_operator;
import com.sj.ligosystem.Model.item_start_card;
import com.sj.ligosystem.R;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.sj.ligosystem.MainActivity.connection;
/**
 * 作業員新增(明細輸入)
 **/
public class Operator_detailsActivity extends AppCompatActivity {
    final String TAG = "OD";

    Toolbar toolbar;
    TextView OrderNoBarCode, EmployeeNameText;
    EditText PassWordEdit, EmployeeEdit;
    Button AddEmployeeButton,DeleteEmployeeButton,LeaveButton;
    RecyclerView recyclerView;

    String EmployeeName,EmployeeID,CreateUserName;
    String PutDate;
    int OrderID;
    int ClickPosition;
    String now;
    String UserID, UserName, UserPassWord;
    boolean isUserBeing = false;
    item_operator_Adapter adapter;
    List<item_operator> item_operators = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_details);
        ClickPosition = 0;
        initView();
        initData();
    }

    void initView(){

        //region Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //endregion


        //region TextView
        OrderNoBarCode = findViewById(R.id.OD_OrderNoBarCode);
        EmployeeNameText = findViewById(R.id.OD_EmployeeName);
        //endregion

        //region EditView
        PassWordEdit = findViewById(R.id.OD_PassWordEdit);
        EmployeeEdit = findViewById(R.id.OD_EmployeeEdit);
        //endregion

        //region 按鈕
        AddEmployeeButton = findViewById(R.id.OD_AddEmployeeButton);
        AddEmployeeButton.setOnClickListener(Click);
        DeleteEmployeeButton = findViewById(R.id.OD_DeleteEmployeeButton);
        DeleteEmployeeButton.setOnClickListener(Click);
        LeaveButton = findViewById(R.id.OD_LeaveButton);
        LeaveButton.setOnClickListener(Click);
        //endregion

        //region 卡號
        //判斷是否有員工
        isUserBeing = false;
        EmployeeEdit.clearFocus();

        EmployeeEdit.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL){
                isUserBeing = false;
            }
            return false;
        });

        EmployeeEdit.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                EmployeeNameText.setText("");
            }else{
                runOnUiThread(()->{
                    for(User user : Util.AllUserList) {
                        if (!EmployeeEdit.getText().toString().toUpperCase().trim().equals(user.getUserID()) || EmployeeEdit.getText().toString().isEmpty()) {
                            if(EmployeeEdit.getText().toString().isEmpty()){
                                EmployeeEdit.setError("無此員工");
                                isUserBeing = false;
                            }else{
                                isUserBeing = false;
                            }
                        } else {
                            Log.e(TAG,"選中的員工: "+ user.getUserID() + " " +user.getUserName());
                            EmployeeEdit.setText(user.getUserID());
                            EmployeeEdit.setSelection(user.getUserID().length());

                            UserID = user.getUserID();
                            UserName = user.getUserName();

                            isUserBeing = true;

                            //關閉Error提示
                            EmployeeEdit.setError(null,null);
                            EmployeeNameText.setText(UserName);

                            break;
                        }
                    }
                });
            }
        });
        //endregion
        //region 列表
        recyclerView = findViewById(R.id.OD_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        adapter = new item_operator_Adapter(this,item_operators);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((view, position) -> {
            ClickPosition = position;
            Log.e(TAG, String.valueOf(ClickPosition));
            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();
        });

        //endregion
    }
    void initData(){
        Intent GetBforeData = getIntent();
        OrderNoBarCode.setText(GetBforeData.getStringExtra("OrderNo")+" - "+GetBforeData.getStringExtra("OrderSN"));
        OrderID = GetBforeData.getIntExtra("OrderID",0);
        EmployeeName = GetBforeData.getStringExtra("EmployeeName");
        EmployeeID = GetBforeData.getStringExtra("EmployeeID");
        UserPassWord = GetBforeData.getStringExtra("PassWord");
        CreateUserName = GetBforeData.getStringExtra("CreateUserName");
        PutDate = GetBforeData.getStringExtra("DayTime");
        GetEmployee();
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.OD_LeaveButton:
                finish();
                break;
            case R.id.OD_DeleteEmployeeButton:
                String ID = item_operators.get(ClickPosition).getEmployeeID();
                String Name = item_operators.get(ClickPosition).getEmployeeName();
                Log.e(TAG,item_operators.get(ClickPosition).getEmployeeName());
                if(!Name.equals(CreateUserName)) {
                    AlertDialog(ID, Name);
                }else{
                    NotDeleteAlertDialog("請勿刪除輸入者!");
                }

                break;
            case R.id.OD_AddEmployeeButton:
                EmployeeEdit.clearFocus();
                Log.e(TAG, UserPassWord);
                Log.e(TAG, String.valueOf(isUserBeing));
                if(isUserBeing && PassWordEdit.getText().toString().equals(UserPassWord)) {
                    AddEmployee();
                }else if(!isUserBeing){
                    Toast.makeText(getApplicationContext(),"請確認作業員編號是否輸入正確!",Toast.LENGTH_LONG).show();
                    EmployeeEdit.requestFocus();
                }else if(!PassWordEdit.getText().toString().equals(UserPassWord)){
                    Toast.makeText(getApplicationContext(),"請確認密碼是否輸入正確!",Toast.LENGTH_LONG).show();
                    PassWordEdit.requestFocus();
                }
                break;
        }
    };
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void Clear(){
        EmployeeEdit.setError(null,null);
        EmployeeNameText.setText("");
        EmployeeEdit.setText("");
        PassWordEdit.setText("");
        PassWordEdit.setError(null,null);
        isUserBeing = false;
    }
    void GetEmployee(){
        try {
            ArrayList<String> list = new ArrayList<>();
            Statement statementE = connection.createStatement();
            ResultSet getEmployeeSet = statementE.executeQuery("SELECT" +
                    " tblOManufactureDailyEmployee.EmployeeID, tblBEmployee.EmployeeName" +
                    " FROM tblOManufactureDailyEmployee INNER JOIN tblBEmployee ON tblOManufactureDailyEmployee.EmployeeID = tblBEmployee.EmployeeID" +
                    " WHERE (((tblOManufactureDailyEmployee.OrderID)=" + OrderID + "))" +
                    " ORDER BY tblOManufactureDailyEmployee.EmployeeID");
            if(getEmployeeSet != null) {
                while (getEmployeeSet.next()) {
                        String EmployeeID = getEmployeeSet.getString("EmployeeID");
                        String EmployeeName = getEmployeeSet.getString("EmployeeName");
                        Log.e(TAG, "作業員: " + EmployeeID + "-" + EmployeeName);
                        list.add(getEmployeeSet.getString("EmployeeName").trim());
                        item_operators.add(new item_operator(EmployeeID, EmployeeName.trim()));
                }
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);

            }
            }catch (Exception e){
            e.printStackTrace();
        }
    }
    void AddEmployee(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dt = new Date();
        now = sdf.format(dt);
        Log.e(TAG, "現在時間: " + now);
        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Add
                Single = "INSERT into tblOManufactureDailyEmployee (" +
                        " OrderID, EmployeeID, EmployeeName, CreateDate, CreateUserNo," +
                        " CreateUserName, ModifyDate, ModifyUserNo, ModifyUserName,SystemModifyDate) VALUES(?,?,?,?,?,?,?,?,?,?)";
                comm = connection.prepareStatement(Single);
                //region
                comm.setString(1,String.valueOf(OrderID));
                comm.setString(2,UserID);//輸入的人ID
                comm.setString(3,UserName);//輸入的人Name
                comm.setString(4,now);
                comm.setString(5,EmployeeID);
                //endregion
                //region
                comm.setString(6,EmployeeName);
                comm.setString(7,now);
                comm.setString(8,EmployeeID);
                comm.setString(9,EmployeeName);
                comm.setString(10,now);
                //endregion
                comm.executeUpdate();
                ShowToast();
                Clear();
            }
        }catch (SQLException e){
            e.printStackTrace();
            runOnUiThread(()-> Toast.makeText(getApplicationContext(),"請勿新增重複作業員!",Toast.LENGTH_LONG).show());
            Log.e(TAG,"資料內容錯誤");
        }
    }
    //region Toast
    void ShowToast(){
        //region Toast
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, findViewById(R.id.custom_toast));
        TextView text = layout.findViewById(R.id.text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        text.setText("新增成功!");
        toast.show();
        item_operators.clear();
        GetEmployee();
        //endregion
    }
    //endregion
    void DeleteEmployee(String ID){

        try{
            if(connection == null){
                Log.e(TAG,"connection NULL");
            }else{
                PreparedStatement comm;
                String Single;
                //Delete
                Single ="DELETE tblOManufactureDailyEmployee FROM tblOManufactureDailyEmployee WHERE tblOManufactureDailyEmployee.OrderID =" + OrderID + " AND tblOManufactureDailyEmployee.EmployeeID ='" + ID +"'";
                comm = connection.prepareStatement(Single);
                comm.executeUpdate();
                item_operators.remove(ClickPosition);
                adapter.notifyDataSetChanged();
            }
        }catch (SQLException e){
            e.printStackTrace();
            Log.e(TAG,"資料內容錯誤");
        }

    }
    //region刪除對話框
    private void AlertDialog(String ID, String Name){
        new AlertDialog.Builder(Operator_detailsActivity.this)
                .setTitle("刪除")
                .setMessage("確定要刪除 [ "+ID+" : "+Name+" ] 嗎?")
                .setPositiveButton("確定",((dialog, which) -> {
                    DeleteEmployee(ID);
                    dialog.dismiss();
                }))
                .setNegativeButton("取消",((dialog, which) -> dialog.dismiss()))
                .show();
    }
    //endregion
    //region刪除對話框
    private void NotDeleteAlertDialog(String Text){
        new AlertDialog.Builder(Operator_detailsActivity.this)
                .setTitle("刪除")
                .setMessage(Text)
                .setPositiveButton("確定",((dialog, which) -> {
                    dialog.dismiss();
                }))
                .show();
    }
    //endregion

    //region 對話框
    private void ErrorAlertDialog(Exception error){
        new AlertDialog.Builder(Operator_detailsActivity.this)
                .setTitle("Error")
                .setMessage(error.getMessage())
                .setPositiveButton("關閉視窗", (dialog, which) -> dialog.dismiss())
                .show();
    }
    //endregion
}
