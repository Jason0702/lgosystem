package com.sj.ligosystem.Package;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Control.BaseFragment;
import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_over_passinspecion_nudone_Adapter;
import com.sj.ligosystem.Model.PassInspection;
import com.sj.ligosystem.R;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;

public class OverPassInspectionNudoneActivity extends BaseFragment {
    final String TAG = "OverPassInspectionNudone";
    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    private String Single;
    private int ClickPosition;
    List<PassInspection> Over_Cards = new ArrayList<>();
    item_over_passinspecion_nudone_Adapter adapter;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.over_nudone,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();
        }
        //緩存的mView需要判斷是否已經被加載過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null){
            parent.removeView(mView);
        }
        return mView;
    }

    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        String OrderID = bundle.getString("OrderID");
        String PassWord = bundle.getString("PassWord");
        String EmployeeName = bundle.getString("EmployeeName");
        String EmployeeID = bundle.getString("EmployeeID");
        String MachineID = bundle.getString("MachineID");
        String MachineName = bundle.getString("MachineName");

        //region 列表
        recyclerView = mView.findViewById(R.id.ON_RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new item_over_passinspecion_nudone_Adapter(getContext(),Over_Cards);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((view, position) -> {
            ClickPosition = position;
            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();

            Intent intent = new Intent();
            intent.setClass(getContext(), VerifyPassInspectionAddActivity.class);
            intent.putExtra("PassWord", PassWord);
            intent.putExtra("OrderID", Over_Cards.get(position).getOrderID());
            intent.putExtra("SerialNumber", Over_Cards.get(position).getSerialNumber());
            intent.putExtra("OrderNoBarCode", Over_Cards.get(position).getOrderNoBarCode());
            intent.putExtra("ManufactureOrderID", Over_Cards.get(position).getManufactureOrderID());
            intent.putExtra("ProductsDescription", Over_Cards.get(position).getProductsDescription());
            intent.putExtra("MachineID", Over_Cards.get(position).getMachineID());
            intent.putExtra("ProcessID", Over_Cards.get(position).getProcessID());
            intent.putExtra("ProcessDesc", Over_Cards.get(position).getProcessDesc());

            intent.putExtra("WorkShiftType", Over_Cards.get(position).getWorkShiftType());
            intent.putExtra("StartDate", Over_Cards.get(position).getStartDate());
            intent.putExtra("StartTime", Over_Cards.get(position).getStartTime());
            intent.putExtra("EndDate", Over_Cards.get(position).getEndDate());
            intent.putExtra("EndTime", Over_Cards.get(position).getEndTime());

            intent.putExtra("ProductsTypeID", Over_Cards.get(position).getProductsTypeID());
            intent.putExtra("StandardRequestID", Over_Cards.get(position).getStandardRequestID());
            intent.putExtra("SeamTypeID", Over_Cards.get(position).getSeamTypeID());
            intent.putExtra("RawJobTypeID", Over_Cards.get(position).getRawJobTypeID());
            intent.putExtra("SaleTypeID", Over_Cards.get(position).getSaleTypeID());

            intent.putExtra("SizeID", Over_Cards.get(position).getSizeID());
            intent.putExtra("SizeID2",Over_Cards.get(position).getSizeID2());
            intent.putExtra("ThicknessID",Over_Cards.get(position).getThicknessID());
            intent.putExtra("ThicknessID2",Over_Cards.get(position).getThicknessID2());

            intent.putExtra("InspectionFormID", Over_Cards.get(position).getInspectionFormID());
            intent.putExtra("CompletedFormID", Over_Cards.get(position).getCompletedFormID());
            intent.putExtra("MaterialFormID", Over_Cards.get(position).getMaterialFormID());
            intent.putExtra("DiagramFormID", Over_Cards.get(position).getDiagramFormID());

            intent.putExtra("Qty", Over_Cards.get(position).getQty());
            intent.putExtra("StandardQty", Over_Cards.get(position).getStandardQty());
            intent.putExtra("Inoc", Over_Cards.get(position).getInoc());

            intent.putExtra("EmployeeName", EmployeeName);
            intent.putExtra("EmployeeID", EmployeeID);
            intent.putExtra("MachineID", MachineID);
            intent.putExtra("MachineName", MachineName);

            startActivity(intent);
            getActivity().finish();
           /* }else{
                new AlertDialog.Builder(getContext())
                        .setTitle("此工令產品道次無需檢驗(或未正確設定輸入畫面)！")
                        .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).create()
                        .show();
            }*/
        });

        //endregion
        GetUnfinished(OrderID);
    }
    void GetUnfinished(String OrderID){
        try {
            Statement statement = connection.createStatement();
            Single = "SELECT tblOManufactureDailyDetail.*, tblBProcess.ProcessDesc, [010工作通知單資料表].SizeID AS MOSizeID, [010工作通知單資料表].SizeID2 AS MOSizeID2, [010工作通知單資料表].ThicknessID AS MOThicknessID, [010工作通知單資料表].ThicknessID2 AS MOThicknessID2, [010工作通知單資料表].SeamTypeID AS MOSeamTypeID, [010工作通知單資料表].RawJobTypeID AS MORawJobTypeID, [010工作通知單資料表].SaleTypeID AS MOSaleTypeID, [010工作通知單資料表].客戶 "+
                    " FROM tblOManufactureDailyDetail INNER JOIN tblBProcess ON tblOManufactureDailyDetail.ProcessID = tblBProcess.ProcessID INNER JOIN [010工作通知單資料表] ON tblOManufactureDailyDetail.ManufactureOrderID = [010工作通知單資料表].工令編號 "+
                    " WHERE tblOManufactureDailyDetail.WorkingTimes = 0 AND tblOManufactureDailyDetail.OrderID ='"+OrderID+"'"+
                    " ORDER BY tblOManufactureDailyDetail.SerialNumber";
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){

                    String InputOrderID = SingleResultSet.getString("OrderID");
                    String InputSerialNumber = SingleResultSet.getString("SerialNumber");
                    String InputOrderNoBarCode = SingleResultSet.getString("OrderNoBarCode");
                    String InputManufactureOrderID = SingleResultSet.getString("ManufactureOrderID");
                    String InputProductsDescription = SingleResultSet.getString("ProductsDescription");
                    String InputMachineID = SingleResultSet.getString("MachineID");
                    String InputProcessID = SingleResultSet.getString("ProcessID");
                    String InputProcessDesc = SingleResultSet.getString("ProcessDesc");
                    String InputWorkShiftype = SingleResultSet.getString("WorkShiftType");
                    String InputStartDate = SingleResultSet.getString("StartDate");
                    String InputStartTime = SingleResultSet.getString("StartTime");
                    String InputEndDate = SingleResultSet.getString("EndDate");
                    String InputEndTime = SingleResultSet.getString("EndTime");

                    String InputProductsTypeID = SingleResultSet.getString("ProductsTypeID");
                    String InputStandardRequestID = SingleResultSet.getString("StandardRequestID");
                    String InputSeamTypeID = SingleResultSet.getString("SeamTypeID");
                    String InputRawJobTypeID = SingleResultSet.getString("RawJobTypeID");
                    String InputSaleTypeID = SingleResultSet.getString("SaleTypeID");

                    String SizeID = SingleResultSet.getString("MOSizeID");
                    String SizeID2 = SingleResultSet.getString("MOSizeID2");
                    String ThicknessID = SingleResultSet.getString("MOThicknessID");
                    String ThicknessID2 = SingleResultSet.getString("MOThicknessID2");

                    String InputInspectionFormID = SingleResultSet.getString("InspectionFormID");
                    String InputCompletedFormID = SingleResultSet.getString("CompletedFormID");
                    String InputMaterialFormID = SingleResultSet.getString("MaterialFormID");
                    String InputDiagramFormID = SingleResultSet.getString("DiagramFormID");

                    String InputInoc = SingleResultSet.getString("客戶") != null ? SingleResultSet.getString("客戶").toUpperCase() : "";

                    int Qty = SingleResultSet.getInt("ManufactureQty");

                    String [] InputStandardQty = SingleResultSet.getString("StandardQty").split("\\.");

                    Log.e("ThicknessID", SingleResultSet.getString("MOThicknessID"));
                    Log.e("ThicknessID2", SingleResultSet.getString("MOThicknessID2"));

                    Over_Cards.add(new PassInspection(InputOrderID, InputSerialNumber,InputOrderNoBarCode,InputManufactureOrderID,InputProductsDescription,
                            InputMachineID,InputProcessID,InputProcessDesc,InputWorkShiftype,GetStartDate(InputStartDate),GetStartTime(InputStartTime),GetStartDate(InputEndDate),
                            GetStartTime(InputEndTime),InputProductsTypeID,InputStandardRequestID,InputSeamTypeID,InputRawJobTypeID,InputSaleTypeID,
                            InputInspectionFormID,InputCompletedFormID,InputMaterialFormID,InputDiagramFormID, Qty,InputStandardQty[0], SizeID, SizeID2, ThicknessID, ThicknessID2, InputInoc));

                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private String GetStartTime(String Time){
        try {
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Time);
            String Stat = String.format("%tR", date);
            return Stat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    private String GetStartDate(String Date){
        try{
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Date);
            String Stat = String.format("%tF",date);
            String newStat = Stat.replace("-","/");
            return newStat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }

    public static OverPassInspectionNudoneActivity newInstance(String param, String OrderID, String PassWord, String EmployeeID, String EmployeeName, String MachineID, String MachineName){
        OverPassInspectionNudoneActivity overPassInspectionNudoneActivity = new OverPassInspectionNudoneActivity();
        Bundle args = new Bundle();
        args.putString("args1",param);
        args.putString("OrderID",OrderID);
        args.putString("PassWord",PassWord);

        args.putString("EmployeeName",EmployeeName);
        args.putString("EmployeeID",EmployeeID);
        args.putString("MachineID",MachineID);
        args.putString("MachineName",MachineName);

        overPassInspectionNudoneActivity.setArguments(args);
        return overPassInspectionNudoneActivity;
    }
}
