package com.sj.ligosystem.Package;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.ligosystem.Control.BaseFragment;
import com.sj.ligosystem.Control.SelectedNavItem;
import com.sj.ligosystem.Control.item_over_complete_Adapter;
import com.sj.ligosystem.Model.item_over_card;
import com.sj.ligosystem.R;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.sj.ligosystem.MainActivity.connection;

public class OverCompleteActivity extends BaseFragment {
    final String TAG = "OverCompleteActivity";
    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    private String Single;

    private int ClickPosition;
    List<item_over_card> Over_Cards = new ArrayList<>();
    item_over_complete_Adapter adapter;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.over_complete,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();
        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if(parent != null){
            parent.removeView(mView);
        }
        return mView;
    }
    /**
        ** 初始化物件
        **/
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        String OrderID = bundle.getString("OrderID");
        String EmployeeName = bundle.getString("EmployeeName");
        //region 列表
        recyclerView = mView.findViewById(R.id.OC_RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new item_over_complete_Adapter(getContext(),Over_Cards);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((view, position) -> {
            ClickPosition = position;
            SelectedNavItem.setSlectedNavItem(position);
            adapter.notifyDataSetChanged();
            Intent intent = new Intent();
            intent.setClass(getContext(),VerifyProduceAddActivity.class);
            intent.putExtra("EmployeeName",EmployeeName);
            intent.putExtra("OrderID",Over_Cards.get(position).getOrderID());
            intent.putExtra("SerialNumber",Over_Cards.get(position).getSerialNumber());
            intent.putExtra("Qty",Over_Cards.get(position).getQty());
            intent.putExtra("StandardQty",Over_Cards.get(position).getStandardQty());
            startActivity(intent);
            getActivity().finish();
        });

        //endregion
        Getfinished(OrderID);
    }
    void Getfinished(String OrderID){
        try {
            Statement statement = connection.createStatement();
            Single =
                    "SELECT tblOManufactureDailyDetail.*, tblBProcess.ProcessDesc"+
                            " FROM tblOManufactureDailyDetail INNER JOIN tblBProcess ON tblOManufactureDailyDetail.ProcessID=tblBProcess.ProcessID"+
                            " WHERE tblOManufactureDailyDetail.WorkingTimes > 0 And tblOManufactureDailyDetail.OrderID = '"+OrderID+"'"+
                            " ORDER BY tblOManufactureDailyDetail.SerialNumber";
            ResultSet SingleResultSet = statement.executeQuery(Single);
            if(SingleResultSet != null){
                while (SingleResultSet.next()){
                    String InputOrderID = SingleResultSet.getString("OrderID");
                    String InputSerialNumber = SingleResultSet.getString("SerialNumber");
                    String InputManufactureOrderID = SingleResultSet.getString("ManufactureOrderID");
                    String InputProductsDescription = SingleResultSet.getString("ProductsDescription");
                    String InputMachineID = SingleResultSet.getString("MachineID");
                    String InputProcessID = SingleResultSet.getString("ProcessID");
                    String InputProcessDesc = SingleResultSet.getString("ProcessDesc");
                    String InputWorkShiftype = SingleResultSet.getString("WorkShiftType");
                    String InputStartDate = SingleResultSet.getString("StartDate");
                    String InputStartTime = SingleResultSet.getString("StartTime");
                    String InputEndDate = SingleResultSet.getString("EndDate");
                    String InputEndTime = SingleResultSet.getString("EndTime");
                    int Qty = SingleResultSet.getInt("ManufactureQty");

                    String [] InputStandardQty = SingleResultSet.getString("StandardQty").split("\\.");

                    String [] InputYieldQty = SingleResultSet.getString("YieldQty").split("\\.");
                    String [] InputRestorationQty = SingleResultSet.getString("RestorationQty").split("\\.");
                    String [] InputScrapQty = SingleResultSet.getString("ScrapQty").split("\\.");
                    String [] InputYieldQtyA = SingleResultSet.getString("YieldQtyA").split("\\.");
                    String [] InputYieldQtyB = SingleResultSet.getString("YieldQtyB").split("\\.");

                    Over_Cards.add(new item_over_card(
                            InputOrderID,
                            InputSerialNumber,
                            InputManufactureOrderID,
                            InputProductsDescription,
                            InputMachineID,
                            InputProcessID,
                            InputProcessDesc,
                            InputWorkShiftype,
                            InputStartDate,
                            GetStartTime(InputStartTime),
                            InputEndDate,
                            GetStartTime(InputEndTime),
                            true,
                            Qty,
                            InputYieldQty[0],
                            InputRestorationQty[0],
                            InputScrapQty[0],
                            InputYieldQtyA[0],
                            InputYieldQtyB[0],
                            InputStandardQty[0]
                    ));
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void lazyLoad() {
        if(!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各種物件的數據
        mHasLoadedOnce = true;
    }
    public static OverCompleteActivity newInstance(String param,String OrderID, String EmployeeName){
        OverCompleteActivity overCompleteActivity = new OverCompleteActivity();
        Bundle args = new Bundle();
        args.putString("args1",param);
        args.putString("OrderID",OrderID);
        args.putString("EmployeeName",EmployeeName);
        overCompleteActivity.setArguments(args);
        return overCompleteActivity;
    }
    private String GetStartTime(String Time){
        try {
            Date date;
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(Time);
            String Stat = String.format("%tR", date);
            return Stat;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
