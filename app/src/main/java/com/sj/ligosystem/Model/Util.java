package com.sj.ligosystem.Model;

import com.sj.ligosystem.Package.OverInActivity;

import java.util.ArrayList;
import java.util.List;

public class Util {
   public static ArrayList<User> AllUserList = new ArrayList<>();
   public static ArrayList<Machine> AllMachine = new ArrayList<>();
   public static ArrayList<Process> AllProcess = new ArrayList<>();
   public static ArrayList<Detail> AllManufacture = new ArrayList<>();
   public static List<String> AllProductsType = new ArrayList<>();
   public static List<String> AllSize = new ArrayList<>();
   public static List<String> AllThickness = new ArrayList<>();
   public static int OrderID;
   public static String OrderNoBarCode;
   public static String EmployeeID;
   public static String EmployeePassWord;
   public static String MachineID;
   public static String MachineName;
   public static String EmployeeName;
}
