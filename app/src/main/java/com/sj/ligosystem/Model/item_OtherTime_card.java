package com.sj.ligosystem.Model;

public class item_OtherTime_card {
    private String OrderID;
    private String SerialNumber;
    private String WorkingHourID;
    private String EmployeeID;
    private String EmployeeName;
    private String ProcessDesc;
    private String StartDate;
    private String StartTime;
    private String EndDate;
    private String EndTime;
    private String MachineIDB;
    private String MachineDesc;
    private String WorkShiftType;
    private String MachineID;
    private String WorkingTime;

    public item_OtherTime_card(String orderID, String serialNumber, String workingHourID, String employeeID, String employeeName, String processDesc, String startDate, String startTime, String endDate, String endTime, String workShiftType, String machineID) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        WorkingHourID = workingHourID;
        EmployeeID = employeeID;
        EmployeeName = employeeName;
        ProcessDesc = processDesc;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        WorkShiftType = workShiftType;
        MachineID = machineID;
    }

    public item_OtherTime_card(String orderID, String serialNumber, String workingHourID, String employeeID, String employeeName, String processDesc, String startDate, String startTime, String endDate, String endTime, String machineIDB, String machineDesc, String workShiftType, String machineID) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        WorkingHourID = workingHourID;
        EmployeeID = employeeID;
        EmployeeName = employeeName;
        ProcessDesc = processDesc;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        MachineIDB = machineIDB;
        MachineDesc = machineDesc;
        WorkShiftType = workShiftType;
        MachineID = machineID;
    }

    public item_OtherTime_card(String orderID, String serialNumber, String workingHourID, String employeeID, String employeeName, String processDesc, String startDate, String startTime, String endDate, String endTime, String machineIDB, String machineDesc, String workShiftType, String machineID, String workingTime) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        WorkingHourID = workingHourID;
        EmployeeID = employeeID;
        EmployeeName = employeeName;
        ProcessDesc = processDesc;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        MachineIDB = machineIDB;
        MachineDesc = machineDesc;
        WorkShiftType = workShiftType;
        MachineID = machineID;
        WorkingTime = workingTime;
    }

    public item_OtherTime_card(String orderID, String serialNumber, String workingHourID, String employeeID, String employeeName, String processDesc, String startDate, String startTime, String endDate, String endTime, String workShiftType, String machineID, String workingTime) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        WorkingHourID = workingHourID;
        EmployeeID = employeeID;
        EmployeeName = employeeName;
        ProcessDesc = processDesc;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        WorkShiftType = workShiftType;
        MachineID = machineID;
        WorkingTime = workingTime;
    }

    public String getWorkingTime() {
        return WorkingTime;
    }

    public void setWorkingTime(String workingTime) {
        WorkingTime = workingTime;
    }

    public String getMachineID() {
        return MachineID;
    }

    public void setMachineID(String machineID) {
        MachineID = machineID;
    }

    public String getWorkShiftType() {
        return WorkShiftType;
    }

    public void setWorkShiftType(String workShiftType) {
        WorkShiftType = workShiftType;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getWorkingHourID() {
        return WorkingHourID;
    }

    public void setWorkingHourID(String workingHourID) {
        WorkingHourID = workingHourID;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getProcessDesc() {
        return ProcessDesc;
    }

    public void setProcessDesc(String processDesc) {
        ProcessDesc = processDesc;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getMachineIDB() {
        return MachineIDB;
    }

    public void setMachineIDB(String inputMachineIDB) {
        MachineIDB = inputMachineIDB;
    }

    public String getMachineDesc() {
        return MachineDesc;
    }

    public void setMachineDesc(String inputMachineDesc) {
        MachineDesc = inputMachineDesc;
    }
}
