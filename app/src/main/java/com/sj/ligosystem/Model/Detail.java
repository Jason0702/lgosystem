package com.sj.ligosystem.Model;

public class Detail {
    //工令編號
    private String Order_Number;
    //總工令單編號序號
    private String Order_NumberID;
    //產品類別編號
    private String Product_CategoryID;
    //生產別
    private String Standard_RequestID;
    //尺寸代碼
    private String Size_Code;
    //SW
    private String SW;
    //獎金編號
    private String Bounty_Code;
    //品名
    private String Product_Name;
    //規格
    private String Specification;
    //尺寸
    private String Product_Size;
    //需求成品數
    private String Demand;
    //規範編號
    private String RawJobTypeID;


    public Detail(String order_Number, String order_NumberID, String product_CategoryID, String standard_RequestID, String size_Code, String SW, String bounty_Code, String product_Name, String specification, String product_Size, String demand, String rawJobTypeID) {
        Order_Number = order_Number;
        Order_NumberID = order_NumberID;
        Product_CategoryID = product_CategoryID;
        Standard_RequestID = standard_RequestID;
        Size_Code = size_Code;
        this.SW = SW;
        Bounty_Code = bounty_Code;
        Product_Name = product_Name;
        Specification = specification;
        Product_Size = product_Size;
        Demand = demand;
        RawJobTypeID = rawJobTypeID;
    }



    public String getRawJobTypeID() {
        return RawJobTypeID;
    }

    public void setRawJobTypeID(String rawJobTypeID) {
        RawJobTypeID = rawJobTypeID;
    }

    public String getStandard_RequestID() {
        return Standard_RequestID;
    }

    public void setStandard_RequestID(String standard_RequestID) {
        Standard_RequestID = standard_RequestID;
    }

    public String getOrder_Number() {
        return Order_Number;
    }

    public void setOrder_Number(String order_Number) {
        Order_Number = order_Number;
    }

    public String getOrder_NumberID() {
        return Order_NumberID;
    }

    public void setOrder_NumberID(String order_NumberID) {
        Order_NumberID = order_NumberID;
    }

    public String getProduct_CategoryID() {
        return Product_CategoryID;
    }

    public void setProduct_CategoryID(String product_CategoryID) {
        Product_CategoryID = product_CategoryID;
    }

    public String getSize_Code() {
        return Size_Code;
    }

    public void setSize_Code(String size_Code) {
        Size_Code = size_Code;
    }

    public String getSW() {
        return SW;
    }

    public void setSW(String SW) {
        this.SW = SW;
    }

    public String getBounty_Code() {
        return Bounty_Code;
    }

    public void setBounty_Code(String bounty_Code) {
        Bounty_Code = bounty_Code;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getSpecification() {
        return Specification;
    }

    public void setSpecification(String specification) {
        Specification = specification;
    }

    public String getProduct_Size() {
        return Product_Size;
    }

    public void setProduct_Size(String product_Size) {
        Product_Size = product_Size;
    }

    public String getDemand() {
        return Demand;
    }

    public void setDemand(String demand) {
        Demand = demand;
    }
}
