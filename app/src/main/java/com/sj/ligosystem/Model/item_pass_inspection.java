package com.sj.ligosystem.Model;

public class item_pass_inspection {
    private String InspectionDate;
    private String InspectionTime;
    private String SelfDetermination;

    public item_pass_inspection(String inspectionDate, String inspectionTime, String selfDetermination) {
        InspectionDate = inspectionDate;
        InspectionTime = inspectionTime;
        SelfDetermination = selfDetermination;
    }

    public String getInspectionDate() {
        return InspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
        InspectionDate = inspectionDate;
    }

    public String getInspectionTime() {
        return InspectionTime;
    }

    public void setInspectionTime(String inspectionTime) {
        InspectionTime = inspectionTime;
    }

    public String getSelfDetermination() {
        return SelfDetermination;
    }

    public void setSelfDetermination(String selfDetermination) {
        SelfDetermination = selfDetermination;
    }
}
