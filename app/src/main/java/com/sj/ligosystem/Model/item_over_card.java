package com.sj.ligosystem.Model;

public class item_over_card {
    private String OrderID;
    private String SerialNumber;
    private String ManufactureOrderID;
    private String ProductsDescription;
    private String MachineID;
    private String ProcessID;
    private String ProcessDesc;
    private String WorkShiftype;
    private String StartDate;
    private String StartTime;
    private String EndDate;
    private String EndTime;
    private boolean OK;
    private int Qty;
    private String StandardQty;
    private String YirldQty;
    private String RestorationQty;
    private String ScrapQty;
    private String YieldQtyA;
    private String YieldQtyB;

    public item_over_card(String orderID, String serialNumber, String manufactureOrderID, String productsDescription, String machineID, String processID, String processDesc,String workShiftype, String startDate, String startTime, String endDate, String endTime,boolean ok, int qty, String standardQty) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        ManufactureOrderID = manufactureOrderID;
        ProductsDescription = productsDescription;
        MachineID = machineID;
        ProcessID = processID;
        ProcessDesc = processDesc;
        WorkShiftype = workShiftype;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        OK = ok;
        Qty = qty;
        StandardQty = standardQty;
    }

    public item_over_card(String orderID, String serialNumber, String manufactureOrderID, String productsDescription, String machineID, String processID, String processDesc, String workShiftype, String startDate, String startTime, String endDate, String endTime, boolean OK, int qty, String yirldQty, String restorationQty, String scrapQty, String yieldQtyA, String yieldQtyB, String standardQty) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        ManufactureOrderID = manufactureOrderID;
        ProductsDescription = productsDescription;
        MachineID = machineID;
        ProcessID = processID;
        ProcessDesc = processDesc;
        WorkShiftype = workShiftype;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        this.OK = OK;
        Qty = qty;
        YirldQty = yirldQty;
        RestorationQty = restorationQty;
        ScrapQty = scrapQty;
        YieldQtyA = yieldQtyA;
        YieldQtyB = yieldQtyB;
        StandardQty = standardQty;
    }

    public String getStandardQty() {
        return StandardQty;
    }

    public void setStandardQty(String standardQty) {
        StandardQty = standardQty;
    }

    public String getYirldQty() {
        return YirldQty;
    }

    public void setYirldQty(String yirldQty) {
        YirldQty = yirldQty;
    }

    public String getRestorationQty() {
        return RestorationQty;
    }

    public void setRestorationQty(String restorationQty) {
        RestorationQty = restorationQty;
    }

    public String getScrapQty() {
        return ScrapQty;
    }

    public void setScrapQty(String scrapQty) {
        ScrapQty = scrapQty;
    }

    public String getYieldQtyA() {
        return YieldQtyA;
    }

    public void setYieldQtyA(String yieldQtyA) {
        YieldQtyA = yieldQtyA;
    }

    public String getYieldQtyB() {
        return YieldQtyB;
    }

    public void setYieldQtyB(String yieldQtyB) {
        YieldQtyB = yieldQtyB;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public String getProcessDesc() {
        return ProcessDesc;
    }

    public void setProcessDesc(String processDesc) {
        ProcessDesc = processDesc;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }


    public String getManufactureOrderID() {
        return ManufactureOrderID;
    }

    public void setManufactureOrderID(String manufactureOrderID) {
        ManufactureOrderID = manufactureOrderID;
    }

    public String getProductsDescription() {
        return ProductsDescription;
    }

    public void setProductsDescription(String productsDescription) {
        ProductsDescription = productsDescription;
    }

    public String getMachineID() {
        return MachineID;
    }

    public void setMachineID(String machineID) {
        MachineID = machineID;
    }

    public String getProcessID() {
        return ProcessID;
    }

    public void setProcessID(String processID) {
        ProcessID = processID;
    }

    public String getWorkShiftype() {
        return WorkShiftype;
    }

    public void setWorkShiftype(String workShiftype) {
        WorkShiftype = workShiftype;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }
}
