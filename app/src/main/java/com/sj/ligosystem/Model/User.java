package com.sj.ligosystem.Model;

public class User {
    private String UserID;
    private String UserName;
    private String UserPassWord;
    private String UserTeamLeader;

    public User(String userID, String userName, String userPassWord, String userTeamLeader) {
        UserID = userID;
        UserName = userName;
        UserPassWord = userPassWord;
        UserTeamLeader = userTeamLeader;
    }

    public String getUserTeamLeader() {
        return UserTeamLeader;
    }

    public void setUserTeamLeader(String userTeamLeader) {
        UserTeamLeader = userTeamLeader;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserPassWord() {
        return UserPassWord;
    }

    public void setUserPassWord(String userPassWord) {
        UserPassWord = userPassWord;
    }
}
