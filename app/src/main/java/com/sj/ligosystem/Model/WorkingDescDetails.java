package com.sj.ligosystem.Model;

public class WorkingDescDetails {
    private String StandardIDItemNo;
    private String WorkingDesc;
    private String Rate;
    private String RateA;
    private String RateB;


    public WorkingDescDetails( String standardIDItemNo, String workingDesc, String rate) {
        StandardIDItemNo = standardIDItemNo;
        WorkingDesc = workingDesc;
        Rate = rate;
    }

    public WorkingDescDetails(String standardIDItemNo, String workingDesc, String rate, String rateA, String rateB) {
        StandardIDItemNo = standardIDItemNo;
        WorkingDesc = workingDesc;
        Rate = rate;
        RateA = rateA;
        RateB = rateB;
    }

    public String getRateA() {
        return RateA;
    }

    public void setRateA(String rateA) {
        RateA = rateA;
    }

    public String getRateB() {
        return RateB;
    }

    public void setRateB(String rateB) {
        RateB = rateB;
    }

    public String getStandardIDItemNo() {
        return StandardIDItemNo;
    }

    public void setStandardIDItemNo(String standardIDItemNo) {
        StandardIDItemNo = standardIDItemNo;
    }

    public String getWorkingDesc() {
        return WorkingDesc;
    }

    public void setWorkingDesc(String workingDesc) {
        WorkingDesc = workingDesc;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

}
