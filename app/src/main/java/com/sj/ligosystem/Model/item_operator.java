package com.sj.ligosystem.Model;

public class item_operator {
    private String EmployeeID;
    private String EmployeeName;

    public item_operator(String employeeID, String employeeName) {
        EmployeeID = employeeID;
        EmployeeName = employeeName;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }
}
