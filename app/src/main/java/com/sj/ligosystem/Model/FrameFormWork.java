package com.sj.ligosystem.Model;

public class FrameFormWork {
    private String ProcessID;
    private String ProcessDesc;

    public FrameFormWork(String processID, String processDesc) {
        ProcessID = processID;
        ProcessDesc = processDesc;
    }

    public String getProcessID() {
        return ProcessID;
    }

    public void setProcessID(String processID) {
        ProcessID = processID;
    }

    public String getProcessDesc() {
        return ProcessDesc;
    }

    public void setProcessDesc(String processDesc) {
        ProcessDesc = processDesc;
    }
}
