package com.sj.ligosystem.Model;

public class PassInspection {
    private String OrderID;
    private String SerialNumber;
    private String OrderNoBarCode;
    private String ManufactureOrderID;
    private String ProductsDescription;
    private String MachineID;
    private String ProcessID;
    private String ProcessDesc;

    private String WorkShiftType;
    private String StartDate;
    private String StartTime;
    private String EndDate;
    private String EndTime;

    private String ProductsTypeID;
    private String StandardRequestID;
    private String SeamTypeID;
    private String RawJobTypeID;
    private String SaleTypeID;

    private String SizeID;
    private String SizeID2;
    private String ThicknessID;
    private String ThicknessID2;

    private String InspectionFormID;
    private String CompletedFormID;
    private String MaterialFormID;
    private String DiagramFormID;
    private int Qty;
    private String StandardQty;

    private String YirldQty;
    private String RestorationQty;
    private String ScrapQty;
    private String YieldQtyA;
    private String YieldQtyB;

    private String Inoc;

    public PassInspection(String orderID, String serialNumber, String orderNoBarCode, String manufactureOrderID, String productsDescription, String machineID, String processID, String processDesc, String workShiftType, String startDate, String startTime, String endDate, String endTime, String productsTypeID, String standardRequestID, String seamTypeID, String rawJobTypeID, String saleTypeID, String inspectionFormID, String completedFormID, String materialFormID, String diagramFormID, int qty, String standardQty, String yirldQty, String restorationQty, String scrapQty, String yieldQtyA, String yieldQtyB, String sizeID, String sizeID2, String thicknessID, String thicknessID2, String inoc) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        OrderNoBarCode = orderNoBarCode;
        ManufactureOrderID = manufactureOrderID;
        ProductsDescription = productsDescription;
        MachineID = machineID;
        ProcessID = processID;
        ProcessDesc = processDesc;
        WorkShiftType = workShiftType;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        ProductsTypeID = productsTypeID;
        StandardRequestID = standardRequestID;
        SeamTypeID = seamTypeID;
        RawJobTypeID = rawJobTypeID;
        SaleTypeID = saleTypeID;
        InspectionFormID = inspectionFormID;
        CompletedFormID = completedFormID;
        MaterialFormID = materialFormID;
        DiagramFormID = diagramFormID;
        Qty = qty;
        StandardQty = standardQty;
        YirldQty = yirldQty;
        RestorationQty = restorationQty;
        ScrapQty = scrapQty;
        YieldQtyA = yieldQtyA;
        YieldQtyB = yieldQtyB;
        SizeID = sizeID;
        SizeID2 = sizeID2;
        ThicknessID = thicknessID;
        ThicknessID2 = thicknessID2;
        Inoc = inoc;
    }

    public PassInspection(String orderID, String serialNumber, String orderNoBarCode, String manufactureOrderID, String productsDescription, String machineID, String processID, String processDesc, String workShiftType, String startDate, String startTime, String endDate, String endTime, String productsTypeID, String standardRequestID, String seamTypeID, String rawJobTypeID, String saleTypeID, String inspectionFormID, String completedFormID, String materialFormID, String diagramFormID, int qty, String standardQty, String sizeID, String sizeID2, String thicknessID, String thicknessID2, String inoc) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        OrderNoBarCode = orderNoBarCode;
        ManufactureOrderID = manufactureOrderID;
        ProductsDescription = productsDescription;
        MachineID = machineID;
        ProcessID = processID;
        ProcessDesc = processDesc;
        WorkShiftType = workShiftType;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        ProductsTypeID = productsTypeID;
        StandardRequestID = standardRequestID;
        SeamTypeID = seamTypeID;
        RawJobTypeID = rawJobTypeID;
        SaleTypeID = saleTypeID;
        InspectionFormID = inspectionFormID;
        CompletedFormID = completedFormID;
        MaterialFormID = materialFormID;
        DiagramFormID = diagramFormID;
        Qty = qty;
        StandardQty = standardQty;
        SizeID = sizeID;
        SizeID2 = sizeID2;
        ThicknessID = thicknessID;
        ThicknessID2 = thicknessID2;
        Inoc = inoc;
    }

    public String getYirldQty() {
        return YirldQty;
    }

    public void setYirldQty(String yirldQty) {
        YirldQty = yirldQty;
    }

    public String getRestorationQty() {
        return RestorationQty;
    }

    public void setRestorationQty(String restorationQty) {
        RestorationQty = restorationQty;
    }

    public String getScrapQty() {
        return ScrapQty;
    }

    public void setScrapQty(String scrapQty) {
        ScrapQty = scrapQty;
    }

    public String getYieldQtyA() {
        return YieldQtyA;
    }

    public void setYieldQtyA(String yieldQtyA) {
        YieldQtyA = yieldQtyA;
    }

    public String getYieldQtyB() {
        return YieldQtyB;
    }

    public void setYieldQtyB(String yieldQtyB) {
        YieldQtyB = yieldQtyB;
    }

    public String getStandardQty() {
        return StandardQty;
    }

    public void setStandardQty(String standardQty) {
        StandardQty = standardQty;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getOrderNoBarCode() {
        return OrderNoBarCode;
    }

    public void setOrderNoBarCode(String orderNoBarCode) {
        OrderNoBarCode = orderNoBarCode;
    }

    public String getManufactureOrderID() {
        return ManufactureOrderID;
    }

    public void setManufactureOrderID(String manufactureOrderID) {
        ManufactureOrderID = manufactureOrderID;
    }

    public String getProductsDescription() {
        return ProductsDescription;
    }

    public void setProductsDescription(String productsDescription) {
        ProductsDescription = productsDescription;
    }

    public String getMachineID() {
        return MachineID;
    }

    public void setMachineID(String machineID) {
        MachineID = machineID;
    }

    public String getProcessID() {
        return ProcessID;
    }

    public void setProcessID(String processID) {
        ProcessID = processID;
    }

    public String getProcessDesc() {
        return ProcessDesc;
    }

    public void setProcessDesc(String processDesc) {
        ProcessDesc = processDesc;
    }

    public String getWorkShiftType() {
        return WorkShiftType;
    }

    public void setWorkShiftType(String workShiftType) {
        WorkShiftType = workShiftType;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getProductsTypeID() {
        return ProductsTypeID;
    }

    public void setProductsTypeID(String productsTypeID) {
        ProductsTypeID = productsTypeID;
    }

    public String getStandardRequestID() {
        return StandardRequestID;
    }

    public void setStandardRequestID(String standardRequestID) {
        StandardRequestID = standardRequestID;
    }

    public String getSeamTypeID() {
        return SeamTypeID;
    }

    public void setSeamTypeID(String seamTypeID) {
        SeamTypeID = seamTypeID;
    }

    public String getRawJobTypeID() {
        return RawJobTypeID;
    }

    public void setRawJobTypeID(String rawJobTypeID) {
        RawJobTypeID = rawJobTypeID;
    }

    public String getSaleTypeID() {
        return SaleTypeID;
    }

    public void setSaleTypeID(String saleTypeID) {
        SaleTypeID = saleTypeID;
    }

    public String getInspectionFormID() {
        return InspectionFormID;
    }

    public void setInspectionFormID(String inspectionFormID) {
        InspectionFormID = inspectionFormID;
    }

    public String getCompletedFormID() {
        return CompletedFormID;
    }

    public void setCompletedFormID(String completedFormID) {
        CompletedFormID = completedFormID;
    }

    public String getMaterialFormID() {
        return MaterialFormID;
    }

    public void setMaterialFormID(String materialFormID) {
        MaterialFormID = materialFormID;
    }

    public String getDiagramFormID() {
        return DiagramFormID;
    }

    public void setDiagramFormID(String diagramFormID) {
        DiagramFormID = diagramFormID;
    }

    public String getSizeID() {
        return SizeID;
    }

    public void setSizeID(String sizeID) {
        SizeID = sizeID;
    }

    public String getSizeID2() {
        return SizeID2;
    }

    public void setSizeID2(String sizeID2) {
        SizeID2 = sizeID2;
    }

    public String getThicknessID() {
        return ThicknessID;
    }

    public void setThicknessID(String thicknessID) {
        ThicknessID = thicknessID;
    }

    public String getThicknessID2() {
        return ThicknessID2;
    }

    public void setThicknessID2(String thicknessID2) {
        ThicknessID2 = thicknessID2;
    }

    public String getInoc() {
        return Inoc;
    }

    public void setInoc(String inoc) {
        Inoc = inoc;
    }
}
