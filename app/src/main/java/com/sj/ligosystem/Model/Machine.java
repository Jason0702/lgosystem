package com.sj.ligosystem.Model;

public class Machine {
    private String MachineID;
    private String MachineName;

    public Machine(String machineID, String machineName) {
        MachineID = machineID;
        MachineName = machineName;
    }

    public String getMachineID() {
        return MachineID;
    }

    public void setMachineID(String machineID) {
        MachineID = machineID;
    }

    public String getMachineName() {
        return MachineName;
    }

    public void setMachineName(String machineName) {
        MachineName = machineName;
    }
}
