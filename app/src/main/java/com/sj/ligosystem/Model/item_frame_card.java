package com.sj.ligosystem.Model;

public class item_frame_card {

    private String OrderID;
    private String SerialNumber;
    private String EmployeeID;
    private String WorkingHourID;
    private String ManufactureOrderID;

    private String MachineID;
    private String ProcessID;
    private String WorkShiftType;
    private String StartDate;
    private String StartTime;

    private String EndDate;
    private String EndTime;
    private String WorkingTimes;
    private String ProductsTypeID;
    private String SizeID;

    private String ThicknessID;
    private String BProductsTypeID;
    private String BSizeID;
    private String BThicknessID;
    private String BManufactureOrderID;

    private String Rate;
    private String WorkingDescID;
    private String StandardID;
    private String SizeQty;
    private String BSizeQty;

    private String WorkingTimesB;
    private String Amount;
    private String StandardIDItemNo;
    private String BStandardIDItemNo;
    private String AmendDate;

    private String WorkHoursStatisticsTypeID;

    private String EmployeeName;
    private String ProcessDesc;
    private String MachineDesc;

    private String SizeCaption;
    private String BSizeCaption;

    public item_frame_card(String orderID, String serialNumber, String employeeID, String workingHourID, String manufactureOrderID, String machineID, String processID, String workShiftType, String startDate, String startTime, String endDate, String endTime, String workingTimes, String productsTypeID, String sizeID, String thicknessID, String BProductsTypeID, String BSizeID, String BThicknessID, String BManufactureOrderID, String rate, String workingDescID, String standardID, String sizeQty, String BSizeQty, String workingTimesB, String amount, String standardIDItemNo, String BStandardIDItemNo, String amendDate, String workHoursStatisticsTypeID, String employeeName, String processDesc, String machineDesc, String sizeCaption, String bsizecaption) {
        OrderID = orderID;
        SerialNumber = serialNumber;
        EmployeeID = employeeID;
        WorkingHourID = workingHourID;
        ManufactureOrderID = manufactureOrderID;
        MachineID = machineID;
        ProcessID = processID;
        WorkShiftType = workShiftType;
        StartDate = startDate;
        StartTime = startTime;
        EndDate = endDate;
        EndTime = endTime;
        WorkingTimes = workingTimes;
        ProductsTypeID = productsTypeID;
        SizeID = sizeID;
        ThicknessID = thicknessID;
        this.BProductsTypeID = BProductsTypeID;
        this.BSizeID = BSizeID;
        this.BThicknessID = BThicknessID;
        this.BManufactureOrderID = BManufactureOrderID;
        Rate = rate;
        WorkingDescID = workingDescID;
        StandardID = standardID;
        SizeQty = sizeQty;
        this.BSizeQty = BSizeQty;
        WorkingTimesB = workingTimesB;
        Amount = amount;
        StandardIDItemNo = standardIDItemNo;
        this.BStandardIDItemNo = BStandardIDItemNo;
        AmendDate = amendDate;
        WorkHoursStatisticsTypeID = workHoursStatisticsTypeID;
        EmployeeName = employeeName;
        MachineDesc = machineDesc;
        ProcessDesc = processDesc;
        SizeCaption = sizeCaption;
        BSizeCaption = bsizecaption;
    }

    public String getSizeCaption() {
        return SizeCaption;
    }

    public void setSizeCaption(String sizeCaption) {
        SizeCaption = sizeCaption;
    }

    public String getBSizeCaption() {
        return BSizeCaption;
    }

    public void setBSizeCaption(String BSizeCaption) {
        this.BSizeCaption = BSizeCaption;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getProcessDesc() {
        return ProcessDesc;
    }

    public void setProcessDesc(String processDesc) {
        ProcessDesc = processDesc;
    }

    public String getMachineDesc() {
        return MachineDesc;
    }

    public void setMachineDesc(String machineDesc) {
        MachineDesc = machineDesc;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getWorkingHourID() {
        return WorkingHourID;
    }

    public void setWorkingHourID(String workingHourID) {
        WorkingHourID = workingHourID;
    }

    public String getManufactureOrderID() {
        return ManufactureOrderID;
    }

    public void setManufactureOrderID(String manufactureOrderID) {
        ManufactureOrderID = manufactureOrderID;
    }

    public String getMachineID() {
        return MachineID;
    }

    public void setMachineID(String machineID) {
        MachineID = machineID;
    }

    public String getProcessID() {
        return ProcessID;
    }

    public void setProcessID(String processID) {
        ProcessID = processID;
    }

    public String getWorkShiftType() {
        return WorkShiftType;
    }

    public void setWorkShiftType(String workShiftType) {
        WorkShiftType = workShiftType;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getWorkingTimes() {
        return WorkingTimes;
    }

    public void setWorkingTimes(String workingTimes) {
        WorkingTimes = workingTimes;
    }

    public String getProductsTypeID() {
        return ProductsTypeID;
    }

    public void setProductsTypeID(String productsTypeID) {
        ProductsTypeID = productsTypeID;
    }

    public String getSizeID() {
        return SizeID;
    }

    public void setSizeID(String sizeID) {
        SizeID = sizeID;
    }

    public String getThicknessID() {
        return ThicknessID;
    }

    public void setThicknessID(String thicknessID) {
        ThicknessID = thicknessID;
    }

    public String getBProductsTypeID() {
        return BProductsTypeID;
    }

    public void setBProductsTypeID(String BProductsTypeID) {
        this.BProductsTypeID = BProductsTypeID;
    }

    public String getBSizeID() {
        return BSizeID;
    }

    public void setBSizeID(String BSizeID) {
        this.BSizeID = BSizeID;
    }

    public String getBThicknessID() {
        return BThicknessID;
    }

    public void setBThicknessID(String BThicknessID) {
        this.BThicknessID = BThicknessID;
    }

    public String getBManufactureOrderID() {
        return BManufactureOrderID;
    }

    public void setBManufactureOrderID(String BManufactureOrderID) {
        this.BManufactureOrderID = BManufactureOrderID;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getWorkingDescID() {
        return WorkingDescID;
    }

    public void setWorkingDescID(String workingDescID) {
        WorkingDescID = workingDescID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSizeQty() {
        return SizeQty;
    }

    public void setSizeQty(String sizeQty) {
        SizeQty = sizeQty;
    }

    public String getBSizeQty() {
        return BSizeQty;
    }

    public void setBSizeQty(String BSizeQty) {
        this.BSizeQty = BSizeQty;
    }

    public String getWorkingTimesB() {
        return WorkingTimesB;
    }

    public void setWorkingTimesB(String workingTimesB) {
        WorkingTimesB = workingTimesB;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getStandardIDItemNo() {
        return StandardIDItemNo;
    }

    public void setStandardIDItemNo(String standardIDItemNo) {
        StandardIDItemNo = standardIDItemNo;
    }

    public String getBStandardIDItemNo() {
        return BStandardIDItemNo;
    }

    public void setBStandardIDItemNo(String BStandardIDItemNo) {
        this.BStandardIDItemNo = BStandardIDItemNo;
    }

    public String getAmendDate() {
        return AmendDate;
    }

    public void setAmendDate(String amendDate) {
        AmendDate = amendDate;
    }

    public String getWorkHoursStatisticsTypeID() {
        return WorkHoursStatisticsTypeID;
    }

    public void setWorkHoursStatisticsTypeID(String workHoursStatisticsTypeID) {
        WorkHoursStatisticsTypeID = workHoursStatisticsTypeID;
    }
}
