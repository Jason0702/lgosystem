package com.sj.ligosystem.Model;

public class Process {
    private String ProcessID;
    private String ProcessDesc;
    private String WorkingHourTypeID;

    public Process(String processID, String processDesc, String workingHourTypeID) {
        ProcessID = processID;
        ProcessDesc = processDesc;
        WorkingHourTypeID = workingHourTypeID;
    }

    public String getWorkingHourTypeID() { return WorkingHourTypeID; }

    public void setWorkingHourTypeID(String workingHourTypeID) { WorkingHourTypeID = workingHourTypeID; }

    public String getProcessID() {
        return ProcessID;
    }

    public void setProcessID(String processID) {
        ProcessID = processID;
    }

    public String getProcessDesc() {
        return ProcessDesc;
    }

    public void setProcessDesc(String processDesc) {
        ProcessDesc = processDesc;
    }
}
