package com.sj.ligosystem.Model;

import java.util.ArrayList;

public class item_start_card {
    private int OrderID;
    private String OrderNO;
    private String OrderSN;
    private String Number;
    private String OrderNoBarCode;
    private String Leader;
    private String InputUserID;
    private String InputUser;
    private ArrayList<String> Operator;

    public item_start_card(){
        super();
    }

    public item_start_card(int orderID, String orderNO, String orderSN, String number, String orderNoBarCode, String leader, String inputUserID, String inputUser, ArrayList<String> operator) {
        OrderID = orderID;
        OrderNO = orderNO;
        OrderSN = orderSN;
        Number = number;
        OrderNoBarCode = orderNoBarCode;
        Leader = leader;
        InputUserID = inputUserID;
        InputUser = inputUser;
        Operator = operator;
    }

    public String getInputUserID() {
        return InputUserID;
    }

    public void setInputUserID(String inputUserID) {
        InputUserID = inputUserID;
    }

    public String getOrderNoBarCode() {
        return OrderNoBarCode;
    }

    public void setOrderNoBarCode(String orderNoBarCode) {
        OrderNoBarCode = orderNoBarCode;
    }

    public String getOrderNO() {
        return OrderNO;
    }

    public void setOrderNO(String orderNO) {
        OrderNO = orderNO;
    }

    public String getOrderSN() {
        return OrderSN;
    }

    public void setOrderSN(String orderSN) {
        OrderSN = orderSN;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getLeader() {
        return Leader;
    }

    public void setLeader(String leader) {
        Leader = leader;
    }

    public String getInputUser() {
        return InputUser;
    }

    public void setInputUser(String inputUser) {
        InputUser = inputUser;
    }

    public ArrayList<String> getOperator() {
        return Operator;
    }

    public void setOperator(ArrayList<String> operator) {
        Operator = operator;
    }
}
